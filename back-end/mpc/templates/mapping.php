<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<style>
body {
  margin:0;
  padding:20px;
  font-family: Helvetica, sans-serif;
  font-size: 14px;
}

table {
  width: 100%;
  border-spacing: 0;
  border-left: 1px solid #cacbcc;
  border-top: 1px solid #cacbcc;
  margin-bottom:20px;
}

th, td {
  border-right: 1px solid #cacbcc;
  border-bottom: 1px solid #cacbcc;
}

td {
  padding: 3px 5px;
}

td[colspan="9"] {
  padding: 10px;
  font-style: italic;
  color: red;
  text-align: center;
}

td:nth-child(4), td:nth-child(5), td:nth-child(6), td:nth-child(7), td:nth-child(8), td:nth-child(9), .center {
  text-align: center;
}

input[type="submit"] {
  margin-left: 9px;
  margin-top: -15px;
}

form > div:nth-child(1) {
  float: left;
  margin-bottom: 6px;
}

form > div:nth-child(2) {
  float: right;
}

form {
  display: block;
  margin-bottom: 10px;
}

.current-page {
  color: red;
}

td div span.label {
  display: inline-block;
  width: 55px;
}

img {
  max-width: 120px;
}

.google_url_edit, .loader_img,
.google_search_url_edit, .loader_img {
  display: none;
}
.google_url_edit .input_field,
.google_search_url_edit .input_field {
  width: 400px;
}
#select_all_checked { margin-bottom: 10px; }
#save_all_checked { width: 70px; }

#sku {
  width: 80px;
}

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	// update
	$('.google_url > :button').click (
		function (event) {
			var sku = $(this).data('sku');
			$('.google_url_edit').hide();
			$('.google_url').show();
			$('#google_url_'+ sku).hide();
			$('#google_url_'+ sku +'_edit').show();
		}
	);
	$('.google_search_url > :button').click (
		function (event) {
			var sku = $(this).data('sku');
			$('.google_search_url_edit').hide();
			$('.google_search_url').show();
			$('#google_search_url_'+ sku).hide();
			$('#google_search_url_'+ sku +'_edit').show();
		}
	);
	// save
	$('.google_url_edit > :button').click (
		function (event) {
			_self = $(this);
			var sku = _self.data('sku');
			var product_url = $('#google_url_'+ sku +'_edit > .input_field').val();
			_self.hide();
			$('#google_url_' + sku + '_edit img').show();

			if ($('#checked_box_'+ sku).is(':checked')) {
				alert("Checked item can't be edited. Save it as unchecked first.");
				_parent = $('#google_url_'+ sku + '_edit');
				$('img', _parent).hide();
				$(':button', _parent).show();
				return;
			}
			$.ajax({
				type: "POST",
				dataType: "json",
				//contentType: 'application/json; charset=UTF-8',
				url: "http://viomart.com/tools/mpc/command/manual_mapper.php",
				data: { 'sku': sku, 'url': product_url },
				success: function(json) {
					_save_container = $('#google_url_'+ sku + '_edit');
					$('img', _save_container).hide();
					$(':button', _save_container).show();
					_save_container.hide();

					_update_container = $('#google_url_'+ sku);
					google_link = $('.link', _update_container);
					google_link.html("<a href='"+ json.url +"'>"+ json.name +"</a>");
					$('#google_image_'+ sku).html("<img src='"+ json.image +"'/>");
					$('#ready_box_'+ sku).prop('checked', true);
					$('#last_sync_'+ sku).html(json.lastSync);
					_update_container.show();
				},
				error: function(errObj, textStatus) {
					alert(sku + " update failed. Error message: "+ textStatus);
					_parent = $('#google_url_'+ sku + '_edit');
					$('img', _parent).hide();
					$(':button', _parent).show();
				}
			});
		}
	);
	$('.google_search_url_edit > :button').click (
		function (event) {
			_self = $(this);
			var sku = _self.data('sku');
			var product_search_url = $('#google_search_url_'+ sku +'_edit > .input_field').val();
			_self.hide();
			$('#google_search_url_' + sku + '_edit img').show();

			if ($('#checked_box_'+ sku).is(':checked')) {
				alert("Checked item can't be edited. Save it as unchecked first.");
				_parent = $('#google_search_url_'+ sku + '_edit');
				$('img', _parent).hide();
				$(':button', _parent).show();
				return;
			}

			$.ajax({
				type: "POST",
				dataType: "json",
				//contentType: 'application/json; charset=UTF-8',
				url: "http://viomart.com/tools/mpc/command/manual_loader.php",
				data: { 'sku': sku, 'url': product_search_url },
				success: function(json) {
					_save_container = $('#google_search_url_'+ sku + '_edit');
					$('img', _save_container).hide();
					$(':button', _save_container).show();
					_save_container.hide();

					_update_container = $('#google_search_url_'+ sku);
					google_search_link = $('.link', _update_container);
					google_search_link.html("<a href='"+ json.url +"'>"+ json.url +"</a>");
					_update_container.show();
				},
				error: function(errObj, textStatus) {
					alert(sku + " update failed. Error message: "+ textStatus);
					_parent = $('#google_search_url_'+ sku + '_edit');
					$('img', _parent).hide();
					$(':button', _parent).show();
				}
			});
		}
	);
	$('#select_all_checked').change(
		function (event) {
			state = $(this).is(":checked");
			$("input:checkbox.checked_box").prop('checked', state);
		}
	);
	
	checked_to_save = 0;
	$('#save_all_checked').click(
		function (event) {
			saving = $("input:checkbox.checked_box:checked"); // checked only. remove :checked for all
			checked_to_save = saving.size(); 
			saving_current = 0;
			save_button = $('#save_all_checked');
			if (checked_to_save > 0 ) save_button.attr('disabled','disabled');
			saving.each(function() {
				self = $(this);
				if (self.is(":checked")) {
					sku = self.data('sku');
					$.ajax({
						type: "POST",
						dataType: "json",
						//contentType: 'application/json; charset=UTF-8',
						url: "http://viomart.com/tools/mpc/command/manual_checked.php",
						data: { 'sku': sku, 'checked': true },
						success: function(json) {
							saving_current++;
							save_button.attr('value', saving_current + "/" + checked_to_save);
							if (saving_current >= checked_to_save) location.reload();
						},
						error: function(errObj, textStatus) {
							saving_current++;
							save_button.attr('value', saving_current + "/" + checked_to_save);
							if (saving_current >= checked_to_save) location.reload();
						}
					});
				}
			});
		}
	);

	$('.ready_box_container').click(
		function (event) {
			_box = $(this).find("input:checkbox");
			_box.prop("checked", !_box.prop("checked"));
		}
	);
	$('.ready_box_container input:checkbox').click(
		function (event) { event.stopPropagation(); }
	);
	$('.checked_box_container').click(
		function (event) {
			_box = $(this).find("input:checkbox");
			_box.prop("checked", !_box.prop("checked"));
		}
	);
	$('.checked_box_container input:checkbox').click(
		function (event) { event.stopPropagation(); }
	);
	$('.ignored_box_container').click(
		function (event) {
			_box = $(this).find("input:checkbox");
			_box.prop("checked", !_box.prop("checked"));
		}
	);
	$('.ignored_box_container input:checkbox').click(
		function (event) { event.stopPropagation(); }
	);

});
</script>

</head>
<body>
	<form action="<?php echo $_SERVER['PHP_SELF'];?>">
		<div>
			Filter: 
			<input id="ready" type="checkbox" name="ready" value="1"<?php echo empty($data['filterVars']['ready']) ? '' : ' checked'; ?>><label for="ready">ready</label>
			<input id="checked" type="checkbox" name="checked" value="1"<?php echo empty($data['filterVars']['checked']) ? '' : ' checked'; ?>><label for="checked">checked</label>
			<input id="outdated" type="checkbox" name="outdated" value="1"<?php echo empty($data['filterVars']['outdated']) ? '' : ' checked'; ?>><label for="outdated">outdated</label>
			<input id="ignored" type="checkbox" name="ignored" value="1"<?php echo empty($data['filterVars']['ignored']) ? '' : ' checked'; ?>><label for="ignored">ignored</label>
			<select id="vendor" name="vendor">
				<option value="">all</option>
				<?php 
					$vendors = array (
						//'2547' => '300watches',
						//'2667' => 'Airbac',
						'2987' => 'Allstate Leather',
						'2854' => 'American West',
						'2775' => 'Anuschka',
						//'2715' => 'Apera',
						'2725' => 'Balboa',
						'2834' => 'Benjamin Walk',
						'2965' => 'Bigalli',
						'2650' => 'Buxton',
						'2678' => 'Dan Post',
						'2828' => 'Evanese',
						//'2438' => 'GMGND',
						//'2095' => 'GND',
						'2129' => 'Hades',
						'2281' => 'Henschel',
						//'2141' => 'InfiniteShopping',
						//'2821' => 'Leggsington',
						'2973' => 'Piel Leather',
						//'2502' => 'ResultCo',
						'2444' => 'Rockline Dropship',
						'2980' => 'Soho Apparel',
						'2860' => 'Scully',
						//'2553' => 'Seta Corporation',
						//'2769' => 'Stuller',
						'2128' => 'SugarLips',
						'2977' => 'Winn'
					);
					foreach ($vendors as $code => $name) {
						echo '<option value="'. $code .'" ';
						echo (!empty($data['filterVars']['vendor']) && $data['filterVars']['vendor'] == $code) ? ' selected' : '';
						echo '>'. $name .'</option>';
					}
				?>
			</select>
			<label for="vendor">vendor</label>
			<input id="sku" type="text" name="sku" value="<?php echo $data['filterVars']['sku']; ?>"/><label for="sku"> SKU</label>
			<input type="submit" value="Apply">
		</div>
		<div>
		Total: <?php echo $data['totalProducts']; ?>.
		Items per page:
			<select name="items" onchange="document.forms[0].submit();">
				<?php foreach ($data['itemsFilter'] as $items) { ?>
					<?php echo '<option value="' . $items . '"' . ($items == $data['filterVars']['items'] ? ' selected' : '') . '>' . $items . '</option>'; ?>
				<?php } ?>
			</select> 
		</div>
	</form>
	<form action="<?php echo $_SERVER['PHP_SELF']?>">
		<input type="hidden" name="ready" value="<?php echo $data['filterVars']['ready']; ?>">
		<input type="hidden" name="checked" value="<?php echo $data['filterVars']['checked']; ?>">
		<input type="hidden" name="outdated" value="<?php echo $data['filterVars']['outdated']; ?>">
		<input type="hidden" name="ignored" value="<?php echo $data['filterVars']['ignored']; ?>">
		<input type="hidden" name="vendor" value="<?php echo $data['filterVars']['vendor']; ?>">
		<input type="hidden" name="sku" value="<?php echo $data['filterVars']['sku']; ?>">
		<input type="hidden" name="items" value="<?php echo $data['filterVars']['items']; ?>">
		<input type="hidden" name="page" value="<?php echo $data['filterVars']['page']; ?>">
		<table>
			<tr>
				<th>SKU</th>
				<th>MSKU</th>
				<th>Name</th>
				<th>Vio Image</th>
				<th>Google Image</th>
				<th>Ready</th>
				<th><label for="select_all_checked">Checked</label>
					<input type="checkbox" id="select_all_checked"/><br/>
					<input type="button" id="save_all_checked" value="Save All"/>
					<div id="saving_checked_status"/>
				</th>
				<th>Last Sync</th>
				<th>Ignore</th>
				<th>Actions</th>
			</tr>
			<?php if (!empty($data['products'])) { ?>
				<?php foreach ($data['products'] as $product) { ?>
					<tr>
						<td><?php echo $product['sku']; ?><!--<br/>
							<input type="submit" name="delete[<?php echo $product['id']; ?>]" value="Delete">-->
						</td>
						<td><?php echo $product['msku']; ?></td>
						<td>
							<div><span class="label">Viomart:</span> <?php echo '<a href="' . $product['vioUrl'] . '" onclick="this.target=\'_blank\'">' . $product['name'] . '</a>'; ?></div>
							<div id="google_url_<?php echo $product['sku']; ?>" class="google_url">
							     <span class="label">Google:</span>
							     <span class="link">
						    	<?php if (!empty($product['googleName'])) {
								echo '<a href="' . $product['googleUrl'] . '" onclick="this.target=\'_blank\'">' . $product['googleName'] . '</a>';
							 }?>
							     </span>
							     <input type="button" value="update" data-sku="<?php echo $product['sku']; ?>"/>
							</div>
							<div id="google_url_<?php echo $product['sku']; ?>_edit" class="google_url_edit">
							     <span class="label">Google:</span> 
							     <input type="text" class="input_field" value="<?php echo $product['googleUrl']; ?>"/>
							     <input type="button" value="save" data-sku="<?php echo $product['sku']; ?>"/>
							     <img class="loader_img" src="http://viomart.com/skin/frontend/base/default/images/mirasvit/loader.gif"/>
							</div>
							<?php $search_request = substr($product['googleSearchUrl'], strpos($product['googleSearchUrl'], "?q=") + 3);
							      $search_request = urldecode(substr($product['googleSearchUrl'], 0, strpos($product['googleSearchUrl'], "&tbm=shop")));
							?>
							<div id="google_search_url_<?php echo $product['sku']; ?>" class="google_search_url">
							     <span class="label">Request:</span> 
							     <span class="link">
						    	<?php if (!empty($product['googleSearchUrl'])) {
								echo '<a href="' . $product['googleSearchUrl'] . '" onclick="this.target=\'_blank\'">' . $search_request . '</a>';
							}?>
							     </span>
							     <input type="button" value="update" data-sku="<?php echo $product['sku']; ?>"/>
							</div>
							<div id="google_search_url_<?php echo $product['sku']; ?>_edit" class="google_search_url_edit">
							     <span class="label">Request:</span> 
							     <input type="text" class="input_field" value="<?php echo $product['googleSearchUrl']; ?>"/>
							     <input type="button" value="save" data-sku="<?php echo $product['sku']; ?>"/>
							     <img class="loader_img" src="http://viomart.com/skin/frontend/base/default/images/mirasvit/loader.gif"/>
							</div>
						    <?php if ($product['manual'] < 1) { ?>
							<div><span class="label">Result:</span> 
								<?php   $mapping_location = ($data['filterVars']['ready']) ? 'positive/' : 'negative/';
									echo '<a href="http://viomart.com/tools/mpc/googlepages/mapping/'.$mapping_location.$product['sku'].'.html" onclick="this.target=\'_blank\'">' . $product['sku'] . '</a>'; ?>
							</div>
						    <?php }?>
						    <?php if (count($product['sizes']) > 0) { ?>
							<div><span class="label">Sizes:</span> 
								<?php foreach ($product['sizes'] as $size_sku => $url) {
									echo '<a href="'.$url.'">'.$size_sku. '</a>, '; 
								      }?>
							</div>
						    <?php }?>
						</td>
						<td><?php echo $product['vioImage'] ? '<img src="' . $product['vioImage'] . '">' : ''; ?></td>
						<td id="google_image_<?php echo $product['sku']; ?>"><?php echo $product['googleImage'] ? '<img src="' . $product['googleImage'] . '">' : ''; ?></td>
						<td class="ready_box_container"><input type="checkbox" <?php 
							echo 'id="ready_box_'.$product['sku'].'" ';
							echo 'name="setReady[' . $product['id'] . ']"' . ($product['ready'] ? ' checked' : ''); ?>></td>
						<td class="checked_box_container"><input id="checked_box_<?php echo $product['sku']; ?>"
							   type="checkbox" class="checked_box"
							   data-sku="<?php echo $product['sku']; ?>"
							   <?php echo 'name="setChecked[' . $product['id'] . ']"' . ($product['checked'] ? ' checked' : ''); ?>></td>
						<td id="last_sync_<?php echo $product['sku']; ?>"><?php echo $product['lastSync']; ?></td>
						<td class="ignored_box_container"><input type="checkbox" <?php echo 'name="setIgnored[' . $product['id'] . ']"' . ($product['ignored'] ? ' checked' : ''); ?>></td>
						<td><input type="submit" name="save[<?php echo $product['id']; ?>]" value="Save"></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
					<tr><td colspan="9"><i>No items matching your criteria</i></td></tr>
			<?php } ?>
		</table>
	</form>
	<?php if (!empty($data['products'])) { ?>
		<div class="center">Pages:
			<?php foreach (range(1, $data['totalPages']) as $page) {
				$url = 'page=' . $page;
				$url .= !empty($data['filterVars']['ready']) ? '&ready=1' : '';
				$url .= !empty($data['filterVars']['checked']) ? '&checked=1' : '';
				$url .= !empty($data['filterVars']['outdated']) ? '&outdated=1' : '';
				$url .= !empty($data['filterVars']['ignored']) ? '&ignored=1' : '';
				$url .= !empty($data['filterVars']['vendor']) ? '&vendor='.$data['filterVars']['vendor'] : '';
				$url .= !empty($data['filterVars']['sku']) ? '&sku='.$data['filterVars']['sku'] : '';
				$url .= '&items=' . (!empty($data['filterVars']['items']) ? $data['filterVars']['items'] : $data['itemsFilter'][0]);
				echo ' <a href="' . $_SERVER['PHP_SELF'] . '?' . $url . '"' . ($page == $data['filterVars']['page'] ? 'class="current-page"' : '') . '>' . $page . '</a>';
			} ?>
		</div>
	<?php } ?>
</body>
</html>