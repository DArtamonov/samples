var wnUserAgentParser = new UAParser();

var wnVideoUtils = {
    // Determine is html5 video is supported
    hasHtmlVideo: !!document.createElement('video').canPlayType,

    // Format milli seconds to format HH:MM:SS
    getTimeAsHHMMSS: function(msecs) {
        var time = '';
        msecs = parseInt(msecs, 10);

        if (typeof(msecs) == 'number' && !isNaN(msecs)) {
            msecs = msecs / 1000;
            msecs = Math.floor(msecs);
            var hours = Math.floor(msecs / 3600);
            var minutes = Math.floor((msecs - (hours * 3600)) / 60);
            var seconds = msecs - (hours * 3600) - (minutes * 60);
            //if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {
                minutes = '0' + minutes;
            }
            if (seconds < 10) {
                seconds = '0' + seconds;
            }
            time = (hours > 0 ? hours + ':' : '') + minutes + ':' + seconds;
        }

        return time;
    },

    parseQuery: function(query) {
        var params = {};
        if (!query) return params; // return empty object
        var pairs = query.split(/[;&]/);
        for (var i = 0; i < pairs.length; i++) {
            var keyVal = pairs[i].split('=');
            if (!keyVal || keyVal.length != 2) continue;
            var key = unescape(keyVal[0]);
            var val = unescape(keyVal[1]);
            val = val.replace(/\+/g, ' ');
            params[key] = val;
        }
        return params;
    },

    // Custom validation. global.inc's + site configs can let 'null', 'undefined', etc in.
    validateString: function(str) {
        if (str) {
            if (typeof(str) === 'string') {
                str = wnVideoUtils.trim(str);
                if (str === '' || str === 'undefined' || str === 'null') {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    },

    // Convert String to Boolean
    stringToBoolean: function(str) {
        if (str == "true" || str == "True") {
            return true;
        } else {
            return false;
        }
    },

    getResponsiveWidth: function(width) {
        var screenSize = $wn(window).width();

        if (screenSize < width) {
            return screenSize;
        } else {
            return width;
        }
    },

    get: function(prop) {
        try {
            if (typeof prop !== 'undefined') {
                return prop;
            } else {
                return '';
            }
        } catch (e) {
            return '';
        }
    },

    loadScript: function(scriptName, scriptUrl) {
        var oldScript = document.getElementById(scriptName);

        if (oldScript != null) {
            oldScript.parentNode.removeChild(oldScript);
            delete oldScript;
        }

        var head = document.getElementsByTagName("head")[0];
        var script = document.createElement("script");
        script.id = scriptName;
        script.type = "text/javascript";
        script.src = scriptUrl;
        head.appendChild(script);
    },

    getQS: function(qsParameter) {
        var qs = [];
        var url = location.search.substr(1);
        var params = url.split("&");

        for (var i = 0; i < params.length; i++) {
            var param = params[i].split("=");
            qs[param[0].toLowerCase()] = param[1];
        }
        return qs[qsParameter.toLowerCase()];
    },

    trim: function(str) {
        var a = str.replace(/^\s+/, '');
        return a.replace(/\s+$/, '');
    },

    getVideoFormatByUrl: function(url) {
        var pathParts,
            format,
            m = url.match(/(.*)[\/\\]([^\/\\]+)\.(\w+)$/); //regex for extension to determine format
        if (m) {
            pathParts = {
                path: m[1],
                file: m[2],
                ext: m[3]
            };
        } else {
            pathParts = {
                path: "",
                file: "",
                ext: ""
            };
        }
        switch (pathParts.ext) {
            case "mp4":
                format = "video/mp4";
                break;
            case "flv":
                format = "video/x-flv";
                break;
            case "f4m":
                format = "video/f4m";
                break;
            case "smil":
                format = "application/smil";
                break;
            case "m3u8":
                format = "application/x-mpegURL";
                break;
            case "mp3":
                format = "audio/mpeg";
                break;
            case "ogv":
                format = "video/ogg";
                break;
            case "webm":
                format = "video/webm";
                break;
            default:
                format = "";
        }
        return format;
    }

};

function wnLog(strText) {
    if (wnShowConsoleLog) {
        if (window.console) {
            console.log(strText);
        }
    }
}

// Load any external scripts
if (wn.analytics.local && wn.analytics.local.google && wnVideoUtils.get(wn.analytics.local.google.enabled) === true) {
    var wn_gaAccountId = wnVideoUtils.get(wn.analytics.local.google.variables.accountid);

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', wn_gaAccountId]);
    _gaq.push(['_setDomainName', 'none']);
    _gaq.push(['_setAllowLinker', true]);

    if (wnVideoUtils.get(wn.analytics.local.google.enabledpageviews) === true) {
        _gaq.push(['_trackPageview']);
    }

    if (wnVideoUtils.get(wn.analytics.local.google.enabledaddthis) === true) {
        // Set AddThis account
        if (typeof(addthis_config) == 'undefined') {
            addthis_config = {};
        }
        addthis_config = {
            data_ga_property: wn_gaAccountId
        };
    }

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
}

// temp - need a proper solution for all such cases
var wnTempMrss3rdUrl = '';
var wnTempMrssRegEx = '';
if (wn.video.mrss) {
    wnTempMrss3rdUrl = wnVideoUtils.get(wn.video.mrss.thirdpartytemplatemrssurl);
    wnTempMrssRegEx = wnVideoUtils.get(wn.video.mrss.regexpmrssid);
}

var wnTempEnableCommentsWidget = false;
if (wn.socialtools.comments.video) {
    wnTempEnableCommentsWidget = wnVideoUtils.get(wn.socialtools.comments.video.enable);
}

var wnTempMDPartnerId = '';
var wnTempMDPartnerDomain = '';
if (wn.socialtools.mobdub) {
    wnTempMDPartnerId = wnVideoUtils.get(wn.socialtools.mobdub.partnerid);
    wnTempMDPartnerDomain = wnVideoUtils.get(wn.socialtools.mobdub.partnerdomain);
}

var wnSiteConfigGeneral = {
    "baseUrl": wnVideoUtils.get(wn.baseurl),
    "contentDomain": wnVideoUtils.get(wn.contentdomain),
    "feedsApiDomain": wnVideoUtils.get(wn.feedsapidomain),
    "affiliateName": wnVideoUtils.get(wn.affiliatename),
    "affiliateNumber": wnVideoUtils.get(wn.affiliateno),
    "staticFarmImagePrefix": wnVideoUtils.get(wn.staticimagefarmprefix),
    "enableCommentsWidget": wnTempEnableCommentsWidget,
    "cacheVersionBuster": wnVideoUtils.get(wn.jsincludesversion2)
};

var wnSiteConfigVideo = {
    "wmode": "transparent",
    "getFlashUrl": "http://www.macromedia.com/go/getflashplayer",
    "imagePath": wnVideoUtils.get(wn.staticimagefarmprefix) + "/images/static/video/flash/",
    "flashPath": "http://" + wnVideoUtils.get(wn.baseurl) + "/global/video/flash/widgets",
    "defaultThumb": wnVideoUtils.get(wn.staticimagefarmprefix) + "/gfx_defaultThumb.jpg",
    "helpPageUrl": wnVideoUtils.get(wn.video.general.helppage),
    "landingPageUrl": wnVideoUtils.get(wn.video.general.landingpage),
    "slideShowLandingPageUrl": wnVideoUtils.get(wn.video.widgets.imagecanvas.slideshowlandingpage),
    "slideShowInCanvasAdFreq": wnVideoUtils.get(wn.video.widgets.slideshow.incanvasadfrequency),
    "slideShowCompanionAdFreq": wnVideoUtils.get(wn.video.widgets.slideshow.companionadfrequency),
    "enableSlideShowPageViewReports": wnVideoUtils.get(wn.video.widgets.slideshow.enableslideshowpageviewreporting),
    "renderSlideShowAsHtml": wnVideoUtils.get(wn.video.widgets.slideshow.renderslideshowashtml),
    "defaultSummaryText": "", // Mark
    "siteDefaultFormat": wnVideoUtils.get(wn.video.site.defaultformat),
    "useUAF": wnVideoUtils.get(wn.video.site.useuaf),
    "usePreRollMaster": wnVideoUtils.get(wn.video.site.companionads.useprerollmaster),
    "companionAdSizes": wnVideoUtils.get(wn.video.site.companionads.companionadsizes),
    "mobileCompanionAdSizes": wnVideoUtils.get(wn.video.site.companionads.mobilecompanionadsizes),
    "adOwnerAffiliateXML": "", // Mark
    "defaultEmbedCodeFormat": wnVideoUtils.get(wn.video.site.defaultembedcodeformat),
    "defaultPlaybackFormat": wnVideoUtils.get(wn.video.site.playbackformatorder),
    "defaultMaxBitrate": wnVideoUtils.get(wn.video.site.playbackmaxbitrate),
    "dynamicStreamingEnabled": wnVideoUtils.get(wn.video.site.dynamicstreamingenabled),
    "dynamicStreamingBaseUrl": wnVideoUtils.get(wn.video.site.dynamicstreamingbaseurl),
    "dynamicStreamingCPCode": "", // Mark
    "thirdPartyMrssTemplateurl": wnTempMrss3rdUrl,
    "thirdPartyRegexMrssId": wnTempMrssRegEx,
    "enableExpressReports": wnVideoUtils.get(wn.video.reporting.enableexpressreporting),
    "enableAdvertisingZone": wnVideoUtils.get(wn.video.widgets.enableadvertisingzone),
    "pageTitleTag": wnVideoUtils.get(wn.pagetitletag),
    "disableGoogleSDKAdCallOverwrite": wnVideoUtils.get(wn.video.site.googleadsdkdisableadcalloverwrite),
    "googleSDKVideoAdBandwidth": wnVideoUtils.get(wn.video.site.googlesdkvideoadbandwidth),
    "closeCaptionBackgroundTransparency": wnVideoUtils.get(wn.video.site.closecaptionbackgroundtransparency),
    "enableHDSLivestreaming": wnVideoUtils.get(wn.video.site.hdslivestreaming),
    "mobileForceFlash": wnVideoUtils.get(wn.video.site.mobileforceflash),
    "enableSingleClipPage": wnVideoUtils.get(wn.video.site.enablesingleclippage),
    "enableLivestreamCC": wnVideoUtils.get(wn.video.site.enablelivesreamcc),
    "enableCcEnhancementsForVOD": wnVideoUtils.get(wn.video.site.enableccenhancementsforvod),
    "enableCcEnhancementsForLivestream": wnVideoUtils.get(wn.video.site.enableccenhancementsforlivestream),
    "enableAkamaiPlayer": wnVideoUtils.get(wn.video.site.enableakamaiplayer),
    "enableABRStreaming": wnVideoUtils.get(wn.video.site.enableabrstreaming),

    "social": {
        "enableVideoComments": wnTempEnableCommentsWidget,
        "mdPartnerId": wnTempMDPartnerId,
        "mdPartnerDomain": wnTempMDPartnerDomain,
        "enableStoryComments": wnVideoUtils.get(wn.socialtools.comments.story.enable),
        "displayStoryCommentsByDefault": wnVideoUtils.get(wn.socialtools.comments.story.display),
        "numberOfCommentsToDisplay": 3,
        "pubId": wnVideoUtils.get(wn.socialtools.addthistools.pubid)
    },

    "uploadWidget": {
        "headerText": "",
        "browseBtnText": "",
        "browseBtnLabelText": "",
        "emailLabelText": "",
        "titleLabelText": "",
        "dropDownLabelText": "",
        "dropDownBtnText": "",
        "uploadBtnText": "",
        "uploadUrl": "",
        "uploadTerms": "",
        "progressLoadedKB": "",
        "progressTotalKB": "",
        "chkBoxText": "",
        "chkBoxLinkText": "",
        "completedText": "",
        "fileFailedText": "",
        "fileTooLargeText": "",
        "invalidEmailText": "",
        "missingTitleText": "",
        "missingTermsText": "",
        "missingFileText": "",
        "fileUploadedText": ""
    },

    "canvasWidget": {
        "defaultVersion": wnVideoUtils.get(wn.video.defaults.playerversion),
        "defaultType": wnVideoUtils.get(wn.video.defaults.playertype),
        "defaultSkin": wnVideoUtils.get(wn.video.defaults.canvasskin),
        "defaultStylePackage": "",
        "loadingMsg": "",
        "width": wnVideoUtils.get(wn.video.widgets.canvas.width),
        "height": wnVideoUtils.get(wn.video.widgets.canvas.height),
        "windowMode": wnVideoUtils.get(wn.video.widgets.canvas.ui.windowmode),
        "playBtnText": "",
        "pauseBtnText": "",
        "summaryLongBtnText": "",
        "summaryShortBtnText": "",
        "emailLongBtnText": "",
        "emailShortBtnText": "",
        "helpLongBtnText": "",
        "helpShortBtnText": "",
        "CCLongBtnText": "",
        "CCShortBtnText": "",
        "hasEmail": wnVideoUtils.get(wn.video.widgets.canvas.ui.email.hasemail),
        "hasHelp": wnVideoUtils.get(wn.video.widgets.canvas.ui.help.hashelp),
        "openAdPreRollPos": "",
        "openAdPostRollPos": "",
        "openAdLeaderboardPos": "",
        "playAtActualSize": wnVideoUtils.get(wn.video.widgets.canvas.playatactualsize),
        "smoothingMode": wnVideoUtils.get(wn.video.widgets.canvas.smoothingmode),
        "scaleStyle": wnVideoUtils.get(wn.video.widgets.canvas.scalestyle),
        "videoLoadingMsg": "",
        "summaryGfxMsg": "",
        "errorMsg": "",
        "commercialHeadlinePrefix": wnVideoUtils.get(wn.video.widgets.canvas.ui.commercialheadlineprefix),
        "summaryPaneLabelText": "",
        "CCPanelLabelText": "",
        "emailPanelLabelText": "",
        "closePanelLabelText": "",
        "recipientEmailLabelText": "",
        "senderEmailLabelText": "",
        "senderNameLabelText": "",
        "emailMsgLabelText": "",
        "sendEmailBtnText": "",
        "sentEmailMsg": "",
        "invalidEmailMsg": "",
        "invalidSenderEmailMsg": ""
    },

    "galleryWidget": {
        "hasSearch": wnVideoUtils.get(wn.video.widgets.canvas.hassearch),
        "defaultStylePackage": "",
        "catXMLPath": "",
        "width": wnVideoUtils.get(wn.video.widgets.gallery.width),
        "height": wnVideoUtils.get(wn.video.widgets.gallery.height),
        "topCatHeadline": "",
        "isContinousPlay": wnVideoUtils.get(wn.video.widgets.gallery.iscontinousplay),
        "catUnavailableText": "",
        "listItemGfxPos": wnVideoUtils.get(wn.video.widgets.gallery.listitemgraphicposition),
        "listItemScaleStyle": wnVideoUtils.get(wn.video.widgets.gallery.listitemscalestyle),
        "rows": wnVideoUtils.get(wn.video.widgets.gallery.rows),
        "cols": wnVideoUtils.get(wn.video.widgets.gallery.columns),
        "nextBtnText": "",
        "prevBtnText": "",
        "searchBtnText": "",
        "searchLabelText": "",
        "hideResultsText": "",
        "showResultsText": "",
        "moreTabText": "",
        "searchDisplayMsg": "",
        "searchNotFoundMsg": "",
        "searchFoundMsg": "",
        "searchResultsSeparatorDisplayMsg": "",
        "searchNoResultsMainWinMsg": "",
        "catNoResultsWinMsg": ""
    },

    "headlineWidget": {
        "defaultStylePackage": wnVideoUtils.get(wn.video.widgets.headline.defaultstylepackage),
        "defaultTitleText": "",
        "width": wnVideoUtils.get(wn.video.widgets.headline.width),
        "height": wnVideoUtils.get(wn.video.widgets.headline.height),
        "fontSize": wnVideoUtils.get(wn.video.widgets.headline.fontsize),
        "darkBGColors": wnVideoUtils.get(wn.video.widgets.headline.background.dark.colors),
        "darkBGAlphas": wnVideoUtils.get(wn.video.widgets.headline.background.dark.alphas),
        "darkBGRatios": wnVideoUtils.get(wn.video.widgets.headline.background.dark.ratios),
        "darkBGBorderColor": wnVideoUtils.get(wn.video.widgets.headline.background.dark.bordercolor),
        "darkBGRotation": wnVideoUtils.get(wn.video.widgets.headline.background.dark.rotation),
        "darkDropShadowColor": wnVideoUtils.get(wn.video.widgets.headline.background.dark.dropshadowcolor),
        "darkOffFaceColor": wnVideoUtils.get(wn.video.widgets.headline.background.dark.offfacecolor),
        "lightBGColors": wnVideoUtils.get(wn.video.widgets.headline.background.light.colors),
        "lightBGAlphas": wnVideoUtils.get(wn.video.widgets.headline.background.light.alphas),
        "lightBGRatios": wnVideoUtils.get(wn.video.widgets.headline.background.light.ratios),
        "lightBGBorderColor": wnVideoUtils.get(wn.video.widgets.headline.background.light.bordercolor),
        "lightBGRotation": wnVideoUtils.get(wn.video.widgets.headline.background.light.rotation),
        "lightDropShadowColor": wnVideoUtils.get(wn.video.widgets.headline.background.light.dropshadowcolor),
        "lightOffFaceColor": wnVideoUtils.get(wn.video.widgets.headline.background.light.offfacecolor)
    },

    "tickerWidget": {
        "defaultStylePackage": "",
        "width": wnVideoUtils.get(wn.video.widgets.ticker.width),
        "height": wnVideoUtils.get(wn.video.widgets.ticker.height)
    },

    "infoWidget": {
        "defaultStylePackage": "",
        "width": wnVideoUtils.get(wn.video.widgets.infopane.width),
        "height": wnVideoUtils.get(wn.video.widgets.infopane.height)
    },

    "imageCanvasWidget": {
        "defaultStylePackage": "",
        "width": wnVideoUtils.get(wn.video.widgets.imagecanvas.width),
        "height": wnVideoUtils.get(wn.video.widgets.imagecanvas.height),
        "slideTime": wnVideoUtils.get(wn.video.widgets.imagecanvas.slidetime)
    },

    "imageGalleryWidget": {
        "defaultStylePackage": "",
        "width": wnVideoUtils.get(wn.video.widgets.imagegallery.width),
        "height": wnVideoUtils.get(wn.video.widgets.imagegallery.height)
    }
};

// possible values: true or ''
var wn_debug_widgets = wnVideoUtils.getQS("wnDebugWidgets");
var wn_widget_mode = wnVideoUtils.getQS("wnWidgetMode");
var videoCanvasId = "WNVideoCanvas";
var wnWidgetsFlashVars = {};
var wnWidgetsInstallFlashVars = {};
var wnWidgetsAttributes = {};
var wnInstallFlashVarsTemplate = {};
var wnFeedObject = {};
var wnHelpPageUrl = wnSiteConfigVideo.helpPageUrl;
var wnImagePath = wnSiteConfigVideo.imagePath;
var wnPreferredVideoFormat = "FLV"; // ADDED - 02/28/2008 - By default, preferredVideoFormat is FLV for Flash Player...
var wnSilverlightBaseUrl = wnSiteConfigVideo.flashPathSLMediaFiles; // ADDED - 02/28/2008 - We will need this BaseUrl for Silverlight related contents, like: JS,Fonts,Images
wnWidgetsFlashVars.landingPage = wnSiteConfigVideo.landingPageUrl;
wnWidgetsFlashVars.baseUrl = wnSiteConfigGeneral.baseUrl;
wnFeedObject.feeds = []; // ADDED - 02/28/2008 - Initialize Feeds Array
wnFeedObject.hasFeeds = false; // ADDED - 02/28/2008 - Set Initial value to False
wnSiteDefaultVideoFormat = wnSiteConfigVideo.siteDefaultFormat;
var renderSlideShowAsHtml = wnSiteConfigVideo.renderSlideShowAsHtml;
var enableSlideshowPageViewReporting = wnSiteConfigVideo.enableSlideShowPageViewReports;
var enableVideoComments = wnSiteConfigGeneral.enableCommentsWidget;
var wnUsePrerollMaster = wnSiteConfigVideo.usePreRollMaster;
var wnShowConsoleLog = wnVideoUtils.validateString(wnVideoUtils.getQS("wnShowConsoleLog")) && wnVideoUtils.getQS("wnShowConsoleLog") == "true";
var wnCompanionAds = wnSiteConfigVideo.companionAdSizes;
wnInstallFlashVarsTemplate.imgPath = wnSiteConfigVideo.imagePath;
wnInstallFlashVarsTemplate.getFlash = wnSiteConfigVideo.flashPath + "/gfx_getFlash.jpg";
wnInstallFlashVarsTemplate.getFlashUrl = wnSiteConfigVideo.getFlashUrl;
wnInstallFlashVarsTemplate.helpPage = wnSiteConfigVideo.helpPageUrl;
wnInstallFlashVarsTemplate.cancelCopy = "You have cancelled the download process.  You will need to install the flash player to view videos.  Click the help button below if you need assistance.";
wnInstallFlashVarsTemplate.failedCopy = "Flash Player failed to download.  Please click the help button below for assistance.";

// Site type
var wn_isPlatformSite = false;
var wn_isPlatformSiteMobile = false;
if (typeof(wng_pageInfo) === 'object' && wng_pageInfo != null && wng_pageInfo != undefined) {
    wn_isPlatformSite = true;
    wn_isPlatformSiteMobile = wng_pageInfo.isMobile;
}

//CLIENT DETECTION - use independent detection
var wn_isMobile = false;
if (wnVideoUtils.getQS('wnhtml5') == 'true' || (/iphone|ipad|ipod|Android|BlackBerry|Opera Mini|IEMobile/i.test(navigator.userAgent.toLowerCase()))) {
    wn_isMobile = true;
} else if (wn_isPlatformSite) {
    wn_isMobile = wng_pageInfo.isMobile;
}

// Declare mock objects to avoid errors on 3rd party code referencing flash features
if (typeof(CANVAS_SKINS) != 'object') {
    var CANVAS_SKINS = {
        flat: {},
        satin: {}
    };
}

// Set up the mobile environent
if (wn_isMobile) {
    document.write('<scr' + 'ipt id="WNGoogleJSAPI" type="text/javascript" src="http://www.google.com/jsapi"><\/scr' + 'ipt>');
    //cument.write('<scr' + 'ipt type="text/javascript">if (typeof google != "undefined") google.load("ima", "1");<\/scr' + 'ipt>');
    if (wn_debug_widgets == "true") {
        document.write('<scr' + 'ipt type="text/javascript" src="http://imasdk.googleapis.com/js/sdkloader/ima3_debug.js"><\/scr' + 'ipt>');
    } else {
        document.write('<scr' + 'ipt type="text/javascript" src="http://s0.2mdn.net/instream/html5/ima3.js"><\/scr' + 'ipt>');
    }

    // Set mobile companions
    wnCompanionAds = wnSiteConfigVideo.mobileCompanionAdSizes;
}

var WNisProducerRegExp = new RegExp("://manage[A-Za-z0-9.]*\.worldnow.com");
var platform = window.PLATFORM || ''
if (platform == 'off-platform' && WNisProducerRegExp.test(document.location.href) == false && typeof(window.addthis) != 'object') {
    try {
        var wnAddThisPubId = (wnSiteConfigVideo.social.pubId && wnSiteConfigVideo.social.pubId.length > 0 ? wnSiteConfigVideo.social.pubId : 'xa-4bbcc485014c0ab2');
        if (wn_isMobile) {
            wnVideoUtils.loadScript('wnAddThisScript', 'http://s7.addthis.com/js/300/addthis_widget.js?pubid=' + wnAddThisPubId); //...pub=5435668
        } else {
            wnVideoUtils.loadScript('wnAddThisScript', 'http://s7.addthis.com/js/250/addthis_widget.js?pubid=' + wnAddThisPubId); //...pub=5435668
        }
    } catch (e) {}
}

// Get root page value
var wnPageType = "none";
try {
    if (wng_pageInfo && wng_pageInfo.containerType == "S") {
        wnPageType = 'videostorypage';
    }
    if (($wn("meta[name='wn-rootname']").attr("content") == "video") || ($wn("meta[name='wnreadableurl']").attr("content") == "video")) {
        wnPageType = 'videolandingpage';
    }
    if ($wn("meta[name='wn-rootname']").attr("content") == "slideshow" || ($wn("meta[name='wnreadableurl']").attr("content") == "slideshow")) {
        wnPageType = 'slideshowlandingpage';
    }

} catch (e) {}

// Set defaults
// Canvas Style Package - v1
if (!wnSiteConfigVideo.canvasWidget.defaultStylePackage) {
    wnSiteConfigVideo.canvasWidget.defaultStylePackage = 'dark';
}

// Gallery Style Package - v1
if (!wnSiteConfigVideo.galleryWidget.defaultStylePackage) {
    wnSiteConfigVideo.galleryWidget.defaultStylePackage = 'dark';
}

// ----- Begin Canvas auto upgrade defaults -----

// Set default player version
try {
    if (wnSiteConfigVideo.canvasWidget.defaultVersion == '1' || wnSiteConfigVideo.canvasWidget.defaultVersion == '') {
        wnSiteConfigVideo.canvasWidget.defaultPlayerSWF = 'WNVideoCanvas.swf';
    } else if (wnSiteConfigVideo.canvasWidget.defaultVersion == '2') {
        wnSiteConfigVideo.canvasWidget.defaultPlayerSWF = 'WNVideoCanvas2.swf';
    } else {
        wnSiteConfigVideo.canvasWidget.defaultPlayerSWF = 'WNVideoCanvas.swf';
    }
} catch (e) {
    wnSiteConfigVideo.canvasWidget['defaultPlayerSWF'] = 'WNVideoCanvas.swf';
}

// Set Skin
var canvasDefaultSkinPackage = 'satin';
var canvasDefaultSkin = 'silver';
try {
    if (wnSiteConfigVideo.canvasWidget.defaultSkin != "") {
        var skinPkg = wnSiteConfigVideo.canvasWidget.defaultSkin.split('.');
        canvasDefaultSkinPackage = skinPkg[0].toLowerCase();
        canvasDefaultSkin = skinPkg[1].toLowerCase();
    }
} catch (e) {}

// Controls type
try {
    if (wnSiteConfigVideo.canvasWidget.defaultType != "") {
        wnSiteConfigVideo.canvasWidget.defaultType = wnSiteConfigVideo.canvasWidget.defaultType.toLowerCase();
    }
} catch (e) {}

// ----- End Canvas auto upgrade defaults -----

// ----- HTML5 player globals -------//
//var wnFeedsApiDomain = 'http://api.dsys2.worldnow.com:8000';
var wnFeedsApiDomain = 'http://api.worldnow.com';
if (wnVideoUtils.validateString(wnSiteConfigGeneral.feedsApiDomain)) {
    wnFeedsApiDomain = wnSiteConfigGeneral.feedsApiDomain;
}

/*
var wnVideoPlayerGroupId = 'DEFAULT';
var wnVideoAutoStart = false;
*/
// ----- End HTML5 player globals ---//

wnWidgetsAttributes["WNUserUpload"] = {};
wnWidgetsAttributes["WNUserUpload"].uri = wnSiteConfigVideo.flashPath + "/WNUserUpload.swf";
wnWidgetsAttributes["WNUserUpload"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";

wnWidgetsInstallFlashVars["WNUserUpload"] = {};
wnWidgetsInstallFlashVars["WNUserUpload"] = wnInstallFlashVarsTemplate;

wnWidgetsFlashVars["WNUserUpload"] = {};
wnWidgetsFlashVars["WNUserUpload"].headerBarFontSize = "10";
wnWidgetsFlashVars["WNUserUpload"].headerText = wnSiteConfigVideo.uploadWidget.headerText;
wnWidgetsFlashVars["WNUserUpload"].fieldHeight = "23";
wnWidgetsFlashVars["WNUserUpload"].fieldLeft = "62";
wnWidgetsFlashVars["WNUserUpload"].fieldRight = "24";
wnWidgetsFlashVars["WNUserUpload"].browseButtonText = wnSiteConfigVideo.uploadWidget.browseBtnText;
wnWidgetsFlashVars["WNUserUpload"].browseLabelText = wnSiteConfigVideo.uploadWidget.browseBtnLabelText;
wnWidgetsFlashVars["WNUserUpload"].formTextSize = "10";
wnWidgetsFlashVars["WNUserUpload"].padding = "8";
wnWidgetsFlashVars["WNUserUpload"].emailLabelText = wnSiteConfigVideo.uploadWidget.emailLabelText;
wnWidgetsFlashVars["WNUserUpload"].titleLabelText = wnSiteConfigVideo.uploadWidget.titleLabelText;
wnWidgetsFlashVars["WNUserUpload"].dropDownButtonFontSize = "10";
wnWidgetsFlashVars["WNUserUpload"].dropDownLabelText = wnSiteConfigVideo.uploadWidget.dropDownLabelText;
wnWidgetsFlashVars["WNUserUpload"].dropDownButtonTextMessage = wnSiteConfigVideo.uploadWidget.dropDownBtnText;
wnWidgetsFlashVars["WNUserUpload"].dropDownListFontSize = "10";
wnWidgetsFlashVars["WNUserUpload"].uploadButtonText = wnSiteConfigVideo.uploadWidget.uploadBtnText;
wnWidgetsFlashVars["WNUserUpload"].uploadUrl = wnSiteConfigVideo.uploadWidget.uploadUrl;
wnWidgetsFlashVars["WNUserUpload"].affiliate = wnSiteConfigGeneral.affiliateName;
wnWidgetsFlashVars["WNUserUpload"].termsAndConditionsLink = wnSiteConfigVideo.uploadWidget.uploadTerms;
wnWidgetsFlashVars["WNUserUpload"].uploadFileSizeMaxBytes = "100000000";
wnWidgetsFlashVars["WNUserUpload"].progressMsgLoadedKBText = wnSiteConfigVideo.uploadWidget.progressLoadedKB;
wnWidgetsFlashVars["WNUserUpload"].progressMsgTotalKBText = wnSiteConfigVideo.uploadWidget.progressTotalKB;
wnWidgetsFlashVars["WNUserUpload"].progressMsgCompletedText = wnSiteConfigVideo.uploadWidget.completedText;
wnWidgetsFlashVars["WNUserUpload"].loadingMessage = wnSiteConfigVideo.canvasWidget.loadingMsg;
wnWidgetsFlashVars["WNUserUpload"].fileDialogFailText = wnSiteConfigVideo.uploadWidget.fileFailedText;
wnWidgetsFlashVars["WNUserUpload"].fileTooLargeText = wnSiteConfigVideo.uploadWidget.fileTooLargeText;
wnWidgetsFlashVars["WNUserUpload"].missingValidEmailAddressText = wnSiteConfigVideo.uploadWidget.invalidEmailText;
wnWidgetsFlashVars["WNUserUpload"].missingTitleText = wnSiteConfigVideo.uploadWidget.missingTitleText;
wnWidgetsFlashVars["WNUserUpload"].missingConfirmationOfTermsText = wnSiteConfigVideo.uploadWidget.missingTermsText;
wnWidgetsFlashVars["WNUserUpload"].missingFileText = wnSiteConfigVideo.uploadWidget.missingFileText;
wnWidgetsFlashVars["WNUserUpload"].fileJustUploadedText = wnSiteConfigVideo.uploadWidget.fileUploadedText;



wnWidgetsAttributes["WNVideoCanvas"] = {};
wnWidgetsAttributes["WNVideoCanvas"].uri = wnSiteConfigVideo.flashPath + "/" + wnSiteConfigVideo.canvasWidget.defaultPlayerSWF;
wnWidgetsAttributes["WNVideoCanvas"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNVideoCanvas"].wmode = wnSiteConfigVideo.canvasWidget.windowMode;
wnWidgetsAttributes["WNVideoCanvas"].silverlightUri = wnSiteConfigVideo.flashPath + "/"; // ADDED - 02/28/2008 - Silverlight player source XAML

wnWidgetsFlashVars["WNVideoCanvas"] = {};
wnWidgetsFlashVars["WNVideoCanvas"].playerWidth = wnSiteConfigVideo.canvasWidget.width;
wnWidgetsFlashVars["WNVideoCanvas"].playerHeight = wnSiteConfigVideo.canvasWidget.height;
wnWidgetsFlashVars["WNVideoCanvas"].affiliateNumber = wnSiteConfigGeneral.affiliateNumber;
wnWidgetsFlashVars["WNVideoCanvas"].playButtonText = wnSiteConfigVideo.canvasWidget.playBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].pauseButtonText = wnSiteConfigVideo.canvasWidget.pauseBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].summaryLongButtonText = wnSiteConfigVideo.canvasWidget.summaryLongBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].summaryShortButtonText = wnSiteConfigVideo.canvasWidget.summaryShortBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].emailLongButtonText = wnSiteConfigVideo.canvasWidget.emailLongBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].emailShortButtonText = wnSiteConfigVideo.canvasWidget.emailShortBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].helpLongButtonText = wnSiteConfigVideo.canvasWidget.helpLongBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].helpShortButtonText = wnSiteConfigVideo.canvasWidget.helpShortBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].closecaptionLongButtonText = wnSiteConfigVideo.canvasWidget.CCLongBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].closecaptionShortButtonText = wnSiteConfigVideo.canvasWidget.CCShortBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].hasEmail = wnSiteConfigVideo.canvasWidget.hasEmail;
wnWidgetsFlashVars["WNVideoCanvas"].hostDomain = wnSiteConfigGeneral.baseUrl;
wnWidgetsFlashVars["WNVideoCanvas"].affiliate = wnSiteConfigGeneral.affiliateName;
wnWidgetsFlashVars["WNVideoCanvas"].imgPath = wnSiteConfigVideo.imagePath;
wnWidgetsFlashVars["WNVideoCanvas"].emailSubmitURL = "";
wnWidgetsFlashVars["WNVideoCanvas"].addRedirectBase = "";
wnWidgetsFlashVars["WNVideoCanvas"].clipBaseXMLPath = "";
wnWidgetsFlashVars["WNVideoCanvas"].helpPage = wnSiteConfigVideo.helpPageUrl;
wnWidgetsFlashVars["WNVideoCanvas"].clipId = "";
wnWidgetsFlashVars["WNVideoCanvas"].isMute = "";
wnWidgetsFlashVars["WNVideoCanvas"].playAtActualSize = wnSiteConfigVideo.canvasWidget.playAtActualSize;
wnWidgetsFlashVars["WNVideoCanvas"].hasHelp = wnSiteConfigVideo.canvasWidget.hasHelp;
wnWidgetsFlashVars["WNVideoCanvas"].smoothingMode = wnSiteConfigVideo.canvasWidget.smoothingMode;
wnWidgetsFlashVars["WNVideoCanvas"].staticImgPath = wnSiteConfigGeneral.staticFarmImagePrefix;
wnWidgetsFlashVars["WNVideoCanvas"].openAdPrerollPosition = wnSiteConfigVideo.canvasWidget.openAdPreRollPos;
wnWidgetsFlashVars["WNVideoCanvas"].openAdPostrollPosition = wnSiteConfigVideo.canvasWidget.openAdPostRollPos;
wnWidgetsFlashVars["WNVideoCanvas"].openAdLeaderboardPosition = wnSiteConfigVideo.canvasWidget.openAdLeaderboardPos;
wnWidgetsFlashVars["WNVideoCanvas"].playerIncludeType = "";
wnWidgetsFlashVars["WNVideoCanvas"].videoLoadingMessage = wnSiteConfigVideo.canvasWidget.videoLoadingMsg;
wnWidgetsFlashVars["WNVideoCanvas"].summaryGraphicScaleStyle = wnSiteConfigVideo.canvasWidget.scaleStyle;
wnWidgetsFlashVars["WNVideoCanvas"].isAutoStart = "";
wnWidgetsFlashVars["WNVideoCanvas"].hasFullScreen = "true";
wnWidgetsFlashVars["WNVideoCanvas"].fullScreenControlType = "none";
wnWidgetsFlashVars["WNVideoCanvas"].adOwnerAffiliatesXML = wnSiteConfigVideo.adOwnerAffiliateXML;
wnWidgetsFlashVars["WNVideoCanvas"].summaryGraphicMessage = wnSiteConfigVideo.canvasWidget.summaryGfxMsg;
wnWidgetsFlashVars["WNVideoCanvas"].errorMessage = wnSiteConfigVideo.canvasWidget.errorMsg;
wnWidgetsFlashVars["WNVideoCanvas"].loadingMessage = wnSiteConfigVideo.canvasWidget.loadingMsg;
wnWidgetsFlashVars["WNVideoCanvas"].defaultStyle = wnSiteConfigVideo.canvasWidget.defaultStylePackage;
//wnWidgetsFlashVars["WNVideoCanvas"].locationUrl                               = document.location;
wnWidgetsFlashVars["WNVideoCanvas"].commercialHeadlinePrefix = wnSiteConfigVideo.canvasWidget.commercialHeadlinePrefix;
wnWidgetsFlashVars["WNVideoCanvas"].summaryPaneLabelText = wnSiteConfigVideo.canvasWidget.summaryPaneLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].closecaptionPaneLabelText = wnSiteConfigVideo.canvasWidget.CCPanelLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].emailPaneLabelText = wnSiteConfigVideo.canvasWidget.emailPanelLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].closePaneLabelText = wnSiteConfigVideo.canvasWidget.closePanelLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].recipientEmailLabelText = wnSiteConfigVideo.canvasWidget.recipientEmailLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].senderEmailLabelText = wnSiteConfigVideo.canvasWidget.senderEmailLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].senderNameLabelText = wnSiteConfigVideo.canvasWidget.senderNameLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].emailMessageLabelText = wnSiteConfigVideo.canvasWidget.emailMsgLabelText;
wnWidgetsFlashVars["WNVideoCanvas"].sendEmailButtonText = wnSiteConfigVideo.canvasWidget.sendEmailBtnText;
wnWidgetsFlashVars["WNVideoCanvas"].emailSentConfirmationMessage = wnSiteConfigVideo.canvasWidget.sentEmailMsg;
wnWidgetsFlashVars["WNVideoCanvas"].invalidRecipientFieldMessage = wnSiteConfigVideo.canvasWidget.invalidEmailMsg;
wnWidgetsFlashVars["WNVideoCanvas"].invalidSenderFieldMessage = wnSiteConfigVideo.canvasWidget.invalidSenderEmailMsg;
wnWidgetsFlashVars["WNVideoCanvas"].disableTransport = "false";
wnWidgetsFlashVars["WNVideoCanvas"].landingPage = wnSiteConfigVideo.landingPageUrl;
wnWidgetsFlashVars["WNVideoCanvas"].tabHeight = 26;
wnWidgetsFlashVars["WNVideoCanvas"].tabFontSize = 10;
wnWidgetsFlashVars["WNVideoCanvas"].siteDefaultFormat = wnSiteConfigVideo.siteDefaultFormat;
wnWidgetsFlashVars["WNVideoCanvas"].siteDefaultEmbedCodeFormat = wnSiteConfigVideo.defaultEmbedCodeFormat;
wnWidgetsFlashVars["WNVideoCanvas"].playbackFormat = wnSiteConfigVideo.defaultPlaybackFormat;
wnWidgetsFlashVars["WNVideoCanvas"].playbackMaxBitrate = wnSiteConfigVideo.defaultMaxBitrate;
wnWidgetsFlashVars["WNVideoCanvas"].thirdpartytemplatemrssurl = wnSiteConfigVideo.thirdPartyMrssTemplateurl;
wnWidgetsFlashVars["WNVideoCanvas"].regexpmrssid = wnSiteConfigVideo.thirdPartyRegexMrssId;
wnWidgetsFlashVars["WNVideoCanvas"].useUAF = wnSiteConfigVideo.useUAF;
wnWidgetsFlashVars["WNVideoCanvas"].playerType = "STANDARD";
wnWidgetsFlashVars["WNVideoCanvas"].controlPanelButtonsType = "auto";
wnWidgetsFlashVars["WNVideoCanvas"].enableExpressReport = wnSiteConfigVideo.enableExpressReports;
wnWidgetsFlashVars["WNVideoCanvas"].hasComments = "false";
wnWidgetsFlashVars["WNVideoCanvas"].usePrerollMaster = wnUsePrerollMaster;
wnWidgetsFlashVars["WNVideoCanvas"].companionAds = wnCompanionAds;
wnWidgetsFlashVars["WNVideoCanvas"].playerAdvertisingType = wnPageType;
wnWidgetsFlashVars["WNVideoCanvas"].advertisingZone = "";
wnWidgetsFlashVars["WNVideoCanvas"].controlsType = wnSiteConfigVideo.canvasWidget.defaultType;
wnWidgetsFlashVars["WNVideoCanvas"].volumeControlsType = "vertical";
wnWidgetsFlashVars["WNVideoCanvas"].skinPackage = canvasDefaultSkinPackage;
wnWidgetsFlashVars["WNVideoCanvas"].skinName = canvasDefaultSkin;
wnWidgetsFlashVars["WNVideoCanvas"].hasEmbed = "true";
wnWidgetsFlashVars["WNVideoCanvas"].hasVideoShare = "true";
wnWidgetsFlashVars["WNVideoCanvas"].hasCc = "false";
wnWidgetsFlashVars["WNVideoCanvas"].hasVideoSummary = "false";
wnWidgetsFlashVars["WNVideoCanvas"].hasMobdub = "false";
wnWidgetsFlashVars["WNVideoCanvas"].dynamicStreamingEnabled = wnSiteConfigVideo.dynamicStreamingEnabled;
wnWidgetsFlashVars["WNVideoCanvas"].dynamicStreamingBaseUrl = wnSiteConfigVideo.dynamicStreamingBaseUrl;
wnWidgetsFlashVars["WNVideoCanvas"].dynamicStreamingCPCode = wnSiteConfigVideo.dynamicStreamingCPCode;
wnWidgetsFlashVars["WNVideoCanvas"].galleryId = "";
wnWidgetsFlashVars["WNVideoCanvas"].hasEmbeddedGallery = "false";
wnWidgetsFlashVars["WNVideoCanvas"].videoType = "On Demand Clip";
wnWidgetsFlashVars["WNVideoCanvas"].contentClassification = "About Us";
wnWidgetsFlashVars["WNVideoCanvas"].streamHasPreroll = "no";
wnWidgetsFlashVars["WNVideoCanvas"].streamingUrl = "";
wnWidgetsFlashVars["WNVideoCanvas"].streamHeadline = "";
wnWidgetsFlashVars["WNVideoCanvas"].liveStreamGraphicUrl = "";
wnWidgetsFlashVars["WNVideoCanvas"].mobileStreamCcUrl = "";
wnWidgetsFlashVars["WNVideoCanvas"].mobileStream1 = "";
wnWidgetsFlashVars["WNVideoCanvas"].mobileStream2 = "";
wnWidgetsFlashVars["WNVideoCanvas"].mobileStream3 = "";
wnWidgetsFlashVars["WNVideoCanvas"].pageTitleTag = wnSiteConfigVideo.pageTitleTag;
wnWidgetsFlashVars["WNVideoCanvas"].disableGoogleSDKAdCallOverwrite = wnSiteConfigVideo.disableGoogleSDKAdCallOverwrite;
wnWidgetsFlashVars["WNVideoCanvas"].videoAdBandwidth = wnSiteConfigVideo.googleSDKVideoAdBandwidth;
wnWidgetsFlashVars["WNVideoCanvas"].closeCaptionBackgroundTransparency = wnSiteConfigVideo.closeCaptionBackgroundTransparency;
wnWidgetsFlashVars["WNVideoCanvas"].enableHDSLivestreaming = wnSiteConfigVideo.enableHDSLivestreaming;
wnWidgetsFlashVars["WNVideoCanvas"].mobileForceFlash = wnSiteConfigVideo.mobileForceFlash;
wnWidgetsFlashVars["WNVideoCanvas"].enableSingleClipPage = wnSiteConfigVideo.enableSingleClipPage;
//wnWidgetsFlashVars["WNVideoCanvas"].hideCcSettings = (wnVideoUtils.getQS("showcc") == 'true') ? false : true;
wnWidgetsFlashVars["WNVideoCanvas"].enableLivestreamCC = wnSiteConfigVideo.enableLivestreamCC;
wnWidgetsFlashVars["WNVideoCanvas"].enableCcEnhancementsForVOD = wnSiteConfigVideo.enableCcEnhancementsForVOD;
wnWidgetsFlashVars["WNVideoCanvas"].enableCcEnhancementsForLivestream = wnSiteConfigVideo.enableCcEnhancementsForLivestream;



wnWidgetsInstallFlashVars["WNVideoCanvas"] = {};
wnWidgetsInstallFlashVars["WNVideoCanvas"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNVideoCanvas"].playerWidth = wnSiteConfigVideo.canvasWidget.width;
wnWidgetsInstallFlashVars["WNVideoCanvas"].playerHeight = wnSiteConfigVideo.canvasWidget.height;


// Player Version 1
wnWidgetsAttributes["WNVideoCanvas1"] = {};
wnWidgetsAttributes["WNVideoCanvas1"].uri = wnSiteConfigVideo.flashPath + "/WNVideoCanvas.swf";
wnWidgetsAttributes["WNVideoCanvas1"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNVideoCanvas1"].wmode = wnSiteConfigVideo.canvasWidget.windowMode;
wnWidgetsAttributes["WNVideoCanvas1"].silverlightUri = wnSiteConfigVideo.flashPath + ""; // ADDED - 02/28/2008 - Silverlight player source XAML
wnWidgetsInstallFlashVars["WNVideoCanvas1"] = {};
wnWidgetsInstallFlashVars["WNVideoCanvas1"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNVideoCanvas1"].playerWidth = wnSiteConfigVideo.canvasWidget.width;
wnWidgetsInstallFlashVars["WNVideoCanvas1"].playerHeight = wnSiteConfigVideo.canvasWidget.height;
wnWidgetsFlashVars["WNVideoCanvas1"] = wnWidgetsFlashVars["WNVideoCanvas"];
wnWidgetsFlashVars["WNVideoCanvas1"].playerWidth = wnSiteConfigVideo.canvasWidget.width;
wnWidgetsFlashVars["WNVideoCanvas1"].playerHeight = wnSiteConfigVideo.canvasWidget.height;


// Player Version 2
wnWidgetsAttributes["WNVideoCanvas2"] = {};
wnWidgetsAttributes["WNVideoCanvas2"].uri = wnSiteConfigVideo.flashPath + "/WNVideoCanvas2.swf";
wnWidgetsAttributes["WNVideoCanvas2"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNVideoCanvas2"].wmode = wnSiteConfigVideo.canvasWidget.windowMode;
wnWidgetsAttributes["WNVideoCanvas2"].silverlightUri = wnSiteConfigVideo.flashPath + "";
wnWidgetsInstallFlashVars["WNVideoCanvas2"] = {};
wnWidgetsInstallFlashVars["WNVideoCanvas2"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNVideoCanvas2"].playerWidth = wnSiteConfigVideo.canvasWidget.width;
wnWidgetsInstallFlashVars["WNVideoCanvas2"].playerHeight = wnSiteConfigVideo.canvasWidget.height;
wnWidgetsFlashVars["WNVideoCanvas2"] = wnWidgetsFlashVars["WNVideoCanvas"];
wnWidgetsFlashVars["WNVideoCanvas2"].playerWidth = wnSiteConfigVideo.canvasWidget.width;
wnWidgetsFlashVars["WNVideoCanvas2"].playerHeight = wnSiteConfigVideo.canvasWidget.height;

wnWidgetsAttributes["WNGallery"] = {};
wnWidgetsAttributes["WNGallery"].uri = wnSiteConfigVideo.flashPath + "/WNGallery.swf";
wnWidgetsAttributes["WNGallery"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNGallery"].wmode = wnSiteConfigVideo.wmode;

wnWidgetsFlashVars["WNGallery"] = {};
wnWidgetsFlashVars["WNGallery"].affiliateNumber = wnSiteConfigGeneral.affiliateNumber;
wnWidgetsFlashVars["WNGallery"].isContinuousPlay = wnSiteConfigVideo.galleryWidget.isContinousPlay;
wnWidgetsFlashVars["WNGallery"].hasSearch = wnSiteConfigVideo.galleryWidget.hasSearch;
wnWidgetsFlashVars["WNGallery"].failSafeSummaryImageText = wnSiteConfigVideo.defaultSummaryText;
wnWidgetsFlashVars["WNGallery"].categoryUnavailableText = wnSiteConfigVideo.galleryWidget.catUnavailableText;
wnWidgetsFlashVars["WNGallery"].searchDisplayMessage = wnSiteConfigVideo.galleryWidget.searchDisplayMsg;
wnWidgetsFlashVars["WNGallery"].searchNotFoundDisplayMessage = wnSiteConfigVideo.galleryWidget.searchNotFoundMsg;
wnWidgetsFlashVars["WNGallery"].searchFoundDisplayMessage = wnSiteConfigVideo.galleryWidget.searchFoundMsg;
wnWidgetsFlashVars["WNGallery"].resultsSeparatorDisplayMessage = wnSiteConfigVideo.galleryWidget.searchResultsSeparatorDisplayMsg;
wnWidgetsFlashVars["WNGallery"].searchNoResultsMainWindowMessage = wnSiteConfigVideo.galleryWidget.searchNoResultsMainWinMsg;
wnWidgetsFlashVars["WNGallery"].categoryNoResultsMainWindowMessage = wnSiteConfigVideo.galleryWidget.catNoResultsWinMsg;
wnWidgetsFlashVars["WNGallery"].searchButtonText = wnSiteConfigVideo.galleryWidget.searchBtnText;
wnWidgetsFlashVars["WNGallery"].searchLabelText = wnSiteConfigVideo.galleryWidget.searchLabelText;
wnWidgetsFlashVars["WNGallery"].hideResultsLabelText = wnSiteConfigVideo.galleryWidget.hideResultsText;
wnWidgetsFlashVars["WNGallery"].showResultsLabelText = wnSiteConfigVideo.galleryWidget.showResultsText;
wnWidgetsFlashVars["WNGallery"].moreTabLabelText = wnSiteConfigVideo.galleryWidget.moreTabText;
wnWidgetsFlashVars["WNGallery"].strCat = "";
wnWidgetsFlashVars["WNGallery"].hostDomain = wnSiteConfigGeneral.baseUrl;
wnWidgetsFlashVars["WNGallery"].affiliate = wnSiteConfigGeneral.affiliateName;
wnWidgetsFlashVars["WNGallery"].imgPath = wnSiteConfigVideo.imagePath;
wnWidgetsFlashVars["WNGallery"].topCategoryHeadline = wnSiteConfigVideo.galleryWidget.topCatHeadline;
wnWidgetsFlashVars["WNGallery"].categoryXMLPath = wnSiteConfigVideo.galleryWidget.catXMLPath;
wnWidgetsFlashVars["WNGallery"].clipBaseXMLPath = "";
wnWidgetsFlashVars["WNGallery"].clipId = "";
wnWidgetsFlashVars["WNGallery"].listItemGraphicPosition = wnSiteConfigVideo.galleryWidget.listItemGfxPos;
wnWidgetsFlashVars["WNGallery"].staticImgPath = wnSiteConfigGeneral.staticFarmImagePrefix;
wnWidgetsFlashVars["WNGallery"].playerWidth = wnSiteConfigVideo.galleryWidget.width;
wnWidgetsFlashVars["WNGallery"].playerHeight = wnSiteConfigVideo.galleryWidget.height;
wnWidgetsFlashVars["WNGallery"].listItemGraphicScaleStyle = wnSiteConfigVideo.galleryWidget.listItemScaleStyle;
wnWidgetsFlashVars["WNGallery"].rows = wnSiteConfigVideo.galleryWidget.rows;
wnWidgetsFlashVars["WNGallery"].columns = wnSiteConfigVideo.galleryWidget.cols;
wnWidgetsFlashVars["WNGallery"].nextButtonText = wnSiteConfigVideo.galleryWidget.nextBtnText;
wnWidgetsFlashVars["WNGallery"].previousButtonText = wnSiteConfigVideo.galleryWidget.prevBtnText;
wnWidgetsFlashVars["WNGallery"].loadingMessage = wnSiteConfigVideo.canvasWidget.loadingMsg;
wnWidgetsFlashVars["WNGallery"].landingPage = wnSiteConfigVideo.landingPageUrl;
wnWidgetsFlashVars["WNGallery"].siteDefaultFormat = wnSiteConfigVideo.siteDefaultFormat;
wnWidgetsFlashVars["WNGallery"].playbackFormat = wnSiteConfigVideo.defaultPlaybackFormat;
wnWidgetsFlashVars["WNGallery"].playbackMaxBitrate = wnSiteConfigVideo.defaultMaxBitrate;
wnWidgetsFlashVars["WNGallery"].thirdpartytemplatemrssurl = wnSiteConfigVideo.thirdPartyMrssTemplateurl;
wnWidgetsFlashVars["WNGallery"].regexpmrssid = wnSiteConfigVideo.thirdPartyRegexMrssId;
wnWidgetsFlashVars["WNGallery"].useUAF = wnSiteConfigVideo.useUAF;
wnWidgetsFlashVars["WNGallery"].topVideoCatNo = "";
wnWidgetsFlashVars["WNGallery"].tabHeight = 26;
wnWidgetsFlashVars["WNGallery"].tabFontSize = 10;
wnWidgetsFlashVars["WNGallery"].dropDownFontSize = 10;
wnWidgetsFlashVars["WNGallery"].videoListNavigationHeight = 25;
wnWidgetsFlashVars["WNGallery"].videoListPageNumberFontSize = 10;
//wnWidgetsFlashVars["WNGallery"].navigationButtonFontSize                        = 10;

wnWidgetsFlashVars["WNGallery"].videoListItemTopPadding = 14;
wnWidgetsFlashVars["WNGallery"].videoListItemSidePadding = 10;
wnWidgetsFlashVars["WNGallery"].videoListImageWidth = 83;
wnWidgetsFlashVars["WNGallery"].videoListItemTopPaddingPct = 15;
wnWidgetsFlashVars["WNGallery"].videoListItemSidePaddingPct = 5;
wnWidgetsFlashVars["WNGallery"].videoListImageHeight = 63;
wnWidgetsFlashVars["WNGallery"].videoListImagePosition = "left";
wnWidgetsFlashVars["WNGallery"].videoListItemHeadlineFontSize = 10;
wnWidgetsFlashVars["WNGallery"].videoListItemDurationFontSize = 10;
wnWidgetsFlashVars["WNGallery"].videoListItemPositionStyle = "auto";
wnWidgetsFlashVars["WNGallery"].isPrimaryGallery = "true";
wnWidgetsFlashVars["WNGallery"].defaultStyle = wnSiteConfigVideo.galleryWidget.defaultStylePackage;
wnWidgetsFlashVars["WNGallery"].showTooltip = "false";
wnWidgetsFlashVars["WNGallery"].popupLandingPage = "false";
wnWidgetsFlashVars["WNGallery"].leftAlignTabText = "false";
wnWidgetsFlashVars["WNGallery"].showHeadline = "true";
wnWidgetsFlashVars["WNGallery"].dynamicStreamingEnabled = wnSiteConfigVideo.dynamicStreamingEnabled;
wnWidgetsFlashVars["WNGallery"].enableSingleClipPage = wnSiteConfigVideo.enableSingleClipPage;


wnWidgetsInstallFlashVars["WNGallery"] = {};
wnWidgetsInstallFlashVars["WNGallery"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNGallery"].playerWidth = wnSiteConfigVideo.galleryWidget.width;
wnWidgetsInstallFlashVars["WNGallery"].playerHeight = wnSiteConfigVideo.galleryWidget.height;

wnWidgetsAttributes["WNHeadline"] = {};
wnWidgetsAttributes["WNHeadline"].uri = wnSiteConfigVideo.flashPath + "/WNHeadline.swf";
wnWidgetsAttributes["WNHeadline"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNHeadline"].wmode = wnSiteConfigVideo.wmode;

wnWidgetsFlashVars["WNHeadline"] = {};
wnWidgetsFlashVars["WNHeadline"].playerWidth = wnSiteConfigVideo.headlineWidget.width;
wnWidgetsFlashVars["WNHeadline"].playerHeight = wnSiteConfigVideo.headlineWidget.height;
wnWidgetsFlashVars["WNHeadline"].fontSize = wnSiteConfigVideo.headlineWidget.fontSize;
wnWidgetsFlashVars["WNHeadline"].defaultStyle = wnSiteConfigVideo.headlineWidget.defaultStylePackage;
wnWidgetsFlashVars["WNHeadline"].loadingMessage = wnSiteConfigVideo.canvasWidget.loadingMsg;
wnWidgetsFlashVars["WNHeadline"].defaultTitleText = wnSiteConfigVideo.headlineWidget.defaultTitleText;
wnWidgetsFlashVars["WNHeadline"].slideshowHeadline = "";
wnWidgetsFlashVars["WNHeadline"].hasImageCount = "true";





wnWidgetsInstallFlashVars["WNHeadline"] = {};
wnWidgetsInstallFlashVars["WNHeadline"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNHeadline"].playerWidth = wnSiteConfigVideo.headlineWidget.width;
wnWidgetsInstallFlashVars["WNHeadline"].playerHeight = wnSiteConfigVideo.headlineWidget.height;

// TICKER
wnWidgetsAttributes["WNTicker"] = {};
wnWidgetsAttributes["WNTicker"].uri = wnSiteConfigVideo.flashPath + "/WNTicker.swf";
wnWidgetsAttributes["WNTicker"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNTicker"].wmode = wnSiteConfigVideo.wmode;
wnWidgetsFlashVars["WNTicker"] = {};
wnWidgetsFlashVars["WNTicker"].playerWidth = wnSiteConfigVideo.tickerWidget.width;
wnWidgetsFlashVars["WNTicker"].playerHeight = wnSiteConfigVideo.tickerWidget.height;
wnWidgetsFlashVars["WNTicker"].fontSize = "10";
wnWidgetsFlashVars["WNTicker"].defaultStyle = wnSiteConfigVideo.tickerWidget.defaultStylePackage;
wnWidgetsFlashVars["WNTicker"].loadingMessage = wnSiteConfigVideo.canvasWidget.loadingMsg;
wnWidgetsInstallFlashVars["WNTicker"] = {};
wnWidgetsInstallFlashVars["WNTicker"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNTicker"].playerWidth = wnSiteConfigVideo.tickerWidget.width;
wnWidgetsInstallFlashVars["WNTicker"].playerHeight = wnSiteConfigVideo.tickerWidget.height;



wnWidgetsInstallFlashVars["WNInfoPane"] = {};
wnWidgetsInstallFlashVars["WNInfoPane"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNInfoPane"].playerWidth = wnSiteConfigVideo.infoWidget.width;
wnWidgetsInstallFlashVars["WNInfoPane"].playerHeight = wnSiteConfigVideo.infoWidget.height;
wnWidgetsInstallFlashVars["WNInfoPane"].failSafeSummaryImageText = wnSiteConfigVideo.defaultSummaryText;
wnWidgetsFlashVars["WNInfoPane"] = {};
wnWidgetsFlashVars["WNInfoPane"].playerWidth = wnSiteConfigVideo.infoWidget.width;
wnWidgetsFlashVars["WNInfoPane"].playerHeight = wnSiteConfigVideo.infoWidget.height;
wnWidgetsFlashVars["WNInfoPane"].summaryFontSize = "8";
wnWidgetsFlashVars["WNInfoPane"].headlineFontSize = "10";
wnWidgetsFlashVars["WNInfoPane"].durationFontSize = "8";
wnWidgetsFlashVars["WNInfoPane"].hasDuration = "true";
wnWidgetsFlashVars["WNInfoPane"].hasSummary = "true";
wnWidgetsFlashVars["WNInfoPane"].hasImage = "true";
wnWidgetsFlashVars["WNInfoPane"].hasHeadline = "true";
wnWidgetsFlashVars["WNInfoPane"].hasDateStamp = "true";
wnWidgetsFlashVars["WNInfoPane"].hasPhotoCredit = "true";
wnWidgetsFlashVars["WNInfoPane"].imageWidth = "83";
wnWidgetsFlashVars["WNInfoPane"].imageHeight = "63";
wnWidgetsFlashVars["WNInfoPane"].sidePadding = "3";
wnWidgetsFlashVars["WNInfoPane"].topPadding = "3";
wnWidgetsFlashVars["WNInfoPane"].imagePosition = "left";
wnWidgetsFlashVars["WNInfoPane"].positionStyle = "auto";
wnWidgetsFlashVars["WNInfoPane"].topPaddingPct = "8";
wnWidgetsFlashVars["WNInfoPane"].sidePaddingPct = "1";
wnWidgetsFlashVars["WNInfoPane"].defaultStyle = wnSiteConfigVideo.infoWidget.defaultStylePackage;
wnWidgetsFlashVars["WNInfoPane"].loadingMessage = wnSiteConfigVideo.canvasWidget.loadingMsg;
wnWidgetsAttributes["WNInfoPane"] = {};
wnWidgetsAttributes["WNInfoPane"].uri = wnSiteConfigVideo.flashPath + "/WNInfoPane.swf";
wnWidgetsAttributes["WNInfoPane"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNInfoPane"].wmode = wnSiteConfigVideo.wmode;


wnWidgetsFlashVars["WNGraphicsHeader"] = {};
wnWidgetsFlashVars["WNGraphicsHeader"].playerWidth = "";
wnWidgetsFlashVars["WNGraphicsHeader"].playerHeight = "";
wnWidgetsFlashVars["WNGraphicsHeader"].graphicsHeaderImageUrl = "/contentmgmt/images/videoHEADER_graphic.jpg";
wnWidgetsFlashVars["WNGraphicsHeader"].graphicsHeaderClickUrl = "";
wnWidgetsInstallFlashVars["WNGraphicsHeader"] = {};
wnWidgetsInstallFlashVars["WNGraphicsHeader"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNGraphicsHeader"].playerWidth = "";
wnWidgetsInstallFlashVars["WNGraphicsHeader"].playerHeight = "";


wnWidgetsFlashVars["WNGraphicsFooter"] = {};
wnWidgetsFlashVars["WNGraphicsFooter"].playerWidth = "";
wnWidgetsFlashVars["WNGraphicsFooter"].playerHeight = "";
wnWidgetsFlashVars["WNGraphicsFooter"].graphicsFooterImageUrl = "/contentmgmt/images/videoFOOTER_graphic.jpg";
wnWidgetsFlashVars["WNGraphicsFooter"].graphicsFooterClickUrl = "";
wnWidgetsInstallFlashVars["WNGraphicsFooter"] = {};
wnWidgetsInstallFlashVars["WNGraphicsFooter"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNGraphicsFooter"].playerWidth = "";
wnWidgetsInstallFlashVars["WNGraphicsFooter"].playerHeight = "";

// IMAGE CANVAS
wnWidgetsAttributes["WNImageCanvas"] = {};
wnWidgetsAttributes["WNImageCanvas"].uri = wnSiteConfigVideo.flashPath + "/WNImageCanvas.swf";
wnWidgetsAttributes["WNImageCanvas"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNImageCanvas"].wmode = wnSiteConfigVideo.wmode;
wnWidgetsFlashVars["WNImageCanvas"] = {};
wnWidgetsFlashVars["WNImageCanvas"].playerWidth = wnSiteConfigVideo.imageCanvasWidget.width;
wnWidgetsFlashVars["WNImageCanvas"].playerHeight = wnSiteConfigVideo.imageCanvasWidget.height;
wnWidgetsFlashVars["WNImageCanvas"].defaultStyle = wnSiteConfigVideo.imageCanvasWidget.defaultStylePackage;
wnWidgetsFlashVars["WNImageCanvas"].affiliate = wnSiteConfigGeneral.affiliateName;
wnWidgetsFlashVars["WNImageCanvas"].enableSummaryPane = "false";
wnWidgetsFlashVars["WNImageCanvas"].hasSummary = "true";
wnWidgetsFlashVars["WNImageCanvas"].hasDateStamp = "true";
wnWidgetsFlashVars["WNImageCanvas"].hasPhotoCredit = "true";
wnWidgetsFlashVars["WNImageCanvas"].landingPage = wnSiteConfigVideo.slideShowLandingPageUrl;
wnWidgetsFlashVars["WNImageCanvas"].hostDomain = wnSiteConfigGeneral.baseUrl;
wnWidgetsFlashVars["WNImageCanvas"].isInCanvasAdModeEnabled = "false";
wnWidgetsFlashVars["WNImageCanvas"].inCanvasAdFrequency = wnSiteConfigVideo.slideShowInCanvasAdFreq;
wnWidgetsFlashVars["WNImageCanvas"].companionAdFrequency = wnSiteConfigVideo.slideShowCompanionAdFreq;
wnWidgetsInstallFlashVars["WNImageCanvas"] = {};
wnWidgetsInstallFlashVars["WNImageCanvas"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNImageCanvas"].playerWidth = wnSiteConfigVideo.imageCanvasWidget.width;
wnWidgetsInstallFlashVars["WNImageCanvas"].playerHeight = wnSiteConfigVideo.imageCanvasWidget.height;

// IMAGE GALLERY

wnWidgetsAttributes["WNImageGallery"] = {};
wnWidgetsAttributes["WNImageGallery"].uri = wnSiteConfigVideo.flashPath + "/WNImageGallery.swf";
wnWidgetsAttributes["WNImageGallery"].installUri = wnSiteConfigVideo.flashPath + "/WNWidgetInstaller.swf";
wnWidgetsAttributes["WNImageGallery"].wmode = wnSiteConfigVideo.wmode;
wnWidgetsFlashVars["WNImageGallery"] = {};
wnWidgetsFlashVars["WNImageGallery"].playerWidth = wnSiteConfigVideo.imageGalleryWidget.width;
wnWidgetsFlashVars["WNImageGallery"].playerHeight = wnSiteConfigVideo.imageGalleryWidget.height;
wnWidgetsFlashVars["WNImageGallery"].defaultStyle = wnSiteConfigVideo.imageGalleryWidget.defaultStylePackage;
wnWidgetsFlashVars["WNImageGallery"].helpPage = wnSiteConfigVideo.helpPageUrl;
wnWidgetsFlashVars["WNImageGallery"].landingPage = wnSiteConfigVideo.slideShowLandingPageUrl;
wnWidgetsFlashVars["WNImageGallery"].isPrimaryGallery = "true";
wnWidgetsFlashVars["WNImageGallery"].hostDomain = wnSiteConfigGeneral.baseUrl;
wnWidgetsFlashVars["WNImageGallery"].hasShare = "true";
wnWidgetsFlashVars["WNImageGallery"].popupLandingPage = "false";
wnWidgetsFlashVars["WNImageGallery"].isSingleImageGallery = "false";
wnWidgetsInstallFlashVars["WNImageGallery"] = {};
wnWidgetsInstallFlashVars["WNImageGallery"] = wnInstallFlashVarsTemplate;
wnWidgetsInstallFlashVars["WNImageGallery"].playerWidth = wnSiteConfigVideo.imageGalleryWidget.width;
wnWidgetsInstallFlashVars["WNImageGallery"].playerHeight = wnSiteConfigVideo.imageGalleryWidget.height;
// ============================ END WNVIDEO.XSLT =====================================//



/*
- Private WorldNow jQuery reference is undefind on standalone sites
- pointing it to the default jQuery library.
*/
if (typeof $wn == 'undefined') $wn = jQuery;

/* File: WNVideoWidgets.js
Contains the following classes:
- WNVideoWidgets
- WNVideoWidget
- WNEventManager
- WNPlayClipObject
- WNFlashVersionObject
- WNPlayClipObject
*/

/* Class: WNVideoWidgets
- Defines variables needed to manage the widgets.
- Creates variables from value pairs passed in with the javascript include.
- Uses WNFlashVersionObject's CompareVersions method to determine what to serve.
*/
var WNVideoWidgets = function() {
    var os = wnUserAgentParser.getOS();
    var redirectURL = window.location;
    var doctitle = document.title.slice(0, 47) + " - Flash Player Installation";
    var contentaddons = wn.contentaddons || {};
    var betalist = contentaddons.betalist || "";
    this.installParams = "?MMredirectURL=" + redirectURL + "&MMdoctitle=" + doctitle + "&MMplayerType=";
    this.isIE = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
    this.isMac = (navigator.appVersion.toLowerCase().indexOf("mac") != -1) ? true : false;
    this.isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
    if (!wn_isMobile && window.opener != null) {
        try {
            if (typeof(window.opener.wng_pageInfo) != "undefined") wn_isMobile = window.opener.wng_pageInfo.isMobile;
        } catch (e) {
            wnLog("Error: Permission denied to access property 'wng_pageInfo'. window.opener properties aren't accessable from the child window.");
        }
    }
    this.useHtml5 = navigator.userAgent.match(/(iPhone|iPod|iPad)/) || wnVideoUtils.getQS("wnHtml5") == "true" || wn_isMobile;
    this.useAmpPlayer = wnSiteConfigVideo.enableAkamaiPlayer;

    // enable amp for android - CC issue
    try {
        if (os.name.toLowerCase() == 'android' && (betalist.indexOf('enableampandroid') >= 0) ) {
            this.useAmpPlayer = true;
        }
    } catch(e) {}
    // Debug overrides
    var wnplayertype = wnVideoUtils.getQS("wnplayertype"); // amp, wn
    if (wnplayertype != "") {
        if (wnplayertype == "amp") this.useAmpPlayer = true;
        if (wnplayertype == "wn") this.useAmpPlayer = false;
    }

    this.landingPage = wnWidgetsFlashVars.landingPage;
    this.objClip = {};
    this.canvasObjectFlashVars = "";
    var q = this.landingPage.lastIndexOf("?");
    var amp = this.landingPage.lastIndexOf("&");
    var len = this.landingPage.length;

    if (wnVideoUtils.validateString(this.landingPage)) {
        if (q > 0) {
            if (q == len || amp == len) {
                this.landingPage += "";
            } else {
                this.landingPage += "&";
            }
        } else {
            this.landingPage += "?";
        }
    }
    this.canvasObject = {};
    if (wn_isMobile) {
        $wn(window).resize(function() {
            for (var groupId in pageWidgets) {
                var gallery = null;
                var canvas = null;
                for (var wdgt in pageWidgets[groupId]) {
                    if (pageWidgets[groupId][wdgt].widgetClassType == "WNGallery") gallery = pageWidgets[groupId][wdgt];
                    if (pageWidgets[groupId][wdgt].widgetClassType == "WNVideoCanvas" || pageWidgets[groupId][wdgt].widgetClassType == "WNVideoCanvas2") canvas = pageWidgets[groupId][wdgt];
                }
                if (canvas != null && !wnVideoUtils.stringToBoolean(canvas.forceFlash)) {
                    // Html5 player
                    if (typeof canvas.htmlVersion !== "undefined") {
                        /* Don't resize if gallery is on the right side of the canvas (not under it) */
                        if (gallery != null) {
                            var galleryDiv = $wn("#" + gallery.htmlVersion.parentDivId);
                            var canvasDiv = $wn("#" + canvas.htmlVersion.parentDivId);
                            if (canvasDiv.parent() == galleryDiv.parent()) {
                                if (galleryDiv.position().top > (canvasDiv.height() - 10)) {
                                    canvas.htmlVersion.resizeOnScreenFlip();
                                }
                            } else {
                                canvas.htmlVersion.resizeOnScreenFlip();
                            }
                        } else {
                            canvas.htmlVersion.resizeOnScreenFlip();
                        }
                    }
                    // AMP player
                    if (typeof canvas.ampVersion !== "undefined") {
                        /* Don't resize if gallery is on the right side of the canvas (not under it) */
                        if (gallery != null) {
                            var galleryDiv = $wn("#" + gallery.htmlVersion.parentDivId);
                            var canvasDiv = $wn("#" + canvas.ampVersion.parentDivId);
                            if (canvasDiv.parent() == galleryDiv.parent()) {
                                if (galleryDiv.position().top > (canvasDiv.height() - 10)) {
                                    canvas.ampVersion.resizeOnScreenFlip();
                                }
                            } else {
                                canvas.ampVersion.resizeOnScreenFlip();
                            }
                        } else {
                            canvas.ampVersion.resizeOnScreenFlip();
                        }
                    }
                }
            }
        });
    }
};

/* for use by embed code window */
WNVideoWidgets.prototype.GetEmbedCodeClipId = function() {
    if (wnVideoWidgets.objClip.objClip) return wnVideoWidgets.objClip.objClip.id;
};

WNVideoWidgets.prototype.GetEmbedCodeFlvUri = function() {
    if (wnVideoWidgets.objClip.objClip) return wnVideoWidgets.objClip.objClip.flvUri;
};

WNVideoWidgets.prototype.GetEmbedCodeThirdPartyMrssUrl = function() {
    if (wnVideoWidgets.objClip.objClip) return wnVideoWidgets.objClip.objClip.thirdpartymrssurl;
};

WNVideoWidgets.prototype.GetEmbedCodePartnerClipId = function() {
    if (wnVideoWidgets.objClip.objClip) return wnVideoWidgets.objClip.objClip.partnerclipid;
};

WNVideoWidgets.prototype.GetEmbedAdTag = function() {
    if (wnVideoWidgets.objClip.objClip) return wnVideoWidgets.objClip.objClip.adTag;
};

WNVideoWidgets.prototype.GetEmbedCodeLandingPage = function() {
    if (wnVideoWidgets.objClip.objClip) return wnVideoWidgets.objClip.objClip.landingPage;
};

WNVideoWidgets.prototype.GetEmbedCodeIsLandingPageOverride = function() {
    if (wnVideoWidgets.objClip.objClip) return wnVideoWidgets.objClip.objClip.islandingPageoverride;
};

WNVideoWidgets.prototype.StripWhitespace = function(strText) {
    var strippedText = strText.replace(/\s{3,}/g, "  ");
    return strippedText;
};

WNVideoWidgets.prototype.Strip = function(filter, str) {
    var i, curChar;
    var retStr = '';
    for (i = 0; i < str.length; i++) {
        curChar = str.charAt(i);
        if (filter.indexOf(curChar) < 0)
            retStr += curChar;
    }
    return retStr;
};

WNVideoWidgets.prototype.WNEncodeURIComponent = function(text, encodeLayer) {
    if (encodeLayer != parseInt(encodeLayer)) {
        encodeLayer = 1;
    } else if (encodeLayer == 0) {
        return text;
    }

    if (encodeLayer == 1) {
        return encodeURIComponent(text);
    } else {
        return encodeURIComponent(this.WNEncodeURIComponent(text, encodeLayer - 1));
    }
};

WNVideoWidgets.prototype.WNDecodeURIComponent = function(text, decodeLayer) {
    if (decodeLayer != parseInt(decodeLayer)) {
        decodeLayer = 1;
    } else if (decodeLayer == 0) {
        return text;
    }

    if (decodeLayer == 1) {
        return decodeURIComponent(text);
    } else {
        return decodeURIComponent(this.WNDecodeURIComponent(text, decodeLayer - 1));
    }
};

WNVideoWidgets.prototype.RegExpExtractDigit = function(source, pattern) {
    pattern = unescape(pattern);
    var value = "";
    var regexp = new RegExp(pattern);
    if (regexp.test(source)) {
        var list = regexp.exec(source);
        var match = list[0];
        var reg = /\d+/;
        var data = reg.exec(match);
        value = data[0];
    }
    return value;
};

WNVideoWidgets.prototype.ValidateEmail = function(strText, isRecipient) {
    var EmailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var arrEmailValues = [];
    if (isRecipient) {
        strText = strText.replace(/,/g, ";");
        arrEmailValues = strText.split(";");
    }

    if (arrEmailValues.length >= 1) {
        //if multiple addresses exist, validate each one
        for (var i = 0; i < arrEmailValues.length; i++) {
            if (!EmailPattern.test(this.Strip(' ', arrEmailValues[i])))
                return false;
        }
    } else {
        if (!EmailPattern.test(strText))
            return false;
    }
    return true;
};

WNVideoWidgets.prototype.handleEvent = function(e) {
    if (e.valid == true) {
        switch (e.event.type.toLowerCase()) {
            case 'newclip':
            case 'play':
            case 'duration': // amp
            case 'resume':
            case 'ended':
                //case 'mediaended':
            case 'fullscreen':
            case 'volumechange':
            case 'mute':
            case 'unmute':
            case 'email':
            case 'embed':
            case 'start':
            case 'stop':
            case 'next':
            case 'previous':
            case 'gallerynext':
            case 'galleryprevious':
                wnVideoWidgets.reportAnalytics(e);
            case 'pause':
                if (e.clip.hostPlayer == 'html') {
                    wnVideoWidgets.reportAnalytics(e);
                }
                break;
            case 'user_pause':
                e.event.type = 'pause';
                wnVideoWidgets.reportAnalytics(e);
                break;
            case 'clipended':
                e.event.type = 'mediaended';
                wnVideoWidgets.reportAnalytics(e);
                break;
            default:
                //

        }
    }

};

WNVideoWidgets.prototype.reportAnalytics = function(e) {

    var event = e.event;
    var clip = e.clip;

    var isAd = false;
    if (clip.isPreRoll || clip.isPostRoll) {
        isAd = true;
    }

    // FLash duraion is in milliseconds
    if (!clip.currentTime) {
        clip['currentTime'] = 0;
        clip['currentTimePct'] = 0;
    }

    var durationDivider = 1;
    if (clip.hostPlayer == 'flash') {
        durationDivider = 1000;
    }
    clip['currentTimePct'] = parseFloat((clip.currentTime / clip.duration) * durationDivider);
    clip['currentTime'] = parseFloat(clip.currentTime);
    clip.duration = parseFloat(clip.duration / durationDivider);

    if (typeof(e.system) == 'undefined' || e.system == 'video') {
        var videoreport = Namespace_VideoReporting_Worldnow.logVideoEventParameters(
            event.type, //'Duration',
            clip.headline,
            clip.currentTime, //'0',
            clip.id,
            clip.adTag,
            'FlashPlayer.wnfl',
            clip.affiliateName,
            clip.ownerAffiliateName,
            isAd,
            '',
            '',
            '',
            '',
            clip.url,
            clip.duration,
            clip.currentTimePct,
            '',
            '',
            '',
            'STANDARD - HTML',
            wnSiteConfigVideo.enableExpressReports,
            '',
            '',
            '',
            clip.taxonomy1,
            clip.taxonomy2,
            clip.taxonomy3
        );
    } else {
        //logSlideShowEventParameters : function(p_strEventType, p_strTitle, p_strWidgetHeadline, p_strClipId, p_strImgId, p_strClipAdTag, p_strBaseUrl, p_strAffiliateName, p_strPlayerType)
        var slidreport = Namespace_VideoReporting_Worldnow.logSlideShowEventParameters(
            event.type,
            encodeURIComponent(escape(clip.headline)),
            encodeURIComponent(escape(clip.slideshowHeadline)),
            clip.widgetId,
            clip.url,
            clip.adTag,
            '',
            clip.ownerAffiliateName,
            '',
            clip.index
        );
    }
};

/**
* GetTimeString
*<ul>
*<li>Takes in miliseconds (number), convert to seconds, and converts to digital time display</li>
*<li>Returns string value</li>
</ul>
*/
WNVideoWidgets.prototype.GetTimeString = function(sec) {
    //sec = Math.floor(sec / 1000);
    var hours = Math.floor(sec / 3600);
    var minutes = Math.floor((sec % 3600) / 60);
    var seconds = Math.floor(sec % 60);
    var hoursStr = hours.toString();
    var minutesStr = minutes.toString();
    var secondsStr = seconds.toString();
    if (hours < 10) {
        hoursStr = "0" + hoursStr;
    }
    if (minutes < 10) {
        minutesStr = "0" + minutesStr;
    }
    if (seconds < 10) {
        secondsStr = "0" + secondsStr;
    }

    if (hoursStr == "00") {
        return minutesStr + ":" + secondsStr;
    } else {
        return hoursStr + ":" + minutesStr + ":" + secondsStr;
    }
};

WNVideoWidgets.prototype.ConfirmReupload = function(fileName, message) {
    return confirm("\"" + fileName + "\" " + message);
};

WNVideoWidgets.prototype.GetElementPosX = function(obj) {
    var curleft = 0;
    if (obj.offsetParent)
        while (1) {
            curleft += obj.offsetLeft;
            if (!obj.offsetParent)
                break;
            obj = obj.offsetParent;
        } else if (obj.x)
            curleft += obj.x;
    return curleft;
};

WNVideoWidgets.prototype.GetElementPosY = function(obj) {
    var curtop = 0;
    if (obj.offsetParent)
        while (1) {
            curtop += obj.offsetTop;
            if (!obj.offsetParent)
                break;
            obj = obj.offsetParent;
        } else if (obj.y)
            curtop += obj.y;
    return curtop;
};

// Called by the canvas. Loads vast companions into platform ad units.
WNVideoWidgets.prototype.SetVASTCompanions = function(vastProps) {
    var companionAds = vastProps.companions;
    var adProps = vastProps.adProps;
    /*
     * wnsz_35 = 180x150B Rectangle - B
     * wnsz_1  = BannerFull Banner
     * wnsz_26 = Bu120x60A Button - A
     * wnsz_27 = Bu120x60B Button - B
     * wnsz_28 = Bu120x60C Button - C
     * wnsz_24 = We120x15 Weather
     * wnsz_22 = SpH490x25 Sponsorship Header - A
     * wnsz_14 = 120x600 Skyscraper
     * wnsz_20 = 180x150 Rectangle
     * wnsz_23 = VB120x240A Vertical Banner - A
     * wnsz_18 = VB120x240B Vertical Banner - B
     * wnsz_40 = PB468x60 Printable Page Banner
     * wnsz_34 = Video120x60 Video 120x60
     * wnsz_41 = 728x90A Leaderboard A
     * wnsz_42 = 160x600A Wide Skyscraper
     * wnsz_43 = 300x250A Medium Rectangle
     * wnsz_46 = 728x90B Leaderboard B
     * wnsz_44 = 300x75 National Promo
     * wnsz_49 = 500x300A 500x300 Footer A
     * wnsz_50 = 300x175A 300x175 Rectangle A
     * wnsz_53 = HalfBanner
     */

    // Parse and map vast sizes to wn sizes
    var adSizes = {
        '490x25': ['WNAd22'],
        '120x240': ['WNAd23'],
        '120x60': ['WNAd28'],
        '120x600': ['WNAd14'],
        '180x150': ['WNAd20'],
        '468x60': ['WNAd40', 'WNAd104'],
        '728x90': ['WNAd41', 'WNAd46'],
        '320x50': ['WNAd105', 'WNAd106'],
        '160x600': ['WNAd42'],
        '300x250': ['WNAd43', 'WNAd103'],
        '300x150': ['WNAd101'],
        '300x75': ['WNAd44'],
        '500x300': ['WNAd49']
    };

    var adDimension, vastHTML, adSizesLoaded = [];
    for (var x = 0; x < companionAds.length; x++) {
        vastHTML = wnGetVastAdHtml(companionAds[x], this);
        adDimension = companionAds[x].width + 'x' + companionAds[x].height;

        if (adSizes[adDimension]) {
            // In case multiple ad sizes are mapped to the same dimension
            for (var i = 0; i < adSizes[adDimension].length; i++) {
                /* When companion ad creative contains JavaScript, it won't be executed if just rendered into a div.
                Therefore, we have to use loadVastHtmlAd to create iframe and render ad's code into it.
                Known isuue: Rendering into iframe doesn't work in IE because of its security restrictions. */
                if (wnVideoUtils.validateString(companionAds[x].htmlResource) &&
                    companionAds[x].htmlResource.toString().toLowerCase().indexOf('<iframe') < 0 &&
                    companionAds[x].htmlResource.toString().toLowerCase().indexOf('<script') > -1) {
                    Worldnow.AdMan.loadVastHtmlAd(adSizes[adDimension][i], vastHTML, companionAds[x]);
                    Worldnow.AdMan.loadVastHtmlAd(adSizes[adDimension][i], vastHTML, companionAds[x]);
                } else {
                    Worldnow.AdMan.loadVastAd(adSizes[adDimension][i], vastHTML, companionAds[x]);
                }
                adSizesLoaded.push(adSizes[adDimension][i]);
            }
        }
    }

    // Refresh configured companions if a vast companion for that position is not found.
    var companionProps = {
        "wncc": adProps.wncc,
        "apptype": adProps.apptype,
        "owner": adProps.owner
    };

    var adArr = wnCompanionAds.split(","); // These are set up in the site config
    for (var i = 0; i < adArr.length; i++) {
        adArr[i] = wnVideoUtils.trim(adArr[i]);
        if (adSizesLoaded.join().length == 0 || adSizesLoaded.join().indexOf(adArr[i]) < 0) // Yeah yeah, I know. It's clean in this case.
        {
            Worldnow.AdMan.reloadAd(adArr[i], companionProps);
        }
    }
};

WNVideoWidgets.prototype.DisplaySilverlightScreen = function(clipObj) {
    // this function is always called from the flash canvas
    // so make a note of its widget type = WNVideoCanvas
    var type = "WNVideoCanvas";

    // find the id that link all widgets on the page together
    var arrTemp = clipObj.domId.split("div");
    var collectionId = arrTemp[0].substr(type.length);
    if (collectionId == undefined || collectionId == "") return false; // can't go on

    var flashDiv = "div" + arrTemp[1]; // this is the div that was passed to the widget constructor parameter

    // what is the div tag containing the flash object
    // auto generated by the widget constructor
    var flashObjDiv;
    for (var i in this.canvasObject) {
        if (i == collectionId) {
            flashObjDiv = this.canvasObject[i];
        }
    }
    if (flashObjDiv == undefined || flashObjDiv == "") return false; // can't go on

    // does the div tag containing the silverlight canvas exist
    var canvasSliverlightDivName = flashObjDiv + "WNSilverlight";
    var canvasSliverlightDiv = document.getElementById(canvasSliverlightDivName);

    if (canvasSliverlightDiv == undefined) {
        this.RenderSilverlightScreen(clipObj, flashDiv, flashObjDiv, canvasSliverlightDivName);
    } else {
        WNSilverlightTrigger(clipObj, flashObjDiv, canvasSliverlightDivName);
    }
};

WNVideoWidgets.prototype.RenderSilverlightScreen = function(clipObj, flashDiv, flashObjDiv, canvasSliverlightDivName) {
    // this is the div that was passed to the widget constructor parameter
    // the flash object is rendered under it and so we will also render the silverlight object under it
    var videoCanvasDiv = document.getElementById(flashDiv);

    // add silverlight div tag
    var newElem = document.createElement("div");
    newElem.setAttribute("id", canvasSliverlightDivName);
    videoCanvasDiv.appendChild(newElem);

    // add installer div tag - to address DE1493
    var silverlightInstallerID = canvasSliverlightDivName + "InstallerPrompt";
    var silverlightInstaller = document.createElement("div");
    silverlightInstaller.setAttribute("id", silverlightInstallerID);
    videoCanvasDiv.appendChild(silverlightInstaller);

    if (Silverlight.isInstalled('1.0')) {
        clipObj[WNConstFlashLayerDivNameKey.toString()] = flashObjDiv;
        clipObj[WNConstSilverlightLayerDivNameKey.toString()] = canvasSliverlightDivName;

        // render the silverlight canvas
        WNRenderSilverlight(clipObj);
    } else {
        var flashObj = document.getElementById(flashObjDiv);
        // prompt use to install silverlight
        silverlightInstaller.innerHTML = '<a href="http://go.microsoft.com/fwlink/?linkid=124807"><img src="' + wnSilverlightBaseUrl + '/getSilverlight.jpg" border="0" alt="Get Silverlight" title="Get Silverlight" width="' + flashObj.width + '" height="' + flashObj.height + '"/></a>';
        // hide the existing flash canvas
        flashObj.width = 1;
        flashObj.height = 1;
        silverlightInstaller.style.display = "block";
        silverlightInstaller.style.top = 0;
    }
};

/* Method: FireReportingBeacon
- Fired onbeforeunload event
- Loops through the canvasObj (which contains the id for the flash object of a given canvas) and fires the FireBeacon (ExternalInterface) method on each canvas.
*/
WNVideoWidgets.prototype.FireReportingBeacon = function() {
    try {
        var canvasObj = wnVideoWidgets.canvasObject;
        for (var i in canvasObj) {
            document.getElementById(canvasObj[i]).FireBeacon();
        }
    } catch (e) {}
};

WNVideoWidgets.prototype.getSkin = function(widgetGroupId, widgetDomId) {
    return pageWidgets[widgetGroupId][widgetDomId].skin;
};

WNVideoWidgets.prototype.closeAddThisPane = function(divId) {
    var _link = document.getElementById(divId + "_addthis_button");
    if (_link != null) {
        _link.style.visibility = "hidden";
        _link.style.zIndex = "-1";
    }
}
WNVideoWidgets.prototype.openAddThisPane = function(url, title, divId, summary, isHtml) {
    var parentDiv = document.getElementById(divId);
    var leftMargin = isHtml ? 20 : parentDiv.offsetWidth / 2 - 120;
    var topMargin = isHtml ? 20 : (parentDiv.offsetHeight - 32) / 2 - 120;
    if (leftMargin < 0) leftMargin = 15;
    if (topMargin < 0) topMargin = 0;

    var is_iPad = (/ipad/i.test(navigator.userAgent.toLowerCase()) || wnVideoWidgets.useAmpPlayer); // render desktop version
    var _link = document.getElementById(divId + "_addthis_button");
    if (_link != null) parentDiv.removeChild(_link);
    if (!is_iPad && wn_isMobile) {
        _link = document.createElement("div");
    } else {
        _link = document.createElement("a");
    }
    _link.setAttribute("id", divId + "_addthis_button");
    //_link.setAttribute("style", "visibility:hidden; position:absolute; margin-left:" + leftMargin + "px; margin-top:" + topMargin + "px;");

    _link.style.position = "absolute";
    _link.style.marginLeft = leftMargin + "px";
    _link.style.marginTop = topMargin + "px";
    parentDiv.insertBefore(_link, parentDiv.childNodes[0]);
    if (!is_iPad && wn_isMobile) {
        var addthis_html = '<div id="' + divId + '_addthis_toolbox" class="addthis_toolbox addthis_default_style addthis_16x16_style"' +
            'addthis:url="' + unescape(url) + '" addthis:title="' + unescape(title) + '" ' +
            'addthis:description="' + unescape(summary) + '" style="width: 110px;">' +
            '<a class="addthis_button_facebook"></a>' +
            '<a class="addthis_button_twitter"></a>' +
            '<a class="addthis_button_gmail"></a>' +
            '<a class="addthis_button_reddit"></a>' +
            '<a class="addthis_button_more"></a>' +
            '</div>';
        $wn(_link).html(addthis_html);
        /*var addthis_script = 'http://s7.addthis.com/js/300/addthis_widget.js?pub=5435668#domready=1';
        if (window.addthis) window.addthis = null;
        $wn.getScript(addthis_script);*/
        addthis.toolbox("#" + divId + "_addthis_toolbox");
    } else {
        var addthis_share = {
            url: unescape(url),
            title: unescape(title),
            description: unescape(summary)
        };
        addthis.button("#" + divId + "_addthis_button", {}, addthis_share);
    }
    //document.getElementById("addthis_button").onclick();
    _link.style.visibility = isHtml ? "visible" : "hidden";
    _link.style.zIndex = "200";
    if (!isHtml) document.getElementById(divId + "_addthis_button").onmouseover();
};

WNVideoWidgets.prototype.getClipPageUrl = function(clipObj) {
    var clipPageUrl = "";
    var WNisProducerRegExp = new RegExp("://manage[A-Za-z0-9.]*\.worldnow.com");
    if (WNisProducerRegExp.test(document.location.href) == false) {
        clipPageUrl = wnSiteConfigGeneral.baseUrl;
    } else {
        clipPageUrl = document.location.href.substr(0, document.location.href.indexOf("worldnow.com") + 12);
    }
    if (clipPageUrl.indexOf("http:") < 0 && clipPageUrl.indexOf("https:") < 0) {
        clipPageUrl = "http://" + clipPageUrl;
    }
    if (wnVideoUtils.validateString(clipObj.id)) {
        clipPageUrl += "/clip/" + clipObj.id + "/" + clipObj.pageUrl;
        /*clipPageUrl += "/" + clipObj.id + "/";
        var clipHeadline = wnVideoUtils.trim(clipObj.headline).toLowerCase();
        clipHeadline = clipHeadline.replace(/\s+/g, "_");
        clipHeadline = clipHeadline.replace(/[^\w\d]/gi, '');
        clipHeadline = clipHeadline.replace(/[_]+/g, '-');
        clipPageUrl += clipHeadline;*/
    }
    return clipPageUrl;
};

WNVideoWidgets.prototype.getLinkToVideo = function(clip, flashVars) {
    var _linkToVideo = "";
    var enableSingleClipPage = wnVideoUtils.stringToBoolean(flashVars.enableSingleClipPage);
    var landingPage = decodeURIComponent(flashVars.landingPage);
    if (!wnVideoUtils.validateString(landingPage) && !enableSingleClipPage) {
        _linkToVideo = "http://" + wn.baseurl + "/video?autoStart=true&topVideoCatNo=default";
        if (wnVideoUtils.validateString(clip.id)) {
            _linkToVideo += "&clipId=" + clip.id;
        }
    } else {
        if (enableSingleClipPage) {
            _linkToVideo = wnVideoWidgets.getClipPageUrl(clip);
            if (clip.isLiveStream) _linkToVideo += "?";
        } else {
            if (landingPage.indexOf("?") > 0) {
                _linkToVideo = landingPage + "&autoStart=true&topVideoCatNo=default";
            } else {
                _linkToVideo = landingPage + "?autoStart=true&topVideoCatNo=default";
            }
            if (wnVideoUtils.validateString(clip.id)) {
                _linkToVideo += "&clipId=" + clip.id;
            }
        }

        if (clip.isLiveStream) {
            _linkToVideo = document.location.href;

            // var flvUri = clip.flvUri;
            // if (wnVideoUtils.validateString(flvUri) && clip.isLiveStream && wnVideoUtils.validateString(clip.streamUrlNoCC)) {
            //     flvUri = clip.streamUrlNoCC;
            // }
            // if (wnVideoUtils.validateString(clip.flvUri)) {
            //     _linkToVideo += "&flvUri=" + flvUri;
            // } else if (wnVideoUtils.validateString(flashVars.liveStreamUrl)) {
            //     _linkToVideo += "&flvUri=" + flashVars.liveStreamUrl;
            // }
            // if (wnVideoUtils.validateString(clip.partnerclipid)) _linkToVideo += "&partnerclipid=" + clip.partnerclipid;
            // if (wnVideoUtils.validateString(flashVars.wnms1)) _linkToVideo += "&wnms1=" + flashVars.wnms1;
            // if (wnVideoUtils.validateString(flashVars.wnms2)) _linkToVideo += "&wnms2=" + flashVars.wnms2;
            // if (wnVideoUtils.validateString(flashVars.wnms3)) _linkToVideo += "&wnms3=" + flashVars.wnms3;
            // _linkToVideo += "&streamType=" + flashVars.StreamType +
            //        "&adTag=" + clip.adTag +
            //        "&enableAds=" + clip.hasAdPreRoll +
            //        "&headline=" + wnVideoWidgets.WNEncodeURIComponent(clip.headline, 2);
        }
    }
    return _linkToVideo;
};

WNVideoWidgets.prototype.getPlayerEmbedCode = function(clip, fV) {
    // clipId
    var id = "";
    clipId = (clip) ? clip.id * 1 : 0;
    if (clipId < 1) clipId = wnVideoWidgets.GetEmbedCodeClipId() * 1;
    if (isNaN(clipId) || clipId < 1) {
        id = "";
    } else {
        id = clipId.toString();
    }
    // flvUri
    var flvUri = fV.liveStreamUrl;
    if (!wnVideoUtils.validateString(flvUri)) flvUri = wnVideoWidgets.GetEmbedCodeFlvUri();
    if (!wnVideoUtils.validateString(flvUri)) flvUri = "";
    // thirdpartymrssurl
    var thirdpartymrssurl = clip.thirdpartymrssurl;
    if (!wnVideoUtils.validateString(thirdpartymrssurl)) thirdpartymrssurl = wnVideoWidgets.GetEmbedCodeThirdPartyMrssUrl();
    if (!wnVideoUtils.validateString(thirdpartymrssurl)) thirdpartymrssurl = "";
    // partnerclipid
    var partnerclipid = clip.partnerclipid;
    if (!wnVideoUtils.validateString(partnerclipid)) partnerclipid = wnVideoWidgets.GetEmbedCodePartnerClipId();
    if (!wnVideoUtils.validateString(partnerclipid)) partnerclipid = "";
    // adTag
    var adTag = clip.adTag;
    if (!wnVideoUtils.validateString(adTag)) adTag = wnVideoWidgets.GetEmbedAdTag();
    if (!wnVideoUtils.validateString(adTag)) adTag = "";
    adTag = wnVideoWidgets.WNDecodeURIComponent(adTag, 2);
    // landingPage
    var landingPage = clip.landingPage;
    if (!wnVideoUtils.validateString(landingPage)) landingPage = wnVideoWidgets.GetEmbedCodeLandingPage();
    if (!wnVideoUtils.validateString(landingPage)) landingPage = "";
    // islandingPageoverride
    var islandingPageoverride = clip.islandingPageoverride;
    if (!wnVideoUtils.validateString(islandingPageoverride)) islandingPageoverride = wnVideoWidgets.GetEmbedCodeIsLandingPageOverride();
    if (!wnVideoUtils.validateString(islandingPageoverride)) islandingPageoverride = "";
    //var _islandingPageoverride:Boolean = (islandingPageoverride == "undefined") ? false : new Boolean(islandingPageoverride);

    if (id != "") {
        flvUri = "";
        thirdpartymrssurl = "";
    }
    if (thirdpartymrssurl != "") {
        id = "";
        flvUri = "";
    }
    if (flvUri != "") {
        id = "";
        thirdpartymrssurl = "";
    }

    // specific for fisher requirement
    if (thirdpartymrssurl == "" && (fV.partnerclipid == undefined || fV.partnerclipid == "")) {
        partnerclipid = "";
    }

    var embedJSPlayerUrl = fV.staticImgPath + "/interface/js/WNVideo.js" +
        "?rnd=" + Math.floor(Math.random() * 1000000000) +
        ";hostDomain=" + fV.hostDomain +
        ";playerWidth=" + fV.playerWidth +
        ";playerHeight=" + fV.playerHeight +
        ";isShowIcon=true" +
        ";clipId=" + id +
        ";flvUri=" + flvUri;
    if (wnVideoUtils.validateString(fV.wnms1)) embedJSPlayerUrl += ";wnms1=" + fV.wnms1;
    if (wnVideoUtils.validateString(fV.wnms2)) embedJSPlayerUrl += ";wnms2=" + fV.wnms2;
    if (wnVideoUtils.validateString(fV.wnms3)) embedJSPlayerUrl += ";wnms3=" + fV.wnms3;
    embedJSPlayerUrl += ";partnerclipid=" + partnerclipid +
        ";adTag=" + wnVideoWidgets.WNEncodeURIComponent(adTag) +
        ";advertisingZone=" + wnVideoWidgets.WNEncodeURIComponent(fV.advertisingZone, 2) +
        ";enableAds=" + clip.hasAdPreRoll +
        ";landingPage=" + wnVideoWidgets.WNEncodeURIComponent(landingPage, 2) +
        //";islandingPageoverride=" + _islandingPageoverride;
        ";islandingPageoverride=" + islandingPageoverride +
        ";playerType=" + fV.playerType + "_EMBEDDEDscript" +
        ";controlsType=" + fV.controlsType;
    if (wnVideoUtils.validateString(fV.gallerySourceType)) embedJSPlayerUrl += ";galleryType=" + fV.gallerySourceType;
    if (fV.galleryId > 0) embedJSPlayerUrl += ";galleryId=" + fV.galleryId;

    if (fV.videoType == 'livestream') {
        embedJSPlayerUrl += ";isLiveStream=true";
        embedJSPlayerUrl += ";streamType=" + fV.StreamType;
    }
    if (id == "" && clip != null)
        embedJSPlayerUrl += ";headline=" + wnVideoWidgets.WNEncodeURIComponent(clip.mainHeadline, 2);
    var embedJSPlayer = "&lt;script type='text/javascript' src='" + embedJSPlayerUrl + "'&gt;&lt;/script&gt;";
    embedJSPlayer += '&lt;a href="http://' + fV.hostDomain + '" title="' + fV.pageTitleTag + '"&gt;' + fV.pageTitleTag + '&lt;/a&gt;';

    return embedJSPlayer;
}

WNVideoWidgets.prototype.getAmpParams = function(domId) {
	return $wn('#' + domId).data('widget').canvas.getParams();
	//return $wn('#' + domId)[0].amp.getParams();
}


/* Class: WNVideoWidget
- Receives parameters and builds an object containing necessary data to build a widget.
- Contains method for setting variable values
- Contains method for writing out the widget
*/

/* Method: WNVideoWidget Constructor
- Takes the following params
- widget id (WNVideoCanvas, WNGallery, etc)
- string id for the div in which the widget will be written into
- key value, value used to distinguish between multiple instances of the same widget type
- Loops throught the wnWidgetsAttribute object to get default values for all of the widget variables
*/

WNVideoWidget = function(p_id, p_target, p_key) {
    this.widgetClassType = p_id;
    this.wnFeedObject = {};
    this.wnFeedObject.feeds = [];

    for (var i in wnWidgetsAttributes[p_id]) {
        this[i] = wnWidgetsAttributes[p_id][i];
    }
    if (typeof p_key == "undefined") p_key = "";
    this.idKey = wnVideoUtils.validateString(p_key) ? p_key : "DEFAULT";
    this.widgetType = p_id;
    this.id = p_id + this.idKey;

    if (p_target.toLowerCase().indexOf("gallery") > -1) eval("document." + this.id + p_target + "_copy = this");

    this.flashVars = {};
    //TB - May be better way to do this.
    for (var i in wnWidgetsFlashVars[p_id]) {
        this.flashVars[i] = wnWidgetsFlashVars[p_id][i];
    }
    this.flashVars.domId = this.id + p_target;
    this.flashVars.idKey = this.idKey;
    this.installFlashVars = wnWidgetsInstallFlashVars[p_id];
    this.uri;
    this.installUri;
    this.wmode;
    this.targetDiv = wnVideoUtils.validateString(p_target) ? p_target : "div" + this.id;
    this.SetStylePackage(this.flashVars.defaultStyle);
    if (this.widgetClassType.indexOf(videoCanvasId) != -1) {
        wnVideoWidgets.canvasObject[this.idKey] = this.flashVars.domId;
    }
};

/* Method: SetPreferredVideoFormat
- strVideoFormat (WMV,FLV,BOTH)
- Validate the Preferred Video Format and replace the global variable "wnPreferredVideoFormat" with new.
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.SetPreferredVideoFormat = function(strVideoFormat) {
    switch (wnVideoUtils.trim(strVideoFormat).toUpperCase()) {
        case "WMV":
        case "FLV":
        case "BOTH":
            wnPreferredVideoFormat = strVideoFormat;
            break;
    }
};

/* Method: SetLiveStream
- strVideoUrl: wmv stream url
- strHeadline: stream headline
- strAdTag: content classfication for pre-roll and companion ads
- strSummaryImageUrl: summary graphic url
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.SetLiveStream = function(strUrl, strHeadline, strAdTag, strSummaryImageUrl, strSecure, strUrlMobile) {
    this.flashVars["videoType"] = "livestream";
    this.flashVars["preferredFormat"] = "WMV";
    this.flashVars["liveStreamUrl"] = strUrl;
    this.flashVars["liveStreamUrlMobile"] = strUrlMobile;
    this.flashVars["liveStreamHeadline"] = strHeadline;
    this.flashVars["liveStreamAdTag"] = strAdTag;
    this.flashVars["liveStreamSummaryImgUrl"] = strSummaryImageUrl;
    this.flashVars["liveStreamSecure"] = strSecure;
};

/* Method: SetFlashLiveStream
- streamObj: stream parameters object
*/
WNVideoWidget.prototype.SetFlashLiveStream = function(streamObj) {
    this.flashVars["videoType"] = "livestream";
    this.flashVars["liveStreamUrl"] = streamObj.strUrl;
    this.flashVars["StreamType"] = "live"; // live or ondemand
    this.flashVars["liveStreamHeadline"] = streamObj.strHeadline;
    this.flashVars["liveStreamAdTag"] = streamObj.strAdTag;
    this.flashVars["liveStreamHasPreroll"] = streamObj.hasPreroll;
    this.flashVars["liveStreamSummaryImgUrl"] = streamObj.strSummaryImageUrl;
    //this.flashVars["liveStreamSecure"] = strSecure;

    // Multiple urls can be specified for maximum device support
    this.flashVars["liveStreams"] = streamObj["mobileStreams"];
    if (typeof streamObj["mobileStreams"] !== 'undefined' && streamObj["mobileStreams"] != null) {
        for (var i = 0; i < streamObj["mobileStreams"].length; i++) {
            if (typeof streamObj["mobileStreams"][i] !== 'undefined' && streamObj["mobileStreams"][i] != null) {
                this.flashVars["wnms" + (i + 1)] = streamObj["mobileStreams"][i].url;
            }
        }
    }
};


/* Method: SetFlashLiveStream
- type: Data source type (wncategeroy, wnstory, mrss, feedsapi)
- src: Data source (category id, link)
*/
WNVideoWidget.prototype.SetEmbeddedGallery = function(type, src) {
    switch (type) {
        case "wncategory":
            this.flashVars["hasEmbeddedGallery"] = true;
            this.flashVars["gallerySourceType"] = "wncategory";
            if (parseInt(src) > 0) this.flashVars["galleryId"] = parseInt(src);
            break;
        case "wnstory":
            this.flashVars["hasEmbeddedGallery"] = true;
            this.flashVars["gallerySourceType"] = "wnstory";
            if (parseInt(src) > 0) this.flashVars["galleryId"] = parseInt(src);
            break;
            // currently not supported
        case "mrss":
            this.flashVars["hasEmbeddedGallery"] = true;
            this.flashVars["gallerySourceType"] = "mrss";
            this.flashVars["galleryDataUrl"] = src;
            break;
            // currently not supported
        case "feedsapi":
            this.flashVars["hasEmbeddedGallery"] = true;
            this.flashVars["gallerySourceType"] = "feedsapi";
            this.flashVars["galleryDataUrl"] = src;
            break;
    }
};

/* Method: SetStylePackage
- Receives the name of a style package
- Passes the parameters from that package to the widget
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.SetStylePackage = function(p_style) {
    var stylePackage = {};
    try {
        stylePackage = wnDefaultPackage[this.widgetType][p_style]
        for (var i in stylePackage) {
            this.flashVars[i] = stylePackage[i];
        }
        this.flashVars.defaultStyle = p_style;
    } catch (e) {}
};

/* Method: To set the widget player type. Possible value from list...
- EMBEDDED
- POPUP
- MINI
- STANDARD (default - set in WNVideo.asp)
*/
WNVideoWidget.prototype.SetPlayerType = function(type) {
    this.flashVars.playerType = type;
};

/* Method: SetWidth
- Receives specified width and assigns to widget
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.SetWidth = function(p_wid) {
    this.flashVars["playerWidth"] = p_wid;
    this.installFlashVars["playerWidth"] = p_wid;
};

/* Method: SetHeight
- Receives specified height and assigns to widget
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.SetHeight = function(p_ht) {
    this.flashVars["playerHeight"] = p_ht;
    this.installFlashVars["playerHeight"] = p_ht;
};

/* Method: SetVariable
- Receives the variable name and value and assigns to the widget
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.SetVariable = function(p_var, p_value) {
    this.flashVars[p_var] = p_value;
    for (var i in this.installFlashVars) {
        if (i == p_var) this.installFlashVars[i] = p_value;
    }
};

/* Method: SetVideoUrl
- Receives external video parameters and sets coresponding flashVars
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.SetVideoUrl = function(url, enableAds, headline, summary, summaryImageUrl, adTag, partnerclipid) {
    this.flashVars["partnerclipid"] = (partnerclipid == undefined) ? "" : partnerclipid;
    if (this.flashVars["partnerclipid"] != "") {
        url = "";
    }

    this.flashVars["flvUri"] = (url == undefined) ? "" : url;
    this.flashVars["enableAds"] = (enableAds == undefined) ? true : enableAds;
    this.flashVars["headline"] = (headline == undefined) ? "" : headline;
    this.flashVars["summary"] = (summary == undefined) ? "" : summary;
    this.flashVars["summaryImageUrl"] = (summaryImageUrl == undefined) ? "" : summaryImageUrl;
    this.flashVars["adTag"] = (adTag == undefined) ? "" : adTag;

    for (var i in this.installFlashVars) {
        if (i == "flvUri") this.installFlashVars[i] = this.flashVars["flvUri"];
        if (i == "enableAds") this.installFlashVars[i] = this.flashVars["enableAds"];
        if (i == "headline") this.installFlashVars[i] = this.flashVars["headline"];
        if (i == "summary") this.installFlashVars[i] = this.flashVars["summary"];
        if (i == "summaryImageUrl") this.installFlashVars[i] = this.flashVars["summaryImageUrl"];
        if (i == "adTag") this.installFlashVars[i] = this.flashVars["adTag"];
        if (i == "partnerclipid") this.installFlashVars[i] = this.flashVars["partnerclipid"];
    }
};

WNVideoWidget.prototype.SetLandingPageUrl = function(url) {
    this.flashVars.islandingPageoverride = true;
    this.flashVars.landingPage = url;
};

/* Method: addImage
- Receives external image parameters and sets coresponding flashVars
- Must be declared before rendering the widget
*/
WNVideoWidget.prototype.addImage = function(id, url, headline, description, credits, publishingDate, href, adTag) {
    if (this.flashVars["images"] == undefined) this.flashVars["images"] = "[]";
    this.images = JSON.parse(this.flashVars["images"]);
    this.images.push({
        id: id,
        url: url,
        headline: headline,
        description: description,
        credits: credits,
        publishingDate: publishingDate,
        href: href,
        adTag: adTag
    });
    this.flashVars["images"] = JSON.stringify(this.images);
};

WNVideoWidgets.prototype.loadPrimarySlideshowData = function(json) {
    for (var groupId in pageWidgets) {
        for (var wdgt in pageWidgets[groupId]) {
            if (pageWidgets[groupId][wdgt].widgetClassType == "WNImageGallery" &&
                pageWidgets[groupId][wdgt].isPrimaryGallery != undefined &&
                pageWidgets[groupId][wdgt].isPrimaryGallery == "true") {
                pageWidgets[groupId][wdgt].htmlVersion.renderFromJson(json);
                return 0;
            }
        }
    }
};

WNVideoWidgets.prototype.isImageLoaded = function(img) {
    if (typeof img.complete != 'undefined' && !img.complete) {
        return false;
    };
    if (typeof img.naturalWidth != 'undefined' && img.naturalWidth == 0) {
        return false;
    };
    return true;
};

/* Method: RenderWidget
- Writes out the flash widget based on the attributes of the WNVideoWidget object
- Receives a boolean.  If true, write out flash object tag, otherwise write out html
*/

WNVideoWidget.prototype.RenderWidget = function() {
    var divId = document.getElementById(this.targetDiv);
    var id = this.flashVars.domId;
    var hasFlash;
    var uri;
    var swfWidth = this.flashVars.playerWidth;
    var swfHeight = this.flashVars.playerHeight;
    var flashVars = "";

    if (this.widgetClassType == "WNGraphicsHeader") {
        divId.innerHTML = "<a href='" + this.flashVars.graphicsHeaderClickUrl + "' target='_blank'><img src='" + this.flashVars.graphicsHeaderImageUrl + "' style='border: 0px; width: " + this.flashVars.playerWidth + "px; height: " + this.flashVars.playerHeight + "px;'/></a>";
        return;
    }
    if (this.widgetClassType == "WNGraphicsFooter") {
        divId.innerHTML = "<a href='" + this.flashVars.graphicsFooterClickUrl + "' target='_blank'><img src='" + this.flashVars.graphicsFooterImageUrl + "' style='border: 0px; width: " + this.flashVars.playerWidth + "px; height: " + this.flashVars.playerHeight + "px;'/></a>";
        return;
    }
    uri = this.uri + "?ver=" + wnSiteConfigGeneral.cacheVersionBuster;
    autoInstallUri = this.installUri + wnVideoWidgets.installParams;
    flashVars = this.flashVars;

    /**/
    // Get clipId, autoStart and other parameters from the URL if passed
    /**/
    if (this.widgetClassType == "WNVideoCanvas" || this.widgetClassType == "WNVideoCanvas2") {
        if (wnVideoUtils.getQS("clipid") != undefined) flashVars.clipId = wnVideoUtils.getQS("clipid");
        if (wnVideoUtils.getQS("autostart") != undefined) flashVars.isAutoStart = wnVideoUtils.getQS("autostart");
        // once flvUri is set - clipId is no longer valid
        if (flashVars["flvUri"] != undefined) flashVars.clipId = "";
        // there are a number of hops the regexp takes before it is used
        // so escape is needed so chars are not strip/drop
        if (flashVars["regexpmrssid"] != undefined) flashVars.regexpmrssid = encodeURIComponent(encodeURIComponent(encodeURIComponent(flashVars.regexpmrssid)));
        // the url may contain querystring params
        if (flashVars["thirdpartytemplatemrssurl"] != undefined) flashVars.thirdpartytemplatemrssurl = encodeURIComponent(encodeURIComponent(flashVars.thirdpartytemplatemrssurl));
        // the landing page may contain querystring params
        if (flashVars["landingPage"] != undefined) flashVars.landingPage = encodeURIComponent(encodeURIComponent(flashVars.landingPage));
        flashVars["isSafaiOnMac"] = wnVideoWidgets.isMac && (/WebKit/i.test(navigator.userAgent));

        if (wnVideoUtils.getQS("streamType") != undefined && wnVideoUtils.getQS("clipid") == undefined) {
            flashVars.clipId = "";
            var flashLiveStreamObj = {
                strUrl: wnVideoUtils.getQS("flvUri"),
                streamType: wnVideoUtils.getQS("streamType"),
                strHeadline: wnVideoUtils.getQS("headline"),
                strAdTag: wnVideoUtils.getQS("adTag"),
                hasPreroll: wnVideoUtils.getQS("enableAds"),
                mobileStreams: []
            }
            var mobStream1 = wnVideoUtils.getQS("wnms1");
            var mobStream2 = wnVideoUtils.getQS("wnms2");
            var mobStream3 = wnVideoUtils.getQS("wnms3");
            if (typeof mobStream1 !== "undefined" && mobStream1 != "" ||
                typeof mobStream2 !== "undefined" && mobStream2 != "" ||
                typeof mobStream3 !== "undefined" && mobStream3 != "") {
                if (typeof mobStream1 !== "undefined" && mobStream1 != "") flashLiveStreamObj.mobileStreams.push({
                    url: mobStream1,
                    type: "video/mp4"
                });
                if (typeof mobStream2 !== "undefined" && mobStream2 != "") flashLiveStreamObj.mobileStreams.push({
                    url: mobStream2,
                    type: "video/mp4"
                });
                if (typeof mobStream3 !== "undefined" && mobStream3 != "") flashLiveStreamObj.mobileStreams.push({
                    url: mobStream3,
                    type: "video/mp4"
                });
            }
            this.SetFlashLiveStream(flashLiveStreamObj);
        }

        // ---- Auto upgrade defaults. Remove code once fully migrated to the OSMF player ---
        // Set default skin if one was not set by the API user
        try {
            if (!this.skin) {
                this.SetSkin(CANVAS_SKINS[canvasDefaultSkinPackage][canvasDefaultSkin]);
            }
        } catch (e) {}

        // Set overlay buttons
        try {
            if (parseInt(flashVars['playerHeight']) < 300) {
                // If not already defined. We don't want to override any API user settings.
                if (!flashVars.overlayShareButtons)
                    flashVars.overlayShareButtons = 'email,link,share';
            }
        } catch (e) {}
        // ---- End Auto upgrade defaults. Remove code once fully migrated to the OSMF player ---
    }
    if (this.widgetClassType == "WNGallery") {
        // there are a number of hops the regexp takes before it is used
        // so escape is needed so chars are not strip/drop
        if (flashVars["regexpmrssid"] != undefined) flashVars.regexpmrssid = encodeURIComponent(encodeURIComponent(encodeURIComponent(flashVars.regexpmrssid)));
        // the url may contain querystring params
        if (flashVars["thirdpartytemplatemrssurl"] != undefined) flashVars.thirdpartytemplatemrssurl = encodeURIComponent(encodeURIComponent(flashVars.thirdpartytemplatemrssurl));
        // the landing page may contain querystring params
        if (flashVars["landingPage"] != undefined) flashVars.landingPage = encodeURIComponent(encodeURIComponent(flashVars.landingPage));
    }
    // if (wnVideoUtils.getQS("topvideocatno") != undefined) flashVars.topVideoCatNo = wnVideoUtils.getQS("topvideocatno");
    if (this.widgetClassType == "WNImageGallery") {
        if (wnVideoUtils.getQS("widgetId") != undefined && (this.flashVars.isPrimaryGallery == "true" || this.flashVars.isPrimaryGallery == true)) {
            flashVars.urlWidgetId = wnVideoUtils.getQS("widgetId");
        }
        if (wnVideoUtils.getQS("slideshowImageId") != undefined) flashVars.imageId = wnVideoUtils.getQS("slideshowImageId");
        if (wnVideoUtils.getQS("slideshowImageUrl") != undefined) flashVars.imageUrl = wnVideoUtils.getQS("slideShowimageUrl");
        if (wnVideoUtils.getQS("slideshowFeedUrl") != undefined) flashVars.feedUrl = wnVideoUtils.getQS("slideshowFeedUrl");
    }
    if (this.widgetClassType == "WNImageCanvas" && wnVideoUtils.getQS("widgetId") != undefined) {
        flashVars.urlWidgetId = wnVideoUtils.getQS("widgetId");
    }

    var params = {
        allowFullScreen: true,
        wmode: this.wmode,
        movie: uri,
        quality: "high",
        allowScriptAccess: "always",
        bgcolor: "#FFFFFF"
    };
    var attributes = {
        id: id,
        name: id
    };
    var requiredVersion = "10.1.0";

    // Since widgets could be hosted anywhere there needs to be built in mobile detection
    forceFlash = false;
    if (wn_isMobile && (this.widgetClassType == 'WNVideoCanvas' || this.widgetClassType == 'WNVideoCanvas2' || this.widgetClassType == 'WNGallery')) {
        var flashVersion = swfobject.getFlashPlayerVersion();

        if (wnSiteConfigVideo.mobileForceFlash == true) {
            if (flashVersion.major >= 10) {
                forceFlash = true;
            }
        } else if (!wnVideoWidgets.useHtml5) {
            if (flashVersion.major >= 10) {
                forceFlash = true;
            }
        }
    }
    // For debugging ignore forceFlash from site config
    if (wnVideoUtils.getQS('wnhtml5') == 'true') forceFlash = false;
    this.flashVars['forceFlash'] = forceFlash;


    // Html notice to show if flash player isn't installed
    var fontSize = swfHeight > 30 ? 15 : Math.floor(swfHeight / 2);
    var elem = '<div style="text-align:center;font-family:Arial, Verdana;font-size:13px; font-weight: bold;background-color:#eee;overflow: hidden;';
    if (swfHeight > 2 && swfWidth > 2) elem += 'border: 1px solid #000;';
    if (swfHeight > 2) elem += 'height:' + (swfHeight - 2);
    else elem += 'height:' + swfHeight;
    if (swfWidth > 2) elem += ';width:' + (swfWidth - 2);
    else elem += ';width:' + swfWidth;
    elem += 'px;"><div style="margin:' + fontSize + 'px;">';
    if (this.widgetClassType == "WNVideoCanvas" || this.widgetClassType == "WNGallery" || this.widgetClassType == "WNVideoCanvas2" || this.widgetClassType == "WNImageCanvas" || this.widgetClassType == "WNImageGallery") {
        elem += '<a style="color:#333;text-decoration:none;" href="http://www.macromedia.com/go/getflash/" target="_blank">You need to download the latest version of flash player to use this player</a><br/><br/>';
        elem += '<a style="color:#333;text-decoration:none;" href="' + wnHelpPageUrl + '" target="_blank">Need Help?</a>';
    }
    elem += '</div></div>';
    if (wnVideoWidgets.useHtml5) elem = "";

    // Div container for the swf file
    divId.innerHTML = '<div class="wn-share-pane"></div>'
    divId.innerHTML += '<div id="OverLay" style="display:none;"></div>';
    divId.innerHTML += '<div id="' + id + '_swfObject">' + elem + '</div>';
    if (!wnVideoWidgets.useHtml5 && !wnVideoWidgets.useAmpPlayer) $wn('#' + id + '_swfObject').hide();

    // Force disabling of "Email" button
    if (wnVideoUtils.validateString(flashVars["toolsShareButtons"])) {
        if (flashVars["toolsShareButtons"].indexOf("email") > -1) {
            toolsShareButtons = flashVars["toolsShareButtons"].split(",");
            toolsShareButtons.splice( $wn.inArray("email", toolsShareButtons), 1 );
            if (toolsShareButtons.length > 0) {
                flashVars["toolsShareButtons"] = toolsShareButtons.join(",");
            } else {
                flashVars["toolsShareButtons"] = "disabled";
            }
        }
    } else {
            flashVars["toolsShareButtons"] = "link,share,help";
    }
    if (wnVideoUtils.validateString(flashVars["overlayShareButtons"])) {
        if (flashVars["overlayShareButtons"].indexOf("email") > -1) {
            overlayShareButtons = flashVars["overlayShareButtons"].split(",");
            overlayShareButtons.splice( $wn.inArray("email", overlayShareButtons), 1 );
            if (overlayShareButtons.length > 0) {
                flashVars["overlayShareButtons"] = overlayShareButtons.join(",");
            } else {
                flashVars["overlayShareButtons"] = "disabled";
            }
        }
    } else {
            flashVars["overlayShareButtons"] = "link,share,help";
    }

    // Insert swf file into the div
    for (var i in flashVars) {
        if (this.widgetClassType == "WNImageGallery" && i == "images") {
            flashVars.images = escape(flashVars.images);
        } else {
            // Patch to allow objects to be passed without escape since that breaks the format
            if (typeof flashVars[i] != 'object' && i != "debugAdUrl") {
                flashVars[i] = unescape(flashVars[i]);
            }
        }
    }

    if (pageWidgets[this.flashVars.idKey] == undefined) {
        pageWidgets[this.flashVars.idKey] = {};
    }
    if (this.flashVars.isPrimaryGallery != null) this.flashVars.isPrimaryGallery = this.flashVars.isPrimaryGallery.toString().toLowerCase();

    pageWidgets[this.flashVars.idKey][this.flashVars.domId] = this.flashVars;
    pageWidgets[this.flashVars.idKey][this.flashVars.domId].parentObject = this;
    pageWidgets[this.flashVars.idKey][this.flashVars.domId].widgetClassType = this.widgetClassType;
    pageWidgets[this.flashVars.idKey][this.flashVars.domId].isLoaded = false;
    pageWidgets[this.flashVars.idKey][this.flashVars.domId].isReady = false;
    pageWidgets[this.flashVars.idKey][this.flashVars.domId].skin = this.skin;


    var slideshowAsHtml = wnSiteConfigVideo.renderSlideShowAsHtml;
    var isSingleImageGallery = (this.flashVars.isSingleImageGallery && this.flashVars.isSingleImageGallery.toLowerCase() == 'true');
    var hasHtmlImageCanvas = (getWigdetByType(this.idKey, "ic").htmlVersion != undefined && // has related html image camvas
        getWigdetByType(this.idKey, "c").widgetClassType == undefined && // doesn't have related video canvas (possible in the widgetbuilder)
        getWigdetByType(this.idKey, "c2").widgetClassType == undefined); // doesn't have related osmf video canvas (possible in the widgetbuilder)


    if (wnVideoWidgets.useAmpPlayer && (this.widgetClassType == "WNVideoCanvas" || this.widgetClassType == "WNVideoCanvas2")) {
        this.ampVersion = this.flashVars.ampVersion = new WNAmpVideoCanvas(this.targetDiv, this.flashVars);
    } else if (wn_isMobile ||
        wnVideoWidgets.useHtml5 ||
        isSingleImageGallery && this.widgetClassType == "WNImageGallery" ||
        slideshowAsHtml && (this.widgetClassType == "WNImageGallery" ||
            this.widgetClassType == "WNImageCanvas") ||
        hasHtmlImageCanvas && (this.widgetClassType == "WNHeadline" ||
            this.widgetClassType == "WNInfoPane")) {
        if (typeof $wn === 'undefined') {
            wnLog("no jquery");
        } else {
            switch (this.widgetClassType) {
                case "WNVideoCanvas":
                case "WNVideoCanvas2":
                    if (forceFlash) {
                        swfobject.embedSWF(uri, id + "_swfObject", swfWidth, swfHeight, requiredVersion, autoInstallUri, flashVars, params, attributes, this.onRenderWidgetCallback);
                    } else {
                        this.htmlVersion = this.flashVars.htmlVersion = new WNVideoCanvas(this.targetDiv, this.flashVars);
                    }
                    break;
                case "WNGallery":
                    if (forceFlash) {
                        swfobject.embedSWF(uri, id + "_swfObject", swfWidth, swfHeight, requiredVersion, autoInstallUri, flashVars, params, attributes, this.onRenderWidgetCallback);
                    } else {
                        this.htmlVersion = this.flashVars.htmlVersion = new WNVideoGallery(this.targetDiv, this.flashVars);
                    }
                    break;
                case "WNImageGallery":
                    this.htmlVersion = this.flashVars.htmlVersion = new WNImageGallery(this.targetDiv, this.flashVars, this.images);
                    break;
                case "WNImageCanvas":
                    this.htmlVersion = this.flashVars.htmlVersion = new WNImageCanvas(this.targetDiv, this.flashVars);
                    break;
                case "WNHeadline":
                    this.htmlVersion = this.flashVars.htmlVersion = new WNHeadline(this.targetDiv, this.flashVars);
                    break;
                case "WNInfoPane":
                    this.htmlVersion = this.flashVars.htmlVersion = new WNInfoPane(this.targetDiv, this.flashVars);
                    break;
            }
        }
    } else {
        swfobject.embedSWF(uri, id + "_swfObject", swfWidth, swfHeight, requiredVersion, autoInstallUri, flashVars, params, attributes, this.onRenderWidgetCallback);
    }

    if (this.widgetClassType == "WNVideoCanvas" || this.widgetClassType == "WNVideoCanvas2") {
        for (var i in this.flashVars) {
            wnVideoWidgets.canvasObjectFlashVars += i;
            wnVideoWidgets.canvasObjectFlashVars += "=";
            wnVideoWidgets.canvasObjectFlashVars += this.flashVars[i];
            wnVideoWidgets.canvasObjectFlashVars += "&";
        }
        wnVideoWidgets.canvasObjectFlashVars = unescape(wnVideoWidgets.canvasObjectFlashVars);
        if (this.flashVars["videoType"] == "livestream") {
            WNSilverlightWidgetClass = this;
        }
        //divId.innerHTML = '<div id="OverLay" style="display:none;"></div>' + divId.innerHTML;
    }
};

WNVideoWidget.prototype.onRenderWidgetCallback = function(e) {
    if (!e.success) $wn('#' + e.id).show();
}


/**
 * WNGallery customization methods
 */
/**
 * GalleryTab customization methods
 */
/**
 * SetGalleryTabHeight
 */
WNVideoWidget.prototype.SetGalleryTabHeight = function(ht) {
    this.flashVars.tabHeight = ht;
};

/**
 * SetGalleryTabBackground
 */
WNVideoWidget.prototype.SetGalleryTabBackground = function(color1, color2) {
    this.flashVars.tabBackgroundColors = color1 + "," + color2;
};


/**
 * SetGalleryTabSelectedBackground
 * Selects the background color of the selected tab
 */
WNVideoWidget.prototype.SetGalleryTabSelectedBackground = function(color) {
    this.flashVars.tabBackgroundSelectedColors = color;
};

WNVideoWidget.prototype.SetGalleryTabSelectedBorderColor = function(color) {
    this.flashVars.tabBackgroundSelectedBorderColor = color;
};

/**
 * SetGalleryTabFontColors
 */
WNVideoWidget.prototype.SetGalleryTabFontColors = function(offColor, overColor) {
    this.flashVars.tabOffFaceColor = offColor;
    this.flashVars.tabOverFaceColor = overColor;
};

/**
 * SetGalleryTabFontSize
 */
WNVideoWidget.prototype.SetGalleryTabFontSize = function(size) {
    this.flashVars.tabFontSize = size;
};

/**
 * SetGalleryTabInnerBorderColors
 */
WNVideoWidget.prototype.SetGalleryTabInnerBorderColors = function(leftBorderColor, rightBorderColor) {
    this.flashVars.tabLeftBorderColor = leftBorderColor;
    this.flashVars.tabRightBorderColor = rightBorderColor;
};

/**
 * SetGalleryTabBorder
 */
WNVideoWidget.prototype.SetGalleryDropDownTabOverBorderColor = function(color) {
    this.flashVars.dropDownTabOverBorderColor = color;
};

/**
 * Gallery DropDown customization methods below
 */
/**
 * SetGalleryDropDownBackground
 */
WNVideoWidget.prototype.SetGalleryDropDownBackground = function(color1, color2, color3, color4) {
    this.flashVars.dropDownBackgroundColors = color1 + "," + color2 + "," + color3 + "," + color4;
};

/**
 * SetGalleryDropDownBorderColor
 */
WNVideoWidget.prototype.SetGalleryDropDownBorderColor = function(color) {
    this.flashVars.dropDownBorderColor = color;
};

/**
 * SetGalleryDropDownShadowColor
 */
WNVideoWidget.prototype.SetGalleryDropDownShadowColor = function(color) {
    this.flashVars.dropDownDropShadowColor = color;
};

/**
 * SetGalleryDropDownInnerBorderColors
 */
WNVideoWidget.prototype.SetGalleryDropDownInnerBorderColors = function(topBorderColor, bottomBorderColor) {
    this.flashVars.dropDownTopBorderColor = topBorderColor;
    this.flashVars.dropDownBottomBorderColor = bottomBorderColor;
};

/**
 * SetGalleryDropDownFontColors
 */
WNVideoWidget.prototype.SetGalleryDropDownFontColors = function(offFaceColor, overFaceColor, selectedFaceColor) {
    this.flashVars.dropDownOffFaceColor = offFaceColor;
    this.flashVars.dropDownOverFaceColor = overFaceColor;
    this.flashVars.dropDownSelectedFaceColor = selectedFaceColor;
};

/**
 * SetGalleryDropDownFontSize
 */
WNVideoWidget.prototype.SetGalleryDropDownFontSize = function(size) {
    this.flashVars.dropDownFontSize = size;
};

/**
 * Gallery List Functions
 */

/**
 * SetVideoListBackground
 * Sets the background colors for the video list background
 */
WNVideoWidget.prototype.SetVideoListBackground = function(color1, color2) {
    this.flashVars.videoListBackgroundColors = color1 + "," + color2;
};

/**
 * SetVideoListBorderColor
 * Sets the border color for the video list
 */
WNVideoWidget.prototype.SetVideoListBorderColor = function(color) {
    this.flashVars.videoListBorderColor = color;
};

/**
 * SetVideoListItemInnerBorders
 * Sets the colors for the highlight and shadow borders of each list item
 */
WNVideoWidget.prototype.SetVideoListItemInnerBorders = function(highlightColor, shadowColor) {
    this.flashVars.videoListItemHighlightBorderColor = highlightColor;
    this.flashVars.videoListItemShadowBorderColor = shadowColor;
};

/**
 * SetVideoListItemFontColors
 * Sets the color and highlight color for each video list item
 */
WNVideoWidget.prototype.SetVideoListItemFontColors = function(offFaceColor, overFaceColor) {
    this.flashVars.videoListItemOffFaceColor = offFaceColor;
    this.flashVars.videoListItemOverFaceColor = overFaceColor;
};

/**
 * SetVideoListItemFontSize
 * Sets the font sizes for the headline and duration fields
 */
WNVideoWidget.prototype.SetVideoListItemFontSizes = function(headlineSize, durationSize) {
    this.flashVars.videoListItemHeadlineFontSize = headlineSize;
    this.flashVars.videoListItemDurationFontSize = durationSize;
};

/**
 * SetVideoListImageSize
 * Sets the width and height for the video item thumbnail image
 */
WNVideoWidget.prototype.SetVideoListItemImageSize = function(width, height) {
    this.flashVars.videoListImageWidth = width;
    this.flashVars.videoListImageHeight = height;
};

/**
 * Gallery Video List Navigation Methods
 */
/**
 * SetVideoListNavigationHeight
 * Sets the height for the video list navigation
 */
WNVideoWidget.prototype.SetVideoListNavigationHeight = function(ht) {
    this.flashVars.videoListNavigationHeight = ht;
};

/**
 * SetVideoListNavigationBackground
 * Sets the background colors for the video list navigation
 */
WNVideoWidget.prototype.SetVideoListNavigationBackground = function(color1, color2) {
    this.flashVars.videoListNavigationBackgroundColors = color1 + "," + color2;
};

/**
 * SetVideoListNavigationBorderColor
 * Sets the border color for the video list navigation
 */

WNVideoWidget.prototype.SetVideoListNavigationBorderColor = function(color) {
    this.flashVars.videoListNavigationBorderColor = color;
};

/**
 * SetVideoListNavigationColors
 * Sets the colors for the previous and next buttons
 */
WNVideoWidget.prototype.SetVideoListNavigationColors = function(offFaceColor, overFaceColor) {
    this.flashVars.videoListNavigationOffFaceColor = offFaceColor;
    this.flashVars.videoListNavigationOverFaceColor = overFaceColor;
};

/**
 * SetVideoListNavigationPageNumberColors
 * Sets the colors for the page numbers
 */
WNVideoWidget.prototype.SetVideoListNavigationPageNumberColors = function(offFaceColor, overFaceColor, selectedFaceColor) {
    this.flashVars.videoListPageNumberOffColor = offFaceColor;
    this.flashVars.videoListPageNumberOverColor = overFaceColor;
    this.flashVars.videoListPageNumberSelectedColor = selectedFaceColor;
};

/**
 * SetVideoListNavigationPageNumberFontSize
 * Sets the font size for the video list navigation page numbers
 */
WNVideoWidget.prototype.SetVideoListNavigationPageNumberFontSize = function(size) {
    this.flashVars.videoListPageNumberFontSize = size;
};

/**
 * Gallery Search Methods below
 */
/**
 * SetGallerySearchHeight
 * Sets the height for the search area
 */
WNVideoWidget.prototype.SetGallerySearchHeight = function(ht) {
    this.flashVars.gallerySearchFormHeight = ht;
};

/**
 * SetGallerySearchBackground
 * Sets the background colors for the search area
 */
WNVideoWidget.prototype.SetGallerySearchBackground = function(color1, color2) {
    this.flashVars.searchBackgroundColors = color1 + "," + color2;
};

/**
 * SetGallerySearchFontColors
 * Sets the colors for the Search buttons and text
 */
WNVideoWidget.prototype.SetGallerySearchFontColors = function(offFaceColor, overFaceColor) {
    this.flashVars.searchOffFaceColor = offFaceColor;
    this.flashVars.searchOverFaceColor = overFaceColor;
};

/**
 * SetGallerySearchFormBorderColor
 * Sets the border color for the search form
 */
WNVideoWidget.prototype.SetGallerySearchFormBorderColor = function(color) {
    this.flashVars.searchFormBorderColor = color;
};

/**
 * SetGallerySearchFormFontColor
 * Sets the color for the input text in the search form
 */
WNVideoWidget.prototype.SetGallerySearchFormFontColor = function(color) {
    this.flashVars.searchFormFaceColor = color;
};

/**
 * SetGalleryCategory
 * Sets the category to load into the category widget
 * Must be called before rendering the widget.
 */
WNVideoWidget.prototype.SetGalleryCategory = function(id) {
    this.flashVars.topVideoCatNo = id;
};

/**
 * SetGalleryLeadClip
 * Replaces lead clip in the Gallery
 */
WNVideoWidget.prototype.SetGalleryLeadClip = function(id) {
    this.flashVars.clipId = id;
};

/**
 * EnableGalleryControls
 * Used
 */
WNVideoWidget.prototype.EnableControls = function(isEnabled) {
    var widgetType = this.widgetClassType
    switch (widgetType) {
        case "WNGallery":
            var thisGallery = document.getElementById(this.flashVars.domId);
            if (thisGallery.EnableControls) {
                thisGallery.EnableControls(isEnabled);
            } else {
                this.htmlVersion.enableControls(isEnabled);
            }
            break;
    }
};

/**
 * Video Canvas Functions
 */

/**
 * SetReportingKeywords
 */
WNVideoWidget.prototype.SetReportingKeywords = function(keyword) {
    this.flashVars.reportingKeywords = keyword;
};

/**
 * SetAdvertisingKeywords
 */
WNVideoWidget.prototype.SetAdvertisingKeywords = function(keyword) {
    this.flashVars.advertisingKeywords = keyword;
};

/**
 * AddUploadCategory
 */
WNVideoWidget.prototype.AddUploadCategory = function(categories) {
    this.flashVars.categories = categories;
};

/**
 * AddUploadTitlePrefix
 */
WNVideoWidget.prototype.AddUploadTitlePrefix = function(titlePrefix) {
    this.flashVars.titlePrefix = titlePrefix;
};

/**
 * SetAdvertisingZone
 */
WNVideoWidget.prototype.SetAdvertisingZone = function(zone) {
    this.flashVars.advertisingZone = zone;
};

/**
 * EnableEmail
 */
WNVideoWidget.prototype.EnableEmail = function(isEnabled) {
    this.flashVars.hasEmail = isEnabled;
};

/**
 * EnableHelp
 */
WNVideoWidget.prototype.EnableHelp = function(isEnabled) {
    this.flashVars.hasHelp = isEnabled;
};

/**
 * EnableClosedCaption
 */
WNVideoWidget.prototype.EnableClosedCaption = function(isEnabled) {
    this.flashVars.hasCC = isEnabled;
};

/**
 * EnableFullScreen
 */
WNVideoWidget.prototype.EnableFullScreen = function(isEnabled) {
    this.flashVars.hasFullScreen = isEnabled;
};

/**
 * SetErrorMessage
 */
WNVideoWidget.prototype.SetErrorMessage = function(errorMessage) {
    this.flashVars.errorMessage = errorMessage;
};

/**
 * SetVideoScaleStyle
 */
WNVideoWidget.prototype.SetVideoScaleStyle = function(pct) {
    if (pct.toLowerCase == "scaletofit") pct = "0";
    this.flashVars.playAtActualSize = pct;
};

/**
 * SetVideoSmoothingMode
 */
WNVideoWidget.prototype.SetVideoSmoothingMode = function(smoothingMode) {
    this.flashVars.smoothingMode = smoothingMode;
};

/**
 * EnableAutoStart
 */
WNVideoWidget.prototype.EnableAutoStart = function(isEnabled) {
    this.flashVars.isAutoStart = isEnabled;
};
/**
 * EnableMute
 */
WNVideoWidget.prototype.EnableMute = function(isEnabled) {
    this.flashVars.isMute = isEnabled;
};

/**
 * SetSummaryGraphicScaleStyle
 */
WNVideoWidget.prototype.SetSummaryGraphicScaleStyle = function(style) {
    this.flashVars.summaryGraphicScaleStyle = style;
};

/**
 * SetCanvasBackgroundColors
 */
WNVideoWidget.prototype.SetCanvasBackgroundColors = function(outerColor, innerColor) {
    this.flashVars.backgroundColors = outerColor + "," + innerColor + "," + innerColor + "," + outerColor;
    this.flashVars.controlsBackgroundColors = outerColor + "," + innerColor;
};

/**
 * SetVideoControlsInnerBorderColors
 */
WNVideoWidget.prototype.SetVideoControlsInnerBorderColors = function(leftBorder, rightBorder) {
    this.flashVars.controlsButtonLeftBorderColor = leftBorder;
    this.flashVars.controlsButtonRightBorderColor = rightBorder;
};

/**
 * SetVideoControlsColors
 */
WNVideoWidget.prototype.SetVideoControlsColors = function(offFaceColor, overFaceColor) {
    this.flashVars.controlsOffFaceColor = offFaceColor;
    this.flashVars.controlsOverFaceColor = overFaceColor;
};

/**
 * SetVolumeSliderColors
 */
WNVideoWidget.prototype.SetVolumeSliderColors = function(offFaceColor, overFaceColor) {
    this.flashVars.volumeSliderOffColor = offFaceColor;
    this.flashVars.volumeSliderOverColor = overFaceColor;
};

/**
 * SetVideoSliderColors
 */
WNVideoWidget.prototype.SetVideoSliderColors = function(backgroundColor, loadColor, progressColor) {
    this.flashVars.videoSliderBackgroundColor = backgroundColor;
    this.flashVars.videoSliderLoadIndicatorColor = loadColor;
    this.flashVars.videoSliderProgressIndicatorColor = progressColor;
};

/**
 * SetVideoSliderKnobColors
 */
WNVideoWidget.prototype.SetVideoSliderKnobColors = function(backgroundColor, borderColor, offFaceColor, overFaceColor) {
    this.flashVars.videoSliderKnobBackgroundColors = backgroundColor;
    this.flashVars.videoSliderKnobBorderColor = borderColor;
    this.flashVars.videoSliderKnobOffFaceColor = offFaceColor;
    this.flashVars.videoSliderKnobOverFaceColor = overFaceColor;
};

/**
 * SetCanvasOverlayBackgroundColor
 */
WNVideoWidget.prototype.SetCanvasOverlayBackgroundColor = function(color) {
    this.flashVars.overlayBackgroundColors = color;
};

/**
 * SetOverlayFontColors
 */
WNVideoWidget.prototype.SetCanvasOverlayFontColors = function(offFaceColor, overFaceColor, inputColor) {
    this.flashVars.overlayOffFaceColor = offFaceColor;
    this.flashVars.overlayOverFaceColor = overFaceColor;
    this.flashVars.emailInputFaceColor = inputColor;
};

/**
 * SetCanvasOverlayFormFieldColors
 */
WNVideoWidget.prototype.SetCanvasFormFieldBackgroundColor = function(color) {
    this.flashVars.emailFormFieldColors = color;
};

/**
 * SetCanvasOverlayErrorColors
 */
WNVideoWidget.prototype.SetCanvasOverlayErrorColors = function(textColor, borderColor) {
    this.flashVars.emailErrorMessageFaceColor = textColor;
    this.flashVars.emailErrorBorderColor = borderColor;
};

/**
 * SetInfoPaneFontSizes
 */
WNVideoWidget.prototype.SetInfoPaneFontSizes = function(headlineSize, summarySize, durationSize) {
    this.flashVars.headlineFontSize = headlineSize;
    this.flashVars.summaryFontSize = summarySize;
    this.flashVars.durationFontSize = durationSize;
};

/**
 * SetInfoPaneFontColors
 */
WNVideoWidget.prototype.SetInfoPaneFontColors = function(headlineColor, summaryColor, durationColor) {
    this.flashVars.headlineColor = headlineColor;
    this.flashVars.summaryColor = summaryColor;
    this.flashVars.durationColor = durationColor;
};

/**
 * EnableInfoPaneHeadline
 */
WNVideoWidget.prototype.EnableInfoPaneHeadline = function(isEnabled) {
    this.flashVars.hasHeadline = isEnabled;
};

/**
 * EnableInfoPaneSummary
 */
WNVideoWidget.prototype.EnableInfoPaneSummary = function(isEnabled) {
    this.flashVars.hasSummary = isEnabled;
};

/**
 * EnableInfoPaneDuration
 */
WNVideoWidget.prototype.EnableInfoPaneDuration = function(isEnabled) {
    this.flashVars.hasDuration = isEnabled;
};

/**
 * EnableInfoPaneImage
 */
WNVideoWidget.prototype.EnableInfoPaneImage = function(isEnabled) {
    this.flashVars.hasImage = isEnabled;
};

/**
 * SetInfoPaneImageProperties
 */
WNVideoWidget.prototype.SetInfoPaneImageProperties = function(imageWidth, imageHeight, imagePosition) {
    this.flashVars.imageWidth = imageWidth;
    this.flashVars.imageHeight = imageHeight;
    this.flashVars.imagePosition = imagePosition;
};

/**
 * SetInfoPaneBackgroundColors
 */
WNVideoWidget.prototype.SetInfoPaneBackgroundColors = function(outerColor, innerColor) {
    this.flashVars.backgroundColors = outerColor + "," + innerColor + "," + innerColor + "," + outerColor;
};

/**
 * SetHeadlineFontSize
 */
WNVideoWidget.prototype.SetHeadlineFontSize = function(fontSize) {
    this.flashVars.fontSize = fontSize;
};

/**
 * SetHeadlineFontColor
 */
WNVideoWidget.prototype.SetHeadlineFontColor = function(fontColor) {
    this.flashVars.offFaceColor = fontColor;
};

/**
 * SetHeadlineBackgroundColors
 */
WNVideoWidget.prototype.SetHeadlineBackgroundColors = function(color1, color2) {
    this.flashVars.backgroundColors = color1 + "," + color2;
};

WNVideoWidget.prototype.NewCategory = function(catId, clipId) {
    document.getElementById(this.flashVars.domId).wnNewCategory({
        catId: catId,
        clipId: clipId
    });
};

/**
 * Expose Play/Pause toggle
 */
WNVideoWidget.prototype.PlayPause = function() {
    document.getElementById(this.flashVars.domId).playPause();
};
WNVideoWidget.prototype.Play = function() {
    var player = document.getElementById(this.flashVars.domId);
    if (player.hasOwnProperty('playMedia')) {
        player.playMedia();
    } else {
        $wn(player).data('widget').canvas.play();
    }
};
WNVideoWidget.prototype.Pause = function() {
    var player = document.getElementById(this.flashVars.domId);
    if (player.hasOwnProperty('pauseMedia')) {
        player.pauseMedia();
    } else {
        $wn(player).data('widget').canvas.pause();
    }
};


/**
 *
 */
WNVideoWidget.prototype.ShowEmail = function() {
    document.getElementById(this.flashVars.domId).showEmail();
};

WNVideoWidget.prototype.ShowSummary = function() {
    document.getElementById(this.flashVars.domId).showSummary();
};

WNVideoWidget.prototype.ShowClosedCaption = function() {
    document.getElementById(this.flashVars.domId).showClosedCaption();
};

WNVideoWidget.prototype.Mute = function() {
    document.getElementById(this.flashVars.domId).muteToggle();
};

WNVideoWidget.prototype.LaunchHelp = function() {
    document.getElementById(this.flashVars.domId).launchHelp();
}

////// ADDED - 02/28/2008 - TransportControls - Starts
WNVideoWidget.prototype.SetVolume = function(intVal) {
    document.getElementById(this.flashVars.domId).setVolume(intVal);
};
//Disable Transport
WNVideoWidget.prototype.disableDefaultTransport = function() {
    this.flashVars["disableTransport"] = "true";
    for (var i in this.installFlashVars) {
        if (i == "disableTransport") this.installFlashVars["disableTransport"] = "true";
    }
};
//Use Icons or Text for Video Controls.
WNVideoWidget.prototype.showVideoIcons = function(isShowIcon) {
    this.flashVars["isShowIcon"] = isShowIcon;
    for (var i in this.installFlashVars) {
        if (i == "isShowIcon") this.installFlashVars["isShowIcon"] = isShowIcon;
    }
};
//Set Video Time externally
WNVideoWidget.prototype.setVideoTime = function(timeVal) {
    document.getElementById(this.flashVars.domId).ExternalSetVideoTime(timeVal);
};
//Set Video Position externally
WNVideoWidget.prototype.setVideoPosition = function(vidPosition) {
    document.getElementById(this.flashVars.domId).ExternalSetVideoPosition(vidPosition);
};
//External Timer Object for Video
WNVideoWidget.prototype.getTimerObject = function() {
    return document.getElementById(this.flashVars.domId).ExternalGetTimerObject();
};
////// ADDED - 02/28/2008 - TransportControls - Ends

////// ADDED - 02/28/2008 - MRSS - Starts
/* Method: ValidateURL
 * <ul>Generic Method to Validate URLs.</ul>
 */
WNVideoWidget.prototype.ValidateURL = function(url) {
    var v = /^[A-Za-z]+:\/\/+[A-Za-z0-9-_]+\.[A-Za-z0-9-_%&\?\/.=:]+$/;
    if (v.test(url)) {
        return true;
    } else {
        return false;
    }
};
/* Method: AddFeed
 * <ul>Creates a feed object.</ul>
 */
WNVideoWidget.prototype.AddFeed = function(name, source, type, url, priority, contentClass, adOwner, adSplit, policyFile, providerType) {
    if (!this.wnFeedObject.hasFeeds) {
        this.wnFeedObject.hasFeeds = true;
    }
    var feedObj = {};
    //if(this.ValidateURL(url))
    //{
    feedObj.name = name;
    feedObj.source = source;
    feedObj.type = type;
    feedObj.url = url;
    feedObj.priority = priority;
    feedObj.contentClass = contentClass;
    feedObj.adOwner = adOwner;
    feedObj.adSplit = adSplit;
    feedObj.policyFile = policyFile;
    feedObj.providerType = providerType;
    this.wnFeedObject.feeds.push(feedObj);
};
/* Method: SendFeedObject
 * <ul>Returns the feed object to flash.</ul>
 */
WNVideoWidget.prototype.SendFeedObject = function() {
    tmpFeedObject = new Object;
    tmpFeedObject.hasFeeds = this.wnFeedObject.hasFeeds;
    tmpFeedObject.feeds = new Array();
    for (i = 0; i < this.wnFeedObject.feeds.length; i++) {
        tmpFeedObject.feeds.push(this.wnFeedObject.feeds[i]);
    }

    delete this.wnFeedObject;
    return tmpFeedObject;
};
////// ADDED - 02/28/2008 - MRSS - Ends

WNVideoWidget.prototype.SetSkin = function(skinObj) {
    this.skin = skinObj;
    this.flashVars.jsSkinIsSet = true;
};

/* Class: WNAdWidget
Creates an ad widget and registers it for Canvas events.
- p_adWidgetSize
- p_callBackFunctionName
- p_groupId
*/
WNAdWidget = function(p_adWidgetSize, p_callBackFunctionName, p_groupId) {
    this.widgetClassType = "WNAdWidget";
    this.idKey = wnVideoUtils.validateString(p_groupId) ? p_groupId : "DEFAULT";
    this.widgetName = p_adWidgetSize;
    this.size = p_adWidgetSize;
    this.id = p_adWidgetSize + this.idKey;
    this.isInCanvas = false;

    if (pageAds[this.idKey] == undefined) {
        pageAds[this.idKey] = {};
    }
    pageAds[this.idKey][this.id] = this;

    this.uafAd({
        type: 'dom',
        id: this.size.replace("wnsz_", "") // wnsz_52
            ,
        ownerinfo: {
            local: {
                share: 1
            }
        },
        delayinit: true
    });

    //register for events
    evntMgr.Register({
        lstnr: this,
        evnt: "NewClip",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "NewMedia",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "MediaEnded",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "NewCompanionAd",
        func: "SetWNCompanionAd",
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "UAFLoadCompanions",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "UAFLoadVastCompanions",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
};

WNAdWidget.prototype.uafAd = function(options) {
    this.ad = new Worldnow.Ad(options);
}

WNAdWidget.prototype.SetSeqNum = function(num) {
    this.seqNum = num;
};

/* Method: SetTarget
Renders the widget in the specified DOM element.
*  - p_targetElement - DOM element to be written to using the innerHTML method
*/
WNAdWidget.prototype.SetTarget = function(p_targetElement) {
    this.targetElement = p_targetElement;
    this.ad.set('parent', p_targetElement);
};

WNAdWidget.prototype.SetTransitional = function(p_bln) {
    this.isTransitional = p_bln;
};

WNAdWidget.prototype.SetHeight = function(p_height) {
    this.height = p_height;
    this.ad.set('height', p_height);
};

WNAdWidget.prototype.SetWidth = function(p_width) {
    this.width = p_width;
    this.ad.set('width', p_width);
};

WNAdWidget.prototype.SetWNAdsize = function(p_adSize) {
    this.isTransitional = p_adSize;
};

WNAdWidget.prototype.setIsInCanvas = function(isInCanvas) {
    this.isInCanvas = isInCanvas;
};

WNAdWidget.prototype.setApplication = function(application) {
    this.application = application.toLowerCase();
    this.ad.set('application', application.toLowerCase());
};

WNVideoWidgets.prototype.clipObjCallback = function(json, groupid) {
    var clipJson = json.clips[0];
    if (clipJson.uri != undefined && clipJson.uri[0] && clipJson.uri[0].url) {
        //$wn("#"+json.callbackParams).checkAutostartAndPlay(clipJson);
        $wn("#" + json.callbackParams).data("widget").checkAutostartAndPlay(clipJson);
    } else {
        wnLog('Clip does not have a valid url to play.');
    }
};

WNVideoWidgets.prototype.ampObjCallback = function(json, groupid) {
    var clipJson = json.feed;
    if (json.callbackParams.indexOf("|") > -1) {
        var callbackParams = json.callbackParams.split("|");
        $wn("#" + callbackParams[0]).data("widget").loadClip(clipJson, wnVideoUtils.stringToBoolean(callbackParams[1]));
    } else {
        $wn("#" + json.callbackParams).data("widget").loadClip(clipJson);
    }
};

WNVideoWidgets.prototype.loadGallery = function(json) {
    $wn("#" + json.callbackParams).data("widget").loadGallery(json);
};

WNVideoWidgets.prototype.loadGalleryClip = function(json) {
    $wn("#" + json.callbackParams).data("widget").loadGalleryClip(json);
};

WNVideoWidgets.prototype.getPlayableClipUrl = function(clip) {
    var typeH264;
    var type3gp;

    for (var i = 0; i < clip.uri.length; i++) {
        if (clip.uri[i].type == 'video/mp4') {
            wnLog(clip.uri[i].type);
            typeH264 = clip.uri[i].url;
        }
        if (clip.uri[i].type == 'video/3gpp') {
            wnLog(clip.uri[i].type);
            type3gp = clip.uri[i].url;
        }
    };

    // If blackberry then use 3gp if available
    var isBB = (/blackberry|RIM/i.test(navigator.userAgent.toLowerCase()));
    if ((typeof type3gp != 'undefined') && isBB) {
        return type3gp;
    } else {
        return typeH264;
    }

};

WNVideoWidgets.prototype.masterAdExists = function(clipObj) {
    var masterExists = false;

    try {
        var ads = pageAds[clipObj.idKey];
        for (var ad in ads) {
            if (ads[ad].seqNum == 1) {
                // Master ad exists
                masterExists = true;
            }
        }
    } catch (e) {}

    return masterExists;
};

WNVideoWidgets.prototype.UAFHelper = function(adProps, clipObj, type) {
    // Set properties
    adProps.id = "wnad" + adProps.wnsz;
    adProps.wncc = clipObj.adTag;
    adProps.ownerinfo = {
        "owner": clipObj.preRollOwner,
        "pct": clipObj.preRollSplitPct
    };

    try {
        if (wn_isMobile && wn_isPlatformSiteMobile) {
            adProps.apptype = 'mob';

            // override pre adn post roll ad calls for the mobile experience
            if (adProps.wnsz == '30') {
                adProps.ownerinfo = {
                    "owner": clipObj.mobile_preRollOwner,
                    "pct": clipObj.mobile_preRollSplitPct
                };
                adProps.wnsz = '130';
            } else if (adProps.wnsz == '31') {
                adProps.ownerinfo = {
                    "owner": clipObj.mobile_postRollOwner,
                    "pct": clipObj.mobile_postRollSplitPct
                };
                adProps.wnsz = '131';
            }

            adProps.id = "wnad" + adProps.wnsz;
        } else if (typeof(clipObj.apptype) === 'undefined') {
            adProps.apptype = 'video';
        } else {
            adProps.apptype = clipObj.apptype;
        }
    } catch (e) {
        wnLog('Did not complete setting ad info');
    }

    try {
        if (adProps.sequence == 1) {
            Worldnow.AdMan.resetCounters();
        }
    } catch (e) {}

    // Get RealVu configs
    var contentaddons = wn.contentaddons || {};
    var betalist = contentaddons.betalist || "";
    if (betalist.indexOf('realvuvideo-all') >= 0) {
        adProps['realvuvideo'] = 'all';
    }
    if (betalist.indexOf('realvuvideo-loc') >= 0) {
        adProps['realvuvideo'] = adProps.ownerinfo.aff;
    }
    if (betalist.indexOf('realvuvideo-nat') >= 0) {
        adProps['realvuvideo'] = 'worldnow';
    }

    // Get DFP Video configs
    if (betalist.indexOf('dfpvideo-all') >= 0) {
        adProps['dfpvideo'] = 'all';
    }
    if (betalist.indexOf('dfpvideo-loc') >= 0) {
        adProps['dfpvideo'] = adProps.ownerinfo.aff;
    }
    if (betalist.indexOf('dfpvideo-nat') >= 0) {
        adProps['dfpvideo'] = 'worldnow';
    }

    // adding key/vals needed for dfpvideo and realvu
    adProps['clipid'] = clipObj.id;
    if (betalist.indexOf('dfpfeedid-') >= 0) {
        // from betakist, dfpfeedid-546546
        var dfpfeedid = betalist;
        dfpfeedid = dfpfeedid.substring(dfpfeedid.indexOf('dfpfeedid-') + 10);
        if (dfpfeedid.indexOf(',') >= 0) {
            dfpfeedid = dfpfeedid.substring(0, dfpfeedid.indexOf(','));
        }
        adProps['dfpfeedid'] = dfpfeedid;
    } else {
        adProps['dfpfeedid'] = '';
    }

    if (!type) type = 'raw';
    var ad = Worldnow.AdMan.getAdCall(adProps, type);
    ad.set('requestingurl',true) // switches ad.load to simply RETURN a string, as opposed to going out and loading the ad on it's own
    var adUrl = ad.load();
    var adOwner = ad.get('owner');

    return {
        url: adUrl,
        owner: adOwner
    };
};

/* Method: HandleWorldnowAd
- Handles worldnow ads with loc/nat split.
*/
WNAdWidget.prototype.HandleWorldnowAd = function(p_evtObj) {
    var adUrl;
    var adProps = {};
    var objClip = p_evtObj.objClip;
    var adCallDomain = objClip.adCallDomain;

    // UAF - Load companions with new event which gets fired after the pre-roll call is made
    if (p_evtObj.evnt == "UAFLoadCompanions" && wnSiteConfigVideo.useUAF == true) {
        adProps = p_evtObj.adProps;
        adProps.wnsz = this.size.replace("wnsz_", "");
        adProps.width = this.width;
        adProps.height = this.height;
        adProps.sequence = null; // remove the sequence == 1 adprop from the existing pre-roll so that companions can load in order
        ad = wnVideoWidgets.UAFHelper(adProps, objClip, 'iframe')
        adUrl = ad.url;

        if (this.isTransitional) {
            //disable gallery controls
            videoGallery.EnableControls(false);
        }

        var iframeHtml = "<iframe id='" + this.size + "' src='" + adUrl + "' width='" + this.width + "' height='" + this.height + "' frameborder='0' scrolling='no' allowTransparency='true' marginwidth='0' marginheight='0'></iframe>";

        // Show ad
        this.targetElement.innerHTML = iframeHtml;
    }

    // UAF Vast - Load Vast companions
    if (p_evtObj.evnt == "UAFLoadVastCompanions" && wnSiteConfigVideo.useUAF) {
        var vastAd = p_evtObj.vastAd;

        if (this.isTransitional) {
            //disable gallery controls
            videoGallery.EnableControls(false);
        }

        // Show ad
        if (this.width == vastAd.width && this.height == vastAd.height) {
            var vastHTML = wnGetVastAdHtml(vastAd, this);
            this.targetElement.innerHTML = vastHTML;
        }
    }

    //only refresh the ad widgets for when a content clip loads
    if (p_evtObj.evnt == "NewClip" && (!(objClip.isFlashVideoBookend) || !(this.isTransitional))) {
        // Inject UAF method
        if (this.ad) {
            var options = {
                    wnccx: wnWidgetsFlashVars.WNVideoCanvas.advertisingZone,
                    wncc: objClip.adTag
                },
                ad = this.ad;
            this.seqNum == 1 && Worldnow.AdMan.resetCounters();
            if (wnUsePrerollMaster || (objClip.slideshowHeadline != undefined)) {
                if (this.isInCanvas) {
                    if (!objClip.disableInCanvasAd && this.targetElement != null) {
                        ad.load.call(ad, options);
                    }
                } else {
                    if (!objClip.disableCampaignAd) {
                        ad.load.call(ad, options);
                    }
                }
                if (this.isTransitional) {
                    //disable gallery controls
                    videoGallery.EnableControls(false);
                }
            }
        } else {
            if (wnSiteConfigVideo.useUAF == true) {
                // Construct ad properties object
                adProps.width = this.width;
                adProps.height = this.height;
                this.size = this.size.replace("wnsz_", "");
                adProps.wnsz = this.size;
                adProps["wnccx"] = wnWidgetsFlashVars.WNVideoCanvas.advertisingZone;
                adProps.sequence = this.seqNum; // if == 1 admanager counters will be reset

                if (wnVideoUtils.validateString(this.application)) adProps.application = this.application;

                if (wnUsePrerollMaster || (objClip.slideshowHeadline != undefined)) {
                    ad = wnVideoWidgets.UAFHelper(adProps, objClip, 'iframe')
                    adUrl = ad.url;
                }
            } else {
                adUrl = "http://" + adCallDomain + "/global/ad.asp?type=single&src1=" + objClip.preRollOwner + "&spct1=" + objClip.preRollSplitPct + "&sz1=" + this.size + "&cls1=" + objClip.adTag + "&rndNumOverride=" + objClip.rndNum + "&adZoneOverride=" + objClip.advertisingZone + "&contentOwner=" + objClip.ownerAffiliateName;
            }

            var iframeHtml = "<iframe id='" + this.size + "' src='" + adUrl + "' width='" + this.width + "' height='" + this.height + "' frameborder='0' scrolling='no' allowTransparency='true' marginwidth='0' marginheight='0'></iframe>";

            if (wnUsePrerollMaster || (objClip.slideshowHeadline != undefined)) {
                if (this.isInCanvas) {
                    if (!objClip.disableInCanvasAd && this.targetElement != null) this.targetElement.innerHTML = iframeHtml;
                } else {
                    if (!objClip.disableCampaignAd) this.targetElement.innerHTML = iframeHtml;
                }

                if (this.isTransitional) {
                    //disable gallery controls
                    videoGallery.EnableControls(false);
                }
            }
        }
    }

    if (p_evtObj.evnt == "MediaEnded") {
        //find ads that need to be closed after pre-roll|bookends
        if ((objClip.isPreRoll || objClip.isPostRoll || objClip.isFlashVideoBookend) && this.isTransitional) {
            this.targetElement.innerHTML = "";
            //enable gallery controls
            videoGallery.EnableControls(true);
        }
    }

    if (p_evtObj.evnt == "NewMedia") {
        //find ads that need to be closed after pre-roll|bookends
        if (!(objClip.isPreRoll || objClip.isPostRoll || objClip.isFlashVideoBookend) && this.isTransitional) {
            this.targetElement.innerHTML = "";
            //enable gallery controls
            videoGallery.EnableControls(true);
        }
    }
};

/**
* SetWNCompanionAd Function

* SIZEID  = Ad Manager Tag
* wnsz_35 = 180x150B Rectangle - B
* wnsz_1  = BannerFull Banner
* wnsz_26 = Bu120x60A Button - A
* wnsz_27 = Bu120x60B Button - B
* wnsz_28 = Bu120x60C Button - C
* wnsz_24 = We120x15 Weather
* wnsz_22 = SpH490x25 Sponsorship Header - A
* wnsz_14 = 120x600 Skyscraper
* wnsz_20 = 180x150 Rectangle
* wnsz_23 = VB120x240A Vertical Banner - A
* wnsz_18 = VB120x240B Vertical Banner - B
* wnsz_40 = PB468x60 Printable Page Banner
* wnsz_34 = Video120x60 Video 120x60
* wnsz_41 = 728x90A Leaderboard A
* wnsz_42 = 160x600A Wide Skyscraper
* wnsz_43 = 300x250A Medium Rectangle
* wnsz_46 = 728x90B Leaderboard B
* wnsz_44 = 300x75 National Promo
* wnsz_49 = 500x300A 500x300 Footer A
* wnsz_50 = 300x175A 300x175 Rectangle A
* wnsz_53 = HalfBanner
*/
WNAdWidget.prototype.SetWNCompanionAd = function(p_adObj) {
    var objAd = p_adObj.objClip;

    //load the right size, might need to change to id if groups are used
    if (objAd.size == this.widgetName) {
        if (this.isTransitional) {
            //disable gallery controls
            videoGallery.EnableControls(false);
        }

        if (objAd.useRedirect == "true") {
            if (objAd.redirectHtml != "null") {
                this.targetElement.innerHTML = objAd.redirectHtml;
            }
        } else {
            this.targetElement.innerHTML = "<a href='" + objAd.clickUrl + "' target='new'><img src='" + objAd.url + "' width='" + this.width + "' height='" + this.height + "' border='0'></a>";
        }
    }
};

/* Class: WNEventListenerWidget
Creates an event generic listener widget and registers it for Canvas events.
*/
WNEventListenerWidget = function(p_widgetName, p_callBackFunctionName, p_groupId) {
    this.widgetClassType = "WNEventListenerWidget";
    this.idKey = wnVideoUtils.validateString(p_groupId) ? p_groupId : "DEFAULT";
    this.widgetName = p_widgetName;
    this.id = p_widgetName + this.idKey;

    //register for events
    evntMgr.Register({
        lstnr: this,
        evnt: "NewClip",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "NewMedia",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "MediaEnded",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "OpenPane",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "PaneClosed",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
};

/* Class: WNCommentsWidget
Object to handle Moddub comments
*/
// Called by the MD player
var mdVideoSettingsObject; // Do not alter
var mdVideoCommentsObject;
var mdCommentsLoaded = false;

function onMobdubSmilLoad(videoObject) {
    if (mdCommentsLoaded) {
        Mobdub.Comments.loadComments(videoObject);
        mdVideoCommentsObject = null;
    } else {
        mdVideoCommentsObject = videoObject;
    }
}

// Called when comments box has loaded
function onMobdubCommentsReady() {
    mdCommentsLoaded = true;
}

WNCommentsWidget = function(p_widgetName, p_targetDiv, p_groupId) {
    this.widgetClassType = "WNCommentsWidget";
    this.idKey = wnVideoUtils.validateString(p_groupId) ? p_groupId : "DEFAULT";
    this.widgetName = p_widgetName;
    this.id = p_widgetName + this.idKey;
    this.targetDiv = p_targetDiv;
    this.codebaseDomain = "http://partner.mobdub.com";
    this.params = "";
    this.videoWidth = "";
    this.videoHeight = "";
    this.videoMarginTop = "";
    this.videoMarginleft = "";
    this.enableVideoAnnotations = 1;
    var p_callBackFunctionName = "HandleComments";

    //register for events
    evntMgr.Register({
        lstnr: this,
        evnt: "NewClip",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "NewMedia",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "MediaEnded",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "OpenPane",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "PaneClosed",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
    evntMgr.Register({
        lstnr: this,
        evnt: "PaneOpening",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
};

WNCommentsWidget.prototype.SetHeight = function(p_height) {
    document.getElementById(this.targetDiv).style.height = p_height + "px";
    this.height = p_height;
};

WNCommentsWidget.prototype.SetWidth = function(p_width) {
    document.getElementById(this.targetDiv).style.width = p_width + "px";
    this.width = p_width;
};

WNCommentsWidget.prototype.SetCanvasDivName = function(p_canvasDivName) {
    this.canvasDivName = p_canvasDivName;
};

WNCommentsWidget.prototype.SetPartnerId = function(p_mdAffNo) {
    this.mdAffiliateNo = p_mdAffNo;
};

WNCommentsWidget.prototype.SetCodebaseDomain = function(domain) {
    this.codebaseDomain = domain;
};

WNCommentsWidget.prototype.SetTwtFeedUrl = function(feedUrl) {
    try {
        mdTwtFeedUrl = feedUrl;
    } catch (e) {
        var temp = e;
    }
};

var wnEnableAnnotations;
WNCommentsWidget.prototype.EnableVideoAnnotations = function(p_bln) {
    if (p_bln)
        this.enableVideoAnnotations = 1;
    else
        this.enableVideoAnnotations = 0;

    this.ShowVideoAnnotations(p_bln)
};

WNCommentsWidget.prototype.ShowVideoAnnotations = function(p_bln) {
    wnLog('ShowVideoAnnotations: ' + p_bln);
    try {
        if (this.enableVideoAnnotations == 1 && _mdVideoOptions.dubs_display == '1') // _mdVideoOptions is a global object in md's script include
        {
            mdPartnerShowDubs(p_bln); // Function defined in the md js include file
        } else {
            mdPartnerShowDubs(false);
            //_mdVideoOptions.dubs_display = '1';
        }
    } catch (e) {}
};

// Only being used by the OSMF player
function wnMdEnableVideoAnnotations(enable) {
    if (enable) {
        mdPartnerShowDubs(true);
        _mdVideoOptions.dubs_display = "1";
        wnLog('wnMdEnableVideoAnnotations: ' + enable);
    } else if (!enable) {
        mdPartnerShowDubs(false);
        _mdVideoOptions.dubs_display = "0";
        wnLog('wnMdEnableVideoAnnotations: ' + enable);
    }
};

WNCommentsWidget.prototype.AnnotationsActive = function() {
    try {
        if (_mdVideoOptions.dubs_display == '1')
            return true;
        else
            return false;
    } catch (e) {}
};

WNCommentsWidget.prototype.EnableVideoCommentsBox = function(p_bln) {
    this.enableVideoCommentsBox = p_bln;
    if (p_bln) {
        wnVideoUtils.loadScript("mobdubScriptComments", "http://scripts.mobdub.com/videocomments.js")
    }
};

WNCommentsWidget.prototype.SetParams = function(p_params) {
    this.params = p_params;
};

WNCommentsWidget.prototype.SetVideoAnnotationDimension = function(p_width, p_height) {
    this.videoWidth = p_width;
    this.videoHeight = p_height;
};

WNCommentsWidget.prototype.SetVideoAnnotationMargins = function(p_top, p_left) {
    this.videoMarginTop = p_top;
    this.videoMarginleft = p_left;
};

WNCommentsWidget.prototype.SetVideoId = function(p_videoId) {
    this.useVideoId = true;
    this.videoId = p_videoId;
};

var wnClipObj;
var wnIsCanvasOverlayActive = false;
WNCommentsWidget.prototype.HandleComments = function(p_evtObj) {
    if (p_evtObj.evnt == "NewClip") {
        try {
            Mobdub.Comments.clearCommentsPanel();
        } catch (e) {}

        var playerObjId = wnVideoWidgets.canvasObject[this.idKey];
        var clipObj = p_evtObj.objClip;
        wnClipObj = p_evtObj.objClip;

        var wnVideoId = clipObj.id;

        //check to see if there is a valid clip id - otherwise it is assumed to be a MRSS item
        if (wnVideoId == null || wnVideoId == "null" || wnVideoId == undefined || wnVideoId == "") {
            wnVideoId = clipObj.partnerclipid;
        }

        //check to see if there is a valid clip id - otherwise it is assumed to be a MRSS item - failsafe
        if (wnVideoId == null || wnVideoId == "null" || wnVideoId == undefined || wnVideoId == "") {
            wnVideoId = clipObj.flvUri;
        }

        var qs = "&";

        if (this.videoWidth != "") qs = qs + "player_width=" + this.videoWidth;
        if (this.videoHeight != "") qs = qs + "&player_height=" + this.videoHeight;
        if (this.videoMarginTop != "") qs = qs + "&player_margin_top=" + this.videoMarginTop;
        if (this.videoMarginleft != "") qs = qs + "&player_margin_left=" + this.videoMarginleft;

        // TODO: Set global vars here for the plugin file to access in case override settings is needed
        // Mobdub.Comments.video_link_url = [url from api]
        if (this.videoId)
            var scriptSrc = this.codebaseDomain + "/partners/" + this.mdAffiliateNo + "/script.js?video_id=" + this.videoId + "&player_id=" + playerObjId + "&player_div=" + this.canvasDivName + "&title=" + encodeURIComponent(unescape(clipObj.headline)) + qs + "&enable_dubs=" + this.enableVideoAnnotations + "&" + this.params;
        else
            var scriptSrc = this.codebaseDomain + "/partners/" + this.mdAffiliateNo + "/script.js?uri=" + wnVideoId + "&player_id=" + playerObjId + "&player_div=" + this.canvasDivName + "&title=" + encodeURIComponent(unescape(clipObj.headline)) + qs + "&enable_dubs=" + this.enableVideoAnnotations + "&" + this.params;

        wnVideoUtils.loadScript("mobdubScriptPlayer", scriptSrc);
    }

    if (p_evtObj.evnt == "NewMedia") {
        var clipObj = p_evtObj.objClip;
        wnClipObj = p_evtObj.objClip;

        if (clipObj.isPreRoll || clipObj.isPostRoll || clipObj.isFlashVideoBookend) {
            this.ShowVideoAnnotations(false);
            try {
                Mobdub.Comments.stopScrolling();
                mdPartnerShowMobdubSwitch('disabled');
            } catch (e) {}
        } else {
            if (wnIsCanvasOverlayActive != true) {
                this.ShowVideoAnnotations(true);
                try {
                    Mobdub.Comments.startScrolling();
                    mdPartnerShowMobdubSwitch('enabled');
                } catch (e) {}
            }
        }
    }

    if (p_evtObj.evnt == "PaneOpening") {
        wnIsCanvasOverlayActive = true;
        this.ShowVideoAnnotations(false);
        try {
            mdPartnerShowMobdubSwitch('disabled');
        } catch (e) {}

    }

    if (p_evtObj.evnt == "PaneClosed") {
        wnIsCanvasOverlayActive = false;
        try {
            if (wnClipObj.isPreRoll || wnClipObj.isPostRoll || wnClipObj.isFlashVideoBookend) {
                this.ShowVideoAnnotations(false);
                mdPartnerShowMobdubSwitch('disabled');
            } else {
                this.ShowVideoAnnotations(true);
                mdPartnerShowMobdubSwitch('enabled');
            }
        } catch (e) {}
    }
};

/**
 * EventManager class
 * To ensure that all widgets are able to register for events, regardless of the order in which they load on the page, the event object is a javascript object.
 * Since all widgets use this page, the code for the widget object is included with all widgets.
 * To ensure the object is only created once, check to see if it has been created.  If so, do not create.
 * The event object contains an object of listener arrays, a Register function and a Notify function.
 */
WNEventManager = function() {
    this.listeners = {};
};

WNEventManager.prototype.Register = function(evntObj) {
    var lstnr = evntObj.lstnr;
    var func = evntObj.func;
    var evnt = evntObj.evnt;
    var idKey = evntObj.idKey;

    if (typeof this.listeners[idKey] == "undefined") {
        this.listeners[idKey] = new Array();
    }
    var len = this.listeners[idKey].length;
    for (var i = 0; i < len; i++) {
        if (this.listeners[idKey][i].lstnr == lstnr && this.listeners[idKey][i].evnt == evnt) {
            return false;
        }
    }
    this.listeners[idKey].push({
        lstnr: lstnr,
        evnt: evnt,
        func: func
    }); //zr - cross browser/platform?
    return true;
};

WNEventManager.prototype.Notify = function(evntObj, functionObj) {
    var evnt = evntObj.evnt;
    var idKey = evntObj.idKey;

    // Pipe all widget events to a global handler
    if (typeof evntObj.system == 'undefined') {
        evntObj['system'] == 'video';
    }
    evntObj.objClip['hostPlayer'] = 'flash';
    var eventWrap = {
        'event': {
            'type': evntObj.evnt
        },
        'system': evntObj.system,
        'clip': evntObj.objClip,
        'valid': true
    };
    wnVideoWidgets.handleEvent(eventWrap);

    var len = this.listeners[idKey].length;
    var listeners = this.listeners;
    var slideshowAsHtml = false;
    if (wnVideoWidgets.useHtml5 || renderSlideShowAsHtml) slideshowAsHtml = true;

    for (var i = 0; i < len; i++) {
        if (this.listeners[idKey][i].evnt == evnt) {
            var lstnr = this.listeners[idKey][i].lstnr;
            var func = this.listeners[idKey][i].func;
            if (lstnr.widgetClassType == "WNAdWidget" ||
                lstnr.widgetClassType == "WNVideoListener" ||
                lstnr.widgetClassType == "WNEventListenerWidget" ||
                lstnr.widgetClassType == "WNCommentsWidget") {
                lstnr[func](evntObj);
            } else if (lstnr.toString().indexOf("WNVideoCanvas") > -1 && wnVideoWidgets.useAmpPlayer ||
                (wnVideoWidgets.useHtml5 &&
                    (lstnr.toString().indexOf("WNVideoCanvas") > -1 ||
                        lstnr.toString().indexOf("WNGallery") > -1)
                ) ||
                lstnr.toString().indexOf("WNImageCanvas") > -1 && slideshowAsHtml ||
                lstnr.toString().indexOf("WNImageGallery") > -1 && slideshowAsHtml) {
                //$wn("#" + lstnr)[func](evntObj);
                $wn("#" + lstnr).data("widget")[func](evntObj);
            } else if ((lstnr.toString().indexOf("WNHeadline") > -1 ||
                    lstnr.toString().indexOf("WNInfoPane") > -1) &&
                $wn("#" + lstnr).data("widget") != undefined &&
                $wn("#" + lstnr).data("widget")[func] != null) {
                $wn("#" + lstnr).data("widget")[func](evntObj);
            } else //use flash external interface methodology to call functions on swf's
            {
                try {
                    if (document.getElementById(lstnr)[func]) {
                        document.getElementById(lstnr)[func](evntObj);
                    }
                } catch (e) {
                    wnLog("Target SWF element '" + lstnr + "' is missing on the page. '" + func + "' handler for the '" + evnt + "' event is not defined.");
                }
            }
        }
    }
    if (evnt == "NewClip" && getWigdetByType(idKey, "c") != null) {
        getWigdetByType(idKey, "c").clipObj = evntObj.objClip; // add clipObj as Canvas property in the pageWidgets array
    }
};

WNCanvasListener = function(p_callBackFunctionName, p_groupId) {
    this.widgetClassType = "WNVideoListener";
    this.idKey = wnVideoUtils.validateString(p_groupId) ? p_groupId : "DEFAULT";
    evntMgr.Register({
        lstnr: this,
        evnt: "ModelInitiated",
        func: p_callBackFunctionName,
        idKey: this.idKey
    });
};

/* Class: PlayClipObject
- instantiated once when WNVideo is loaded, no need to instantiate the class
- NewClipId: receives a clip id string and passes to the VideoCanvas if available.  If not, passes it to the PopVideoPlayer method
*/

/**
Unexposed methods
- PopVideoPlayer: called by NewClipObject and NewClipId when a VideoCanvas widget is not present. Pops a landing page
- NewClipObject: receives a clip object and passes it to the VideoCanvas when available, if not, extracts the clip id and passes it to the PopVideoPlayer method
*/
WNPlayClipObject = function() {};

WNPlayClipObject.prototype.PopVideoPlayer = function(evntObj) {
    var landingPage = wnVideoWidgets.landingPage;
    if (wnVideoUtils.validateString(landingPage)) {
        var params = "clipId=" + evntObj.objClip.id + "&topVideoCatNo=" + evntObj.topCatId + "&autoStart=true";
        if (evntObj.objClip.popupLandingPage == "true") {
            var popWin = window.open(landingPage + params, "WNFlashLandingPage");
        } else {
            window.location = landingPage + params;
            return;
        }
        try {
            if (popWin != null) {
                popWin.focus();
            }
        } catch (e) {
            self.blur();
        }
    } else {
        // open popup player by using existing function from VideoFunctions.js\
        playVideo(evntObj.objClip.id,
            evntObj.objClip.headline,
            evntObj.objClip.vt,
            evntObj.objClip.strAdTarget,
            evntObj.objClip.duration,
            evntObj.objClip.adTag,
            evntObj.objClip.strQueryStringParams,
            //evntObj.objClip.ownerAffiliateBaseUrl,
            wnVideoWidgets.objClip.objClip.domain.replace('http://', ''),
            evntObj.objClip.siteDefaultFormat);
    }
};

WNPlayClipObject.prototype.NewClipObject = function(obj) {

    //var elemId = wnVideoUtils.validateString(obj.idKey)?videoCanvasId + obj.idKey:videoCanvasId;
    wnVideoWidgets.objClip = obj;
    for (var i in wnVideoWidgets.canvasObject) {
        if (i == obj.idKey) {
            var elemId = wnVideoWidgets.canvasObject[i];
        }
    }
    var videoCanvas = document.getElementById(elemId);
    if (videoCanvas != null) {
        // does the div tag containing the silverlight canvas exist
        var canvasSliverlightDivName = elemId + "WNSilverlight";
        var canvasSliverlightDiv = document.getElementById(canvasSliverlightDivName);
        if (canvasSliverlightDiv != undefined && canvasSliverlightDiv.style.display != "none") {
            WNSilverlightStopClip();
            // hide the silverlight canvas
            canvasSliverlightDiv.style.display = "none";
            // show the flash canvas
            videoCanvas.width = WNSilverlightCanvasWidth;
            videoCanvas.height = WNSilverlightCanvasHeight;
        }
        if (wnVideoWidgets.useAmpPlayer) {
            $wn("#" + elemId).data("widget")['loadClip'](obj.objClip, true);
        } else {
            // document.getElementById(lstnr)[func](evntObj);
            videoCanvas.wnFlashVideoCanvas_PlayClip(obj.objClip);
        }
    } else if (obj.objClip.isUserInitiated) {
        this.PopVideoPlayer(obj);
    }
    //}
    //else if(wnPreferredVideoFormat=="WMV")
    //{
    //if(Silverlight.isInstalled('1.0'))
    //{
    //WNSilverlightTrigger(obj);
    //}
    //}

    return true;
};

/* Method: NewClipId
- Takes clip id and collection id (optional)
- Plays clip on the canvas associated with the collection id
*/
WNPlayClipObject.prototype.NewClipId = function(id, idKey, strHeadline, strType, strAdTarget, strDuration, strlaunchPageAdTag, strQueryStringParams, strHostDomain, strFormat) {
    //var elemId = wnVideoUtils.validateString(idKey)?videoCanvasId + idKey:videoCanvasId;
    var idKey = wnVideoUtils.validateString(idKey) ? idKey : "DEFAULT";
    for (var i in wnVideoWidgets.canvasObject) {
        if (i == idKey) {
            var elemId = wnVideoWidgets.canvasObject[i];
        }
    }
    var videoCanvas = document.getElementById(elemId)
    if (videoCanvas != null) {
        if (wnVideoWidgets.useHtml5) {
            //wnVideoWidgets.playHtmlVideo({ 'clipId': id }, true, idKey);
            //$wn("#"+elemId).playClipById(id);
            $wn("#" + lstnr).data("widget")[func](evntObj);
        } else {
            videoCanvas.wnFlashVideoCanvas_PlayClip({
                id: id,
                isUserInitiated: true
            });
        }
    } else {
        var clip = {};
        if (id != "") clip.id = id;
        if (strHeadline != "") clip.headline = strHeadline;
        if (strType != "") clip.vt = strType;
        if (strAdTarget != "") clip.strAdTarget = strAdTarget;
        if (strDuration != "") clip.duration = strDuration;
        if (strlaunchPageAdTag != "") clip.adTag = strlaunchPageAdTag;
        if (strQueryStringParams != "") clip.strQueryStringParams = strQueryStringParams;
        if (strHostDomain != "") clip.ownerAffiliateBaseUrl = strHostDomain;
        if (strFormat != "") clip.siteDefaultFormat = strFormat;
        this.PopVideoPlayer({
            objClip: clip
        });
    }
    return true;
};




/* Class: SlideshowImage
- instantiated once when WNVideo is loaded, no need to instantiate the class
- receives an image object and passes to the Slideshow Canvas if available. If not, goes to the slideshow landingPage
*/

WNSlideshowImage = function() {};

WNSlideshowImage.prototype.NewSlideshowImage = function(obj) {
    var slideShowCanvas = {};
    //slideShowCanvas.headline = "Title not provided";
    for (var wdgt in pageWidgets[obj.groupId]) {
        if (pageWidgets[obj.groupId][wdgt].widgetClassType == "WNImageCanvas") {
            slideShowCanvas = pageWidgets[obj.groupId][wdgt];
        }
        if (pageWidgets[obj.groupId][wdgt].widgetClassType == "WNImageGallery") {
            /*pageWidgets[obj.groupId][wdgt].isPrimaryGallery != undefined &&
            pageWidgets[obj.groupId][wdgt].isPrimaryGallery == "true") { // return only primary gallery*/
            imageGallery = pageWidgets[obj.groupId][wdgt];
            //var nextImageDelay = (imageGallery.delay != undefined) ? imageGallery.delay : 3;
            var nextImageDelay = wnSiteConfigVideo.imageCanvasWidget.slideTime * 1;
            if (nextImageDelay < 1) nextImageDelay = 3;
            landingPage = pageWidgets[obj.groupId][wdgt].landingPage;
            if (!wnVideoUtils.validateString(landingPage)) {
                landingPage = "http://" + pageWidgets[obj.groupId][wdgt].hostDomain + "/global/slideshow.asp?";
            } else {
                var q = landingPage.lastIndexOf("?");
                var amp = landingPage.lastIndexOf("&");
                var len = landingPage.length;
                if (q > 0) {
                    if (q == len || amp == len) {
                        landingPage += "";
                    } else {
                        landingPage += "&";
                    }
                } else {
                    landingPage += "?";
                }
            }
        }
        /*if (pageWidgets[obj.groupId][wdgt].widgetClassType == "WNHeadline") {
        slideShowCanvas.headline = pageWidgets[obj.groupId][wdgt].slideshowHeadline;
        }*/
    }
    if (document.getElementById(slideShowCanvas.domId) != null) {
        // Handle In Canvas Ad

        // Close ad (every new image closes previous ad)
        this.hideInCanvasAd({
            groupId: obj.groupId
        });

        // Check if new ad should be shown
        if ((slideShowCanvas.isInCanvasAdModeEnabled == "true" ||
                slideShowCanvas.isInCanvasAdModeEnabled == true) &&
            slideShowCanvas.playerWidth >= 300 && slideShowCanvas.playerHeight >= 250) {
            if (slideShowCanvas.inCanvasAdIterator == undefined) {
                if (slideShowCanvas.inCanvasAdFrequency == 0) slideShowCanvas.inCanvasAdIterator = -1; // show ad only once (on load)
                else slideShowCanvas.inCanvasAdIterator = 0; // show according to the frequency
            }
            slideShowCanvas.inCanvasAdIterator++;
            if (slideShowCanvas.inCanvasAdIterator == slideShowCanvas.inCanvasAdFrequency &&
                slideShowCanvas.inCanvasAdFrequency > -1) {
                obj.disableInCanvasAd = false;
                //obj.adTag = slideShowCanvas.adTag;
                slideShowCanvas.inCanvasAdIterator = 0;

                var slideshowAdDivName = slideShowCanvas.domId + "_adDiv";
                var patern = "WNImageCanvas" + slideShowCanvas.idKey;
                slideshowAdDivName = slideshowAdDivName.replace("WNImageCanvas" + slideShowCanvas.idKey, "");
                if (document.getElementById(slideshowAdDivName) != null) {
                    document.getElementById(slideshowAdDivName).style.zIndex = 150;
                    document.getElementById(slideshowAdDivName).style.visibility = "visible";
                    document.getElementById(slideShowCanvas.domId).style.zIndex = -1;
                    document.getElementById(slideshowAdDivName + "_close").onclick = function() {
                        document.getElementById(slideshowAdDivName).style.zIndex = -1;
                        document.getElementById(slideShowCanvas.domId).style.zIndex = 4;
                        document.getElementById(slideshowAdDivName + "_ad").innerHTML = "";
                        //evntMgr.Notify({ evnt: "SkipInCanvasAd", idKey: obj.groupId, objClip: obj });
                        wnSlideshowImage.hideInCanvasAd({
                            groupId: obj.groupId,
                            byTimer: true
                        });
                    };
                    document.getElementById(slideshowAdDivName + "_close").onmouseover = function() {
                        document.body.style.cursor = "pointer";
                    }
                    document.getElementById(slideshowAdDivName + "_close").onmouseout = function() {
                        document.body.style.cursor = "auto";
                    }
                }
                evntMgr.Notify({
                    evnt: "NewInCanvasAd",
                    idKey: obj.groupId,
                    objClip: obj
                });
                if (slideShowCanvas.htmlVersion != null && slideShowCanvas.htmlVersion.resumeAfterAd) {
                    if (slideShowCanvas.adTimer != null) clearTimeout(slideShowCanvas.adTimer);
                    slideShowCanvas.adTimer = setTimeout("wnSlideshowImage.hideInCanvasAd({groupId: '" + obj.groupId + "', byTimer:true})", nextImageDelay * 1000);
                }
            } else {
                obj.disableInCanvasAd = true;
            }
        }

        // Handle Companion Ad
        if (slideShowCanvas.companionAdIterator == undefined) {
            if (slideShowCanvas.companionAdFrequency == 0) slideShowCanvas.companionAdIterator = -1; // show ad only once (on load)
            else slideShowCanvas.companionAdIterator = 0; // show according to the frequency
        }
        slideShowCanvas.companionAdIterator++;
        if (slideShowCanvas.companionAdIterator == slideShowCanvas.companionAdFrequency &&
            slideShowCanvas.companionAdFrequency > -1) {
            obj.disableCampaignAd = false;
            //obj.adTag = slideShowCanvas.adTag;
            slideShowCanvas.companionAdIterator = 0;

            // refresh the "Medium Rectange A" ad
            Worldnow.AdMan.getAds('43', function(ad) {
                //owner = 'worldnow';
                var owner = ad.get('owner');
                if (ad.manager.eventsHistory.hasOwnProperty(owner) && ad.manager.eventsHistory[owner].indexOf('pageready') > -1) {
                    ad.load();
                }
            });
        } else {
            obj.disableCampaignAd = true;
        }

        // Analytics
        try {
            var slidreport = Namespace_VideoReporting_Worldnow.logSlideShowEventParameters(
                "ImageView",
                encodeURIComponent(escape(obj.headline)),
                encodeURIComponent(escape(obj.slideshowHeadline)),
                obj.widgetId,
                obj.url,
                obj.adTag,
                slideShowCanvas.hostDomain,
                slideShowCanvas.affiliate,
                slideShowCanvas.isInCanvasAdModeEnabled,
                obj.index);
        } catch (e) {};

        try {
            eval("addThisWNDynamicWidgetId = " + obj.widgetId)
        } catch (e) {};

        /* Adding ad info */
        obj.preRollSplitPct = 100;
        obj.preRollOwner = "loc";

        evntMgr.Notify({
            evnt: "NewClip",
            idKey: obj.groupId,
            objClip: obj,
            system: 'slidedhow'
        });
    } else if (!obj.onGalleryLoad) {
        var index = (obj.index != undefined) ? (obj.index + 1) : obj.id;
        var params = "widgetId=" + obj.widgetId + "&slideshowImageId=" + index;
        if (imageGallery.popupLandingPage == "true" || imageGallery.popupLandingPage == true) {
            window.open(landingPage + params, "WNFlashLandingPage");
        } else {
            window.location = landingPage + params;
        }
    }
};

WNSlideshowImage.prototype.hideInCanvasAd = function(obj) {
    var adWasOpen = false;
    var slideShowCanvas = {};
    var slideShowCanvas_domId;
    for (var wdgt in pageWidgets[obj.groupId]) {
        if (pageWidgets[obj.groupId][wdgt].widgetClassType == "WNImageCanvas") {
            slideShowCanvas = pageWidgets[obj.groupId][wdgt];
            slideShowCanvas_domId = pageWidgets[obj.groupId][wdgt].domId;
        }
    }
    if (document.getElementById(slideShowCanvas_domId) != null) {
        var slideshowAdDivName = slideShowCanvas_domId + "_adDiv";
        slideshowAdDivName = slideshowAdDivName.replace("WNImageCanvas" + obj.groupId, "");
        if (document.getElementById(slideshowAdDivName) != null) {
            if (document.getElementById(slideshowAdDivName).style.zIndex != -1) adWasOpen = true;
            document.getElementById(slideshowAdDivName).style.zIndex = -1;
            document.getElementById(slideshowAdDivName).style.visibility = "hidden";
            document.getElementById(slideShowCanvas_domId).style.zIndex = 100;
            document.getElementById(slideshowAdDivName + "_ad").innerHTML = "";
        }
    }
    //evntMgr.Notify({ evnt: "SkipInCanvasAd", idKey: obj.groupId, objClip: obj });
    if (slideShowCanvas.adTimer != null) clearTimeout(slideShowCanvas.adTimer);
    if (obj.byTimer) evntMgr.Notify({
        evnt: "InCanvasAdOver",
        idKey: obj.groupId,
        objClip: obj
    });
    return adWasOpen;
};

/*
WNSlideshowImage.prototype.showHideAddThis = function (obj) {
    var slideShowCanvas = new Object();
    for (var wdgt in pageWidgets[obj.groupId]) {
        if (pageWidgets[obj.groupId][wdgt].widgetClassType == "WNImageCanvas") {
            slideShowCanvas = pageWidgets[obj.groupId][wdgt];
        }
    }
    if (document.getElementById(slideShowCanvas.domId) != null) {
        var slideshowAddThisDivName = slideShowCanvas.domId + "_addThis";
        slideshowAddThisDivName = slideshowAddThisDivName.replace("WNImageCanvas" + slideShowCanvas.idKey, "");
        if (obj.isVisible) {
            c = document.getElementById(slideshowAddThisDivName);
            if (c != null) {
                document.getElementById(slideshowAddThisDivName).style.zIndex = 200;
                document.getElementById(slideShowCanvas.domId).style.zIndex = -1;
            }
        } else {
            if (c != null) {
                document.getElementById(slideshowAddThisDivName).style.zIndex = -1;
                document.getElementById(slideShowCanvas.domId).style.zIndex = 200;
            }
        }
    }
};
*/

/**
- Instantiate the EventManager, PlayClipObject and VideoWidgets
- Make callBackFunctions
*/
var wnPlayClip = new WNPlayClipObject();
var wnSlideshowImage = new WNSlideshowImage();
var evntMgr = new WNEventManager();
var wnVideoWidgets = new WNVideoWidgets();
try {
    for (var i in wnCallBackFunctions) {
        wnCallBackFunctions[i]();
    }
} catch (e) {}

/**
- place holder for Motif HTML ad units
*/
function syncRoadBlock(adTag) {}

if (pageWidgets == undefined) var pageWidgets = new Array();
if (pageAds == undefined) var pageAds = new Array();

function getWigdetByType(groupId, type) { // returns first widget of the specified type in the group
    var dependChars = new Array();
    dependChars["c"] = "WNVideoCanvas";
    dependChars["g"] = "WNGallery";
    dependChars["h"] = "WNHeadline";
    dependChars["i"] = "WNInfoPane";
    dependChars["t"] = "WNTicker";
    dependChars["ic"] = "WNImageCanvas";
    dependChars["ig"] = "WNImageGallery";
    dependChars["c2"] = "WNVideoCanvas2";
    var type = dependChars[type];
    if (type == "WNGallery") { // return only primary gallery
        for (var wdgt in pageWidgets[groupId]) {
            if (pageWidgets[groupId][wdgt].widgetClassType == type &&
                pageWidgets[groupId][wdgt].isPrimaryGallery != undefined &&
                (pageWidgets[groupId][wdgt].isPrimaryGallery == "true" || pageWidgets[groupId][wdgt].isPrimaryGallery == true)) {
                return pageWidgets[groupId][wdgt];
            }
        }
    } else {
        for (var wdgt in pageWidgets[groupId]) {
            if (pageWidgets[groupId][wdgt].widgetClassType == type) return pageWidgets[groupId][wdgt];
        }
        // if nothing found and type was "c" (videoCanvas), try to find "c2" (videoCanvas2)
        if (type == "WNVideoCanvas") {
            for (var wdgt in pageWidgets[groupId]) {
                if (pageWidgets[groupId][wdgt].widgetClassType == "WNVideoCanvas2") return pageWidgets[groupId][wdgt];
            }
        }
    }
    return {};
}

function onWidgetLoad(domId, groupId, callBackName, dependsOn, data) { // callBackName - string, dependsOn - char
    pageWidgets[groupId][domId].callBackName = callBackName;
    pageWidgets[groupId][domId].intDependsOn = wnVideoUtils.validateString(dependsOn) ? dependsOn : "";
    pageWidgets[groupId][domId].data = data; // currently only Gallery send data (clip object)
    pageWidgets[groupId][domId].isLoaded = true;

    wnLog(pageWidgets[groupId][domId].widgetClassType + " is Loaded");

    executeCallBack(groupId, domId);
    executeAllCallBacks(groupId);
}

function executeCallBack(groupId, domId) {
    if (pageWidgets[groupId][domId].isLoaded && // if widget itself is ready
        pageWidgets[groupId][domId].callBackName != undefined && // if it has callback function assigned
        pageWidgets[groupId][domId].isReady == false) // and callback function wasn't successfully executed yet
    {
        // check if all required for execution widgets are ready
        var readyToBeCalled = true;
        var chr = 0;
        var dependsOn = pageWidgets[groupId][domId].intDependsOn;
        for (chr = 0; chr < dependsOn.length; chr++) {
            type = dependsOn.charAt(chr);
            if (getWigdetByType(groupId, type).isReady == false) {
                readyToBeCalled = false;
                break;
            }
        }
        if (readyToBeCalled) {
            eval(pageWidgets[groupId][domId].callBackName + "('" + domId + "', '" + groupId + "')");
            pageWidgets[groupId][domId].isReady = true;
            wnLog(pageWidgets[groupId][domId].widgetClassType + " calling isReady() -  ready!");
        }

        // execute API assigned callback function
        /*if (pageWidgets[groupId][domId].isReady &&                            // if widget itself is ready
        pageWidgets[groupId][domId].callBackFunction != undefined &&        // if it has callback function assigned
        pageWidgets[groupId][domId].callBackExecuted == undefined)      // and callback function wasn't executed yet
        {
        var readyToBeCalled = true;
        var chr=0;
        var dependsOn = pageWidgets[groupId][domId].dependsOn == undefined ? "" : pageWidgets[groupId][domId].dependsOn;
        // check if all required for execution widgets are ready
        for (chr=0; chr < dependsOn.length; chr++) {
        type = dependsOn.charAt(chr);
        if (getWigdetByType(groupId, type).isReady == false) {
        readyToBeCalled = false;
        break;
        }
        }
        if (readyToBeCalled) {
        pageWidgets[groupId][domId].callBackExecuted = true;    // mark as executed
        var cleanCallBackString = pageWidgets[groupId][domId].callBackFunction.replace(/[^A-Z0-9_]/gi, "");
        eval(cleanCallBackString+"()");                     // call the function
        callBacks(groupId);                                 // iterate again for the widgets dependent on current widget
        }
        }*/
    }
}

function executeAllCallBacks(groupId) {
    for (var wdgt in pageWidgets[groupId]) executeCallBack(groupId, wdgt);

    var widgetsAreLoaded = true;
    var widgetsAreReady = true;
    for (var wdgt in pageWidgets[groupId]) {
        if (pageWidgets[groupId][wdgt].isLoaded == false) widgetsAreLoaded = false;
        if (pageWidgets[groupId][wdgt].isReady == false) widgetsAreReady = false;
    }
    if (widgetsAreLoaded && !widgetsAreReady) {
        wnLog("Not all the widgets are ready yet. Run again.");
        callBacks(groupId);
    }
}

function galleryIsReady(domId, groupId) {
    if (wnVideoWidgets.useAmpPlayer) {
        var provider = getWigdetByType(groupId, "g");
        var clipObj = provider.data.objClip;
        //if (clipObj.uri && this.fV.isPrimaryGallery) {
        if (clipObj.flvUri) {
            clipObj.isUserInitiated = false;
            //clipObj.domId = this.fV.domId;
            clipObj.ordinance = 0;
            evntMgr.Notify({
                evnt: "NewGalleryClip",
                idKey: groupId,
                objClip: clipObj
            });
            //evntMgr.Notify({ evnt: "NewClip", idKey: groupId, objClip: clipObj });
        }
    }
}

function canvasIsReady(domId, groupId) {
    if (!wnVideoUtils.validateString(pageWidgets[groupId][domId].clipId)) {
        var provider = getWigdetByType(groupId, pageWidgets[groupId][domId].intDependsOn);
        pageWidgets[groupId][domId].clipObj = provider.data.clipObj;
        wnPlayClip.NewClipObject(provider.data);
    }
}

function headlineIsReady(domId, groupId) {
    var provider = getWigdetByType(groupId, pageWidgets[groupId][domId].intDependsOn);
    clipObj = provider.clipObj;
    // if videoCanvas info is missing try imageCanvas
    if (!clipObj) {
        provider = getWigdetByType(groupId, "ic").htmlVersion;
        if (provider != undefined) clipObj = provider.currentImgObj;
    }
    document.getElementById(domId).wnSetHeadline({
        objClip: clipObj
    });
}

function infoPaneIsReady(domId, groupId) {
    var provider = getWigdetByType(groupId, pageWidgets[groupId][domId].intDependsOn);
    clipObj = provider.clipObj;
    // if videoCanvas info is missing try imageCanvas
    if (!clipObj) {
        provider = getWigdetByType(groupId, "ic").htmlVersion;
        if (provider != undefined) clipObj = provider.currentImgObj;
    }
    document.getElementById(domId).wnShowInfo({
        objClip: clipObj
    });
}

function tickerIsReady(domId, groupId) {
    var provider = getWigdetByType(groupId, pageWidgets[groupId][domId].intDependsOn);
    document.getElementById(domId).wnDisplayText({
        tickerItems: provider.data.tickerItems
    });
}

function imageCanvasIsReady(domId, groupId) {
    // show addThis and incanvasAd of the Image Canvas
    var imageCanvas = getWigdetByType(groupId, "ic");
    var addThisDiv = document.getElementById(imageCanvas.addThisDivId);
    var incanvasAdDiv = document.getElementById(imageCanvas.incanvasAdDivId);
    if (addThisDiv) addThisDiv.style.visibility = "visible";
    if (incanvasAdDiv) incanvasAdDiv.style.visibility = "visible";
}

function canvas2IsReady(domId, groupId) {
    var canvas = pageWidgets[groupId][domId];
    if (!wnVideoUtils.validateString(canvas.clipId)) {
        var provider = getWigdetByType(groupId, canvas.intDependsOn);
        if (!provider.hasOwnProperty('data')) return false;
        if (canvas.autostart) {
            canvas.clipObj = provider.data.clipObj;
            wnPlayClip.NewClipObject(provider.data);
        } else {
            var videoCanvas = document.getElementById(domId);
            videoCanvas.wnFlashVideoCanvas2_FetchGalleryClip(provider.data.objClip);
        }
    }
}

function wnVideoReloadCompanionAds(adProps, clipObj) {
    var companionProps = {
        "wncc": clipObj.adTag,
        "apptype": clipObj.apptype,
        "owner": adProps.owner
    };

    if (wn_isMobile && wn_isPlatformSiteMobile) {
        companionProps.apptype = 'mob';
    }

    var i;
    var adArr = wnCompanionAds.split(",");

    for (i = 0; i < adArr.length; i++) {
        adArr[i] = wnVideoUtils.trim(adArr[i]);
        Worldnow.AdMan.reloadAd(adArr[i], companionProps);
    }
}

function wnGetVastAdHtml(vastAd, adWidget) {
    var vastHTML = "";

    if (vastAd.iframeResource) // If iframe
    {
        vastHTML = "<iframe id='vastAdId' src='" + vastAd.iframeResource + "' width='" + vastAd.width + "' height='" + vastAd.height + "' frameborder='0' scrolling='no' allowTransparency='true' marginwidth='0' marginheight='0'></iframe>";
    } else if (vastAd.htmlResource) {
        vastHTML = vastAd.htmlResource;
    } else if (vastAd.staticResource) {
        if (vastAd.staticResourceCreativeType.indexOf('image') >= 0) {
            vastHTML = "<a href='" + vastAd.companionClickThrough + "'><img id='vastAdId' src='" + vastAd.staticResource + "' width='" + vastAd.width + "' height='" + vastAd.height + "' border='0' /></a>";
        }
        if (vastAd.staticResourceCreativeType.indexOf('flash') >= 0) {
            // render flash template
            //swfobject.embedSWF(vastAd.staticResource, adWidget.targetElement.id, vastAd.width, vastAd.height, 10, null, {}, {}, {});
            vastHTML = '<object data="' + vastAd.staticResource + '" type="application/x-shockwave-flash" id="vastCompanion' + vastAd.width + 'x' + vastAd.height + '" width="' + vastAd.width + '" height="' + vastAd.height + '">' +
                '<param name="movie" value="' + vastAd.staticResource + '" />' +
                '<param name="bgcolor" value="#ffffff" />' +
                '<param name="height" value="' + vastAd.height + '" />' +
                '<param name="width" value="' + vastAd.width + '" />' +
                '<param name="quality" value="high" />' +
                '<param name="menu" value="false" />' +
                '<param name="allowscriptaccess" value="samedomain" />' +
                '<embed src="' + vastAd.staticResource + '" quality="high" width="' + vastAd.width + '"' +
                '   height="' + vastAd.height + '" name="vastCompanion' + vastAd.width + 'x' + vastAd.height + '" align=""' +
                '   type="application/x-shockwave-flash"' +
                '   plug inspage="http://www.macromedia.com/go/getflashplayer"> ' +
                '</object>';
        }
    }

    return vastHTML;
}


// BEGIN HTML WIDGETS

/* Class: WNVideoGallery
- HTML version of the Flash video gallery widget
*/
WNVideoGallery = function(parentDivId, flashVars) {
    this.fV = flashVars;
    this.widgetClassType = "WNGallery";
    this.parentDivId = parentDivId;
    this.parentJSName = this.widgetClassType + this.fV.widgetId;
    this.groupId = this.fV.idKey;
    this.categoryId = this.fV.topVideoCatNo,
        this.currentPage = 1;
    this.tabsLoaded = false;
    this.hasClipAssigned = (this.fV.clipId * 1 > 0);
    this.assignedClipIsLoaded = false;
    this.categoryCache = {};
    if (this.fV.isPrimaryGallery !== true) {
        if (wnVideoUtils.validateString(this.fV.isPrimaryGallery) && this.fV.isPrimaryGallery.toLowerCase() == "true") {
            this.fV.isPrimaryGallery = true;
        } else {
            this.fV.isPrimaryGallery = false;
        }
    }
    if (this.fV.isContinuousPlay !== true) {
        if (wnVideoUtils.validateString(this.fV.isContinuousPlay) && this.fV.isContinuousPlay.toLowerCase() == "true") {
            this.fV.isContinuousPlay = true;
        } else {
            this.fV.isContinuousPlay = false;
        }
    }

    // Respond to screen width
    if (wn_isMobile) {
        var div = $wn("#" + this.parentDivId);
        this.fV.widgetsContainer = div.parent();
        var leftOffset = parseInt(div.position().left);
        var oldWidth = parseInt(this.fV.playerWidth) + leftOffset;
        var newWidth = div.parent().parent().width() - leftOffset;
        if (newWidth < oldWidth) {
            var newColumnsNum = Math.floor(this.fV.columns * newWidth / oldWidth);
            newColumnsNum = (newColumnsNum > 1) ? newColumnsNum : 1;
            this.fV.columns = newColumnsNum;
            this.fV.playerWidth = newWidth;
        }
        // resize widgets group container
        containerWidth = div.parent().parent().width();
        oldWidth = div.parent().width();
        if (containerWidth < oldWidth) div.parent().width(containerWidth);
        // Reposition according to the new video canvas size
        var canvas = getWigdetByType(this.groupId, "c");
        if (canvas != {} &&
            canvas.playerHeight < canvas.originalHeight &&
            // are in the same container
            canvas.widgetsContainer.attr("id") == div.parent().attr("id") &&
            // position relative to the canvas
            div.position().top >= (canvas.top + canvas.originalHeight - 10) &&
            div.position().left < (canvas.left + canvas.originalWidth - 10)) {
            var diff = canvas.originalHeight - canvas.playerHeight;
            div.css("top", Math.floor(div.position().top - diff));
            // resize widgets group container
            if (canvas.originalContainerHeight == div.parent().height()) {
                div.parent().height(div.parent().height() - diff);
            }
        }
        this.fV.top = div.position().top;
        this.fV.left = div.position().left;
    }

    this.render();
};

WNVideoGallery.prototype.render = function() {
    var contentItemId, RESTContentType, categoryTabs = '';
    this.tabsLoaded = false;
    this.isStoryVideo = false;
    // check for topvideo category - if not passed in then use root association id
    if (this.categoryId && this.categoryId != '') {
        contentItemId = this.categoryId;
        RESTContentType = 'categories';
    } else if (this.fV.storyId && this.fV.storyId != '') {
        // It's a feature story A json call
        contentItemId = this.fV.storyId;
        RESTContentType = 'stories';
        this.contentType = 'stories';
        this.isStoryVideo = true; // temp bug fix
    } else {
        // If no content id is supplied then use the root video category
        try {
            contentItemId = wn.rootassociations.pop_video_top;
        } catch (e) {
            contentItemId = '0';
        }

        RESTContentType = 'categories';
    }

    if (this.contentType != 'stories') {
        categoryTabs = '<ul class="categories"><li class="more"><select id="moreCategorySelect"><option> More </option></select></li></ul>';
    }

    this.context = '#' + this.fV.domId + "_swfObject";
    $wn(this.context).width(this.fV.playerWidth);

    $wn(this.context).html('<div id="' + this.fV.domId + '" class="wn-gallery">' + categoryTabs + '<div class="video-page-container"><div id="wnVideoLoadingMsg">Loading Videos...</div></div><div class="clear"></div><div class="pager"><a href="#" class="prev">Previous</a><div class="page-numbers-container"><ul class="page-numbers"></ul></div><a href="#" class="next">Next</a><div></div></div><div class="wn-controls-cover"></div>');
    this.controlsCover = $wn('#' + this.fV.domId).find('.wn-controls-cover');
    this.controlsCover.width(this.fV.playerWidth).height(this.fV.playerHeight).hide();

    this.contentType = 'categories';
    this.categoryId = contentItemId;

    //register for events
    $wn("#" + this.fV.domId).data("widget", this);
    evntMgr.Register({
        evnt: "ClipEnded",
        func: "onClipEnded",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });

    this.getContentJSON(contentItemId, RESTContentType);
};

WNVideoGallery.prototype.enableControls = function(isEnabled) {
    if (isEnabled) {
        this.controlsCover.hide();
    } else {
        this.controlsCover.show();
    }
}

WNVideoGallery.prototype.loadGallery = function(categoryJSON) {
    // Add the data to client cache
    if (!this.categoryCache[categoryJSON.categories[0].id]) {
        this.categoryCache[categoryJSON.categories[0].id] = categoryJSON;
    }
    this.loadGalleryTabs(categoryJSON); // Load the tabs which will be consistent for the sesssion
    this.loadGalleryClips(categoryJSON, null); // Load the first gallery data
};

WNVideoGallery.prototype.loadGalleryClip = function(clipJSON) {
    this.loadGalleryClips(null, clipJSON);
};

WNVideoGallery.prototype.prependClip = function(gallery, clip) {
    var galleryClips = gallery.clips;
    var newClip = clip.clips[0];
    for (i = 0; i < gallery.clips.length; i++) {
        if (galleryClips[i].id == newClip.id) galleryClips.splice(i, 1);
    }
    galleryClips.unshift(newClip);
};

WNVideoGallery.prototype.loadGalleryTabs = function(data) {
    if (this.tabsLoaded) return false; // Exit - do not overwrite tabs as the main category has already loaded

    this.totalTabs = Math.floor((this.fV.playerWidth - 80) / 130); // Each tab is 130px wide; subtract 70px for the "more" dropdown
    /* DE14229 - user sets rows and columns to 0 */
    if (this.fV.rows < 1) this.fV.rows = 1;
    if (this.fV.columns < 1) this.fV.columns = 1;

    this.clipsPerPage = this.fV.rows * this.fV.columns;
    this.totalCategories = data.categories.length - 1;
    this.totalClips = data.clips.length - 1;
    this.totalPages = Math.ceil(this.totalClips / this.clipsPerPage);

    var mythis = this;
    var output = '';
    var output2 = '';

    $wn.each(data.categories, function(index, value) {
        var category = data.categories[index];
        if (category.headline != null) { // check for undefined
            // loop through categories
            if (index < mythis.totalTabs) {
                if (index == 0) {
                    output = output + '<li><a categoryId="' + category.id + '" href="#" class="tabs active">' + category.headline + '</a></li>';
                } else {
                    output = output + '<li><a categoryId="' + category.id + '" class="tabs" href="#">' + category.headline + '</a></li>';
                }
            } else {
                output2 = output2 + '<option value="' + category.id + '">' + category.headline + '</option>';
                $wn('.categories .more', mythis.context).show();
            }
        }
    });

    $wn('ul.categories', mythis.context).append(output);
    $wn('ul.categories select', mythis.context).append(output2);

    if ($wn('ul.categories select', mythis.context).width() > (this.fV.playerWidth - 10)) {
        $wn('ul.categories select', mythis.context).css("width", (this.fV.playerWidth - 10));
    }

    // Bind category tabs event handlers
    $wn('.categories .tabs', mythis.context).click(function() {
        $wn('.categories .tabs').removeClass('active');
        $wn(this).addClass('active');
        // Load new category
        mythis.getContentJSON($wn(this).attr('categoryId'), 'categories');
        return false;
    });
    $wn('.categories #moreCategorySelect', mythis.context).change(function() {
        $wn('.categories .tabs').removeClass('active');

        // Load new category
        mythis.getContentJSON($wn(this).val(), 'categories');

        return false;
    });

    // display the gallery once we're done building it
    $wn(this.context).show();
    this.tabsLoaded = true;
};

WNVideoGallery.prototype.loadGalleryClips = function(data, clip) {
    if (this.hasClipAssigned) {
        if (clip) {
            this.clipJSON = clip;
            if (this.galleryJSON) this.prependClip(this.galleryJSON, this.clipJSON);
        }
        if (data) {
            this.galleryJSON = data;
            if (this.clipJSON && this.galleryJSON.categories[0].id == this.categoryId)
                this.prependClip(this.galleryJSON, this.clipJSON);
        }
        if (this.galleryJSON && (this.clipJSON ||
            this.galleryJSON.categories[0].id == this.categoryId)) {
            data = this.galleryJSON;
        } else {
            return false;
        }
    }

    // get current gallery clips div
    $wn('.video-page-container', this.context).empty();
    $wn('.video-page-container', this.context).html('loading...');

    // Create HTML for clips
    // remember to subtract one for the empty "{}" at the end of the json array
    this.clips = data.clips;
    this.totalTabs = Math.floor((this.fV.playerWidth - 70) / 130); // Each tab is 130px wide; subtract 70px for the "more" dropdown
    this.clipsPerPage = this.fV.rows * this.fV.columns;
    this.totalCategories = data.categories.length - 1;
    this.totalClips = data.clips.length - 1;
    this.totalPages = Math.ceil(this.totalClips / this.clipsPerPage);
    if (this.totalPages < 1) this.totalPages = 1;

    var mythis = this;
    //var params = this.params;
    output = "";

    $wn.each(data.clips, function(index, value) {
        var el = $wn(this);
        var cur = 0;
        var newPage = false;
        if (data.clips[index].mainHeadline != null) {
            // loop through clips
            var clip = data.clips[index];

            if (index % mythis.clipsPerPage == 0) {
                newPage = true;
            } else {
                newPage = false;
            }

            cur = Math.floor(index / mythis.clipsPerPage) + 1;
            if (newPage) {
                if (cur > 1) output = output + "</div>"; // close last page
                output = output + '<div class="video-page video-page-' + cur + '"><ul>';
            }

            // Thumbnail sizes
            var thumb_width = 96,
                thumb_height = 54,
                title_margin_left = 104;
            if (mythis.fV.videoListImageWidth) {
                thumb_width = mythis.fV.videoListImageWidth;
            }
            if (mythis.fV.videoListImageHeight) {
                thumb_height = mythis.fV.videoListImageHeight;
            }

            try {
                title_margin_left = parseInt(thumb_width) + 6;
            } catch (e) {}

            // Get the right format for the device
            var clipUrl = wnVideoWidgets.getPlayableClipUrl(clip);
            var clipUrl = unescape(clip.uri[0].url);

            var thumbHtml = '<a href="' + clipUrl + '"><li class="video-thumb" clipIndex="' + index + '" id="clipid_' + clip.id + '">' +
                //                  '<img class="thumb" style="width:' + thumb_width + 'px;height:' + thumb_height + 'px;" src="' + clip.graphic + '"/>' +
                '<img class="thumb" style="width:' + thumb_width + 'px;height:' + thumb_height + 'px;" src="' + clip.thumb + '"/>' +
                '<span class="title" style="margin-left:' + title_margin_left + 'px">' + clip.mainHeadline + '</span><span class="length" style="margin-left:' + title_margin_left + 'px">' + wnVideoUtils.getTimeAsHHMMSS(clip.duration) + '</span></li></a>';

            output = output + thumbHtml;

            //TODO - add support for standalone canvases

        }
    });

    if (data.clips.length < 2) {
        output = '<div class="video-page video-page-1"><div class="wn-gallery-message">' +
            'There are no videos in the selected category</div></div>';
    }
    $wn('div.video-page-container', this.context).html(output + "</div>");

    // Bind clip event handlers
    //   Only bind to event in case video playing in html5 video element is supported
    if (wnVideoUtils.hasHtmlVideo) {
        $wn('div.video-page ul li', this.context).click(function() {
            var clipObj = data.clips[$wn(this).attr('clipIndex')];
            clipObj.domId = mythis.fV.domId;
            clipObj.ordinance = parseInt($wn(this).attr('clipIndex'));
            clipObj.isUserInitiated = true;
            if (wnVideoUtils.stringToBoolean(mythis.fV.enableSingleClipPage)) {
                document.location.href = wnVideoWidgets.getClipPageUrl(clipObj);
            } else {
                // Find a matching groupid canvas on the page - if not found then rediret to landing page
                if ($wn('#wnVideoHTML', '#wn_html_video_' + mythis.groupId).length) {
                    evntMgr.Notify({
                        evnt: "NewGalleryClip",
                        idKey: mythis.groupId,
                        objClip: clipObj
                    });
                } else {
                    var landingPage = wnVideoWidgets.landingPage + '&clipId=' + data.clips[$wn(this).attr('clipIndex')].id + '&autostart=true';
                    document.location.href = landingPage;
                }
            }
            return false;
        });
    }
    $wn('div.video-page ul li', this.context).mouseover(function() {
        $wn(this).addClass("wn-highlight");
        return false;
    });
    $wn('div.video-page ul li', this.context).mouseout(function() {
        $wn(this).removeClass("wn-highlight");
        return false;
    });

    // Generate page numbers
    $wn('div.pager ul.page-numbers', this.context).empty();
    for (var i = 1; i <= this.totalPages; i++) {
        var li = '<li><a href="#" class="page-' + i;
        if (i == 1) li += ' active';
        li += '" rel="' + this.fV.playerWidth * (i - 1) + '">' + i + '</a></li>'
        $wn('div.pager ul', this.context).append(li);
    }
    // width of page number container (centers itself on the bar)
    var page_numbers_container = $wn('div.pager div.page-numbers-container', this.context);
    var pages_width = this.fV.playerWidth - 128;
    page_numbers_container.css('width', pages_width);
    $wn('div.pager ul.page-numbers', this.context).css('width', 26 * this.totalPages);
    // page number click events
    $wn('div.pager ul li a', this.context).click(function() {
        // move thumbnails container
        var pos = $wn(this).attr('rel');
        $wn('div.video-page-container', mythis.context).animate({
            left: (0 - pos) + 'px'
        });
        // move pager container
        var left = $wn(this).parent()[0].offsetLeft;
        left -= (pages_width / 2 - 26 / 2);
        page_numbers_container.animate({
            scrollLeft: left + 'px'
        });
        // set active page
        $wn(this).parent().parent().find('.active').removeClass('active');
        $wn(this).addClass('active');
        return false;
    });

    // CSS resizing
    $wn('div.video-page-container', this.context).css('left', '0px');
    $wn(this.context).css({
        width: this.fV.playerWidth + 'px'
    });
    $wn('div.video-page-container', this.context).css('width', (this.fV.playerWidth * this.totalPages) + 'px');

    // Adjust height to take into account that the tabs aren't visible for a story page
    var reduce_height = 70;
    if (this.isStoryVideo == true) {
        reduce_height = 37
    }

    $wn('div.video-page-container, div.wn-gallery div.video-page', this.context).css({
        height: (this.fV.playerHeight - reduce_height) + 'px'
    });
    $wn('div.video-page div.wn-gallery-message', this.context).css({
        marginTop: ((this.fV.playerHeight - reduce_height) / 2 - 10) + 'px'
    });
    $wn('div.video-page', this.context).css({
        width: this.fV.playerWidth + 'px'
    });

    $wn('div.video-page ul li', this.context).width(this.fV.playerWidth / this.fV.columns);
    $wn('div.video-page ul li', this.context).height((this.fV.playerHeight - reduce_height) / this.fV.rows);

    // bind click events for prev-next pager
    $wn('div.pager a.next', this.context).click(function() {
        var pos = $wn('div.video-page-container', mythis.context).position();
        if (pos.left <= 0 - (mythis.fV.playerWidth * (mythis.totalPages - 1))) {} else {
            var scrollToPos = parseInt(pos.left) - parseInt(mythis.fV.playerWidth);
            var linkPos = (-1) * Math.round(scrollToPos / parseInt(mythis.fV.playerWidth));
            $wn('div.pager ul li:eq(' + linkPos + ') a', mythis.context).trigger('click');
            /*$wn('div.video-page-container', mythis.context).animate({
            left: (pos.left - mythis.fV.playerWidth) + 'px'
            });*/
        }
        return false;
    });
    $wn('div.pager a.prev', this.context).click(function() {
        var pos = $wn('div.video-page-container', mythis.context).position();
        if (pos.left >= 0) {} else {
            var scrollToPos = parseInt(pos.left) + parseInt(mythis.fV.playerWidth);
            var linkPos = (-1) * Math.round(scrollToPos / parseInt(mythis.fV.playerWidth));
            $wn('div.pager ul li:eq(' + linkPos + ') a', mythis.context).trigger('click');
            /*$wn('div.video-page-container', mythis.context).animate({
            left: scrollToPos + 'px'
            });*/
        }
        return false;
    });

    // display the gallery once we're done building it
    $wn(this.context).show();
    var clipObj = data.clips[0];
    if (clipObj.uri && this.fV.isPrimaryGallery) {
        clipObj.isUserInitiated = false;
        clipObj.domId = this.fV.domId;
        clipObj.ordinance = 0;
        onWidgetLoad(this.fV.domId, this.fV.idKey, "", "", {
            'objClip': clipObj
        });
        evntMgr.Notify({
            evnt: "NewGalleryClip",
            idKey: mythis.groupId,
            objClip: clipObj
        });
    }
};

WNVideoGallery.prototype.getContentJSON = function(contentId, contentType) {
    // construct feeds api call
    if (contentId && contentType) {
        var timestamp = new Date().getTime();
        var feedUrl = wnFeedsApiDomain + '/feed/v3.0/' + contentType + '/' + contentId + '/all?alt=json&callbackparams=' + this.fV.domId + '&callback=wnVideoWidgets.loadGallery';

        if (this.categoryCache[contentId]) {
            // Cached version found - load from cache
            this.loadGallery(this.categoryCache[contentId]);
        } else {
            // Use local JS script loader to avoid issues with conflicts in older jQuery versions
            wnVideoUtils.loadScript("wnGalleryScript" + timestamp, feedUrl);
            //wnVideoUtils.loadScript("wnGalleryScript" + timestamp, "/category.json");
        }
        if (this.hasClipAssigned && contentId == this.categoryId) {
            var feedUrl = wnFeedsApiDomain + '/feed/v3.0/clips/' + this.fV.clipId + '?alt=json&callbackparams=' + this.fV.domId + '&callback=wnVideoWidgets.loadGalleryClip';
            wnVideoUtils.loadScript("wnGalleryClipScript" + timestamp, feedUrl);
        }
    }
};

WNVideoGallery.prototype.onClipEnded = function(p_evtObj) {
    var clipObj = p_evtObj.objClip;
    if (this.fV.isContinuousPlay) {
        if (this.fV.domId == clipObj.domId) // make sure the same gallery is serving its clips in sequence
        {
            if ((clipObj.ordinance + 1) < this.totalClips) {
                var newClipObj = this.clips[clipObj.ordinance + 1];
                newClipObj.isUserInitiated = true;
                newClipObj.domId = this.fV.domId;
                newClipObj.ordinance = clipObj.ordinance + 1;
                evntMgr.Notify({
                    evnt: "NewGalleryClip",
                    idKey: this.groupId,
                    objClip: newClipObj
                });
            }
        } else if (clipObj.domId == undefined && this.fV.isPrimaryGallery && this.totalClips > 1) {
            var newClipObj = this.clips[0];
            newClipObj.isUserInitiated = true;
            newClipObj.domId = this.fV.domId;
            newClipObj.ordinance = 0;
            evntMgr.Notify({
                evnt: "NewGalleryClip",
                idKey: this.groupId,
                objClip: newClipObj
            });
        }
    }
};



/* Class: WNVideoCanvasPlayBackControl
- HTML version of the PlayBack control for video canvas widget
*/
WNVideoCanvasPlayBackControl = function(parentDivId, flashVars, options) {

    this.fV = flashVars;
    this.widgetClassType = "WNVideoCanvasPlayBackControl";
    this.parentDivId = parentDivId;
    this.groupId = this.fV.idKey;
    this.video = $wn('#wnVideoHTML', '#wn_html_video_' + this.groupId)[0]; // DOM video element
    this.options = options;
    this.options = $wn.extend({
        overlay_elem_class: '',
        overlay_elem_id: ''
    }, options);
    this.needShowHideControl = true;
    this.timeoutId = 0;
    this.isVisible = true;

    // Find overlay by Id or style class if specified in options
    if (this.options.overlay_elem_id.length > 0) {
        this.elementOverlay = $wn('#' + this.options.overlay_elem_id, '#' + this.fV.domId);
    } else if (this.options.overlay_elem_class.length > 0) {
        this.elementOverlay = $wn('.' + this.options.overlay_elem_class, '#' + this.fV.domId);
    } else {
        this.elementOverlay = null;
    }

    // Store overlay current height for restoring
    if (this.elementOverlay) {
        this.elementOverlayHeight = $wn(this.elementOverlay).outerHeight(true);
    } else {
        this.elementOverlayHeight = 0;
    }

    // Render PlayBack control
    this.render();

    // Bind Video player event handlers
    //this.bindVideoPlayerEvents(); - moved to show()

    this.show();
};

WNVideoCanvasPlayBackControl.prototype.render = function() {

    var _this = this,
        adsManager = null;
    isPlaying = false;

    /* Set control elements */
    var playBackControlMarkup = '<div class="wn-video-controls-container">';

    var os = wnUserAgentParser.getOS();
    if (os.name.toLowerCase() != 'ios' || os.name.toLowerCase() == 'ios' && parseFloat(os.version) >= 8) {
        playBackControlMarkup += '   <div class="wn-canvas-player-play-control">' +
            '       <span class="' + (this.video.autoplay ? 'wn-canvas-player-paused' : 'wn-canvas-player-playing') + '"></span>' +
            '   </div>';
    }
    playBackControlMarkup += '   <div class="wn-canvas-player-loading-control">' +
        '       <span class="wn-canvas-player-loading"></span>' +
        '       <span class="wn-canvas-player-loading-spinner"></span>' +
        '   </div>' +
        '   <div class="wn-canvas-player-mute-control">' +
        '       <span class="wn-canvas-player-mute"></span>' +
        '   </div>' +
        '   <div class="wn-canvas-player-fullscreen-control"><span class="wn-canvas-player-fullscreen"></span></div>' +
        '</div>';

    $wn("#" + this.parentDivId).append(playBackControlMarkup);

    this.elementContainer = $wn('div.wn-video-controls-container', '#' + this.fV.domId);
    this.elementPlayPauseWrapper = $wn('div.wn-canvas-player-play-control', '#' + this.fV.domId);
    this.elementPlayPause = $wn('div.wn-canvas-player-play-control > span', '#' + this.fV.domId);
    this.elementProgressMessage = $wn('.wn-canvas-player-loading', '#' + this.fV.domId);
    this.elementProgressSpinner = $wn('.wn-canvas-player-loading-spinner', '#' + this.fV.domId);
    this.elementProgressWrapper = $wn('div.wn-canvas-player-loading-control', '#' + this.fV.domId);
    this.elementFullScreen = $wn('div.wn-canvas-player-fullscreen-control > span', '#' + this.fV.domId);
    this.elementFullScreenWrapper = $wn('div.wn-canvas-player-fullscreen-control', '#' + this.fV.domId);
    this.elementMute = $wn('div.wn-canvas-player-mute-control > span', '#' + this.fV.domId);
    this.elementMuteWrapper = $wn('div.wn-canvas-player-mute-control', '#' + this.fV.domId);

    var widthPlayPauseCtrl = $wn(this.elementPlayPauseWrapper).outerWidth(true);
    var widthFullScreenCtrl = $wn(this.elementFullScreenWrapper).outerWidth(true);
    var widthMuteVolumeCtrl = $wn(this.elementMuteWrapper).outerWidth(true);
    var offSet = (widthPlayPauseCtrl + widthFullScreenCtrl + widthMuteVolumeCtrl) + 7;

    this.elementProgressWrapper.css('width', (this.fV.playerWidth - offSet) + 'px !important');
    //this.elementOverlayCurrentHeight = 48;  // adjusted for iPad
    this.elementOverlayCurrentHeight = 30; // adjusted for iPad

    $wn(this.elementContainer)
        .css('display', 'none')
        .css('position', 'relative')
        .css('clear', 'none')
        .css('width', this.fV.playerWidth + 'px')
        .css('height', this.elementOverlayCurrentHeight + 'px')
        .css('top', (this.fV.playerHeight - (this.elementOverlayCurrentHeight)) + 'px');

    // Hide Mute and Full-screen buttons for iOS-based devices
    if (wnVideoWidgets.useHtml5) {
        this.elementFullScreenWrapper.hide();
        this.elementMuteWrapper.hide();
    }
    this.bindControlInternalEvents();
};

// Bind playback buttons event handlers
WNVideoCanvasPlayBackControl.prototype.bindControlInternalEvents = function() {

    var _this = this;

    $wn(_this.elementPlayPause).bind('click', function() {
        if (_this.isPlaying) {
            wnLog('Playback control: Pause button clicked');
            _this.adsManager.pause();
            /*
            Moved to the On Ad Pause handler - google.ima.AdEvent.Type.PAUSED
            this.isPlaying = false;
            _this.setPlayPauseCtrl();
            */
        } else {
            wnLog('Playback control: Play button clicked');
            _this.adsManager.resume();
            /*
            Moved to the On Ad Resumed handler - google.ima.AdEvent.Type.RESUMED
            this.isPlaying = true;
            _this.setPlayPauseCtrl();
            */
        }
    });

    $wn(_this.elementMute).bind('click', function() {
        if (_this.video.readyState < _this.video.HAVE_CURRENT_DATA) return false;
        _this.video.muted = !_this.video.muted;
        try {
            _this.video.prop('muted', _this.video.muted);
        } catch (e) {
            _this.video.attr('muted', _this.video.muted);
        }
        _this.setMuteCtrl();

        wnLog('Playback control: Mute button clicked');
    });

    $wn(_this.elementFullScreen).bind('click', function() {
        if (_this.video.readyState < _this.video.HAVE_CURRENT_DATA) return false;
        //Do full-screen depends on browser
        if ($wn.browser.webkit) {
            _this.video.webkitEnterFullscreen();
        } else if ($wn.browser.mozilla) {
            _this.video.mozRequestFullScreen();
        } else {
            // Do nothing because feature isn't supported
        }
        wnLog('Playback control: Full-screen button clicked');
    });
};

// Bind player event handlers
WNVideoCanvasPlayBackControl.prototype.bindVideoPlayerEvents = function() {

    var _this = this;
    /*    var _video = $wn('#wnVideoHTML', '#wn_html_video_' + this.groupId);

        $wn(_video).bind('loadstart', function () {
            _this.setProgressCtrl();
            wnLog('onloadstart');
        });

        $wn(_video).bind('progress', function () {
            _this.setProgressCtrl();
            //wnLog('onprogress');
        });

        $wn(_video).bind('suspend', function () {
            _this.setProgressCtrl();
            wnLog('onsuspend');
        });

        $wn(_video).bind('emptied', function () {
            _this.setProgressCtrl();
            wnLog('onemptied');
        });

        $wn(_video).bind('error', function () {
            _this.setProgressCtrl();
            wnLog('onerror');
        });

        $wn(_video).bind('pause', function () {
            _this.setPlayPauseCtrl();
            wnLog('onpause');
        });

        $wn(_video).bind('loadeddata', function () {
            _this.setProgressCtrl();
            wnLog('onloadstart');
        });

        $wn(_video).bind('waiting', function () {
            _this.setProgressCtrl();
            wnLog('onloadeddata');
        });

        $wn(_video).bind('playing', function () {
            _this.setPlayPauseCtrl();
            wnLog('onplaying');
        });

        $wn(_video).bind('canplay', function () {
            _this.setProgressCtrl();
            wnLog('oncanplay');
        });

        $wn(_video).bind('timeupdate', function () {
            _this.setCurrentTimeCtrl();
        });

        $wn(_video).bind('ended', function () {
            _this.onVideoEnded();
            wnLog('onended');
        });

        $wn(_video).bind('volumechange', function () {
            _this.setMuteCtrl();
            wnLog('onvolumechange');
        });
    */
    _this.adsManager.addEventListener(
        google.ima.AdEvent.Type.PAUSED,
        function(event) {
            //$wn(_video).bind('pause', function () {
            wnLog('g: on PAUSED');
            _this.isPlaying = false;
            _this.setPlayPauseCtrl();
        });

    _this.adsManager.addEventListener(
        google.ima.AdEvent.Type.RESUMED,
        function(event) {
            //$wn(_video).bind('playing', function () {
            wnLog('g: on RESUMED');
            _this.isPlaying = true;
            _this.setPlayPauseCtrl();
        });

    /*_this.adsManager.addEventListener (
        google.ima.AdEvent.Type.ALL_ADS_COMPLETED, function (event) {
    //$wn(_video).bind('ended', function () {
        _this.onVideoEnded();
        wnLog('g: on ALL_ADS_COMPLETED');
    });*/
    /**/
};

// Set control Play/Pause button depends on current Video player state
WNVideoCanvasPlayBackControl.prototype.setPlayPauseCtrl = function() {
    //if (this.video.paused) {
    if (this.isPlaying) {
        $wn(this.elementPlayPause)
            .removeClass('wn-canvas-player-paused')
            .addClass('wn-canvas-player-playing');
        $wn(this.elementProgressSpinner).empty();
    } else {
        $wn(this.elementPlayPause)
            .removeClass('wn-canvas-player-playing')
            .addClass('wn-canvas-player-paused');
        var os = wnUserAgentParser.getOS();
        if (os.name.toLowerCase() == 'ios' && parseFloat(os.version) < 8) {
            $wn(this.elementProgressSpinner).html('&nbsp; &nbsp; &nbsp; Click PLAY to continue. After the commercial finishes, your video will begin.');
            $wn(this.elementProgressSpinner).css('overflow', 'hidden').css('height', '16px').css('position', 'relative').css('top', '-15px');
        } else {
            $wn(this.elementProgressSpinner).html('Click Play to continue');
        }
    }
    this.setProgressCtrl();

    //wnLog('WNVideoCanvasPlayBackControl.setPlayPauseCtrl() was executed');
};

WNVideoCanvasPlayBackControl.prototype.onVideoEnded = function() {

    //  if (this.video.ended) {
    $wn(this.elementPlayPause)
        .removeClass('wn-canvas-player-playing')
        .addClass('wn-canvas-player-paused');

    $wn(this.elementProgressSpinner).empty();

    // Hide control when clip was ended
    this.hide();
    //  }
    //wnLog('WNVideoCanvasPlayBackControl.onVideoEnded() was executed');
};

// Set control Mute/Volume button depends on current Video player state
WNVideoCanvasPlayBackControl.prototype.setMuteCtrl = function() {

    if (this.video.muted) {
        $wn(this.elementMute).addClass('vol-0');
    } else {
        $wn(this.elementMute).removeClass('vol-0');
    }
    //wnLog('WNVideoCanvasPlayBackControl.setMuteCtrl() was executed');
};

// Set control progress panel depends on current Video player state
WNVideoCanvasPlayBackControl.prototype.setProgressCtrl = function() {
    /*
        if (!this.video || !this.isVisible) return false;

        if (this.video.readyState < this.video.HAVE_CURRENT_DATA) {
            $wn(this.elementProgressMessage).show();
            $wn(this.elementProgressSpinner).hide();
        } else {
            $wn(this.elementProgressMessage).hide();
            $wn(this.elementProgressSpinner).show();
        }

        if (this.video.readyState < this.video.HAVE_CURRENT_DATA) {
            $wn(this.elementProgressMessage).html('Loading...');
            if (this.video.networkState < this.video.NETWORK_EMPTY) {
                $wn(this.elementProgressMessage).html('Connection was lost');
            }
            if (this.video.readyState < this.video.HAVE_NOTHING) {
                $wn(this.elementProgressMessage).html('Video is not available');
            }
        } else {
            if (this.video.seeking) {
                $wn(this.elementProgressMessage).html('Loading...');
            }
        }
        //wnLog('WNVideoCanvasPlayBackControl.setProgressCtrl() was executed');
    */
};

// Set control countdown timer depends on current Video player state
WNVideoCanvasPlayBackControl.prototype.setCurrentTimeCtrl = function() {
    /*
        if (this.video.readyState >= this.video.HAVE_CURRENT_DATA && !this.video.paused) {
            $wn(this.elementProgressSpinner).html('Ad: Your video resumes in ' + (parseInt(this.video.duration - this.video.currentTime)) + ' sec').show();
        }
    */
};


// Show control
WNVideoCanvasPlayBackControl.prototype.show = function(adsManager) {

    //if (!this.adsManager) this.adsManager = adsManager;
    this.adsManager = adsManager;
    this.isPlaying = true;
    this.setPlayPauseCtrl();

    // Bind Video player event handlers
    if (typeof adsManager != "undefined") this.bindVideoPlayerEvents();

    // Display control ...
    $wn(this.elementContainer).show();

    // ... and hide standard playback control
    try {
        $wn(this.video).prop('controls', '');
    } catch (e) {
        $wn(this.video).attr('controls', '');
    }

    this.isVisible = true;

    // Set overlay layer height if exists
    if (this.elementOverlay && this.elementOverlayCurrentHeight) {
        $wn(this.elementOverlay).css('height', (this.fV.playerHeight - (this.elementOverlayCurrentHeight + 2)) + 'px');
        $wn(this.elementOverlay).show();
    }
    wnLog('Canvas PlayBackControl was displayed');
};

// Hide control
WNVideoCanvasPlayBackControl.prototype.hide = function() {

    // Restore previous overlay height
    /*if (this.elementOverlay && this.elementOverlayHeight > 0) {
    $wn(this.elementOverlay).css('height', this.elementOverlayHeight);
    }*/
    // overlay div should be 0 heigh when content clip is playing
    if (this.elementOverlay) {
        $wn(this.elementOverlay).css('height', 0).hide();
    }
    // Hide control...
    $wn(this.elementContainer).hide();

    // ... and display standard playback control
    try {
        $wn(this.video).prop('controls', 'controls');
    } catch (e) {
        $wn(this.video).attr('controls', 'controls');
    }
    this.isVisible = false;

    wnLog('Canvas PlayBackControl was hidden');
};


/* Class: WNAmpVideoCanvas
 */
WNAmpVideoCanvas = function(parentDivId, flashVars) {
    var contentaddons = wn.contentaddons || {};
    var betalist = contentaddons.betalist || "";
    this.fV = flashVars;
    this.amp_config = {
        "paths": {
            "player": wn.contentdomain + "/global/vendor/amp.premier/",
            "resources": wn.contentdomain + "/global/vendor/amp.premier/"
            /*"player": "http://localhost:8080/global/vendor/amp.premier/",
            "resources": "http://localhost:8080/global/vendor/amp.premier/"*/
        },
        "resources": [
            {
                "src": "#{paths.player}amp.premier.min.css",
                "type": "text/css"
            }
        ],
        params: {
        	"wnadcall": ""
        },
        target: "_top",
        fullscreen: {
            "native": true,
            "enabled": (this.fV.hasFullScreen.toLowerCase() == "true")
        },
        controls: {
            //'mode': (wn_isMobile ? 'persistent' : 'auto'),
            'mode': 'auto',
            autoHide: 2
        },
        flash: {
            "swf": "#{paths.player}AkamaiPremierPlayer.swf",
            "debug": "#{paths.player}AkamaiPremierPlayer-debug.swf",
            "expressInstallSWF": "#{paths.player}playerProductInstall.swf",
            vars: {
                context_menu_mode: "short",
                core_ads_enabled: true,
                aasp_bypass: (betalist.indexOf('videolivestreamcdn-3rdparty') >= 0)
            },
            "params": {"wmode": "transparent"}, // In Firefox and IE10, embed form overlay doesn't work, when wmode = direct
            "static": {
                "enabled": false,
                "swf": "#{paths.player}AkamaiPremierPlayer-static.swf",
                "debug": "#{paths.player}AkamaiPremierPlayer-static-debug.swf"
                /*,
                plugins: {
                    AkamaiAdvancedStreamingPlugin: "com.akamai.osmf.AkamaiAdvancedStreamingPluginInfo",
                    CaptioningPlugin: "org.osmf.captioning.CaptioningPluginInfo"
                }*/
            }
        },
        captioning: {
            enabled: (wnVideoUtils.validateString(this.fV.transportShareButtons) && this.fV.transportShareButtons.indexOf("cc") > -1)
        },
        mediaanalytics: {}, // Don't remove. "Ready" event, won't fire in HTML5
        share: {
            facebook: false,
            twitter: false,
            enabled: (typeof this.fV.toolsShareButtons == "undefined" && typeof this.fV.overlayShareButtons == "undefined" ||
		        wnVideoUtils.validateString(this.fV.toolsShareButtons) && this.fV.toolsShareButtons.indexOf("share") > -1 ||
        		wnVideoUtils.validateString(this.fV.overlayShareButtons) && this.fV.overlayShareButtons.indexOf("share") > -1)
        },
        ima: {
            "enabled": true,
            "version": 3,
            //plugin: "http://imasdk.googleapis.com/js/sdkloader/ima3.js",
            "resources": [{
                    'type': "text/javascript",
                    'src': "//imasdk.googleapis.com/js/sdkloader/ima3.js"
            }],
            //adTagUrl: "http://pubads.g.doubleclick.net/gampad/ads?sz=400x300&iu=%2F6062%2Fiab_vast_samples&ciu_szs=300x250%2C728x90&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=url&correlator=timestamp&cust_params=iab_vast_samples%3Dlinear",
            //adTagUrl: "#{window._getAdUrl()}",
            //adTagUrl: "#{media.link}",
            "adTagUrl": "#{window.wnVideoWidgets.getAmpParams('"+ this.fV.domId +"').wnadcall}",
            "disableCompanionAds": false,
            "maxBitrate": 100000,
            "vpaidMode": "enabled"
        },
        // Gets overwritten by local saved value (flash cache). Use setAutoplay instead.
        autoplay: this.fV.isAutoStart.toLowerCase() == 'true',
        /*branding: {
            //logo: wn.contentdomain + "/global/vendor/amp.premier/worldnow-logo.png"
        },*/
        recommendations: {
            // enabled: wnVideoUtils.stringToBoolean(this.fV.hasEmbeddedGallery),
            enabled: true,
            url: ((wnVideoUtils.stringToBoolean(this.fV.hasEmbeddedGallery) && this.fV.galleryId != "") ? 
                "http://" + wn.baseurl + "/" + (this.fV.gallerySourceType == "wnstory" ? "story" : "category") + "/" + this.fV.galleryId + "/gallery?clienttype=mrssjson" : 
                "http://" + wn.baseurl + "/category/" + wn.rootassociations.pop_video_top + "/gallery?clienttype=mrssjson"),
            //url: "http://Wnow.dsys1.worldnow.com/category/3732/gallery?clienttype=mrssjson",
            //url: "http://projects.mediadev.edgesuite.net/customers/akamai/mdt-html5-core/premier/2.26.0.0003/resources/feeds/recommendations.xml",
            target: "_top"
        },
        locales: {
            en: {
                MSG_NEXT_VIDEO: "Content starts in ",
                MSG_SECONDS: "sec"
            }
        },
        // User's preferred volume is persisted in local storage. In this sense, the configuration value serves as a fallback for cases where the user is visiting for the first time or local storage is disabled
        volume: (wnVideoUtils.stringToBoolean(this.fV.isMute) ? 0 : 1),
        // Either vertical or horizontal with the caveat that this parameter will have no effect on Flash. Currently Flash only supports the vertical volume panel.
        volumepanel: {
            direction: "vertical"
        },
        //volumepanel: {direction: this.fV.volumeControlsType},
        rules: {
            android_4_gets_m3u8: {
                regexp: "Android 4"
            }
        },
        // adchoices: {
        //     target: "http://www.worldnow.com"
        // },
        // adCount: {},
        playoverlay: {
            enabled: true
        }
    }
    // Apply clip end experience logic
    if (betalist.indexOf('videomrssgallery-') >= 0) {
        // Expecting videomrssgallery-123 or videomrssgallery-http://feed-url
        var mrssgallery = betalist;
        mrssgallery = mrssgallery.substring(mrssgallery.indexOf('mrssgallery-') + 12);
        if (mrssgallery.indexOf(',') >= 0) {
            mrssgallery = mrssgallery.substring(0, mrssgallery.indexOf(','));
        }
        if (isNaN(mrssgallery)) {
            this.amp_config.recommendations.url = mrssgallery;
        } else {
            this.amp_config.recommendations.url = "http://" + wn.baseurl + "/category/" + mrssgallery + "/gallery?clienttype=mrssjson";
        }
    }
    // Show "view all" link only on front end desktop
    if (!wn_isMobile && this.fV.producerMode != "clip-edit") {
        var landingPage = wnSiteConfigVideo.landingPageUrl;
        if (landingPage.indexOf("://") > -1) {
        	landingPage = landingPage.substr(landingPage.indexOf("://") + 3);
        }
        this.amp_config.domain = landingPage;
    }
    // Render player in html mode on clip edit page in producer, or if debug set accordingly
    if (typeof this.fV.producerMode !== 'undefined' && this.fV.producerMode == "clip-edit" ||
        wnVideoUtils.getQS('wnhtml5') == 'true')
    {
        this.amp_config.mode = "html";
    }

    // SMEF-256 - WVVA Front End Video Issues {
    var WNisProducerRegExp = new RegExp("://manage[A-Za-z0-9.]*\.worldnow.com");
    if ((betalist.indexOf('videoamphtml5-') >= 0 || // Expecting videomrssgallery-mac|windows
        wnVideoUtils.getQS('videoamphtml5') == 'mac') && // debug via URL
        WNisProducerRegExp.test(document.location.href) == false && // don't apply in widget builder
        typeof this.fV.producerMode === 'undefined') // don't apply in producer
    {
        var videoamphtml5 = betalist;
        videoamphtml5 = videoamphtml5.substring(videoamphtml5.indexOf('videoamphtml5-') + 14);
        if (videoamphtml5.indexOf(',') >= 0) {
            videoamphtml5 = videoamphtml5.substring(0, videoamphtml5.indexOf(','));
        }
        var os = wnUserAgentParser.getOS().name.toLowerCase();
        if (wnUserAgentParser.getBrowser().name == 'Chrome' && // Chrome
            (os == 'mac os x' && videoamphtml5.indexOf('mac') >= 0 || // Mac or windows and siteconfig
             os.indexOf('windows') >= 0 && videoamphtml5.indexOf('windows') >= 0 ||
             wnVideoUtils.getQS('videoamphtml5') == 'mac') && // debug
            this.fV['videoType'] != 'livestream' // VOD
        ) {
            this.amp_config.mode = "html";
        }
    }
    // SMEF-256 }

    // Companion ads config
    var ad_size_map = {
        'WNAd1': {id: "WNAd1", width: 468, height: 60},
        'WNAd14': {id: "WNAd14", width: 120, height: 600},
        'WNAd16': {id: "WNAd16", width: 1, height: 1},
        'WNAd17': {id: "WNAd17", width: 1, height: 1},
        'WNAd18': {id: "WNAd18", width: 120, height: 240},
        'WNAd20': {id: "WNAd20", width: 180, height: 150},
        'WNAd21': {id: "WNAd21", width: 88, height: 31},
        'WNAd22': {id: "WNAd22", width: 490, height: 25},
        'WNAd23': {id: "WNAd23", width: 120, height: 240},
        'WNAd24': {id: "WNAd24", width: 120, height: 15},
        'WNAd26': {id: "WNAd26", width: 120, height: 60},
        'WNAd27': {id: "WNAd27", width: 120, height: 60},
        'WNAd28': {id: "WNAd28", width: 120, height: 60},
        'WNAd30': {id: "WNAd30", width: 10, height: 10},
        'WNAd31': {id: "WNAd31", width: 11, height: 11},
        'WNAd35': {id: "WNAd35", width: 180, height: 150},
        'WNAd36': {id: "WNAd36", width: 468, height: 60},
        'WNAd37': {id: "WNAd37", width: 0, height: 0},
        'WNAd38': {id: "WNAd38", width: 0, height: 0},
        'WNAd39': {id: "WNAd39", width: 0, height: 0},
        'WNAd40': {id: "WNAd40", width: 468, height: 60},
        'WNAd41': {id: "WNAd41", width: 728, height: 90},
        'WNAd42': {id: "WNAd42", width: 160, height: 600},
        'WNAd43': {id: "WNAd43", width: 300, height: 250},
        'WNAd44': {id: "WNAd44", width: 300, height: 75},
        'WNAd45': {id: "WNAd44", width: 1, height: 1},
        'WNAd46': {id: "WNAd46", width: 728, height: 90},
        'WNAd47': {id: "WNAd47", width: 300, height: 150},
        'WNAd48': {id: "WNAd48", width: 0, height: 0},
        'WNAd49': {id: "WNAd49", width: 500, height: 300},
        'WNAd50': {id: "WNAd50", width: 300, height: 175},
        'WNAd52': {id: "WNAd52", width: 300, height: 250},
        'WNAd54': {id: "WNAd54", width: 645, height: 300},
        'WNAd63': {id: "WNAd63", width: 300, height: 600},
        'WNAd85': {id: "WNAd85", width: 850, height: 30},
        'WNAd86': {id: "WNAd86", width: 980, height: 30},
        'WNAd88': {id: "WNAd88", width: 94, height: 19},
        'WNAd101': {id: "WNAd101", width: 300, height: 150},
        'WNAd103': {id: "WNAd103", width: 300, height: 250},
        'WNAd104': {id: "WNAd104", width: 468, height: 60},
        'WNAd105': {id: "WNAd105", width: 320, height: 50},
        'WNAd106': {id: "WNAd106", width: 320, height: 50},
        'WNAd130': {id: "WNAd130", width: 10, height: 10},
        'WNAd131': {id: "WNAd131", width: 11, height: 11}
    }
    var enabled_companions = wnCompanionAds.split(","); // These are set up in the site config
    this.amp_config.ima['companions'] = new Array();
    for (j=0; j < enabled_companions.length; j++) {
        this.amp_config.ima.companions.push(ad_size_map[enabled_companions[j]]);
    }

    // Color scheme
    if (typeof this.fV.controlsType != "undefined" && this.fV.controlsType == "overlay") {
        var skin_colors = {
            'assets': 'premier_assets_dark.swf',
            'controls_BackgroundAlpha': 0.9,
            'controls_bgGradientTop': "#111111",
            'controls_bgGradientBottom': "#111111",
            'progressBar_trackBgColor': "#646464",
            'progressBar_bufferedColor': "#5b5b5b",
            'progressBar_playedColor': "#3e3e3e",
            'volumeBar_trackBgColor': "#5b5b5b",
            'volumeBar_volumeLevelColor': "#3e3e3e"
        }
    } else {
        if (this.fV.skin['#menuBar']['bgGradientTop'] == "#000000") this.fV.skin['#menuBar']['bgGradientTop'] = "#010101";
        if (this.fV.skin['#menuBar']['bgGradientBottom'] == "#000000") this.fV.skin['#menuBar']['bgGradientBottom'] = "#010101";
        var assets = "premier_assets_dark.swf";
        switch (this.fV.skinName) {
            case "bubblegum":
            case "chili":
            case "cocoa":
            case "cobalt":
            case "keylime":
            case "latte":
            case "plum":
            case "tangerine":
            case "wheat":
            case "wine":
                assets = "premier_assets_light.swf";
                break;
            case "ash":
            case "ebony":
            case "frost":
            case "sapphire":
            case "silver":
            case "sunshine":
            case "midnight":
                assets = "premier_assets_dark.swf";
                break;
        }
        var skin_colors = {
            'assets': assets,
            'controls_BackgroundAlpha': 1,
            'controls_bgGradientTop': this.fV.skin['#menuBar']['bgGradientTop'],
            'controls_bgGradientBottom': this.fV.skin['#menuBar']['bgGradientBottom'],
            'progressBar_trackBgColor': this.fV.skin['#progressBar']['trackBgColor'],
            'progressBar_bufferedColor': this.fV.skin['#progressBar, #volumeBar']['bufferedColor'],
            'progressBar_playedColor': this.fV.skin['#progressBar, #volumeBar']['playedColor'],
            'volumeBar_trackBgColor': this.fV.skin['#volumeBarVertical']['trackBgColor'],
            'volumeBar_volumeLevelColor': this.fV.skin['#volumeBarVertical']['volumeLevelColor']
        }
    }
    // UI config
    this.amp_config.flash.xml = "<application>" +
        "   <player " +
        "       core_player_name='worldnow-player' " +
        //"     auto_play='true'" +
        "       volume='" + (wnVideoUtils.stringToBoolean(this.fV.isMute) ? 0 : 1) + "' " +
        "       branding_preload='none' " + // hides Akamai logo before video is loaded
        "       mbr_start_index='2' " +
        "       use_last_known_bitrate='false' " +
        "       use_netsession_client='false'" +
        "       netsession_install_prompt_frequency_secs='-1' " +
        //      scale_view_to_fit="true"
        //      core_ads_enabled="false"
        "       ad_server_timeout='20' " +
        "       ad_control_enabled='true' " +
        "       dvr_enabled='1' " +
        "       hds_live_low_latency='true' " +
        "       fullscreen_enabled = '" + this.fV.hasFullScreen.toLowerCase() + "'" +
        "       recommendations_partner='mrssjson'" +
        "       show_feature_bar='false' " +
        //    "       caption_language= 'en-us'"+
        "       hds_fragment_retry_data_gap_threshold= '20' " +
        //    "       auto_replay='false' "
        "       >" +
        "    </player>" +
        "    <view skin='" + wn.contentdomain + "/global/vendor/amp.premier/" + skin_colors.assets + "'>" +
        "        <element id='adOptions' style=\"backgroundColor: 'rgba(0, 0, 0, 1)'\">" +
        // "           <element id='adChoices' target='http://worldnow.com/' />" +
        "           <element id='adCountdown' />" +
        "           <element id='adCount' />" +
        "        </element>" +
        "        <element id='infoOverlay' style='top: 0px;' />" +
        "        <element id='recommendationSlate' viewMode='grid' items='5' />" +
        "        <element id='captionDisplay' initState='cookie|off' position='relative' style='bottom: 0px; windowColor:0x000000; windowOpacity:0; font:Arial; fontColor:0xffffff; fontOpacity:1; fontBGColor:0x000000; fontBGOpacity:0; edgeType:none; edgeColor:0x000000; edgeOpacity:1; scroll:popOut; fontSize:12;' settingsEnabled='true' />" +
        "        <element id='logoOverlay' style='width: 180px; height: 46px; right: 15px; top: 15px; opacity: 1;' />" +
        "        <element id='controls' autoHide='5' height='25' backgroundAlpha='"+ skin_colors.controls_BackgroundAlpha +"' style='background: linear-gradient(90deg, "+ skin_colors.controls_bgGradientTop +", "+ skin_colors.controls_bgGradientBottom +")'>" +
//        "            <element id='replayBtn' />" + // small button in the bottom left corner over the play button
        "            <element id='playPauseBtn' />";
    if (betalist.indexOf('videolivestreamcdn-3rdpart') > -1 && this.fV['videoType'] == 'livestream') {
        // hide scrub bar
    } else {
        this.amp_config.flash.xml += 
        "            <element id='rewindBtn' />" +
        "            <element id='scrubBar' collapsedHeight='5'  style='background: linear-gradient(90deg, " + skin_colors.progressBar_trackBgColor + ", " + skin_colors.progressBar_trackBgColor + "); height: 6px;' />" +
        "            <element id='progressBar' style='background: linear-gradient(90deg, " + skin_colors.progressBar_bufferedColor + ", " + skin_colors.progressBar_bufferedColor + ");' />" +
        "            <element id='loadedBar' style='background: linear-gradient(90deg, " + skin_colors.progressBar_playedColor + ", " + skin_colors.progressBar_playedColor + ");' />" +
        "            <element id='liveIndicator' />" +
        "            <element id='streamTimeIndicator' position='right' exclude='live'>" +
        "               <element id='streamTime' />" +
        "               <element id='streamDuration' color='#666666' /> " +
        "            </element>";
    }
    this.amp_config.flash.xml += 
//        "            <element id='playlistBtn' />" +
        "            <element id='timeLocationIndicator' type='arrow' color='#FFFFFF' />" +
        "            <element id='goLiveBtn' />";
    if (typeof this.fV.toolsShareButtons == "undefined" && typeof this.fV.overlayShareButtons == "undefined" ||
        wnVideoUtils.validateString(this.fV.toolsShareButtons) && this.fV.toolsShareButtons.indexOf("share") > -1 ||
        wnVideoUtils.validateString(this.fV.overlayShareButtons) && this.fV.overlayShareButtons.indexOf("share") > -1) {
        this.amp_config.flash.xml += "            <element id='shareBtn' />";
    }
    /*this.amp_config.flash.xml +=
        "            <element id='facebookBtn' />" +
        "            <element id='twitterBtn' />";*/
    if (wnVideoUtils.validateString(this.fV.transportShareButtons) && this.fV.transportShareButtons.indexOf("cc") > -1) {
        this.amp_config.flash.xml += "            <element id='captionBtn' />";
    }
    this.amp_config.flash.xml +=
        // "            <element id='statsBtn'/>" +
        "            <element id='hdClientBtn'/>" +
        // "            <element id='chromecastBtn'/>" +
        "            <element id='volumeBar' backgroundColor='#111111' trackColor='" + skin_colors.volumeBar_trackBgColor +
        "'              color='" + skin_colors.volumeBar_volumeLevelColor + "' position='top' width='25' />" +
        "            <element id='volumeBtn' />";
    if (this.fV.hasFullScreen.toLowerCase() == "true") {
        this.amp_config.flash.xml += "            <element id='fullscreenBtn'/>"; // action='external'
    }
    this.amp_config.flash.xml +=
        "        </element>" +
        "        <element id='bufferingView' mode='default' tether='true' />" +
        "        <element id='loadingView' increments='30' radius='15' curve='0' height='2' width='15'/>" +
        "        <element id='replayView' />" + // replay button in the top left corner
        "        <element id='endSlate' enabled='true' hideElements='brandingLogo'/>" + // hideElements='brandingLogo|viewAll'
        "    </view>" +
        "</application>";

    /*debug_config = {"paths":{
            "player": "http://localhost:8080/global/vendor/amp.premier/",
            "resources": "http://localhost:8080/global/vendor/amp.premier/"
        },
        "resources":[{"src":"#{paths.player}amp.premier.css","type":"text/css"}],
        "ima": {
            resources: [
                {
                    type: "text/javascript",
                    src: "//imasdk.googleapis.com/js/sdkloader/ima3.js"
                }
            ],
            enabled: true,
            version: 3,
            //adTagUrl: "//pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=xml_vmap1&unviewed_position_start=1&cust_params=sample_ar%3Dpremidpostpod&cmsid=496&vid=short_onecue&correlator=",
            adTagUrl: "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=",
            disableCompanionAds: false,
            ppid: "ABCDE123456789012345678901234567",
            vpaidMode: "enabled",
            companions: [
                {
                    id: "companion-container",
                    width: 300,
                    height: 250
                }
            ]
        },
        "rules":{"android_4_gets_m3u8":{"regexp":"Android 4"}},"autoplay":true,"loop":false,"target":"_blank","domain":"akamai.com","fullscreen":{},"controls":{},"volumepanel":{"direction":"vertical"},"captioning":{},"playoverlay":{},"flash":{"swf":"#{paths.player}AkamaiPremierPlayer.swf","debug":"#{paths.player}AkamaiPremierPlayer-debug.swf","expressInstallSWF":"#{paths.player}playerProductInstall.swf","static":{"enabled":false,"swf":"#{paths.player}AkamaiPremierPlayer-static.swf","debug":"#{paths.player}AkamaiPremierPlayer-debug.swf"},"view":{"attributes":{"skin":"#{paths.player}premier.assets.swf"},"elements":{"infoOverlay":{"attributes":{"style":"top: 0px;"}},"recommendationSlate":{"attributes":{"items":"5","viewMode":"grid"}},"adOptions":{"attributes":{"style":"backgroundColor: 'rgba(0, 0, 0, 1)'"},"elements":{"adChoices":{"attributes":{"target":"http://www.akamai.com/"}},"adCountdown":{},"adCount":{}}},"captionDisplay":{"attributes":{"defaultMessage":"Sorry! No caption avalaible for this stream","initState":"cookie","position":"relative","settingsEnabled":"true","style":"bottom: 0px; windowColor:0x000000; windowOpacity:0; font:Monospaced Serif; fontColor:0xffffff; fontOpacity:1; fontBGColor:0x000000; fontBGOpacity:0; edgeType:none; edgeColor:0x000000; edgeOpacity:1; scroll:Pop-Out; fontSize:12;"}},"logoOverlay":{"attributes":{"style":"width: 150px; height: 53px; right: 15px; top: 15px; opacity: 0.5; background-image: url('../resources/images/sola.png');"}},"controls":{"attributes":{"height":"28","itemMargin":"5","scrubPosition":"inline"},"elements":{"replayBtn":{},"playPauseBtn":{},"streamTimeIndicator":{"attributes":{"exclude":"live"},"elements":{"streamTime":{},"streamDuration":{}}},"rewindBtn":{},"scrubBar":{"attributes":{"style":"height: 18px;"}},"progressBar":{"attributes":{"style":"background: linear-gradient(90deg, #FF0000, #1B1B1B);"}},"loadedBar":{"attributes":{"style":"background: linear-gradient(90deg, #00FF00, #1B1B1B);"}},"liveIndicator":{},"timeLocationIndicator":{},"goLiveBtn":{},"shareBtn":{},"captionBtn":{},"statsBtn":{},"volumeBar":{"attributes":{"color":"#CA2127"}},"volumeBtn":{},"fullscreenBtn":{}}},"endSlate":{"attributes":{"enabled":"true","hideElements":"brandingLogo|viewAll"}},"replayView":{},"bufferingView":{}}},
        "xml":"<?xml version=\"1.0\" encoding=\"UTF-8\"?><application><player ad_control_enabled=\"true\" ad_server_timeout=\"20\" auto_play=\"true\" auto_play_list=\"false\" branding_preload=\"none\" caption_language=\"en\" core_ads_enabled=\"false\" core_player_name=\"amp-premier-player\" domain=\"akamai.com\" embed_domain=\"aep.com\" eventmanagementstates_enabled=\"false\" eventmanagementstates_status_interval=\"5\" eventmanagementstates_status_url=\"../resources/eventmanagement/playerstate.txt\" external_target=\"_blank\" hds_fragment_retry_data_gap_threshold=\"20\" locale_setting=\"en\" mbr_starting_index=\"3\" next_video_timer=\"15\" recommendations_partner=\"mrss\" scale_view_to_fit=\"true\" show_feature_bar=\"false\" subply_time_method=\"encoder\" use_last_known_bitrate=\"false\" video_id=\"4168856\" volume=\".5\"></player></application>"},
        "branding":{"logo":"#{paths.resources}images/akamai_logo1.png"},"dash":{"resources":[{"type":"text/javascript","src":"http://dashif.org/reference/players/javascript/1.4.0/dist/dash.all.js"}],"buffer":1},"locales":{"en":{"MSG_TIME_SEPARATOR":" / ","MSG_EMAIL_TO":"To","MSG_EMAIL_FROM":"From","MSG_EMAIL_VIDEO":"Email this video","MSG_EMAIL_MESSAGE_DEFAULT":"Check out this video from xxx","MSG_EMAIL_MESSAGE":"Message","MSG_EMAIL_ADDRESS_INVALID":"Invalid Email Address","MSG_EMAIL_MESSAGE_INVALID":"Please limit your message to 500 characters or less.","MSG_EMAIL_CHARACTERS_REMAINING_TEXT":" characters left","MSG_EMAIL_SEND_FAILURE":"Message could not be sent.","MSG_EMAIL_SEND_SUCCESS_MESSAGE":"Your email has been sent!","MSG_SHARE_VIDEO_TEXT":"Share this video...","MSG_POST_TEXT":"Post","MSG_EMBED_TEXT":"Embed","MSG_LINK_TEXT":"Link","MSG_SHARE_CONNECT_FAILURE":"Unable to connect. Please try again.","MSG_SHARE_CONTENT_DISABLED":"Share and embed are disabled.","MSG_VERSION_TEXT":"Version","MSG_BUFFERING_TEXT":"buffering","MSG_CUSTOMIZE_CLIP_POINTS":"Customize the start and end point of the video.","MSG_PAUSE":"Pause","MSG_PREVIEW":"Preview","MSG_CURRENT":"Currrent","MSG_SEEK_TO":"Seek to","MSG_LIVE":"LIVE","MSG_DEFAULT_ERROR_MESSAGE":"Sorry, we were unable to play the media you selected. Please try again, or select alternate media.","MSG_ERROR_PREFIX":"Error encountered:","MSG_STREAM_NOT_FOUND":"Stream not found","MSG_CURRENT_WORKING_BANDWIDTH":"Current working bandwidth","MSG_CURRENT_BITRATE_PLAYING":"Current bitrate playing","MSG_MAX_BITRATE_AVAILABLE":"Max bitrate available","MSG_NOT_AVAILABLE":"Not Available","MSG_GO_LIVE":"GO LIVE","MSG_REPLAY":"Replay","MSG_NEXT_VIDEO":"Video starts in: ","MSG_RECOMMENDED":"Recommended","MSG_VIEW_ALL":"View all ","MSG_VIDEO":" videos","MSG_CC":"CC","MSG_CC_TITLE":"Caption","MSG_CC_LANGUAGE":"Track :","MSG_CC_PRESETS":"Presets :","MSG_CC_FONT":"Font :","MSG_CC_EDGE":"Edge :","MSG_CC_SIZE":"Size :","MSG_CC_SCROLL":"Scroll :","MSG_CC_COLOR":"Color :","MSG_CC_BACKGROUND":"Background :","MSG_CC_WINDOW":"Window :","MSG_CC_OPACITY":"Opacity :","MSG_CC_SHOW_ADVANCED":"Show Advanced Settings","MSG_CC_HIDE_ADVANCED":"Hide Advanced Settings","MSG_NEXT_AD":"Next ad starts in: ","MSG_CC_RESET":"Default","MSG_CC_CANCEL":"Cancel","MSG_CC_APPLY":"Apply","MSG_EN":"English","MSG_ES":"Spanish","MSG_DE":"German","MSG_FR":"French","MSG_IT":"Italian","MSG_RU":"Russian","MSG_ZH":"Chinese","MSG_CHROMECAST_MESSAGE":"Video playing on another screen","MSG_RETRY_MESSAGE":"Content not yet available, retrying in","MSG_SECONDS":"seconds","MSG_RETRY_FAILED":"Retry failed","MSG_RECOMMENDATIONS_TITLE":"Recommended"},"es":{"MSG_TIME_SEPARATOR":" / ","MSG_EMAIL_TO":"a","MSG_EMAIL_FROM":"de","MSG_EMAIL_VIDEO":"Enviar este vГ­deo","MSG_EMAIL_MESSAGE_DEFAULT":"Echa un vistazo a este video de xxx","MSG_EMAIL_MESSAGE":"mensaje","MSG_EMAIL_ADDRESS_INVALID":"DirecciГіn de correo electrГіnico no vГЎlida","MSG_EMAIL_MESSAGE_INVALID":"Por favor limite su mensaje a 500 caracteres o menos.","MSG_EMAIL_CHARACTERS_REMAINING_TEXT":"personajes de la izquierda","MSG_EMAIL_SEND_FAILURE":"El mensaje no pudo ser enviado.","MSG_EMAIL_SEND_SUCCESS_MESSAGE":"Tu email ha sido enviado!","MSG_SHARE_VIDEO_TEXT":"Comparte este vГ­deo...","MSG_POST_TEXT":"enviar","MSG_EMBED_TEXT":"incrustar","MSG_LINK_TEXT":"enlace","MSG_SHARE_CONNECT_FAILURE":"No se puede conectar. Por favor, intГ©ntelo de nuevo.","MSG_SHARE_CONTENT_DISABLED":"Compartir e incrustar estГЎn desactivados.","MSG_VERSION_TEXT":"versiГіn","MSG_BUFFERING_TEXT":"el almacenamiento en bГєfer","MSG_CUSTOMIZE_CLIP_POINTS":"Personalizar el inicio y el punto final del video.","MSG_PAUSE":"romper","MSG_PREVIEW":"vista previa","MSG_CURRENT":"corriente","MSG_SEEK_TO":"Tratar de","MSG_LIVE":"EN VIVO","MSG_DEFAULT_ERROR_MESSAGE":"Lo sentimos, no hemos podido jugar los medios de comunicaciГіn seleccionados. Por favor, intГ©ntelo de nuevo, o seleccionar los medios de comunicaciГіn alternativos.","MSG_ERROR_PREFIX":"Se produjo un error:","MSG_STREAM_NOT_FOUND":"Stream que no se encuentra","MSG_CURRENT_WORKING_BANDWIDTH":"Ancho de banda actual de trabajo","MSG_CURRENT_BITRATE_PLAYING":"Tasa de bits de reproducciГіn actual","MSG_MAX_BITRATE_AVAILABLE":"Tasa de bits mГЎxima disponible","MSG_NOT_AVAILABLE":"No estГЎ disponible","MSG_GO_LIVE":"IR A VIVIR","MSG_REPLAY":"Repetir","MSG_NEXT_VIDEO":"El prГіximo video comienza en: ","MSG_RECOMMENDED":"Recomendado","MSG_CC":"CC","MSG_VIEW_ALL":"ver todo ","MSG_VIDEO":" vГ­deos.","MSG_EN":"InglГ©s","MSG_ES":"EspaГ±ol","MSG_DE":"AlemГЎn","MSG_FR":"FrancГ©s","MSG_IT":"Italiano","MSG_RU":"Ruso","MSG_ZH":"Chino","MSG_RETRY_MESSAGE":"Content not yet available, retrying in","MSG_SECONDS":"seconds","MSG_RETRY_FAILED":"Retry failed","MSG_RECOMMENDATIONS_TITLE":"Recomendado"}}
    }
    this.amp_config = debug_config;*/

    this.widgetClassType = "WNVideoCanvas";
    this.parentDivId = parentDivId;
    this.parentJSName = this.widgetClassType + this.fV.widgetId;
    this.groupId = this.fV.idKey;

    // Respond to screen width
    if (wn_isMobile) {
        // Get height/width ratio
        var div = $wn("#" + this.parentDivId);
        this.fV.widgetsContainer = div.parent();
        var ratio = this.fV.playerHeight / this.fV.playerWidth;
        var leftOffset = parseInt(div.position().left);
        var oldWidth = parseInt(this.fV.playerWidth) + leftOffset;
        var newWidth = div.parent().parent().width() - leftOffset;
        //var newWidth = wnVideoUtils.getResponsiveWidth(this.fV.playerWidth);
        if (newWidth < oldWidth) {
            this.fV.playerWidth = newWidth;
            // Calculate scaled height
            this.fV.originalHeight = parseInt(this.fV.playerHeight);
            this.fV.originalWidth = oldWidth;
            this.fV.top = div.position().top;
            this.fV.left = div.position().left;
            this.fV.originalContainerHeight = div.parent().height();
            this.fV.playerHeight = Math.round(this.fV.playerWidth * ratio);
            // resize widgets group container
            var diff = this.fV.originalHeight - this.fV.playerHeight;
            div.parent().height(div.parent().height() - diff);
            containerWidth = div.parent().parent().width();
            oldWidth = div.parent().width();
            if (containerWidth < oldWidth) div.parent().width(containerWidth);
        }
    }

    // Fired by Html gallery
    evntMgr.Register({
        evnt: "NewGalleryClip",
        func: "onNewGalleryClip",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    // Fired by Flash galery
    //evntMgr.Register({ evnt: "NewClip", func: "onNewGalleryClip", idKey: this.groupId, lstnr: this.fV.domId });

    // Render VideoCanvas
    this.render();

    //register for events
    $wn("#" + this.fV.domId).data("widget", this);
}

WNAmpVideoCanvas.prototype.resizeOnScreenFlip = function() {
    var div = $wn("#" + this.parentDivId);
    var leftOffset = parseInt(div.position().left);
    var defaultWidth = parseInt(this.fV.playerWidth) + leftOffset;
    var defaultHeight = parseInt(this.fV.playerHeight);
    var fitIntoWidth = div.parent().parent().width() - leftOffset;
    var containerWidth = div.parent().width();

    var gallery = getWigdetByType(this.groupId, "g");
    // if gallery and canvas are in the same container
    if (gallery.hasOwnProperty("widgetsContainer") && gallery.widgetsContainer.attr("id") == div.parent().attr("id")) {
        // if gallery is on the right side of the canvas
        var canvasBottom = div.position().top + parseInt(this.fV.playerHeight);
        if (gallery.top <= canvasBottom && gallery.left >= defaultWidth) {
            fitIntoWidth = (fitIntoWidth < gallery.left) ? fitIntoWidth : gallery.left;
            containerWidth = gallery.left;
        }
    }

    if (fitIntoWidth > containerWidth) fitIntoWidth = containerWidth;
    var newWidth = fitIntoWidth;
    //$wn(this.canvas).width(newWidth);
    div.width(newWidth);
    div.height(defaultHeight);
    //$wn("#" + this.fV.domId).width(newWidth);
    div.find(".wn-embed-code").width(newWidth - 160);
    div.find(".share-link").width(newWidth - 160);
}

WNAmpVideoCanvas.prototype.render = function() {
    var that = this; //http://www.crockford.com/javascript/private.html
    var config;

    // Html gallery will look for following structure - $wn('#wnVideoHTML', '#wn_html_video_' + mythis.groupId)
    var AMPMarkup = '<div id="' + this.fV.domId + '">' +
        '    <div id="wn_html_video_' + this.groupId + '" >' +
        '        <div id="wnVideoHTML"></div>' +
        '</div></div>';
    $wn("#" + this.fV.domId + "_swfObject").html(AMPMarkup);
    $wn("#" + this.parentDivId).addClass("ampVideoCanvas"); 
    $wn("#" + this.parentDivId + " .wn-share-pane").html(
        '       <div class="wn-close_button">Close X</div>' +
        '       <div class="wn-embed-fields">' +
        '          <div class="wn-sh-label">Embed Video Code</div>' +
        '          <textarea class="wn-embed-code sh-input" readonly></textarea>' +
        '       </div>' +
        '       <div class="wn-link-fields">' +
        '          <div class="wn-sh-label">Link to Video</div>' +
        '          <input type="text" class="share-link sh-input"/>' +
        '       </div>'
    );

    /* Set styles and dimentions */
    style = "<style type='text/css'>" +
        "#" + this.fV.domId + " {width: 100%; height: 100%;} ";
    // On mobile device scale down to fit UI controls properly
    if (wn_isMobile) {
        style += "#" + this.fV.domId + "_swfObject {width: 140%; height: 140%; -webkit-transform: scale(0.715,0.715); -webkit-transform-origin: top left; -moz-transform: scale(0.715,0.715); -moz-transform-origin: top left;} "; // simple transform doesn't work on iOS, using -webkit-transform
        style += "#" + this.fV.domId + "_swfObject .akamai-captioning-settings { top: 10%; }";
    } else {
        style += "#" + this.fV.domId + "_swfObject {width: 100%; height: 100%; } ";
    }
    style += "#" + this.parentDivId + " {width: " + this.fV.playerWidth + "px; height:" + this.fV.playerHeight + "px;} " +
    "#" + this.parentDivId + " .wn-share-pane {height: " + this.fV.playerHeight + "px}" +
    "#" + this.parentDivId + " .wn-share-pane .sh-input {width: " + (this.fV.playerWidth - 160) + "px}";
    embedFieldHeight = (this.fV.playerHeight > 170) ? (this.fV.playerHeight - 150) : (this.fV.playerHeight - 130);
    style += "#" + this.parentDivId + " .wn-share-pane .wn-embed-code {height: " + embedFieldHeight + "px}" +
        "</style>";
    $wn("#" + this.parentDivId).append(style);

    if (typeof akamai.amp.AMP.defaults !== 'undefined' && akamai.amp.AMP.defaults.loaded) {
            akamai.amp.AMP.loadDefaults(this.amp_config);
            this.createAmpPlayerObj();
    } else {
        akamai.amp.AMP.loadDefaults(this.amp_config, function () {
            that.createAmpPlayerObj();
        });
    }

    $wn('#' + this.parentDivId).find('.wn-close_button').click(
        function() {
            $wn(this).parent().fadeOut(300);
        }
    );
}
WNAmpVideoCanvas.prototype.createAmpPlayerObj = function() {
    var config = {feed: {}};
    this.canvas = akamai.amp.AMP.create(this.fV.domId, config);
    $wn("#" + this.fV.domId).data("amp", this.canvas);
    this.canvas["widget"] = this;
    this.canvas.addEventListener("ready", this.renderComplete);
}

WNAmpVideoCanvas.prototype.companionHandler = function(e) {
    var ads = e.data.companions;
    //var canvas = (typeof e.player !== 'undefined') ? e.player.widget : e.target.player.widget;
    var canvas = e.target.player.widget;
    if (ads.length < 1) {
        canvas.setUAFCompanions(canvas.wnAdProps);
    } else {
        // convert to the flash compatible vast format
        for (i = 0; i < ads.length; i++) {
            ads[i].htmlResource = ads[i].data;
            // AdWidgets
            evntMgr.Notify({
                evnt: "UAFLoadVastCompanions",
                idKey: canvas.groupId,
                objClip: {},
                adProps: {},
                vastAd: ads[i]
            });
        }
        // Platform ads - need to pass the entire list in order to compare to enabled site config companions
        wnVideoWidgets.SetVASTCompanions({
            companions: ads,
            adProps: canvas.wnAdProps
        });
    }
}

WNAmpVideoCanvas.prototype.setUAFCompanions = function(adProps) {
    // Platform ads
    wnVideoReloadCompanionAds(adProps, this.currentClip);
    // Ad Widgets
    evntMgr.Notify({
        evnt: "UAFLoadCompanions",
        idKey: this.groupId,
        objClip: this.currentClip,
        adProps: adProps
    });
};

WNAmpVideoCanvas.prototype.renderComplete = function(e) {
    this.isRendered = true;

    // livestream
    if (this.widget.fV['videoType'] == 'livestream') {
        var streams = new Array;

        // desktop stream
        if (this.widget.fV['liveStreamUrl']) {
            streams.push({
                'url': this.widget.fV['liveStreamUrl'],
                'type': 'video/f4m'
            });
        }
        // mobile streams
        if (this.widget.fV['liveStreams']) {
            /* UX-1792 - The video widget do not play on IE8
               In IE8 iterating over an array as "for (i in Array)" will try to go over all its' children, including methods (indexOf), not just elements.
               Must be replaced with "for (i=0; i < Array.length; i++) "
            */
            // for (i in this.widget.fV['liveStreams']) {
            for (i=0; i < this.widget.fV['liveStreams'].length; i++) {
                strm = this.widget.fV['liveStreams'][i];
                if (strm['url'] != "") {
                    var type = (strm['url'].indexOf(".m3u8") != -1) ? "application/x-mpegURL" : strm['type'];
                    streams.push({
                        'url': strm['url'],
                        'type': type
                    });
                }
            }
        }

        var liveStreamJSON = {
            'videoType': 'livestream',
            'isLiveStream': true,
            'id': '0',
            'uri': streams,
            'headline': this.widget.fV['liveStreamHeadline'],
            'mainHeadline': this.widget.fV['liveStreamHeadline'],
            'graphic': this.widget.fV['liveStreamSummaryImgUrl'],
            'adTag': this.widget.fV['liveStreamAdTag'],
            'ownerAffiliateNumber': this.widget.fV['affiliateNumber'],
            'ownerAffiliateName': this.widget.fV['affiliate'],
            'ownerAffiliateBaseUrl': '',
            'isMilliseconds': 'true',
            'vt': 'V',
            'hasCC': '0',
            'duration': "",
            'summary': '',
            'creationdate': ' ', // important. If passed as empty string, will brake Amp logic
            'isAd': 'False',
            'serveAdsFromContentOwner': 'False',
            'displaysize': '20',
            'hasAdPreRoll': this.widget.fV['liveStreamHasPreroll'] == 'true' ? 'true' : 'false',
            'preRollSplitPct': '100',
            'preRollOwner': 'loc',
            'hasAdPostRoll': this.widget.fV['liveStreamHasPostroll'] == 'true' ? 'true' : 'false',
            'postRollSplitPct': '100',
            'postRollOwner': 'loc',
            // mobile ads config
            'mobile_hasAdPreRoll': this.widget.fV['liveStreamHasPreroll'] == 'true' ? 'true' : 'false',
            'mobile_preRollSplitPct': '100',
            'mobile_preRollOwner': 'loc',
            'mobile_hasAdPostRoll': this.widget.fV['liveStreamHasPostroll'] == 'true' ? 'true' : 'false',
            'mobile_postRollSplitPct': '100',
            'mobile_postRollOwner': 'loc'
        };

        this.widget.loadClip(liveStreamJSON);
    }
    // clip by id
    else if (this.widget.fV.clipId > 0) {
        var clipPageUrl = wnVideoWidgets.getClipPageUrl({'id': this.widget.fV.clipId, 'pageUrl':"videoclip"});
        var mrssjsonUrl = clipPageUrl + '?clienttype=mrssjson&callback=wnVideoWidgets.ampObjCallback&callbackparams=' + this.widget.fV.domId;
        wnVideoUtils.loadScript('wnmrssjson', mrssjsonUrl);
    // clip via SetVideoUrl()
    } else if (this.widget.fV.flvUri) {
        var streams = new Array ({ 'url': this.widget.fV.flvUri, 'type': wnVideoUtils.getVideoFormatByUrl(this.widget.fV.flvUri) });
        var video_url_json = {
            'id': '0',
            'uri': streams,
            'headline': '',
            'mainHeadline': '',
            'graphic': '',
            'adTag': '',
            'ownerAffiliateNumber': '',
            'ownerAffiliateName': '',
            'ownerAffiliateBaseUrl': '',
            'isMilliseconds': 'true',
            'vt': 'V',
            'hasCC': '0',
            'summary': '',
            'creationdate': ' ', // important. If passed as empty string, will brake Amp logic
            'isAd': 'False'
        };
        if (this.widget.fV.enableAds == false) {
            video_url_json['hasAdPreRoll'] = 'false';
            video_url_json['hasAdPostRoll'] = 'false';
            video_url_json['mobile_hasAdPreRoll'] = 'false';
            video_url_json['mobile_hasAdPostRoll'] = 'false';
        }
        this.widget.loadClip(video_url_json);
    } else {
        var provider = getWigdetByType(this.widget.groupId, "g");
        if (provider.data) { // gallery loaded and is ready to provide data
            var clipObj = provider.data.objClip;
            //if (clipObj.uri && this.fV.isPrimaryGallery) {
            if (clipObj) {
                clipObj.isUserInitiated = false;
                clipObj.domId = this.widget.fV.domId;
                clipObj.ordinance = 0;
                this.widget.loadClip(clipObj);
            }
        }
    }
    // For backward compatibility firing following event, but it's not needed for AMP
    onWidgetLoad(this.widget.fV.domId, this.widget.groupId, "canvas2IsReady", "");


    if (this.widget.canvas.share != null) {
        var that = this;
        //this.widget.canvas.share.addEventListener("share", that.widget.shareHandler); // opens AddThis default UI
        this.widget.canvas.share.addEventListener("share", that.widget.showEmbed); // shows embed code form
    }

    // bind companion ads handler
    if (this.widget.canvas.ads != null) this.widget.canvas.ads.addEventListener("companion", this.widget.companionHandler);
    // set ad call url right befor the playback process starts
    this.widget.canvas.addEventListener("mediaPlayerPlaybackSequenceInitiated", this.widget.onMediaPlayerPlaybackSequenceInitiated);
    // bind analytics events
    this.widget.canvas.addEventListener('started', this.widget.reportEvent);
    this.widget.canvas.addEventListener('pause', this.widget.reportEvent);
    this.widget.canvas.addEventListener('ended', this.widget.reportEvent);
    /* UX-1582 - "Resume" google analytic call fired when clip/live stream renders after preroll
    For backward compatibility this event shouldn't be reported
    this.widget.canvas.addEventListener('resume', this.widget.reportEvent); */
    this.widget.canvas.addEventListener('emptied', this.widget.reportEvent);
    this.widget.canvas.addEventListener('loadstart', this.widget.reportEvent);
    this.widget.canvas.addEventListener('progress', this.widget.reportEvent);
    this.widget.canvas.addEventListener('suspend', this.widget.reportEvent);
    this.widget.canvas.addEventListener('abort', this.widget.reportEvent);
    this.widget.canvas.addEventListener('error', this.widget.reportEvent);
    this.widget.canvas.addEventListener('emptied', this.widget.reportEvent);
    this.widget.canvas.addEventListener('stalled', this.widget.reportEvent);
    this.widget.canvas.addEventListener('loadedmetadata', this.widget.reportEvent);
    this.widget.canvas.addEventListener('loadeddata', this.widget.reportEvent);
    this.widget.canvas.addEventListener('canplay', this.widget.reportEvent);
    this.widget.canvas.addEventListener('canplaythrough', this.widget.reportEvent);
    this.widget.canvas.addEventListener('play', this.widget.reportEvent);
    //this.widget.canvas.addEventListener('playing', this.widget.reportEvent);
    this.widget.canvas.addEventListener('waiting', this.widget.reportEvent);
    this.widget.canvas.addEventListener('seeking', this.widget.reportEvent);
    this.widget.canvas.addEventListener('seeked', this.widget.reportEvent);
    this.widget.canvas.addEventListener('durationchange', this.widget.reportEvent);
    this.widget.canvas.addEventListener('ratechange', this.widget.reportEvent);
    this.widget.canvas.addEventListener('volumechange', this.widget.reportEvent);
    this.widget.canvas.addEventListener('mediaPlayerFullscreenChange', this.widget.reportEvent); // desktop
    this.widget.canvas.addEventListener('fullscreenchange', this.widget.reportEvent); // mobile

    gamp = this.widget.canvas;
}
var gamp;

WNAmpVideoCanvas.prototype.onMediaPlayerPlaybackSequenceInitiated = function(e) {
    // set ad call url
    if (e.target.ima.config.enabled) e.target.setParams({"wnadcall" : e.target.widget.getAdUrl()});
}

WNAmpVideoCanvas.prototype.reportEvent = function(e) {
    // Report events only for content clips, ignore ads
    if (e.target.ads.getInProgress()) return false;
    // Disable reporting in producer
    if (typeof e.target.widget.fV.producerMode !== 'undefined' && e.target.widget.fV.producerMode == "clip-edit") return false;

    var data = e.target.feed.getData();
    var clip = {};
    for (i in e.target.widget.currentClip) clip[i] = e.target.widget.currentClip[i];
    clip.url = clip.landingPageURL;
    clip.currentTime = e.target.getCurrentTime() * 1000;
    clip.currentTimePct = Math.round(clip.currentTime/clip.duration * 100);
    // adjust event types
    var type = e.type;
    switch (type) {
        case "started":
            type = "duration";
            break;
        case "pause":
            type = "user_pause";
            break;
        /* UX-1582 - "Resume" google analytic call fired when clip/live stream renders after preroll
        For backward compatibility this event shouldn't be reported AT ALL
        case "play":
            if (e.target.getCurrentTime() > 0) {
                type = "resume";
            }
            break;*/
        case "ended":
            type = "clipended";
            break;
        case "volumechange":
            if (e.data.requestSource != "user") {
                if (e.target.getVolume() == 0) {
                    type = "mute";
                } else if (typeof this.volume_level !== 'undefined' && this.volume_level == 0) {
                    type = "unmute";
                } else {
                    type = "";
                }
            }
            this.volume_level = e.target.getVolume();
            break;
        // case "mediaPlayerFullscreenChange": - fired only on desktop
        case "fullscreenchange":
            if (e.data) { // true - on fellscreen mode enter, false - on fullscreen mode exit
                type = "fullscreen";
            } else {
                type = "";
            }
            break;
    }
    if (type != "") {
        wnVideoWidgets.handleEvent({
            'event': {
                'type': type
            },
            'system': 'video',
            'clip': clip,
            'valid': true
        });
    }
}

WNAmpVideoCanvas.prototype.MrssJsonToWNClip = function(mrssJson) {
    var item = mrssJson.channel.item;
    // General mrssjson properties
    var wnClip = {
        // ad properties
        'ownerAffiliateNumber': this.fV['affiliateNumber'],
        'ownerAffiliateName': this.fV['affiliate'],
        // According to the business logic ownerAffiliateName belongs to the playing canvas, while ownerAffiliateNumber belongs to the clip (potentially shared from the different affiliate)
        'affiliateName': wnSiteConfigGeneral.affiliateName,
        // content properties
        'id': item.guid,
        'headline': item.title,
        'mainHeadline': item.title,
        'pageUrl': item.link.substring(item.link.lastIndexOf("/")+1),
        'autostart': false, // widget setting will be checked latter
        'graphic': item['media-group']['media-thumbnail']['@attributes'].url,
        'HasGraphic': (item['media-group']['media-thumbnail']['@attributes'].url != ""),
        //'HasThumb': (item['media-group']['media-thumbnail']['@attributes'].url != ""),
        //'hasCC': true,
        'summary': item.description,
        'creationDate': item.pubDate,
        'lasteditedDate': item.pubDate,
        'duration': item['media-group']['media-content'][0]['@attributes'].duration * 1000,
        'isMilliseconds': false,
        'isFlash': false,
        'isAd': false,
        'isAdIntro': false,
        'isAdExit': false,
        'isAdFrame': false,
        'isASX': false
    }
    // Worldnow specific properties
    wnClip['adTag'] = (typeof item['wn-contentclassification'] !== 'undefined') ? item['wn-contentclassification'] : "";
    if (typeof item['wn-ads'] !== 'undefined') {
        wnClip['hasAdPreRoll'] = item['wn-ads'].wnsz_30.show;
        wnClip['preRollOwner'] = item['wn-ads'].wnsz_30.owner;
        wnClip['preRollSplitPct'] = item['wn-ads'].wnsz_30.splitpct;
        wnClip['hasAdPostRoll'] = item['wn-ads'].wnsz_31.show;
        wnClip['postRollOwner'] = item['wn-ads'].wnsz_31.owner;
        wnClip['postRollSplitPct'] = item['wn-ads'].wnsz_31.splitpct;
        wnClip['mobile_hasAdPreRoll'] = item['wn-ads'].wnsz_130.show;
        wnClip['mobile_preRollOwner'] = item['wn-ads'].wnsz_130.owner;
        wnClip['mobile_preRollSplitPct'] = item['wn-ads'].wnsz_130.splitpct;
        wnClip['mobile_hasAdPostRoll'] = item['wn-ads'].wnsz_131.show;
        wnClip['mobile_postRollOwner'] = item['wn-ads'].wnsz_131.owner;
        wnClip['mobile_postRollSplitPct'] = item['wn-ads'].wnsz_131.splitpct;
    } else {
        wnClip['hasAdPreRoll'] = "false";
        wnClip['preRollOwner'] = "nat";
        wnClip['preRollSplitPct'] = "100";
        wnClip['hasAdPostRoll'] = "false";
        wnClip['postRollOwner'] = "nat"
        wnClip['postRollSplitPct'] = "100"
        wnClip['mobile_hasAdPreRoll'] = "false";
        wnClip['mobile_preRollOwner'] = "nat";
        wnClip['mobile_preRollSplitPct'] = "100";
        wnClip['mobile_hasAdPostRoll'] = "false";
        wnClip['mobile_postRollOwner'] = "nat";
        wnClip['mobile_postRollSplitPct'] = "100";
    }
    if (typeof item['wn-taxonomy'] !== 'undefined') {
        wnClip['taxonomy1'] = item['wn-taxonomy'].level1;
        wnClip['taxonomy2'] = item['wn-taxonomy'].level2;
        wnClip['taxonomy3'] = item['wn-taxonomy'].level3;
    } else {
        wnClip['taxonomy1'] = "";
        wnClip['taxonomy2'] = "";
        wnClip['taxonomy3'] = "";
    }
    return wnClip;
}

WNAmpVideoCanvas.prototype.WNClipToMrssJson = function(wnClip) {
    mrssJson = {
        "@attributes": {
            "version": "2.0"
        },
        "channel": {
            "category": "",
            "pubDate": (wnClip.creationdate != undefined) ? wnClip.creationdate : wnClip.creationDate, // "Wed, 03 Aug 2011 10:00:21 -0400"
            "language": "en",
            "ttl": "10",
            "item": {
                "title": wnClip.headline,
                //"link": wnVideoWidgets.getLinkToVideo(this.currentClip, this.fV),
                "link": wnVideoWidgets.getLinkToVideo(wnClip, this.fV),
                "description": wnClip.summary,
                "category": "",
                "pubDate": (wnClip.creationdate != undefined) ? wnClip.creationdate : wnClip.creationDate, // "Wed, 03 Aug 2011 10:00:21 -0400"
                "guid": wnClip.id,
                "media-group": {
                    //"media-content" : [],
                    "media-title": wnClip.headline,
                    "media-description": wnClip.summary,
                    "media-subTitle": [{
                            "@attributes": {
                                "type" : (wnClip.videoType == 'livestream') ? "live-oncaptioninfo" : "application/ttml+xml",
                                "lang": "en-us",
                                "href": (wnClip.videoType == 'livestream') ? "" : wn.staticimagefarmprefix + "/metadata/" + wnClip.id + ".ttml"
                            }
                        }
                        /*,{
                            "@attributes" : {
                            "type" : "application/ttml+xml",
                            "lang" : "de",
                            "href" : "German_track.xml"
                            }
                        }*/
                    ],
                    "media-embed": {
                        "@attributes": {
                            /* TODO: Look at share_button.click( of WNVideoCanvas.prototype.render */
                            "url": "http://player.js",
                            "width": "604",
                            "height": "341"
                        },
                        "media-param": {
                            "@attributes": {
                                "name": "type"
                            },
                            "#text": "text/javascript"
                        }
                    }
                    /*,"media-thumbnail" : {
                        "@attributes" : {
                            "url" : "http://projects.mediadev.edgesuite.net/customers/akamai/mdt-html5-core/premier/2.2.0004/resources/images/hd_world.jpg",
                            "width" : "604",
                            "height" : "341"
                        }
                    }*/
                }
            }
        }
    }

    // Set graphic
    if (wnClip.graphic) {
        mrssJson.channel.item['media-group']['media-thumbnail'] = {
            '@attributes': {
                url: wnClip.graphic
                //width : "604", // TODO: set real size
                //height : "341" // TODO: set real size
            }
        };
    } else {
        mrssJson.channel.item['media-group']['media-thumbnail'] = {
            '@attributes': {
                url: " "
            }
        };
    }

    // Create video sources
    mrssJson.channel.item['media-group']['media-content'] = new Array();
    // live stream or clip from api clip.json call
    if (wnClip.videoType == 'livestream' || wnClip.uri) {
        for (j in wnClip.uri) {
            if (wnClip.uri[j].type && wnClip.uri[j].url) {
                mrssJson.channel.item['media-group']['media-content'].push({
                    '@attributes': {
                        isDefault: (j == "0"), // "true"
                        url: decodeURIComponent(wnClip.uri[j].url),
                        type: wnClip.uri[j].type,
                        medium: "video",
                        duration: Math.floor(wnClip.duration/1000)
                    }
                });
            }
        }
    // clip from gallery
    } else if (wnClip.flvUri) {
        mrssJson.channel.item['media-group']['media-content'].push({
            '@attributes': {
                isDefault: "true",
                url: wnClip.flvUri,
                type: "video/mp4", ///////////////////////////////////
                medium: "video",
                duration: Math.floor(wnClip.duration/1000)
                //duration: wnClip.duration
            }
        });
    } else {
        wnLog('Unexpected video source format');
    }

    return mrssJson;
}

WNAmpVideoCanvas.prototype.loadClip = function(data, playOnLoad) { // data - either WN clip object or Mrss Json object
    if (typeof playOnLoad === 'undefined') playOnLoad = false;
    // AMP Feed object
    if (data.hasOwnProperty("channel")) {
        var mrssJson = data;
        // If clip has other clips assigned
        if ($wn.isArray(mrssJson.channel.item)) {
            mrssJson.channel.item = mrssJson.channel.item[0];
        }
        var item = mrssJson.channel.item;
        var mockProps = {
            "owner": "loc",
            "splitpct": "100",
            "show": "false"
        };
        // clean up item obj
        if(!item['wn-ads'])
            item['wn-ads'] = {};
        if(!item['wn-ads']['wnsz_30'])
            item['wn-ads']['wnsz_30'] = mockProps
        if(!item['wn-ads']['wnsz_31'])
            item['wn-ads']['wnsz_31'] = mockProps;
        if(!item['wn-ads']['wnsz_130'])
            item['wn-ads']['wnsz_130'] = mockProps;
        if(!item['wn-ads']['wnsz_131'])
            item['wn-ads']['wnsz_131'] = mockProps;
        var wnClip = this.MrssJsonToWNClip(mrssJson);
    // WN clip object
    } else {
        // Clip object passed from a gallery is missing some video formats
        // Instead of creating mrssJson based on passed clip object,
        // Make a mrssjson call using clip id
        if (data.id > 0) {
            var mrssjsonUrl = 'http://' + wn.baseurl + '/clip/' + data.id + '/videoclip?clienttype=mrssjson&callback=wnVideoWidgets.ampObjCallback&callbackparams=' + this.fV.domId + '|' + playOnLoad;
            wnVideoUtils.loadScript('wnmrssjson', mrssjsonUrl);
            return;
        // livestream or clip via SetVideoUrl()
        } else {
            var wnClip = {};
            for (i in data) wnClip[i] = data[i];
            wnClip.headline = unescape(data.headline);
            wnClip.HasThumb = (data.thumb && data.thumb.length > 0) ? true : false;
            wnClip.isMilliseconds = false;
            wnClip.isFlash = false;
            wnClip.isAd = false;
            wnClip.isAdIntro = false;
            wnClip.isAdExit = false;
            wnClip.isAdFrame = false;
            wnClip.isASX = false;
            // According to the business logic ownerAffiliateName belongs to the playing canvas, while ownerAffiliateNumber belongs to the clip (potentially shared from the different affiliate)
            wnClip.affiliateName = wnSiteConfigGeneral.affiliateName;
            // For mobile sites use mobile pre/post roll info
            if (wn_isMobile && wn_isPlatformSiteMobile) {
                wnClip.mobile_hasAdPreRoll = (typeof data.mobile_hasAdPreRoll == "boolean") ? data.mobile_hasAdPreRoll.toString() : data.mobile_hasAdPreRoll;
                wnClip.mobile_hasAdPostRoll = (typeof data.mobile_hasAdPostRoll == "boolean") ? data.mobile_hasAdPostRoll.toString() : data.mobile_hasAdPostRoll;
            } else {
                wnClip.hasAdPreRoll = (typeof wnClip.hasAdPreRoll == "boolean") ? wnClip.hasAdPreRoll.toString() : data.hasAdPreRoll;
                wnClip.hasAdPostRoll = (typeof wnClip.hasAdPostRoll == "boolean") ? wnClip.hasAdPostRoll.toString() : data.hasAdPostRoll;
            }
            wnClip.preRollSplitPct = parseInt(data.preRollSplitPct);
            wnClip.preRollOwner = data.preRollOwner;
            wnClip.postRollSplitPct = parseInt(data.postRollSplitPct);
            wnClip.postRollOwner = data.postRollOwner;
            wnClip.mobile_preRollSplitPct = parseInt(data.mobile_preRollSplitPct);
            wnClip.mobile_preRollOwner = data.mobile_preRollOwner;
            wnClip.mobile_postRollSplitPct = parseInt(data.mobile_postRollSplitPct);
            wnClip.mobile_postRollOwner = data.mobile_postRollOwner;
            var mrssJson = this.WNClipToMrssJson(wnClip);
        }
    }
    this.currentClip = {};
    for (i in wnClip) this.currentClip[i] = wnClip[i];

    // Ad logic
    // ignore post-rolls for now
    /*if (wn_isMobile && (wnVideoUtils.stringToBoolean(this.currentClip.mobile_hasAdPreRoll) || wnVideoUtils.stringToBoolean(this.currentClip.mobile_hasAdPostRoll)) ||
        !wn_isMobile && wnVideoUtils.stringToBoolean(this.currentClip.hasAdPreRoll) || wnVideoUtils.stringToBoolean(this.currentClip.hasAdPostRoll))*/
    if (wn_isMobile && wn_isPlatformSiteMobile && (wnVideoUtils.stringToBoolean(this.currentClip.mobile_hasAdPreRoll)) ||
        !(wn_isMobile && wn_isPlatformSiteMobile) && wnVideoUtils.stringToBoolean(this.currentClip.hasAdPreRoll))
    {
            //this.canvas.ima.enable(); - is not implemented in html5 by AMP
            this.canvas.ima.config.enabled = true;
            this.canvas.setParams({"wnadcall" : this.getAdUrl()});
    } else {
            //this.canvas.ima.disable(); - is not implemented in html5 by AMP
            this.canvas.ima.config.enabled = false;
            this.canvas.setParams({"wnadcall" : ""});
    }



    // Set prefered clip format based on device type
    if (this.currentClip.videoType != "livestream") {
        var media = mrssJson.channel.item;
        var os = wnUserAgentParser.getOS().name.toLowerCase();
        var version = wnUserAgentParser.getOS().version;
        if (typeof(version) === 'string' && version.match('[\.]([^.]+)[\.]')) { // if format 10.10.3, return 10.1
    		version = version.substr(0, version.lastIndexOf("."));
    	}
    	version = Number(version);
        var tempMediafiles = [];
        var format;
        var tempcount = 0;

        for (var i = 0; i < media['media-group']['media-content'].length; i++) {
            format = wnVideoUtils.getVideoFormatByUrl(media['media-group']['media-content'][i]['@attributes'].url);
            // SMEF-256 - WVVA Front End Video Issues { 
            var contentaddons = wn.contentaddons || {};
            var betalist = contentaddons.betalist || "";
            var WNisProducerRegExp = new RegExp("://manage[A-Za-z0-9.]*\.worldnow.com");
            if (os != 'ios' && os != 'android' &&
                typeof this.fV.producerMode === 'undefined' && // don't apply in producer
                WNisProducerRegExp.test(document.location.href) == false && // don't apply in widget builder
                (betalist.indexOf('videoamphtml5-') >= 0 || // Expecting videomrssgallery-mac|windows
                wnVideoUtils.getQS('videoamphtml5') == 'mac')) // debug via URL
            {
                var videoamphtml5 = betalist;
                videoamphtml5 = videoamphtml5.substring(videoamphtml5.indexOf('videoamphtml5-') + 14);
                if (videoamphtml5.indexOf(',') >= 0) {
                    videoamphtml5 = videoamphtml5.substring(0, videoamphtml5.indexOf(','));
                }
                if (wnUserAgentParser.getBrowser().name == 'Chrome' && // Chrome
                    (os == 'mac os x' && videoamphtml5.indexOf('mac') >= 0 || // Mac or windows and siteconfig
                     os.indexOf('windows') >= 0 && videoamphtml5.indexOf('windows') >= 0 ||
                     wnVideoUtils.getQS('videoamphtml5') == 'mac') && // debug
                    format != 'video/mp4' 
                ) {
                    // dont add
                } else {
                    tempMediafiles[tempcount] = media['media-group']['media-content'][i];
                    tempcount = tempcount + 1;
                }
            // SMEF-256 }
            } else if (!wnSiteConfigVideo.enableABRStreaming) {
                if(format == 'video/f4m' || format == 'application/x-mpegURL') { // f4m or m3u8
                    // dont add
                } else {
                    tempMediafiles[tempcount] = media['media-group']['media-content'][i];
                    tempcount = tempcount + 1;
                }
            } else if (os == 'ios' || os == 'android') {
                if(format == 'video/f4m') { // f4m
                    // dont add
                } else if (format == 'application/x-mpegURL' && // m3u8
                    os == 'android' &&
                    wnVideoUtils.validateString(wn.contentaddons.betalist) && wn.contentaddons.betalist.indexOf("videodisableabrandroid") > -1 &&
                    !isNaN(version) && version <= 3.7)
                {
                    // dont add
                } else {
                    tempMediafiles[tempcount] = media['media-group']['media-content'][i];
                    tempcount = tempcount + 1;
                }
            } else {
                if(format == 'application/x-mpegURL') { // m3u8
                    // dont add
                } else {
                    //
                    // Temporary fix 05/26/2015. Disable f4m for chrome and mac. {
                    //
                    if (format == 'video/f4m' &&
                        wnVideoUtils.validateString(wn.contentaddons.betalist) && wn.contentaddons.betalist.indexOf("videodisablehdschrome") > -1 &&
                        os == 'mac os x' &&
                        !isNaN(version) && version >= 10.1 &&
                        wnUserAgentParser.getBrowser().name == 'Chrome')
                    {
                        // dont add
                    //
                    // } Temporary fix 05/26/2015
                    //
                    } else {
                        tempMediafiles[tempcount] = media['media-group']['media-content'][i];
                        tempcount = tempcount + 1;
                    }
                }
            }
        };
        // swap array
        mrssJson.channel.item['media-group']['media-content'] = tempMediafiles;
    }

    // Handle empty thumbnail graphics
    // UX-1767 - Lower-left Hand Play Button Doesn't Work on Desktop AMP
    if (mrssJson.channel.item['media-group']['media-thumbnail']['@attributes']['url'] == "") {
        mrssJson.channel.item['media-group']['media-thumbnail']['@attributes']['url'] = " ";
    }

    if (playOnLoad) {
        this.canvas.setAutoplay(true);
    } else {
        this.canvas.setAutoplay(this.fV.isAutoStart.toLowerCase() == 'true');
    }

    evntMgr.Notify({
        evnt: 'NewClip',
        idKey: this.groupId,
        objClip: wnClip
    });

    // Pass data to the Amp canvas
    this.canvas.feed.setData(mrssJson);
};

WNAmpVideoCanvas.prototype.onNewGalleryClip = function(p_evtObj) {
    if (!this.canvas.isRendered) return;
    wnLog('onNewGalleryClip()');
    var clipObj = p_evtObj.objClip;
    if (clipObj.isUserInitiated) { // user manually clicked on the clip thumbnail in the gallery
        this.loadClip(clipObj, true);
    } else { // on gallery load
        // if canvas doesn't have a clipId or a live stream assigned
        if ((!wnVideoUtils.validateString(this.fV.clipId) || parseInt(this.fV.clipId) < 1) && this.fV['videoType'] != 'livestream') {
            this.loadClip(clipObj, false);
        }
    }
};

WNAmpVideoCanvas.prototype.getAdUrl = function() {
    var adProps = new Object();
    adProps.wncc = this.currentClip.adTag;
    adProps.wnccx = this.fV.advertisingZone;
    if (this.fV.playerAdvertisingType != "none") {
        adProps.apptype = this.fV.playerAdvertisingType;
    } else {
        // Override the player ad name for previously specified players
        switch (this.fV.playerType.toLowerCase()) {
            case 'popup':
                adProps.apptype = 'videopopup';
                break;
            case 'standard':
                adProps.apptype = 'videostandard';
                break;
            case 'embedded':
                adProps.apptype = 'videoembedded';
                break;
            case 'landingpage':
                adProps.apptype = 'videolandingpage';
                break;
            default:
                adProps.apptype = 'video';
        }
    }
    this.currentClip.apptype = adProps.apptype;
    this.currentClip.idKey = this.idKey;

    // adding property needed for dfpvideo and realvu
    adProps['videoplayerdivid'] = this.parentDivId; // wnVideoCanvasContainer1
    adProps['playerwidth'] = this.fV.playerWidth;
    adProps['playerheight'] = this.fV.playerHeight;

    //case "preroll":
    adProps.width = adProps.height = "10";
    adProps.wnsz = "30";
    if (this.fV.usePrerollMaster.toLowerCase() == "true") {
        adProps.sequence = 1; // Forces the admanager.js to reset its counters (tile=1, new ord number etc)
    } else {
        // If no master ad widget exists on the page then reset admanager counters
        var masterAdExists = false;
        masterAdExists = wnVideoWidgets.masterAdExists(this.currentClip);
        if (masterAdExists != true) {
            adProps.sequence = 1;
        }
    }

    /*
    With new IMA SDK 3 logic all ad calls are made before the content. Separate ad call for post-roll isn't sent any more.
    case "postroll":
        adProps.width = adProps.height = "11";
        adProps.wnsz = "31";
        break
    */

    var adTagUrl = "";
    var ad = wnVideoWidgets.UAFHelper(adProps, this.currentClip, 'raw');
    if (ad != null) {
        adTagUrl = ad.url;
        // Debug
        if (wn_debug_widgets == "true" && wnVideoUtils.validateString(this.fV.debugAdUrl)) {
            adTagUrl = this.fV.debugAdUrl;
        }
        adProps.owner = ad.owner; // Override the companion owners with the pre-roll owner
    }
    this.wnAdProps = adProps;
    return adTagUrl;
};

WNAmpVideoCanvas.prototype.shareHandler = function(e) {
    e.target.player.exitFullScreen();
    var that = e.target.player.widget;
    var _linkToVideo = wnVideoWidgets.getLinkToVideo(that.currentClip, that.fV);
    /*wnLog("Share: ", e.data);
    wnLog("   Provider: " + e.data.provider);
    //wnLog(" Link: " + e.data.link);
    wnLog("   Link: " + _linkToVideo);
    wnLog("   Embed: " + e.data.embed.url);
    wnLog("   width: " + e.data.embed.width);
    wnLog("   height: " + e.data.embed.height);*/
    var parentDiv = document.getElementById(that.parentDivId);
    var _link = document.getElementById(that.parentDivId + "_addthis_button");
    if (_link != null) parentDiv.removeChild(_link);
    _link = document.createElement("a");
    _link.setAttribute("id", that.parentDivId + "_addthis_button");
    if (/ipad/i.test(navigator.userAgent.toLowerCase())) { // on iPad an element isn't clickable, if it doesn't have an onclick handler assigned
        _link.click = new function() { return false; }
    }
    _link.style.position = "absolute";
    parentDiv.insertBefore(_link, parentDiv.childNodes[0]);
    var addthis_share = {
        url: unescape(_linkToVideo),
        title: unescape(that.currentClip['headline']),
        description: unescape(that.currentClip['summary'])
    };
    addthis.button("#" + that.parentDivId + "_addthis_button", {}, addthis_share);
    _link.style.visibility = "hidden";
    _link.style.zIndex = "1";
    document.getElementById(that.parentDivId + "_addthis_button").onclick();

    // Report event
    /* Should not be fired according to the current logic. Reporting for shares should be handled by AddThis.
    var reporting_event = {
        'target': e.target.player,
        'type': "share"
    }
    that.reportEvent(reporting_event);
    */
    return false;
}

WNAmpVideoCanvas.prototype.showEmbed = function(e) {
    e.target.player.exitFullScreen();
    var that = e.target.player.widget;
    var parentDiv = $wn("#" + that.parentDivId);
    // TODO: address no clip id (livestream, direct video url)
    var _linkToVideo = wnVideoWidgets.getLinkToVideo(that.currentClip, that.fV);
    var _embedCode = wnVideoWidgets.getPlayerEmbedCode(that.currentClip, that.fV);
    parentDiv.find('.share-link').val(_linkToVideo);
    parentDiv.find('.wn-embed-code').html(_embedCode);
    parentDiv.find('.wn-share-pane').fadeIn(300);

    // Render AddThis buttons
    var divId = that.parentDivId;
    var parentDiv = $wn("#" + divId).find('.wn-share-pane')[0];
    var _link = document.getElementById(divId + "_addthis_button");
    if (_link != null) parentDiv.removeChild(_link);
    _link = document.createElement("div");
    _link.setAttribute("id", divId + "_addthis_button");
    _link.style.position = "absolute";
    _link.style.marginLeft = "20px";
    _link.style.marginTop = "20px";
    parentDiv.insertBefore(_link, parentDiv.childNodes[0]);
    var headline = unescape(that.currentClip['headline'].replace(/"/g, "'"));
    var summary = unescape(that.currentClip['summary'].replace(/"/g, "'"));
    var addthis_html = '<div id="' + divId + '_addthis_toolbox" class="addthis_toolbox addthis_default_style addthis_16x16_style"' +
        'addthis:url="' + unescape(_linkToVideo) + '" addthis:title="' + headline + '" ' + 'addthis:description="' + summary + '" style="width: 110px;">' +
        '<a class="addthis_button_facebook"></a>' +
        '<a class="addthis_button_twitter"></a>' +
        '<a class="addthis_button_gmail"></a>' +
        '<a class="addthis_button_reddit"></a>';
    if (/iphone/i.test(navigator.userAgent.toLowerCase())) {
        addthis_html += '<a class="addthis_button_more"></a>';
    } else {
        addthis_html += '<a class="addthis_button_compact"></a>';
    }
    addthis_html += '</div>';
    $wn(_link).html(addthis_html);
    addthis.toolbox("#" + divId + "_addthis_toolbox");
    _link.style.visibility = "visible";
    _link.style.zIndex = "200";
}

WNAmpVideoCanvas.prototype.pauseMedia = function() {
    this.canvas.pause();
}

WNAmpVideoCanvas.prototype.playMedia = function() {
    this.canvas.play();
}

WNAmpVideoCanvas.prototype.ExternalSetVideoTime = function(timeVal) {
    this.canvas.setCurrentTime(timeVal);
}

WNAmpVideoCanvas.prototype.ExternalGetTimerObject = function() {
    return {'videoCurrentTime': this.canvas.getCurrentTime()}
}



/* Class: WNVideoCanvas
- HTML version of the Flash video canvas widget
*/
WNVideoCanvas = function(parentDivId, flashVars) {
    this.labels = {
        'SHARE': '<img style="display:inline !important" width="25" height="25" title="Share This Video" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAIAAABLixI0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAXVJREFUeNqsVTGWREAUXDP7RA4wcnIiycg5wMgdgJycCziAHPlmEyA3B3AAkcgBtmb+Pmtps9pTUfvdqn/Xr98tlGX5cRA+d/xTVVVRFDS2LMu27Z1cSZLkeT5+Nk3zeDyCIMD4xEXUtu2UiPD1AjcXTseM13XNzbWGYRj4uLqugzTMqcvlslV7sKRpSqIw4bruHy7sCTmgrqZpKDNtNWWRJOl2u+m6Hscx4vQXgp7n0WKBvDqrNIAV4J2yABiMRcAsKEzTHINPLmTk+z4z+SXLP75fEwLHiaJoCwvhRKIw5yDcdqIfLlKOWT4ulz25xuZcNofjOG+sMMMZ1qC8Zj6E3n3fIzX0B6ZkWZ6mjzre73fMIi6K4m8dR39R00/9haTgLzosSoGNoWAYhjN/0cmELXfhlJGJLMuw95ns/x6qquLIWA1/UhsvYRgGR2/bL6zda4fdOWRDPi50HzN+vV65uRRFgXBrZxd2vGlr75Bw4Pv4LcAASzDOV39SERwAAAAASUVORK5CYII=" />',
        'CLOSE_X': 'Close X',
        'EMBED_VIDEO': 'Embed Video Code:',
        'LINK_VIDEO': 'Link to Video:',
        'CC_OFF': '<img id="wnVideoCCOFF" style="display:inline" width="52" height="25" title="Play Without Closed Captions" alt="Play Without Closed Captions" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAZCAIAAADxKJpUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAt5JREFUeNrkV0FoE0EUrU1oMGmwotE2UI0QKfSkEPHS0nM9J15Deiw0J0HIQdFDRPCUQntL6VHSsz2XHBUKQoWSQLcW0kosCkkXjAR98Wd/f2dnN3vqBpzDsDuZ/f/t+/+9nQRT76sjwzpGR4Z4BJX77lm79WXXPKxfMo5QbPLq3WQ4cd8R3OnO9tfNEvD5RdXNhcXEcoFvA/HMEl2BsPq7wp/fHR/riIp1zfa1B49V5r59qNh3R2cfgvCxW1Otvd1O8/hX80QbFOUYi01iNo1ap3mCWRsqnEj+/Fh1CsIwprN5FZxSzYlH83eyeaTs36dzxK6xVpTRkfLecuF8m1WEo81VCRG4Z16WcBFPLzW2yloi5OMI66hWYE8+KyopCcrs242QtY4WQUqHbWVKwK/ab6PIOILjV6X3vVoJgt5+kqFr1AhU7b/KN7Y2ODq1KaVh1rEB246EnqazK05ZgQz4uN29WgmGRLb3PEfJQHX3rEVoAuHxf9ueAh8h23+dpyL2bMioUwWDkah77ng6N5GaM9bfaHtUD45L0KiUZSNSowQiUTR1b1tqjta/72zL6KR6EsfAwhGFiAzKB4OTjfLjU9UuJRmXLgirHFixL7oMqpUdn9vn6zLdGC4zvN9WLREqODitvfkk/5AYWQmHs780HsQ22SHuA216sF4cDA4G27E8lmXLt1ArJEaaRURrvS9b7kV4JLZh9kIY1AMP8sQcqY/FgQSYkZuQ9SOabSkOmPDMixLRjBnX1nseuyODaD6vZFyko7ESOAhsgsSIZEpxe35bKRNzOMXcWFhkttQ4lm9rCUMdBypaLwiY6qnF34VTg1HDT/xtPVgrahFQsWRunBukH7kTxuMKH9OVcHQABG0By+iRgPtMDhT9emoeJxd+AW1iNAZCOQWRgyGdg8MzaEzfPUU296hy+JG68+UkzMguMMftYh7WfEEWik0pp6+gvYG8m+d//dfwrwADAAUigxLFL+mkAAAAAElFTkSuQmCC" />',
        'CC_ON': '<img id="wnVideoCCON" style="display:inline" width="52" height="25" title="Play With Closed Captions" alt="Play With Closed Captions" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAZCAIAAADxKJpUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAmhJREFUeNrsl7GSAUEQhq260AOQk/MAHoAYOTliJUaMnAcgl1s5DyC3OQ9wX+mqvq7Z2V2n6hBcB1uzu73d//zzd89ssN/vc59q+dwH25dzf7vdjsfj+Xx+MY5isVipVMrlciK43W63XC7B9y6qGo3GaDTyLOvpdJrNZm9Epux4mNtsNnHvWq1WvBvQL5dLFEXeoCxHqVTiih5w86qCUDiEYZgURGH0+30XnMNZvV7HCVj2IRCn06mNTkoWIu62WCwsRGDN53MG3W53vV57ibCfV6vVxGoF1mQycVJifLNarfQ5EiFlkhu47VRlUCgUCM5bR/uPthKCttttGUMSQhwOh0xXozN7TaOs44CbrSd965UB+CTOL1oJ1mq1FFmv15Nk9BcGkg9YXJmADHg+GAxkEaUNyQrK2xQDHIyik6TO5QGnSwAZVogiFFIiautGidno4BuPx1IcmQsnFBLZFmkiOCsUAeGUko0rg8Ph4LiFd3u8g4iK4vjStq9X9jy2h8/dW6/XazY4WmhcfJZ/VCy9Q3mNNwU+xM0qJN1kc8rWXHQ3SQ8UKx1upVpBg+QRvqDnOTVhsdIjBX2z2UyHhQ+wkgSa925w2ktJAwFUqCJTzrbbrR4o2A8EKFfGOs90ZGDqdDoppeNpJXQQcshi1e/mzFUaMswxDTYJy5YT5znCMgqCpqr8WaN18UopIYEXAblZd5sbVdl+lE6YWqDHdCecrBe0aaMnAWzFQ+CAm+6wTMCbWHaUpCDWFNIPOD6Dlbf3FCvuvD1KcPjJ3BD/+iRsjwuB8/eFXF7/A6Eqck5fwf+v4ZP2LcAAHl2NFeObn0UAAAAASUVORK5CYII=" />'
    }

    this.fV = flashVars;
    this.widgetClassType = "WNVideoCanvas";
    this.parentDivId = parentDivId;
    this.parentJSName = this.widgetClassType + this.fV.widgetId;
    this.groupId = this.fV.idKey;
    this.showCCButton = false;

    this.clipPlaySeqTracker = {
        hasPreroll: false,
        prerollIsOver: false,
        hasPostroll: false,
        hasIndependentPostroll: false,
        postrollIsOver: false,
        contentClipIsOver: false,
        whatIsPlaying: "content"
    }
    this.clipPlaySeqTracker.reset = function(_hasPreroll, _hasPostroll) {
        this.hasPreroll = _hasPreroll;
        this.hasPostroll = _hasPostroll;
        this.prerollIsOver = false;
        this.postrollIsOver = false;
        this.hasIndependentPostroll = false;
        this.contentClipIsOver = false;
        this.whatIsPlaying = "content";
    }
    this.clipPlaySeqTracker.prerollPlayable = function() {
        wnLog("> clip.mobile_hasAdPreRoll = " + this.hasPreroll);
        return (this.hasPreroll && !this.prerollIsOver && (typeof google != "undefined") && (typeof google.ima != "undefined"));
    }
    this.clipPlaySeqTracker.postrollPlayable = function() {
        wnLog("> clip.mobile_hasAdPostRoll = " + this.hasPostroll);
        return (this.hasPostroll && !this.postrollIsOver && (typeof google != "undefined") && (typeof google.ima != "undefined"));
    }

    // Respond to screen width
    if (wn_isMobile) {
        // Get height/width ratio
        var div = $wn("#" + this.parentDivId);
        this.fV.widgetsContainer = div.parent();
        var ratio = this.fV.playerHeight / this.fV.playerWidth;
        var leftOffset = parseInt(div.position().left);
        var oldWidth = parseInt(this.fV.playerWidth) + leftOffset;
        var newWidth = div.parent().parent().width() - leftOffset;
        //var newWidth = wnVideoUtils.getResponsiveWidth(this.fV.playerWidth);
        if (newWidth < oldWidth) {
            this.fV.playerWidth = newWidth;
            // Calculate scaled height
            this.fV.originalHeight = parseInt(this.fV.playerHeight);
            this.fV.originalWidth = oldWidth;
            this.fV.top = div.position().top;
            this.fV.left = div.position().left;
            this.fV.originalContainerHeight = div.parent().height();
            this.fV.playerHeight = Math.round(this.fV.playerWidth * ratio);
            // resize widgets group container
            var diff = this.fV.originalHeight - this.fV.playerHeight;
            div.parent().height(div.parent().height() - diff);
            containerWidth = div.parent().parent().width();
            oldWidth = div.parent().width();
            if (containerWidth < oldWidth) div.parent().width(containerWidth);
        }
    }

    evntMgr.Register({
        evnt: "NewGalleryClip",
        func: "onNewGalleryClip",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });

    // Render VideoCanvas
    this.render();

    //register for events
    $wn("#" + this.fV.domId).data("widget", this);
};

WNVideoCanvas.prototype.resizeOnScreenFlip = function() {
    var div = $wn("#" + this.parentDivId);
    var leftOffset = parseInt(div.position().left);
    var defaultWidth = parseInt(this.fV.playerWidth) + leftOffset;
    var fitIntoWidth = div.parent().parent().width() - leftOffset;
    var containerWidth = div.parent().width();

    // if gallery is on the right side of the canvas
    var gallery = getWigdetByType(this.groupId, "g");
    // if gallery and canvas are in the same container
    if (gallery.hasOwnProperty("widgetsContainer") && gallery.widgetsContainer.attr("id") == div.parent().attr("id")) {
        // if gallery is on the right side of the canvas
        var canvasBottom = div.position().top + parseInt(this.fV.playerHeight);
        if (gallery.top <= canvasBottom && gallery.left >= defaultWidth) {
            fitIntoWidth = (fitIntoWidth < gallery.left) ? fitIntoWidth : gallery.left;
            containerWidth = gallery.left;
        }
    }

    if (fitIntoWidth > containerWidth) fitIntoWidth = containerWidth;
    var newWidth = fitIntoWidth;
    $wn(this.canvas).width(newWidth);
    div.width(newWidth);
    $wn("#" + this.fV.domId).width(newWidth);
    div.find(".wn-embed-code").width(newWidth - 160);
    div.find(".share-link").width(newWidth - 160);
}

WNVideoCanvas.prototype.handleCC = function(enableCC, canvas) {
    if (enableCC) {
        this.videoSources = this.videoSourcesCC;
        this.liveCCon = true;
        $wn('.wn-cc-button').html(this.labels.CC_OFF);
    } else {
        this.videoSources = this.originalVideoSources;
        this.liveCCon = false;
        $wn('.wn-cc-button').html(this.labels.CC_ON);
    }

    this.v.find('source').remove();
    this.v.removeAttr('src');
    this.v.append(this.videoSources);
    this.v[0].load();
    this.v[0].play();
};

WNVideoCanvas.prototype.render = function() {

    var that = this; //http://www.crockford.com/javascript/private.html
    var autoplay = '';
    if (this.fV.isAutoStart == 'true') {
        autoplay = 'autoplay="true"'; // Do not autostart=false since its only looking for the autostart var not its value
    }

    var config;
    var os = wnUserAgentParser.getOS();
    if (os.name.toLowerCase() == 'android') {
        config = wn.video.mobile.app.android;
    } else if (os.name.toLowerCase() == 'ios') {
        config = wn.video.mobile.app.ios;
    } else {
        config = {
            "enablelive": false
        };
    }

    // Set up properties required for the object
    if (typeof this.fV.transportShareButtons !== 'undefined') {
        var transportButtonArr = this.fV.transportShareButtons.split(',');
        for (var i = 0, n = transportButtonArr.length; i < n; i++) {
            transportButtonArr[i] = wnVideoUtils.trim(transportButtonArr[i]);
            //if (transportButtonArr[i] == 'cc') {
            if (transportButtonArr[i] == 'cc' && !config.enablelive) {
                this.ccButtonEnabled = true;
            }
            if (transportButtonArr[i] == 'share') {
                this.shareButtonEnabled = true;
            }
        }
    }

    var HTML5VideoMarkup = '<div id="' + this.fV.domId + '" class="wnVideoCanvas">' +
        '    <div id="wn_html_video_' + this.groupId + '" class="wn-video-container-div">' +
        '        <video id="wnVideoHTML" class="wnVideoHtml" controls="controls" ' + autoplay + '></video>' +
        '    </div>' +
	'    <div class="wn-video-overlay-container"></div>';
    if (os.name.toLowerCase() == 'android') {
        	HTML5VideoMarkup += '    <div class="wn-video-play-button-overlay"><div class="wn-video-play-button"></div></div>';
    }
    HTML5VideoMarkup += '    <div class="wn-controls" style="display:inline">' +
        '        <div class="wn-cc-button" style="display:inline">' + this.labels.CC_ON + '</div> ' +
        '        <div class="wn-share-button" style="display:inline">' + this.labels.SHARE + '</div> ' +
        '    </div> ' +
        '    <div class="wn-share-pane">' +
        '       <div class="wn-close_button">' + this.labels.CLOSE_X + '</div>' +
        '       <div class="wn-embed-fields">' +
        '          <div class="wn-sh-label">' + this.labels.EMBED_VIDEO + '</div>' +
        '          <div class="wn-embed-code sh-input"></div>' +
        '       </div>' +
        '       <div class="wn-link-fields">' +
        '          <div class="wn-sh-label">' + this.labels.LINK_VIDEO + '</div>' +
        '          <input type="text" class="share-link sh-input"/>' +
        '       </div>' +
        '    </div>' +
        '</div>';

    $wn("#" + this.fV.domId + "_swfObject").html(HTML5VideoMarkup);

    /* Set styles and dimentions */
    style = "<style type='text/css'>" +
        "#" + this.fV.domId + " {width: " + this.fV.playerWidth + "px; height:" + this.fV.playerHeight + "px;}" +
        "#" + this.fV.domId + " .wnVideoHtml {width: " + this.fV.playerWidth + "px; height:" + (this.fV.playerHeight - 20) + "px;}" +
        //"#" + this.fV.domId + " .wn-video-overlay-container {width: " + this.fV.playerWidth + "px; height:" + (this.fV.playerHeight - 32 - 20) + "px;}" +
        "#" + this.fV.domId + " .wn-video-overlay-container {width: " + this.fV.playerWidth + "px; height:" + (this.fV.playerHeight - 35 - 20) + "px; top: 20px;}";
    if (os.name.toLowerCase() == 'android') {
        	//"#" + this.fV.domId + " .wn-video-play-button-overlay {left: " + ((1*this.fV.playerWidth)/2-50) + "px; top:" + ((1*this.fV.playerHeight)/2-50) + "px; position: absolute; width: 100px; height: 60px; padding-top: 40px; background-color: #000000; text-align: center; cursor: pointer;}" +
    	style += "#" + this.fV.domId + " .wn-video-play-button-overlay {width: " + this.fV.playerWidth + "px; height:" + (this.fV.playerHeight-20) + "px;}";
    }
    style += "#" + this.fV.domId + " .wn-share-pane {height: " + this.fV.playerHeight + "px}" +
        "#" + this.fV.domId + " .wn-share-pane .sh-input {width: " + (this.fV.playerWidth - 160) + "px}";
    embedFieldHeight = (this.fV.playerHeight > 170) ? (this.fV.playerHeight - 150) : (this.fV.playerHeight - 130);
    style += "#" + this.fV.domId + " .wn-share-pane .wn-embed-code {height: " + embedFieldHeight + "px}" +
        "</style>";
    $wn("#" + this.parentDivId).append(style);

    this.canvas = $wn('#wnVideoHTML', '#wn_html_video_' + this.groupId)[0]; // DOM video element


    if (os.name.toLowerCase() == 'android') {
	    var play_button = $wn('#' + this.fV.domId).find('.wn-video-play-button-overlay');
		play_button.click(function() {
			$wn(this).hide();
			that.canvas.play();
			that.canvas.pause();

			var app_launched = that.checkMobileApp(that.currentClip.videoType);
			if (app_launched) {
			    that.canvas.pause();
			    return false;
			}

			that.clipPlaySeqTracker.onStartFired = true;
			that.close_button.trigger("click");
			if (that.clipPlaySeqTracker.prerollPlayable()) {
			    wnLog("Starting preroll logic");
			    wnLog("Content paused");
			    that.canvas.pause();
			    that.adLogic("preroll");
			} else {
			    wnLog("> content started: showCCButton=" + that.showCCButton);
			    if (that.showCCButton == true) {
			        $wn('.wn-cc-button').show();
			    }
			    evntMgr.Notify({
			        evnt: "NewMedia",
			        idKey: that.groupId,
			        objClip: that.currentClip
			    });
			    that.fireReportingBeacon({
			        eventype: "duration",
			        position: "0"
			    });
			}
		});
    }


    var cc_button = $wn('#' + this.fV.domId).find('.wn-cc-button');
    var share_button = $wn('#' + this.fV.domId).find('.wn-share-button');
    this.close_button = $wn('#' + this.fV.domId).find('.wn-close_button');
    share_button.add(this.close_button).add(cc_button).mouseover(
        function(e) {
            $wn(this).css('cursor', 'pointer');
        }
    ).mouseout(
        function(e) {
            $wn(this).css('cursor', 'default');
        }
    );
    cc_button.click(
        function() {
            if (that.liveCCon) {
                that.handleCC(false, that.canvas);
            } else {
                that.handleCC(true, that.canvas);
            }
        }
    );
    this.close_button.click(
        function() {
            if (/iphone/i.test(navigator.userAgent.toLowerCase())) {
                $wn(that.canvas).show();
            }
            if (that.clipPlaySeqTracker.whatIsPlaying == "content") {
                $wn(that.canvas).prop('controls', 'controls');
            }
            $wn(this).parent().fadeOut(300);
            $wn(this).parent().parent().find('.wn-controls').fadeIn(300);
            //wnSlideshowImage.showHideAddThis({ groupId: thisWNImgCnvs.groupId, isVisible: false });
            wnVideoWidgets.closeAddThisPane(that.parentDivId);
        }
    );
    this.close_button.trigger("click");
    share_button.click(
        function() {
            $wn(that.canvas).removeAttr('controls');
            // default html5 controls can't be removed on iPhone. hiding the canvas instead.
            if (/iphone/i.test(navigator.userAgent.toLowerCase())) {
                $wn(that.canvas).hide();
            }
            $wn(this).parent().fadeOut(300);
            $wn(this).parent().parent().find('.wn-share-pane').fadeIn(300);
            //wnSlideshowImage.showHideAddThis({ groupId: thisWNImgCnvs.groupId, isVisible: true });
            //wnVideoWidgets.openAddThisPane(url, title, divId, summary);
            //var landingPage = decodeURIComponent(that.fV.landingPage);
            var clipId = (that.currentClip) ? that.currentClip.id * 1 : 0;
            if (clipId < 1) clipId = "";
            var _linkToVideo = "";
            if (that.currentClip != null) {
                _linkToVideo = wnVideoWidgets.getLinkToVideo(that.currentClip, that.fV);
                /*if (!wnVideoUtils.validateString(landingPage)) {
                    _linkToVideo = "http://" + that.fV.hostDomain + "/global/video/popup/pop_playerLaunch.asp?vt1=v" +
                                    "&clipFormat=" + that.fV.siteDefaultFormat +
                                    "&clipId1=" + that.currentClip.id +
                                    "&at1=" + that.currentClip.adTag +
                                    "&h1=" + that.currentClip.headline +
                                    "&flvUri=&partnerclipid=";
                }
                else {
                    if (landingPage.indexOf("?") > 0) {
                        _linkToVideo = landingPage + "&autoStart=true&topVideoCatNo=default";
                    }
                    else {
                        _linkToVideo = landingPage + "?autoStart=true&topVideoCatNo=default";
                    }
                    if (wnVideoUtils.validateString(clipId)) {
                        _linkToVideo += "&clipId=" + clipId;
                    } else {
                        if (wnVideoUtils.validateString(that.fV.liveStreamUrl)) _linkToVideo += "&flvUri=" + that.fV.liveStreamUrl;
                        if (wnVideoUtils.validateString(that.fV.wnms1)) _linkToVideo += "&wnms1=" + that.fV.wnms1;
                        if (wnVideoUtils.validateString(that.fV.wnms2)) _linkToVideo += "&wnms2=" + that.fV.wnms2;
                        if (wnVideoUtils.validateString(that.fV.wnms3)) _linkToVideo += "&wnms3=" + that.fV.wnms3;
                        if (wnVideoUtils.validateString(that.currentClip.partnerclipid)) _linkToVideo += "&partnerclipid=" + that.currentClip.partnerclipid;
                    }
                    if (that.fV.videoType == 'livestream') {
                        _linkToVideo += "&streamType=" + that.fV.StreamType +
                                         "&adTag=" + that.currentClip.adTag +
                                         "&enableAds=" + that.currentClip.hasAdPreRoll +
                                         "&headline=" + wnVideoWidgets.WNEncodeURIComponent(that.currentClip.headline, 2);
                    }
                }*/
            }
            //var share_link = $wn('#' + that.fV.domId).find('.share-link');
            $wn('.share-link', '#' + that.fV.domId).val(_linkToVideo);

            // clipId
            var id = "";
            clipId = (that.currentClip) ? that.currentClip.id * 1 : 0;
            if (clipId < 1) clipId = wnVideoWidgets.GetEmbedCodeClipId() * 1;
            if (isNaN(clipId) || clipId < 1) {
                id = "";
            } else {
                id = clipId.toString();
            }
            // flvUri
            var flvUri = that.fV.liveStreamUrl;
            if (!wnVideoUtils.validateString(flvUri)) flvUri = wnVideoWidgets.GetEmbedCodeFlvUri();
            if (!wnVideoUtils.validateString(flvUri)) flvUri = "";
            // thirdpartymrssurl
            var thirdpartymrssurl = that.currentClip.thirdpartymrssurl;
            if (!wnVideoUtils.validateString(thirdpartymrssurl)) thirdpartymrssurl = wnVideoWidgets.GetEmbedCodeThirdPartyMrssUrl();
            if (!wnVideoUtils.validateString(thirdpartymrssurl)) thirdpartymrssurl = "";
            // partnerclipid
            var partnerclipid = that.currentClip.partnerclipid;
            if (!wnVideoUtils.validateString(partnerclipid)) partnerclipid = wnVideoWidgets.GetEmbedCodePartnerClipId();
            if (!wnVideoUtils.validateString(partnerclipid)) partnerclipid = "";
            // adTag
            var adTag = that.currentClip.adTag;
            if (!wnVideoUtils.validateString(adTag)) adTag = wnVideoWidgets.GetEmbedAdTag();
            if (!wnVideoUtils.validateString(adTag)) adTag = "";
            adTag = wnVideoWidgets.WNDecodeURIComponent(adTag, 2);
            // landingPage
            var landingPage = that.currentClip.landingPage;
            if (!wnVideoUtils.validateString(landingPage)) landingPage = wnVideoWidgets.GetEmbedCodeLandingPage();
            if (!wnVideoUtils.validateString(landingPage)) landingPage = "";
            // islandingPageoverride
            var islandingPageoverride = that.currentClip.islandingPageoverride;
            if (!wnVideoUtils.validateString(islandingPageoverride)) islandingPageoverride = wnVideoWidgets.GetEmbedCodeIsLandingPageOverride();
            if (!wnVideoUtils.validateString(islandingPageoverride)) islandingPageoverride = "";
            //var _islandingPageoverride:Boolean = (islandingPageoverride == "undefined") ? false : new Boolean(islandingPageoverride);

            if (id != "") {
                flvUri = "";
                thirdpartymrssurl = "";
            }
            if (thirdpartymrssurl != "") {
                id = "";
                flvUri = "";
            }
            if (flvUri != "") {
                id = "";
                thirdpartymrssurl = "";
            }

            // specific for fisher requirement
            if (thirdpartymrssurl == "" && (that.fV.partnerclipid == undefined || that.fV.partnerclipid == "")) {
                partnerclipid = "";
            }

            var embedJSPlayerUrl = that.fV.staticImgPath + "/interface/js/WNVideo.js" +
                "?rnd=" + Math.floor(Math.random() * 1000000000) +
                ";hostDomain=" + that.fV.hostDomain +
                ";playerWidth=" + that.fV.playerWidth +
                ";playerHeight=" + that.fV.playerHeight +
                ";isShowIcon=true" +
                ";clipId=" + id +
                ";flvUri=" + flvUri;
            if (wnVideoUtils.validateString(that.fV.wnms1)) embedJSPlayerUrl += ";wnms1=" + that.fV.wnms1;
            if (wnVideoUtils.validateString(that.fV.wnms2)) embedJSPlayerUrl += ";wnms2=" + that.fV.wnms2;
            if (wnVideoUtils.validateString(that.fV.wnms3)) embedJSPlayerUrl += ";wnms3=" + that.fV.wnms3;
            embedJSPlayerUrl += ";partnerclipid=" + partnerclipid +
                ";adTag=" + wnVideoWidgets.WNEncodeURIComponent(adTag) +
                ";advertisingZone=" + wnVideoWidgets.WNEncodeURIComponent(that.fV.advertisingZone, 2) +
                ";enableAds=" + that.currentClip.hasAdPreRoll +
                ";landingPage=" + wnVideoWidgets.WNEncodeURIComponent(landingPage, 2) +
                //";islandingPageoverride=" + _islandingPageoverride;
                ";islandingPageoverride=" + islandingPageoverride +
                ";playerType=" + that.fV.playerType + "_EMBEDDEDscript" +
                ";controlsType=" + that.fV.controlsType;
            if (wnVideoUtils.validateString(that.fV.gallerySourceType)) embedJSPlayerUrl += ";galleryType=" + that.fV.gallerySourceType;
            if (that.fV.galleryId > 0) embedJSPlayerUrl += ";galleryId=" + that.fV.galleryId;

            if (that.fV.videoType == 'livestream') {
                embedJSPlayerUrl += ";isLiveStream=true";
                embedJSPlayerUrl += ";streamType=" + that.fV.StreamType;
            }
            if (id == "" && that.currentClip != null)
                embedJSPlayerUrl += ";headline=" + wnVideoWidgets.WNEncodeURIComponent(that.currentClip.mainHeadline, 2);
            var embedJSPlayer = "&lt;script type='text/javascript' src='" + embedJSPlayerUrl + "'&gt;&lt;/script&gt;";
            embedJSPlayer += '&lt;a href="http://' + that.fV.hostDomain + '" title="' + that.fV.pageTitleTag + '"&gt;' + that.fV.pageTitleTag + '&lt;/a&gt;';

            $wn('.wn-embed-code', '#' + that.fV.domId).html(embedJSPlayer);

            wnVideoWidgets.openAddThisPane(_linkToVideo, that.currentClip['headline'], that.parentDivId, that.currentClip['summary'], true);
        }
    );

    // Initialize canvas PlayBack control ...
    this.canvasPlayBackCtrl = new WNVideoCanvasPlayBackControl(that.fV.domId, that.fV, {
        overlay_elem_class: "wn-video-overlay-container"
    });

    // ... and hide it
    this.canvasPlayBackCtrl.hide();

    // Bind events and handlers for the rendered <video> tag
    $wn(this.canvas).bind('loadedmetadata', function() {
        that.clipPlaySeqTracker.onStartFired = false;
        // on Android ads don't start playing after metadataload. Following code fixes it.
        /*if (that.clipPlaySeqTracker.whatIsPlaying == "preroll" || that.clipPlaySeqTracker.whatIsPlaying == "postroll") {
            that.canvas.play();
        }*/
    });
    $wn(this.canvas).bind('playing', function() {
		if (os.name.toLowerCase() == 'android') {
			$wn('#' + that.fV.domId).find('.wn-video-play-button-overlay').hide();
		}
        /*
            Calling checkMobileApp from here is quite dangerous. Because its execution involves
            usage of UAFHelper and rely on few siteconfigs. If it fails (because of UAF or for some other reason),
            video will be played in native html5 player without CC.
            But it's the only place to intercept video click event reliably.
            TODO: should be discussed
        */
        //that.ended = false;
        var app_launched = that.checkMobileApp(that.currentClip.videoType);
        if (app_launched) {
            that.canvas.pause();
            return false;
        }

        if (!that.clipPlaySeqTracker.onStartFired) {
            that.clipPlaySeqTracker.onStartFired = true;
            that.close_button.trigger("click");
            switch (that.clipPlaySeqTracker.whatIsPlaying) {
                case "preroll":
                    // Fired after the "google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED",
                    // when preroll actually already started and content clip is paused by the Google SDK

                    wnLog("> preroll started");
                    $wn('.wn-cc-button').hide();
                    break;
                case "content":
                    if (that.isFirstPlay && that.clipPlaySeqTracker.prerollPlayable()) {
                        wnLog("Starting preroll logic");
                        wnLog("Content paused");
                        that.canvas.pause();
                        that.adLogic("preroll");
                    } else {
                        wnLog("> content started: showCCButton=" + that.showCCButton);
                        if (that.showCCButton == true) {
                            $wn('.wn-cc-button').show();
                        }
                        evntMgr.Notify({
                            evnt: "NewMedia",
                            idKey: that.groupId,
                            objClip: that.currentClip
                        });
                        that.fireReportingBeacon({
                            eventype: "duration",
                            position: "0"
                        });
                    }
                    break;
                case "postroll":
                    wnLog("> postroll started");
                    $wn('.wn-cc-button').hide();
                    that.clipPlaySeqTracker.postrollIsOver = true;
                    break;
            }
        }
    });

    $wn(this.canvas).bind('ended', function() {
        /* This event is fired before the google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED */
        // Switch to the native controls
        //if (that.canvasPlayBackCtrl) that.canvasPlayBackCtrl.hide();
        //that.ended = true;
        switch (that.clipPlaySeqTracker.whatIsPlaying) {
            case "preroll":
                wnLog("> preroll ended");
                that.clipPlaySeqTracker.prerollIsOver = true;
                that.clipPlaySeqTracker.whatIsPlaying = "content";
                // Restore and play content clip
                /*if (that.adManager != undefined) that.adManager.unload();
                that.v.find('source').remove();
                that.v.removeAttr('src');
                that.v.append(that.videoSources);
                that.v[0].load();
                that.v[0].play();
                */
                break;
            case "content":
                wnLog("> content ended");
                //that.currentClip.hasBeaconFired = false;
                if (that.fV["videoType"] != "livestream") that.canvas.currentTime = 0;
                that.canvas.pause();
                if (that.clipPlaySeqTracker.postrollPlayable()) {
                    if (that.clipPlaySeqTracker.hasIndependentPostroll) {
                        if (that.adManager != undefined) that.adManager.destroy();
                        if (that.adDisplayContainer != undefined) that.adDisplayContainer.destroy();
                        that.adLogic("postroll");
                    } else {
                        that.adsLoader.contentComplete(); // Signals to the SDK that the content is finished. This will allow the SDK to play post-roll ads, if any are loaded via ad rules.
                    }


                } else {
                    that.completeSequence();
                }
                break;
            case "postroll":
                wnLog("> postroll ended");
                // Restore content clip
                /*that.v.find('source').remove();
                that.v.removeAttr('src');
                that.v.append(that.videoSources);
                that.v[0].load();*/

                // Finish playback
                // that.completeSequence(); - will be finished in onAllAdsCompleted handler
                break;
        }
    });

    //Start, Pause, Full Screen, E-mail, Embed, Share
    var mediaEvents = 'emptied loadstart progress suspend abort error emptied stalled loadedmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange play pause ratechange volumechange';
    $wn(this.canvas).bind(mediaEvents, function() {
        // Append properties to clip
        that.currentClip['hostPlayer'] = 'html';
        that.currentClip['currentTime'] = this.currentTime;
        that.currentClip['duration'] = this.duration;
        var valid = true;
        if (event != null && (event.type == "play" || event.type == "pause") &&
            that.clipPlaySeqTracker.whatIsPlaying == "content" && that.clipPlaySeqTracker.prerollPlayable()) {
            valid = false;
        }
        if (event != null && event.type == "ended") event.type = "clipended";
        /*if (event != null && event.type == "play") {
            this.checkMobileApp(that.currentClip.videoType);
        }*/
        // Pipe all widget events to a global handler
        if (event != null) {
            wnVideoWidgets.handleEvent({
                'event': event,
                'system': 'video',
                'clip': that.currentClip,
                'valid': valid
            });
        }
    });


    $wn(this.canvas).bind('error', function() {
        wnLog("playback error");
        // Check what is currently playing (preroll, content clip or postroll)
        // and act accordingly - done in the "ended" handler
        $wn(that.canvas).trigger('ended');
    });

    $wn(this.canvas).bind('abort', function() {
        // Unload previous ads
        /* Ads are unloaded in the onNewGalleryClip() */
        wnLog('playback aborted');
    });

    // IF this is a livestream
    if (this.fV['videoType'] == 'livestream') {
        var streams = new Array;

        if (this.fV['liveStreams']) {
            streams = streams.concat(this.fV['liveStreams']);
        }

        if (this.fV['liveStreamUrl']) {
            streams.push({
                'url': this.fV['liveStreamUrl'],
                'type': 'video/mp4'
            });
        }

        var liveStreamJSON = {
            'videoType': 'livestream',
            'isLiveStream': true,
            'id': '0',
            'uri': streams,
            'headline': this.fV['liveStreamHeadline'],
            'mainHeadline': this.fV['liveStreamHeadline'],
            'graphic': this.fV['liveStreamSummaryImgUrl'],
            'adTag': this.fV['liveStreamAdTag'],
            'ownerAffiliateNumber': this.fV['affiliateNumber'],
            'ownerAffiliateName': this.fV['affiliate'],
            'ownerAffiliateBaseUrl': '',
            'isMilliseconds': 'true',
            'vt': 'V',
            'hasCC': '0',
            'summary': '',
            'isAd': 'False',
            'serveAdsFromContentOwner': 'False',
            'displaysize': '20',
            'hasAdPreRoll': this.fV['liveStreamHasPreroll'] == 'true' ? 'true' : 'false',
            'preRollSplitPct': '100',
            'preRollOwner': 'loc',
            'hasAdPostRoll': this.fV['liveStreamHasPostroll'] == 'true' ? 'true' : 'false',
            'postRollSplitPct': '100',
            'postRollOwner': 'loc',
            // mobile ads config
            'mobile_hasAdPreRoll': this.fV['liveStreamHasPreroll'] == 'true' ? 'true' : 'false',
            'mobile_preRollSplitPct': '100',
            'mobile_preRollOwner': 'loc',
            'mobile_hasAdPostRoll': this.fV['liveStreamHasPostroll'] == 'true' ? 'true' : 'false',
            'mobile_postRollSplitPct': '100',
            'mobile_postRollOwner': 'loc'
        };

        this.checkAutostartAndPlay(liveStreamJSON);
    } else // Regular clipid play
    {
        //wnVideoWidgets.playHtmlVideo({ 'clipId': this.fV.clipId }, null, this.groupId);
        if (this.fV.clipId > 0) this.playClipById(this.fV.clipId);
    }
};

WNVideoCanvas.prototype.playClipById = function(clipId) {
    var timestamp = new Date().getTime();
    var feedUrl = wnFeedsApiDomain + '/feed/v3.0/clips/' + clipId + '?alt=json&callbackparams=' + this.fV.domId + '&callback=wnVideoWidgets.clipObjCallback';
    if (clipId) {
        wnVideoUtils.loadScript('wnClipScript' + timestamp, feedUrl);
    } else {
        wnLog('No ClipId supplied');
    }
};

WNVideoCanvas.prototype.checkAutostartAndPlay = function(jsonClipObj) {
    if (this.fV.isAutoStart.toLowerCase() == 'true') {
        jsonClipObj.autoplay = true;
        this.fV.isAutoStart = 'false';
    }
    this.playClipFromJson(jsonClipObj);
};

WNVideoCanvas.prototype.playClipFromJson = function(jsonClipObj) {
    if (jsonClipObj.videoType == 'livestream' && this.ccButtonEnabled) {
        this.showCCButton = true;
        $('.wn-cc-button').show();
        //$('.wn-cc-button').css('display', 'inline !important');
    } else {
        this.showCCButton = false;
        $('.wn-cc-button').hide();
        // $('.wn-cc-button').css('display', 'none');
        // $('#wnVideoCCOFF').css('display', 'none');
        //wnLog('display set to none');
    }

    wnLog("playClipFromJson();");
    // Copy the jsonClipObj object into clipObj
    var clipObj = {};
    for (i in jsonClipObj) clipObj[i] = jsonClipObj[i];

    clipObj.headline = unescape(jsonClipObj.headline);
    clipObj.HasThumb = (jsonClipObj.thumb && jsonClipObj.thumb.length > 0) ? true : false;
    //clipObj.hasBeaconFired = false;
    clipObj.isFlash = false;
    clipObj.isAd = false;
    clipObj.isAdIntro = false;
    clipObj.isAdExit = false;
    clipObj.isAdFrame = false;
    clipObj.isASX = false;
    // According to the business logic ownerAffiliateName belongs to the playing canvas,
    // while ownerAffiliateNumber belongs to the clip (potentially shared from the different affiliate)
    //clipObj.ownerAffiliateName = wnSiteConfigGeneral.affiliateName;
    clipObj.affiliateName = wnSiteConfigGeneral.affiliateName;

    // For mobile sites use mobile pre/post roll info
    if (wn_isMobile && wn_isPlatformSiteMobile) {
        clipObj.hasAdPreRoll = (wnVideoUtils.validateString(jsonClipObj.mobile_hasAdPreRoll) && jsonClipObj.mobile_hasAdPreRoll.toLowerCase() == 'true') ? true : false;
        clipObj.hasAdPostRoll = (wnVideoUtils.validateString(jsonClipObj.mobile_hasAdPostRoll) && jsonClipObj.mobile_hasAdPostRoll.toLowerCase() == 'true') ? true : false;
    } else {
        clipObj.hasAdPreRoll = (wnVideoUtils.validateString(jsonClipObj.hasAdPreRoll) && jsonClipObj.hasAdPreRoll.toLowerCase() == 'true') ? true : false;
        clipObj.hasAdPostRoll = (wnVideoUtils.validateString(jsonClipObj.hasAdPostRoll) && jsonClipObj.hasAdPostRoll.toLowerCase() == 'true') ? true : false;
    }

    clipObj.preRollSplitPct = parseInt(jsonClipObj.preRollSplitPct);
    clipObj.preRollOwner = jsonClipObj.preRollOwner;
    clipObj.postRollSplitPct = parseInt(jsonClipObj.postRollSplitPct);
    clipObj.postRollOwner = jsonClipObj.postRollOwner;

    clipObj.mobile_preRollSplitPct = parseInt(jsonClipObj.mobile_preRollSplitPct);
    clipObj.mobile_preRollOwner = jsonClipObj.mobile_preRollOwner;
    clipObj.mobile_postRollSplitPct = parseInt(jsonClipObj.mobile_postRollSplitPct);
    clipObj.mobile_postRollOwner = jsonClipObj.mobile_postRollOwner;

    this.clipPlaySeqTracker.reset(clipObj.hasAdPreRoll, clipObj.hasAdPostRoll);
    this.currentClip = clipObj;
    evntMgr.Notify({
        evnt: 'NewClip',
        idKey: this.groupId,
        objClip: clipObj
    });

    if (this.canvasPlayBackCtrl) this.canvasPlayBackCtrl.hide();

    // Adding video info to the canvas
    this.v = $wn(this.canvas);

    //if (this.currentClip.autostart) v.attr('autoplay', 'true');

    if (this.currentClip.graphic) {
        this.v.attr('poster', this.currentClip.graphic);
    } else {
        this.v.attr('poster', '');
    }

    // Remove previously added video sources, if exist
    this.v.find('source').remove();
    this.v.removeAttr('src');

    // Create video sources
    this.videoSources = '';
    this.videoSourcesCC = '';
    var pattern = /(^.*\/)([^_@]*)(.*\/)/g; // expecting - http://wnow_live_test-f.akamaihd.net/i/DAVE_01@92456/master.m3u8
    for (j in jsonClipObj.uri) {
        if (jsonClipObj.uri[j].type && jsonClipObj.uri[j].url &&
            (jsonClipObj.uri[j].type == "video/mp4" || jsonClipObj.uri[j].type == "video/3gpp")) // Filter out video formats not supported by HTML5 player
        {
            this.videoSources += '<source src="' + decodeURIComponent(jsonClipObj.uri[j].url) + '" media="all"></source>';
            this.videoSourcesCC += '<source  src="' + decodeURIComponent(jsonClipObj.uri[j].url).replace(pattern, "$1" + "$2" + "CC" + "$3") + '" media="all"></source>';

            var source = document.createElement("source");
            source.setAttribute("src", decodeURIComponent(jsonClipObj.uri[j].url));
            source.setAttribute("media", "all");
            this.v[0].appendChild(source);
        }
    }

    this.originalVideoSources = this.videoSources;
    this.videoSourcesCC = this.videoSourcesCC;

    if (this.currentClip.autostart) {
        //this.checkMobileApp(jsonClipObj.videoType);
        this.v[0].load();
        if (this.clipPlaySeqTracker.prerollPlayable()) {
            this.adLogic("preroll");
        } else {
            this.v[0].play();
        }
    } else {
        this.isFirstPlay = true;
    }

    try {
        this.v.prop('controls', 'controls');
    } catch (e) {
        this.v.attr('controls', 'controls');
    }
};

WNVideoCanvas.prototype.checkMobileApp = function(streamtype) {
    // If on a supported mobile app platform then request user to watch video in app
    var config;
    var os = wnUserAgentParser.getOS();
    //os.name = "android";

    // Set the right os and its site configs to avoid duplication
    var enableapp = false;
    if (os.name.toLowerCase() == 'android') {
        config = wn.video.mobile.app.android;
        config['os'] = 'android';
    } else if (os.name.toLowerCase() == 'ios') {
        config = wn.video.mobile.app.ios;
        config['os'] = 'ios';
    } else {
        wnLog('not a supported mobile os')
        return false;
    }

    // live or vod
    if (streamtype == 'livestream') {
        config['video_type'] = 'live';
        if (config.enablelive) {
            enableapp = true;
        }
    } else {
        config['video_type'] = 'vod';
        if (config.enablevod) {
            enableapp = true;
        }
    }

    if (enableapp && wnvideo_getCookie('WN_MOBILE_APP_REJECTED') == null) {
        // simple window.location = new_link; won't let you use window.history.back() on mobile;
        var that = this;
        if (config['os'] == 'ios') {
            window.location = that.getAppLaunchLink(config);

            setTimeout(function() {
                // If the user is still here, open the App Store
                if (!document.webkitHidden) {
                    var downloadPageLink = that.getAppLaunchLink(config);

                    downloadPageLink = downloadPageLink.replace('wnvideo://', 'http://');

                    window.location = downloadPageLink;
                }
            }, 25);
        } else {
            setTimeout(function() {
                window.location = that.getAppLaunchLink(config);
            }, 0);
        }
        return true;
    }
    return false;
};

WNVideoCanvas.prototype.getAppLaunchLink = function(config) {
    var clip = this.currentClip;
    var shareUrl = wnVideoWidgets.getLinkToVideo(this.currentClip, this.fV);
    var streamUrl = clip.uri[0].url;

    // Get HLS VOD link
    for (j in clip.uri) {
        if (clip.uri[j].type && clip.uri[j].url && clip.uri[j].type == 'application/x-mpegURL') {
            streamUrl = clip.uri[j].url;
        }
    }

    // encode live steam url only. vod urls are already encoded.
    if (config.video_type == 'live') {
        streamUrl = encodeURIComponent(streamUrl);
    }

    // for ios strip web vtt qs
    if (config.os == 'ios') {
        var streamUrlArr = streamUrl.split('?');
        streamUrl = streamUrlArr[0];
    }

    // ad call set up
    var adProps = {
        'apptype': 'video',
        'wncc': clip.adTag,
        'wnccx': this.fV.advertisingZone,
        'width': '10',
        'height': '10',
        'wnsz': '30'
    };

    // Taking care of UAF instability
    var ad = {
        url: ""
    };
    try {
        ad = wnVideoWidgets.UAFHelper(adProps, this.currentClip, 'raw');
    } catch (e) {
        wnLog("Error in UAFHelper. Can't get ad url.");
    }
    var addThisPubId = wnAddThisPubId;

    // comscore info
    var qsCs = '&cs_c2=' + 6036361 +
        '&cs_ns_st_ci=' + clip.id +
        '&cs_ns_st_ge=' + encodeURIComponent(clip.adTag) +
        '&cs_ns_st_ci=' + clip.id +
        '&cs_ns_jspageurl=' + encodeURIComponent(document.location) +
        '&cs_ns_st_st=' + clip.affiliateName +
        '&ga_trackingId=' + encodeURIComponent(wn_gaAccountId);

    var qsParams = 'stream=' + encodeURIComponent(streamUrl) +
        '&video_type=' + config.video_type +
        '&clip_title=' + encodeURIComponent(clip.headline) +
        '&clip_date=' + encodeURIComponent(clip.lastediteddate) +
        '&addThis_pubid=' + encodeURIComponent(addThisPubId) +
        qsCs +
        '&share_url=' + encodeURIComponent(shareUrl) +
        '&ad_enable_preroll=' + clip.mobile_hasAdPreRoll +
        '&ad_url=' + encodeURIComponent(ad.url);


    // don't hardcode values in order to maintain configurability via site configs
    var launchLink = config.launchlinkprefix + qsParams;

    // alert(launchLink);
    // wnLog(launchLink)
    return launchLink;
};

WNVideoCanvas.prototype.onNewGalleryClip = function(p_evtObj) {
    var clipObj = p_evtObj.objClip;
    wnLog('onNewGalleryClip()');
    //this.ended = false;
    if (this.adManager != undefined) {
        wnLog('cleanup...');
        this.adManager.removeEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, this.onContentResumeRequested);
        this.adManager.removeEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, this.onContentPauseRequested);
        this.adManager.removeEventListener(google.ima.AdEvent.Type.SKIPPED, this.onAdSkipped);
        this.adManager.removeEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, this.onAdError);
        this.adManager.destroy();
    }
    if (clipObj.isUserInitiated) { // user manually clicked on the clip thumbnail in the gallery
        // On iPhone ondemand clips won't play after a livestream - whole video canvas must be rerendered
        if (/iphone/i.test(navigator.userAgent.toLowerCase()) && this.currentClip.videoType == 'livestream') {
            this.render();
            $wn("#" + this.fV.domId).data("widget", this);
        }
        clipObj.autostart = true;
        this.isFirstPlay = false;
        this.playClipFromJson(clipObj);
    } else { // on gallery load
        // if canvas doesn't have a clipId or a live stream assigned
        if ((!wnVideoUtils.validateString(this.fV.clipId) || parseInt(this.fV.clipId) < 1) &&
            this.fV['videoType'] != 'livestream') {
            this.checkAutostartAndPlay(clipObj);
        }
    }
};

WNVideoCanvas.prototype.completeSequence = function() {
    this.clipPlaySeqTracker.reset(this.currentClip.hasAdPreRoll, this.currentClip.hasAdPostRoll);
    /*this.canvas.currentTime = 0;
    this.canvas.pause();*/
    evntMgr.Notify({
        evnt: "ClipEnded",
        idKey: this.groupId,
        objClip: this.currentClip
    });
};

WNVideoCanvas.prototype.adLogic = function(adPosition) {
    wnLog("> adLogic('" + adPosition + "') called");
    this.clipPlaySeqTracker.whatIsPlaying = adPosition;
    var adProps = new Object();
    adProps.wncc = this.currentClip.adTag;
    adProps.wnccx = this.fV.advertisingZone;
    if (this.fV.playerAdvertisingType != "none") {
        adProps.apptype = this.fV.playerAdvertisingType;
    } else {
        // Override the player ad name for previously specified players
        switch (this.fV.playerType.toLowerCase()) {
            case 'popup':
                adProps.apptype = 'videopopup';
                break;
            case 'standard':
                adProps.apptype = 'videostandard';
                break;
            case 'embedded':
                adProps.apptype = 'videoembedded';
                break;
            case 'landingpage':
                adProps.apptype = 'videolandingpage';
                break;
            default:
                adProps.apptype = 'video';
        }
    }

    this.currentClip.apptype = adProps.apptype;
    this.currentClip.idKey = this.idKey;

    switch (adPosition) {
        case "preroll":
            adProps.width = adProps.height = "10";
            adProps.wnsz = "30";
            if (this.fV.usePrerollMaster.toLowerCase() == "true") {
                adProps.sequence = 1; // Forces the admanager.js to reset its counters (tile=1, new ord number etc)
            } else {
                // If no master ad widget exists on the page then reset admanager counters
                var masterAdExists = false;
                masterAdExists = wnVideoWidgets.masterAdExists(this.currentClip);
                if (masterAdExists != true) {
                    adProps.sequence = 1;
                }
            }
            break;
        case "postroll":
            adProps.width = adProps.height = "11";
            adProps.wnsz = "31";
            break
        case "overlay":
            // overlay
            this.clipPlaySeqTracker.whatIsPlaying = "content";
            break;
        default:
            wnLog('Unexpected adPosition = ' + adPosition);
            this.playContentClip();
    }

    // adding property needed for dfpvideo and RealVu
    adProps['videoplayerdivid'] = this.parentDivId;
    adProps['playerwidth'] = this.canvas.clientWidth;
    adProps['playerheight'] = this.canvas.clientHeight;

    var ad = wnVideoWidgets.UAFHelper(adProps, this.currentClip, 'raw');
    if (ad != null) {
        var adUrl = ad.url;
        adProps.owner = ad.owner; // Override the companion owners with the pre-roll owner

        // Debug
        if (wn_debug_widgets == "true" && wnVideoUtils.validateString(this.fV.debugAdUrl)) {
            adUrl = this.fV.debugAdUrl;
        }

        var contentPlayback = this.canvas;
        $wn('.wn-video-overlay-container', '#' + this.fV.domId + '_swfObject').html("");
        this.adDisplayContainer = new google.ima.AdDisplayContainer(
            $wn('.wn-video-overlay-container', '#' + this.fV.domId + '_swfObject')[0],
            contentPlayback
        );
        
        this.adDisplayContainer.initialize();

        // Create an ads loader instance.
        //        this.adsLoader = new google.ima.AdsLoader();
        this.adsLoader = new google.ima.AdsLoader(this.adDisplayContainer);
        // Listen and respond to ads loaded and error events.
        this.adsLoader.addEventListener(
            //            google.ima.AdsLoadedEvent.Type.ADS_LOADED,
            google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
            //            this.onAdsLoaded,
            this.onAdsManagerLoaded,
            false);
        this.adsLoader.addEventListener(
            google.ima.AdErrorEvent.Type.AD_ERROR,
            this.onAdError,
            false);
        this.adsLoader.wnVideoCanvas = this;
        this.adsLoader.wnAdProps = adProps;

        var adsRequest = new google.ima.AdsRequest();
        adsRequest.adTagUrl = adUrl;
        // Specify both the linear and nonlinear slot sizes. This helps the SDK to
        // select the correct creative if multiple creatives are returned.
        adsRequest.linearAdSlotWidth = this.fV.playerWidth * 1;
        adsRequest.linearAdSlotHeight = this.fV.playerHeight - 20;
        //adsRequest.linearAdSlotHeight = this.fV.playerHeight - 30;
        adsRequest.nonLinearAdSlotWidth = this.fV.playerWidth * 1;
        adsRequest.nonLinearAdSlotHeight = this.fV.playerHeight - 20;
        //adsRequest.nonLinearAdSlotHeight = this.fV.playerHeight - 30;

        this.adsLoader.requestAds(adsRequest, this);
        wnLog("g: Ad request sent");
        // Optional parameter - adType: "overlay" or "video"
        // wnAdsLoader.requestAds({adTagUrl: adUrl, adType: "overlay"}, {clipObj:this.currentClip});
    } else {
        this.onAdError("Error: UAFHelper returned null");
    }
};

WNVideoCanvas.prototype.onAdsManagerLoaded = function(AdsManagerLoadedEvent) {
    wnLog("g: Ad response received");
    var contentPlayback = this.wnVideoCanvas.canvas;
    var adsRenderingSettings = new google.ima.AdsRenderingSettings();
    adsRenderingSettings.restoreCustomPlaybackStateOnAdBreakComplete = true;
    adManager = this.wnVideoCanvas.adManager = AdsManagerLoadedEvent.getAdsManager(
        contentPlayback, adsRenderingSettings
    );
    wnLog("g: isCustomPlaybackUsed = " + adManager.isCustomPlaybackUsed());
    adManager.wnVideoCanvas = this.wnVideoCanvas;
    adManager.wnAdProps = this.wnAdProps;

    // Check vast provider - WorldNow or 3rd party
    adManager.isWNAd = false;
    wnLog("g: adding event listeners");
    adManager.addEventListener(
        google.ima.AdErrorEvent.Type.AD_ERROR, this.wnVideoCanvas.onAdError);
    adManager.addEventListener(
        google.ima.AdEvent.Type.SKIPPED, this.wnVideoCanvas.onAdSkipped);
    adManager.addEventListener(
        google.ima.AdEvent.Type.ALL_ADS_COMPLETED, this.wnVideoCanvas.onAllAdsCompleted);
    adManager.addEventListener(
        google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, this.wnVideoCanvas.onContentPauseRequested);
    adManager.addEventListener(
        google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, this.wnVideoCanvas.onContentResumeRequested);

    try {
        // Initialize the ads manager. Ad rules playlist will start at this time.
        var width = parseInt(this.wnVideoCanvas.fV.playerWidth);
        var height = parseInt(this.wnVideoCanvas.fV.playerHeight) - 20;
        adManager.init(width, height, google.ima.ViewMode.NORMAL);
        // Listen to the STARTED event.
        adManager.addEventListener(google.ima.AdEvent.Type.STARTED, this.wnVideoCanvas.showCompanionAds);
        // Call play to start showing the ad. Single video and overlay ads will
        // start at this time; the call will be ignored for ad rules.
        wnLog("g: calling adManager.start() to start ad playback");
        adManager.start();
    } catch (adError) {
        wnLog("Error: " + adError);
    }
};

WNVideoCanvas.prototype.onAdError = function(adErrorEvent) {
    wnLog("g: onAdError");

    if (this.wnVideoCanvas.clipPlaySeqTracker.whatIsPlaying == "preroll") {

        var err = adErrorEvent.getError();
        if (err.getErrorCode() == 3102 || err.getErrorCode() == 3104) {
            // Just companions error. No need to interrupt the preroll. // DE15459 and DE15217
        } else {
            // Preroll error. Skip to the content clip.
            this.wnVideoCanvas.clipPlaySeqTracker.onStartFired = false;
            this.wnVideoCanvas.clipPlaySeqTracker.prerollIsOver = true;
            this.wnVideoCanvas.clipPlaySeqTracker.whatIsPlaying = "content";
            this.wnVideoCanvas.canvas.play();
            this.wnVideoCanvas.setUAFCompanions(this.wnAdProps);
        }

    } else if (this.wnVideoCanvas.clipPlaySeqTracker.whatIsPlaying == "postroll") {
        var err = adErrorEvent.getError();
        if (err.getErrorCode() == 3102 || err.getErrorCode() == 3104) {
            // Just companions error. No need to interrupt the preroll. // DE15459 and DE15217
        } else {
            this.wnVideoCanvas.completeSequence();
        }
    }

    // Handle the error logging.
    if (typeof(adErrorEvent.getError) == "function") {
        var err = adErrorEvent.getError();
        if (err) {
            wnLog(err.getMessage() + " See error object below:");
            wnLog(err);
        }
    } else {
        wnLog(adErrorEvent);
    }
};

WNVideoCanvas.prototype.onAdSkipped = function(adEvent) {
    wnLog("g: adSkipped");
    if (this.wnVideoCanvas.clipPlaySeqTracker.whatIsPlaying == "preroll") {
        // Skip to the content clip.
        this.wnVideoCanvas.clipPlaySeqTracker.onStartFired = false;
        this.wnVideoCanvas.clipPlaySeqTracker.prerollIsOver = true;
        this.wnVideoCanvas.clipPlaySeqTracker.whatIsPlaying = "content";
    }
};

WNVideoCanvas.prototype.showCompanionAds = function(event) {
    wnLog("g: on AdEvent.Type.STARTED - showCompanionAds");
    var ad = event.getAd();
    var companionAds = new Array;
    if (wng_pageInfo) {
        for (var ad_size in wng_pageInfo.ads) {
            ad_container = wng_pageInfo.ads[ad_size];
            var companionAd = ad.getCompanionAds(ad_container.width, ad_container.height, {
                creativeType: google.ima.CompanionAdSelectionSettings.CreativeType.ALL,
                resourceType: google.ima.CompanionAdSelectionSettings.ResourceType.ALL,
                sizeCriteria: google.ima.CompanionAdSelectionSettings.SizeCriteria.IGNORE // doesn't work any more in IMA SDK 3
            });
            if (companionAd.length > 0) companionAds.push(companionAd[0]);
        }
    }

    if (adManager.isWNAd && companionAds.length < 1) {
        this.wnVideoCanvas.setUAFCompanions(this.wnAdProps);
    } else {
        try {
            // convert to the flash compatible vast format
            for (i = 0; i < companionAds.length; i++) {
                //companionAds[i].contentType = companionAds[i].getContentType();
                companionAds[i].htmlResource = companionAds[i].getContent();
                companionAds[i].width = companionAds[i].getWidth();
                companionAds[i].height = companionAds[i].getHeight();
                // AdWidgets
                evntMgr.Notify({
                    evnt: "UAFLoadVastCompanions",
                    idKey: this.wnVideoCanvas.groupId,
                    objClip: {},
                    adProps: {},
                    vastAd: companionAds[i]
                });
            }
        } catch (e) {
            wnLog('Error: Setting Vast Companion Ads');
        }

        // Platform ads - need to pass the entire list in order to compare to enabled site config companions
        wnVideoWidgets.SetVASTCompanions({
            companions: companionAds,
            adProps: this.wnAdProps
        });
    }
}

WNVideoCanvas.prototype.onContentPauseRequested = function(event) {
    wnLog("g: CONTENT_PAUSE_REQUESTED");
    this.wnVideoCanvas.canvas.pause();
    //document.getElementById('wnVideoHTML').pause();
    // Switch to the custom controls
    if (this.wnVideoCanvas.canvasPlayBackCtrl) {
        this.wnVideoCanvas.canvasPlayBackCtrl.show(adManager);

        // On iPhone with iOS below version 8 "pause" event isn't fired, when you exit native player by clicking "Done"
        // Therefore we are rendering ad controls initially in pause state
        var os = wnUserAgentParser.getOS();
        if (os.name.toLowerCase() == 'ios' && parseFloat(os.version) < 8) {
            $wn(this.wnVideoCanvas.canvasPlayBackCtrl.elementPlayPause)
                .removeClass('wn-canvas-player-playing')
                .addClass('wn-canvas-player-paused');
            $wn(this.wnVideoCanvas.canvasPlayBackCtrl.elementProgressSpinner).html('&nbsp; &nbsp; &nbsp; Click PLAY to continue. After the commercial finishes, your video will begin.');
            $wn(this.wnVideoCanvas.canvasPlayBackCtrl.elementProgressSpinner).css('overflow', 'hidden').css('height', '16px').css('position', 'relative').css('top', '-15px');
        }
    }
    // Show ad title
    evntMgr.Notify({
        evnt: "NewMedia",
        idKey: this.wnVideoCanvas.groupId,
        objClip: {
            headline: this.adTitle
        }
    });
};

WNVideoCanvas.prototype.onContentResumeRequested = function(event) {
    wnLog("g: CONTENT_RESUME_REQUESTED");
    /*
        CONTENT_RESUME_REQUESTED isn't fired for livesteams. Handler logic moved to onAllAdsCompleted.
    */
};

WNVideoCanvas.prototype.onAllAdsCompleted = function(event) {
    wnLog("g: ALL_ADS_COMPLETED");
    if (this.wnVideoCanvas.canvasPlayBackCtrl) this.wnVideoCanvas.canvasPlayBackCtrl.hide();
    //if (this.wnVideoCanvas.ended) {
    if (this.wnVideoCanvas.clipPlaySeqTracker.whatIsPlaying == "postroll") {
        this.wnVideoCanvas.ended = false;
        this.wnVideoCanvas.completeSequence();
    } else {
        var ad = event.getAd();
        try {
            var totalAds = ad.getAdPodInfo().getTotalAds(); // Defaults to 1 if this ad is not part of a pod.
            if (totalAds > 1) {
                // Finished ad is part of a pod that might include a postroll,
                // that will be played when adsLoader.contentComplete() is called.
                // Additional ad call isn't required.
                this.wnVideoCanvas.clipPlaySeqTracker.hasIndependentPostroll = false;
            }
        } catch (e) {
            wnLog("g: adPodInfo is unavailable");
            this.wnVideoCanvas.clipPlaySeqTracker.hasIndependentPostroll = true;
        }
        //this.wnVideoCanvas.clipPlaySeqTracker.whatIsPlaying = "content";
        this.wnVideoCanvas.canvas.play();
    }
    if (this.wnVideoCanvas.showCCButton == true) {
        $wn('.wn-cc-button').show();
    }
};

WNVideoCanvas.prototype.setUAFCompanions = function(adProps) {
    // Platform ads
    wnVideoReloadCompanionAds(adProps, this.currentClip);
    // Ad Widgets
    evntMgr.Notify({
        evnt: "UAFLoadCompanions",
        idKey: this.groupId,
        objClip: this.currentClip,
        adProps: adProps
    });
};

WNVideoCanvas.prototype.fireReportingBeacon = function(params) {
    var isAd = false;
    if (this.currentClip.isPreRoll || this.currentClip.isPostRoll) {
        isAd = true;
    }
    try {
        var videoreport = Namespace_VideoReporting_Worldnow.logVideoEventParameters(
            params.eventype, //'Duration',
            this.currentClip.headline,
            params.position, //'0',
            this.currentClip.id,
            this.currentClip.adTag,
            'FlashPlayer.wnfl',
            this.currentClip.affiliateName,
            this.currentClip.ownerAffiliateName,
            isAd,
            '',
            '',
            '',
            '',
            this.currentClip.url,
            '',
            0,
            '',
            '',
            '',
            'STANDARD - HTML',
            wnSiteConfigVideo.enableExpressReports,
            '',
            '',
            '',
            this.currentClip.taxonomy1,
            this.currentClip.taxonomy2,
            this.currentClip.taxonomy3
        );
        this.currentClip.hasBeaconFired = true;
    } catch (e) {
        wnLog('Analytics beacon fire for clip id = ' + this.currentClip.id + ' failed: ' + e);
    }
};



/* Class: WNImageCanvas
- HTML version of the Flash slideshow canvas widget
*/
WNImageCanvas = function(parentDivId, flashVars) {
    this.fV = flashVars;
    this.widgetClassType = "WNImageCanvas";
    this.parentDivId = parentDivId;
    this.parentJSName = this.widgetClassType + this.fV.widgetId;
    this.groupId = this.fV.idKey;
    this.current_image = false;
    this.delay = parseInt(wnSiteConfigVideo.imageCanvasWidget.slideTime);
    if (isNaN(this.delay) || this.delay < 1) this.delay = 3;

    // Remove static AddThis link. replaced with dynamic generation in wnVideoWidgets.openAddThisPane()
    $wn("#" + this.parentDivId + "_addThis > div").remove();

    // Render ImageCanvas
    // Respond to screen width
    if (wn_isMobile) {
        var leftOffset = parseInt($wn("#" + this.parentDivId).position().left);
        var oldWidth = parseInt(this.fV.playerWidth) + leftOffset;
        var newWidth = $wn("#" + this.parentDivId).parent().parent().width() - leftOffset;
        //var newWidth = wnVideoUtils.getResponsiveWidth(this.fV.playerWidth);
        if (newWidth < oldWidth) {
            this.fV.playerWidth = newWidth;
            $wn("#" + this.parentDivId + "_adDiv").width(newWidth);
            var diff = oldWidth - newWidth;
            $wn("#" + this.parentDivId + "_adDiv_ad").css("left", Math.floor($wn("#" + this.parentDivId + "_adDiv_ad").position().left - diff / 2))
            $wn("#" + this.parentDivId + "_adDiv_close").css("left", Math.floor($wn("#" + this.parentDivId + "_adDiv_close").position().left - diff / 2))
        }
    }

    this.render();

    // Prepare incanvas ad container
    if ((this.fV.isInCanvasAdModeEnabled == "true" || this.fV.isInCanvasAdModeEnabled == true) &&
        this.fV.playerWidth >= 300 && this.fV.playerHeight >= 250) {
        var slideshowAdDivName = this.parentDivId + "_adDiv";
        var patern = "WNImageCanvas" + this.groupId;
        slideshowAdDivName = slideshowAdDivName.replace("WNImageCanvas" + this.groupId, "");

        var slideshowAd = new WNAdWidget("53", "HandleWorldnowAd", this.groupId);
        slideshowAd.SetWidth(300);
        slideshowAd.SetHeight(250);
        slideshowAd.SetSeqNum(1);
        slideshowAd.SetTarget(document.getElementById(slideshowAdDivName + "_ad"));
        slideshowAd.setApplication("slideshow");
        slideshowAd.setIsInCanvas(true);
        /*slideshowAd.uafAd({ - moved to the AdWidget
            type:'dom'
            ,id:53
            ,application:'slideshow'
            ,width:300
            ,height:250
            ,ownerinfo:{local:{share:1}}
            ,parent:slideshowAdDivName + "_ad"
            ,delayinit:true
        });*/

        // Fire 'pagestart' if wasn't fired yet (on off-platform) to get GPT lib loaded
        var owner = slideshowAd.ad.get('owner');
        if (!slideshowAd.ad.manager.eventsHistory.hasOwnProperty(owner) ||
            slideshowAd.ad.manager.eventsHistory.hasOwnProperty(owner) &&
            slideshowAd.ad.manager.eventsHistory[owner].indexOf('pagestart') < 0) {
            slideshowAd.ad.manager.start();
        }

    }

    //register for events1
    $wn("#" + this.fV.domId).data("widget", this);
    evntMgr.Register({
        evnt: "NewClip",
        func: "onNewImage",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "primaryImageGalleryLoad",
        func: "onPrimaryImageGalleryLoad",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "NewInCanvasAd",
        func: "onNewInCanvasAd",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "InCanvasAdOver",
        func: "onInCanvasAdOver",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "LastImageReached",
        func: "onLastImageReached",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });

    // Rerender Headline and InfoPane widgets associated with this ImageCanvas */
    var headline = getWigdetByType(this.groupId, "h");
    if (headline.widgetClassType != undefined) headline.htmlVersion = new WNHeadline(headline.parentObject.targetDiv, headline);
    var infopane = getWigdetByType(this.groupId, "i");
    if (infopane.widgetClassType != undefined) infopane.htmlVersion = new WNInfoPane(infopane.parentObject.targetDiv, infopane);

    onWidgetLoad(this.fV.domId, this.fV.idKey, "imageCanvasIsReady", "", {});
    //imageCanvasIsReady(this.parentDivId, this.fV.idKey);
    evntMgr.Notify({
        evnt: "imageCanvasLoad",
        idKey: this.fV.idKey,
        objClip: {}
    });
};

WNImageCanvas.prototype.render = function() {
    var thisWNImgCnvs = this;
    var output = "";
    output += "<div id=" + this.fV.domId + " class='wn-ad-gallery wnSlideshowCanvas'>";
    output += "   <div class='wn-ad-image-wrapper'>";
    //output += "      <img src='loader.gif' class='wn-ad-loader'>";
    output += "      <div class='wn-ad-next'>";
    output += "         <div class='wn-ad-next-image'/>";
    output += "      </div>";
    output += "      <div class='wn-ad-prev'>";
    output += "         <div class='wn-ad-prev-image'/>";
    output += "      </div>";
    output += "   </div>";
    output += "   <div class='wn-controls ";
    if (wn_isMobile) output += 'wn-mobile';
    output += "'>";
    output += "      <div class='wn-start-pause'>start</div>";
    output += "      <div class='wn-share-button'>share</div>";
    output += "      <div class='wn-number'>&nbsp; &nbsp; &nbsp;</div>";
    output += "   </div>";
    output += "   <div class='wn-share-pane'>";
    output += "      <div class='wn-close_button'>Close X</div>";
    output += "      <div class='wn-embed-fields'>";
    output += "         <div class='wn-sh-label'>Embed Slideshow Code:</div>";
    output += "         <div class='wn-embed-code sh-input'></div>";
    output += "      </div>";
    output += "      <div class='wn-link-fields'>";
    output += "         <div class='wn-sh-label'>Link to Slideshow:</div>";
    output += "         <input type='text' class='share-link sh-input'/>";
    output += "      </div>";
    output += "   </div>";
    output += "</div>";

    /* Set styles and dimentions */
    output += "<style type='text/css'>";
    output += "#" + this.fV.domId + " {width: " + this.fV.playerWidth + "px; height: " + this.fV.playerHeight + "px}";
    output += "#" + this.fV.domId + " .wn-ad-image-wrapper {height: " + this.fV.playerHeight + "px}";
    output += "#" + this.fV.domId + " .wn-ad-image-wrapper .wn-ad-image {height: " + this.fV.playerHeight + "px; width: " + this.fV.playerWidth + "px;}";
    output += "#" + this.fV.domId + " .wn-share-pane {height: " + this.fV.playerHeight + "px}";
    output += "#" + this.fV.domId + " .wn-share-pane .sh-input {width: " + (this.fV.playerWidth - 160) + "px}";
    output += "#" + this.fV.domId + " .wn-share-pane .wn-embed-code {height: " + (this.fV.playerHeight - 150) + "px}";
    output += "#" + this.fV.domId + " .wn-ad-image-wrapper .wn-ad-prev {top: " + (this.fV.playerHeight / 2 - 26) + "px;}";
    output += "#" + this.fV.domId + " .wn-ad-image-wrapper .wn-ad-next {top: " + (this.fV.playerHeight / 2 - 26) + "px;}";
    output += "</style>";

    $wn("#" + this.parentDivId).empty();
    $wn("#" + this.parentDivId).append(output);
    //$wn("#" + this.parentDivId).parent().attr('class', 'WNWidgetsContainer');
    var current_class = $wn("#" + this.parentDivId).attr('class');
    if (typeof current_class != 'undefined') {
        $wn("#" + this.parentDivId).attr('class', current_class + ' WNWidgetsContainer');
    } else {
        $wn("#" + this.parentDivId).attr('class', 'WNWidgetsContainer');
    }

    this.prev_link = $wn('#' + this.fV.domId).find('.wn-ad-prev');
    this.next_link = $wn('#' + this.fV.domId).find('.wn-ad-next');
    this.prev_link.add(this.next_link).mouseover(
        function(e) {
            // IE 6 hides the wrapper div, so we have to set it's width
            $wn(this).css('height', 52);
            //$wn(this).find('div').show();
            $wn(this).find('div').css('opacity', 1.0);
        }
    ).mouseout(
        function(e) {
            //$wn(this).find('div').hide();
            $wn(this).find('div').css('opacity', 0.2);
        }
    ).click(
        function() {
            thisWNImgCnvs.pauseSlideshow(); // "next/prev" clicked manually
            if ($wn(this).is('.wn-ad-next')) {
                if (!wnSlideshowImage.hideInCanvasAd({
                    groupId: thisWNImgCnvs.groupId
                })) {
                    thisWNImgCnvs.nextImage();
                    evntMgr.Notify({
                        evnt: "next",
                        idKey: thisWNImgCnvs.fV.idKey,
                        objClip: thisWNImgCnvs.currentImgObj,
                        system: "slideshow"
                    });
                }
            } else {
                if (!wnSlideshowImage.hideInCanvasAd({
                    groupId: thisWNImgCnvs.groupId
                })) {
                    thisWNImgCnvs.prevImage();
                    evntMgr.Notify({
                        evnt: "previous",
                        idKey: thisWNImgCnvs.fV.idKey,
                        objClip: thisWNImgCnvs.currentImgObj,
                        system: "slideshow"
                    });
                }
            };
        }
    ).find('div').css('opacity', 0.2);

    var start_pause_button = $wn('#' + this.fV.domId).find('.wn-start-pause');
    var share_button = $wn('#' + this.fV.domId).find('.wn-share-button');
    var close_button = $wn('#' + this.fV.domId).find('.wn-close_button');
    start_pause_button.add(share_button).add(close_button).mouseover(
        function(e) {
            $wn(this).css('cursor', 'pointer');
        }
    ).mouseout(
        function(e) {
            $wn(this).css('cursor', 'default');
        }
    );
    close_button.click(
        function() {
            $wn(this).parent().fadeOut(300);
            $wn(this).parent().parent().find('.wn-controls').fadeIn(300);
            $wn(this).parent().parent().find('.wn-ad-image-description').fadeIn(300);
            //wnSlideshowImage.showHideAddThis({ groupId: thisWNImgCnvs.groupId, isVisible: false });
            wnVideoWidgets.closeAddThisPane(thisWNImgCnvs.parentDivId);
        }
    );
    share_button.click(
        function() {
            $wn(this).parent().fadeOut(300);
            $wn(this).parent().parent().find('.wn-share-pane').fadeIn(300);
            $wn(this).parent().parent().find('.wn-ad-image-description').fadeOut(300);
            //wnSlideshowImage.showHideAddThis({ groupId: thisWNImgCnvs.groupId, isVisible: true });
            wnVideoWidgets.openAddThisPane(thisWNImgCnvs.share_link[0].value, "Slideshow", thisWNImgCnvs.parentDivId, "", true);
        }
    );
    start_pause_button.click(
        function() {
            if ($wn(this).html() == "start") {
                if ((thisWNImgCnvs.currentImgObj.index + 1) == thisWNImgCnvs.currentImgObj.total_number) {
                    evntMgr.Notify({
                        evnt: "NextImageRequest",
                        idKey: thisWNImgCnvs.fV.idKey,
                        objClip: {
                            index: -1,
                            widgetId: thisWNImgCnvs.currentImgObj.widgetId
                        }
                    });
                }
                thisWNImgCnvs.startSlideshow();
                evntMgr.Notify({
                    evnt: "start",
                    idKey: thisWNImgCnvs.fV.idKey,
                    objClip: thisWNImgCnvs.currentImgObj,
                    system: "slideshow"
                });
            } else {
                thisWNImgCnvs.pauseSlideshow();
                evntMgr.Notify({
                    evnt: "stop",
                    idKey: thisWNImgCnvs.fV.idKey,
                    objClip: thisWNImgCnvs.currentImgObj,
                    system: "slideshow"
                });
            }
        }
    );

    /* Ad close button */
    if (typeof($wn("#" + this.parentDivId + "_adDiv")[0]) != "undefined") {
        ad_top = $wn("#" + this.parentDivId + "_adDiv").css("top").replace("px", "") * 1;
        ad_left = $wn("#" + this.parentDivId + "_adDiv").css("left").replace("px", "") * 1;
        ad_width = $wn("#" + this.parentDivId + "_adDiv").css("width").replace("px", "") * 1;
        ad_height = $wn("#" + this.parentDivId + "_adDiv").css("height").replace("px", "") * 1;
        output = "<div id='" + this.parentDivId + "_adDiv_close_x' style='left: 260px; width: 30px;'>X</div>";
        $wn("#" + this.parentDivId + "_adDiv_close").append(output);
        /*
        $wn("#" + this.parentDivId + "_adDiv").css("top", "-=1");
        $wn("#" + this.parentDivId + "_adDiv").css("left", "-=1");
        $wn("#" + this.parentDivId + "_adDiv").css("width", "+=2");
        $wn("#" + this.parentDivId + "_adDiv").css("height", "+=2");
        */
        //$wn("#" + this.parentDivId + "_adDiv").css("top", (ad_top-1) + "px");
        $wn("#" + this.parentDivId + "_adDiv").css("top", 0);
        $wn("#" + this.parentDivId + "_adDiv").css("left", (ad_left - 1) + "px");
        $wn("#" + this.parentDivId + "_adDiv").css("width", (ad_width + 2) + "px");
        $wn("#" + this.parentDivId + "_adDiv").css("height", (ad_height + 2) + "px");

        // Move incanvas ad div under the next/prev buttons
        $wn("#" + this.fV.domId + " > div.wn-ad-image-wrapper").append($wn("#" + this.parentDivId + "_adDiv"));
        //$wn("#" + this.parentDivId + "_adDiv").remove();
    }
};

WNImageCanvas.prototype.onNewInCanvasAd = function(p_evtObj) {
    if (this.timer) { // slideshow is running
        this.pauseSlideshow();
        this.resumeAfterAd = true;
    }
};

WNImageCanvas.prototype.onInCanvasAdOver = function(p_evtObj) {
    if (this.resumeAfterAd) this.startSlideshow();
};

WNImageCanvas.prototype.onLastImageReached = function(p_evtObj) {
    this.pauseSlideshow();
};

WNImageCanvas.prototype.startSlideshow = function() {
    var start_pause_button = $wn('#' + this.fV.domId).find('.wn-start-pause');
    start_pause_button.html('pause');
    //this.timer = setInterval(this.parentJSName + '.htmlVersion.nextImage()', this.delay);
    var slideshow_canvas_widget_reference = "pageWidgets['" + this.groupId + "']." + this.widgetClassType + this.groupId + this.parentDivId;
    this.timer = setInterval(slideshow_canvas_widget_reference + '.htmlVersion.nextImage()', this.delay * 1000);
};

WNImageCanvas.prototype.pauseSlideshow = function() {
    var start_pause_button = $wn('#' + this.fV.domId).find('.wn-start-pause');
    start_pause_button.html('start');
    clearInterval(this.timer);
    this.timer = false;
    this.resumeAfterAd = false;
};

WNImageCanvas.prototype.onNewImage = function(p_evtObj) {
    var thisWNImgCnvs = this;
    // insert image
    var canvasDiv = $wn('#' + this.fV.domId);
    var image_wrapper = canvasDiv.find('.wn-ad-image-wrapper');
    this.currentImgObj = p_evtObj.objClip;
    var img_container = $wn(document.createElement('div')).addClass('wn-ad-image');
    var img = $wn(new Image()).attr('src', this.currentImgObj.url);
    if (this.currentImgObj != undefined && this.currentImgObj.href != "") {
        var link = $wn("<a href='" + this.currentImgObj.href + "' target='blank'></a>");
        link.append(img);
        img_container.append(link);
    } else {
        img_container.append(img);
    }
    image_wrapper.prepend(img_container);
    // resize and center the image
    if (typeof img !== 'undefined' && !wnVideoWidgets.isImageLoaded(img[0])) {
        img.load(
            function() {
                thisWNImgCnvs.resizeAndCenter(img_container);
            }
        );
    } else {
        this.resizeAndCenter(img_container);
    }
    // description
    if (this.fV.enableSummaryPane.toLowerCase() == "true") {
        var hasSummary = this.fV.hasSummary.toLowerCase() == "true" && this.currentImgObj.summary != "";
        var hasDateStamp = this.fV.hasDateStamp.toLowerCase() == "true" && this.currentImgObj.publishingDate != "";
        var hasPhotoCredit = this.fV.hasPhotoCredit.toLowerCase() == "true" && this.currentImgObj.credits != "";
        var description_text = '<p class="wn-ad-image-description">';
        if (hasSummary) description_text += this.currentImgObj.summary;
        description_text += "<span>";
        if (hasSummary && (hasDateStamp || hasPhotoCredit)) description_text += " | ";
        if (hasDateStamp) description_text += this.currentImgObj.publishingDate + " &nbsp;";
        if (hasPhotoCredit) description_text += "  &nbsp;" + this.currentImgObj.credits;
        description_text += "</span></p>";
        var desc = $wn(description_text);
        img_container.append(desc);

        // "rel" attribute is used to save mouseover status [is over the canvas = true/false]
        if (img_container.parent().attr("rel") == "true") {
            desc.show();
        } else {
            desc.hide();
        }
        // remove previously assigned handlers
        img_container.parent().unbind('mouseenter mouseleave');
        // assign new handlers
        img_container.parent().hover(
            function() {
                desc.stop(true, true).fadeIn();
                img_container.parent().attr("rel", "true");
            },
            function() {
                desc.stop(true, true).fadeOut();
                img_container.parent().attr("rel", "false");
            }
        );
    }

    // image number
    var controls = canvasDiv.find('.wn-controls');
    var image_number = controls.find('.wn-number');
    image_number.html(this.currentImgObj.index + 1 + '/' + this.currentImgObj.total_number);

    // sharing fields
    var share_pane = canvasDiv.find('.wn-share-pane');
    var embed_code = share_pane.find('.wn-embed-code');
    var apiUrl = "";
    /*
    var pattern = /\.([A-Za-z0-9]*)\.worldnow/gi;
    var match = pattern.exec(this.fV.hostDomain);
    if (match[1] != null) {
    apiUrl = "http://api."+ match[1] +".worldnow.com:8000/feed/v3.0/widgets/";
    } else {
    apiUrl = "http://api.worldnow.com/feed/v3.0/widgets/";
    }
    */
    apiUrl = wnFeedsApiDomain + "/feed/v3.0/widgets/";
    embed_code.empty();
    embed_code.append("&lt;script type='text/javascript' src='" + apiUrl + this.currentImgObj.widgetId + "?alt=js&contextaffiliate=" + wnSiteConfigGeneral.affiliateNumber + "'&gt;&lt;/script&gt;");

    this.share_link = share_pane.find('.share-link');
    var slideshow_link_info = 'widgetid=' + this.currentImgObj.widgetId + '&slideshowimageid=' + (this.currentImgObj.index + 1);
    if (this.fV.landingPage != undefined && this.fV.landingPage != "") {
        var smbl = (this.fV.landingPage.indexOf("?") > 0) ? "&" : "?";
        this.share_link[0].value = this.fV.landingPage + smbl + slideshow_link_info;
    } else {
        this.share_link[0].value = "http://" + this.fV.hostDomain + "/global/slideshow.asp?" + slideshow_link_info;
    }

    img_container.css('opacity', 0);
    var animation = {
        old_image: {
            opacity: 0
        },
        new_image: {
            opacity: 1
        }
    };

    if (this.current_image) {
        var old_image = this.current_image;
        var old_description = this.current_description;
        old_image.animate({
                opacity: 0
            }, 400, 'swing',
            function() {
                old_image.remove();
                if (old_description) old_description.remove();
            }
        );
    }
    /* else {
            image_wrapper.find('.wn-ad-loader').hide();
        }*/

    this.current_image = img_container;
    this.current_description = desc;

    img_container.animate({
            opacity: 1
        }, 400, 'swing',
        function() {
            if (thisWNImgCnvs.currentImgObj.index == (thisWNImgCnvs.currentImgObj.total_number - 1)) {
                thisWNImgCnvs.next_link.hide();
            } else {
                thisWNImgCnvs.next_link.show();
            }
            if (thisWNImgCnvs.currentImgObj.index == 0) {
                thisWNImgCnvs.prev_link.hide();
            } else {
                thisWNImgCnvs.prev_link.show();
            }
            /*if(!this.settings.cycle) {
            // Needed for IE
            this.prev_link.show().css('height', this.image_wrapper_height);
            this.next_link.show().css('height', this.image_wrapper_height);
            if(this.current_index == (this.images.length - 1)) {
            this.next_link.hide();
            };
            if(this.current_index == 0) {
            this.prev_link.hide();
            };
            };
            this.fireCallback(this.settings.callbacks.afterImageVisible);

            thisWNImgCnvs.fireCallback(callback);*/
        }
    );

    // pause slideshow if gallery thumbnail was clicked
    if (p_evtObj.objClip.isClicked) {
        var start_pause_button = $wn('#' + this.fV.domId).find('.wn-start-pause');
        if (start_pause_button.html() == "pause") {
            thisWNImgCnvs.pauseSlideshow();
        }
    }
};

WNImageCanvas.prototype.onPrimaryImageGalleryLoad = function(p_evtObj) {
    var imgIndex = (p_evtObj.objClip.urlImageIndex > 0) ? p_evtObj.objClip.urlImageIndex - 1 : -1;
    if (!this.currentImgObj) {
        var widgetId = (parseInt(this.fV.urlWidgetId) > 0) ? this.fV.urlWidgetId : this.fV.widgetId;
        evntMgr.Notify({
            evnt: "NextImageRequest",
            idKey: this.fV.idKey,
            objClip: {
                index: imgIndex,
                widgetId: widgetId
            }
        });
    }
};

WNImageCanvas.prototype.nextImage = function() {
    evntMgr.Notify({
        evnt: "NextImageRequest",
        idKey: this.fV.idKey,
        objClip: this.getCurrentImageObj()
    });
};

WNImageCanvas.prototype.prevImage = function() {
    evntMgr.Notify({
        evnt: "PrevImageRequest",
        idKey: this.fV.idKey,
        objClip: this.getCurrentImageObj()
    });
};

WNImageCanvas.prototype.getCurrentImageObj = function() {
    var image = this.currentImgObj;
    if (!image) image = {
        index: -1,
        widgetId: this.fV.widgetId
    };
    return image;
};


WNImageCanvas.prototype.resizeAndCenter = function(img_container) {
    var img = img_container.find("img:first");
    if (img.length > 0) {
        var size = this.getContainedImageSize(img[0].width, img[0].height);
        img.attr('width', size.width);
        img.attr('height', size.height);
        this.centerImage(img_container, size.width, size.height);
    }
};

/**
 * Checks if the image is small enough to fit inside the container
 * If it's not, shrink it proportionally
 */
WNImageCanvas.prototype.getContainedImageSize = function(image_width, image_height) {
    if (image_height > this.fV.playerHeight) {
        var ratio = image_width / image_height;
        image_height = this.fV.playerHeight;
        image_width = this.fV.playerHeight * ratio;
    };
    if (image_width > this.fV.playerWidth) {
        var ratio = image_height / image_width;
        image_width = this.fV.playerWidth;
        image_height = this.fV.playerWidth * ratio;
    };
    return {
        width: image_width,
        height: image_height
    };
};

/**
 * If the image dimensions are smaller than the wrapper, we position
 * it in the middle anyway
 */
WNImageCanvas.prototype.centerImage = function(img_container, image_width, image_height) {
    img_container.css('top', '0px');
    if (image_height < this.fV.playerHeight) {
        var dif = this.fV.playerHeight - image_height;
        //img_container.css('top', (dif / 2) +'px');
        img_container.find('img').css('top', (dif / 2) + 'px');
    };
    img_container.css('left', '0px');
    if (image_width < this.fV.playerWidth) {
        var dif = this.fV.playerWidth - image_width;
        img_container.find('img').css('left', (dif / 2) + 'px');
    };
};




/* Class: WNImageGallery
- HTML version of the Flash slideshow gallery widget
*/
WNImageGallery = function(parentDivId, flashVars, images) {
    this.fV = flashVars;
    this.widgetClassType = "WNImageGallery";
    this.parentDivId = parentDivId;
    this.parentJSName = this.widgetClassType + this.fV.widgetId;
    this.groupId = this.fV.idKey;
    this.images = (images != undefined) ? images : [];

    // clear parent div of the flash related content
    $wn("#" + this.parentDivId).empty();
    if (this.fV.isSingleImageGallery.toLowerCase() == "true") {
        this.renderAsSingleImageGallery();
    } else {
        if (this.fV.isPrimaryGallery.toLowerCase() == "true") {
            this.isPrimaryGallery = true
        }

        // if url widget id is specified, get extrnal data
        if (this.fV.urlWidgetId != undefined && this.isPrimaryGallery && (parseInt(this.fV.urlWidgetId) > 0)) {
            // send request to feeds api to get images by the widget id
            // http://api.dsys1.worldnow.com:8000/feed/v3.0/widgets/24855?alt=json
            var timestamp = new Date().getTime();
            var feedUrl = wnFeedsApiDomain + '/feed/v3.0/widgets/' + this.fV.urlWidgetId + '?alt=json&callback=wnVideoWidgets.loadPrimarySlideshowData&contextaffiliate=' + wn.affiliateno;
            wnVideoUtils.loadScript("wnSlideshowScript" + timestamp, feedUrl);
        } else {

            // Respond to screen width
            if (wn_isMobile) {
                var leftOffset = parseInt($wn("#" + this.parentDivId).position().left);
                var oldWidth = parseInt(this.fV.playerWidth) + leftOffset;
                var newWidth = $wn("#" + this.parentDivId).parent().parent().width() - leftOffset;
                //var newWidth = wnVideoUtils.getResponsiveWidth(this.fV.playerWidth);
                /*alert($wn("#"+this.parentDivId).position().left);
                alert($wn("#"+this.parentDivId).parent().parent().attr("id") + ": " + newWidth +" - "+ oldWidth);*/
                if (newWidth < oldWidth) this.fV.playerWidth = newWidth;
            }

            this.render();
        }
    }
};

WNImageGallery.prototype.highlightThumb = function(thumbIndex) {
    var thumbs = $wn('#' + this.fV.domId).find('a');
    if (thumbs.length > 0) {
        var thumb = $wn(thumbs[thumbIndex]);
        thumb.parent().parent().find('.wn-ad-active').removeClass('wn-ad-active');
        thumb.addClass('wn-ad-active');
        var thumbs_wrapper = thumb.parents('.wn-ad-thumbs');
        var thumbs_wrapper_list = thumb.parents('.wn-ad-thumb-list');
        var scroll_forward = thumbs_wrapper.siblings('.wn-ad-forward');
        var scroll_back = thumbs_wrapper.siblings('.wn-ad-back');
        var left = thumb.parent()[0].offsetLeft;
        left -= (thumb.parent().parent().parent().width() / 2) - (thumb[0].offsetWidth / 2);
        // Disabling animation for the widget builder. Conflicts with an other script on the page.
        if (((document.location).toString().indexOf("https://manage") > -1 ||
                (document.location).toString().indexOf("http://manage") > -1) &&
            (document.location).toString().indexOf("worldnow.com") > -1) {
            thumb.parent().parent().parent().scrollLeft(left);
            WNImageGallery.handleGalleryPrevNextUI(thumbs_wrapper.width(), thumbs_wrapper.scrollLeft(), thumbs_wrapper_list.width(), scroll_forward, scroll_back);
        } else {
            thumb.parent().parent().parent().animate({
                scrollLeft: left + 'px'
            }, function() {
                // Handle Prev/Next UI
                WNImageGallery.handleGalleryPrevNextUI(thumbs_wrapper.width(), thumbs_wrapper.scrollLeft(), thumbs_wrapper_list.width(), scroll_forward, scroll_back);
            });
        }
    }
};

WNImageGallery.prototype.renderFromJson = function(json) {
    if (json.widgets != undefined) {
        for (var i in json.widgets[0].json) {
            if (json.widgets[0].json[i].images != undefined) {
                this.images = json.widgets[0].json[i].images;
                this.fV.adTag = json.widgets[0].json[i].adTag;
                this.fV.slideshowHeadline = json.widgets[0].json[i].slideshowHeadline;
            }
        }
    }
    this.render();
};

WNImageGallery.prototype.renderAsSingleImageGallery = function() {
    var thisWNImgGlry = this;
    var landingPage = "";
    if (this.fV.landingPage != undefined && this.fV.landingPage != "") {
        var smbl = (this.fV.landingPage.indexOf("?") > 0) ? "&" : "?";
        landingPage = this.fV.landingPage + smbl + 'widgetid=' + this.fV.widgetId;
    } else {
        landingPage = "http://" + this.fV.hostDomain + "/global/slideshow.asp?" + 'widgetid=' + this.fV.widgetId;
    }
    var popupLandingPage = (typeof this.fV.popupLandingPage != 'undefined' && this.fV.popupLandingPage.toLowerCase() == "true");
    var output = "";
    output += "<div id=" + this.fV.domId + ">";
    output += "   <div class='wnSingleImageGallery'>";
    output += "      <div class='wn-gallery-thumbnail'>";
    var image = this.images[0];
    var title = this.fV.slideshowHeadline;
    var number_of_photo = this.images.length;
    if (typeof image == 'undefined') {
        output += "         <div class='wn-single-image-placeholder'>&nbsp;</div>";
    } else {
        // title = image.headline; // uncomment if slideshow title is a fist image title
        output += "         <a href='" + landingPage + "' ";
        if (popupLandingPage) output += "target='blank' ";
        output += "><img src='" + unescape(image.url) + "' height='80'/></a>";
    }
    output += "      </div>";
    output += "      <div class='wn-gallery-description'>";
    output += "         <a href='" + landingPage + "'";
    if (popupLandingPage) output += "target='blank' ";
    output += ">" + unescape(title) + "</a><br/>";
    output += "         <span class='photo_number'>" + number_of_photo + " photo";
    if (number_of_photo > 1) output += "s";
    output += "</span>";
    output += "      </div>";
    output += "   </div>";
    output += "</div>";

    /* Set styles and dimentions */
    output += "<style type='text/css'>";
    output += "#" + this.fV.domId + " .wnSingleImageGallery {width: " + this.fV.playerWidth + "px; height: " + this.fV.playerHeight + "px}";
    output += "</style>";
    $wn("#" + this.parentDivId).append(output);
    //$wn("#" + this.parentDivId).parent().attr('class', 'WNWidgetsContainer');
    var current_class = $wn("#" + this.parentDivId).attr('class');
    if (typeof current_class != 'indefined') {
        $wn("#" + this.parentDivId).attr('class', current_class + ' WNWidgetsContainer');
    } else {
        $wn("#" + this.parentDivId).attr('class', 'WNWidgetsContainer');
    }
};

WNImageGallery.prototype.render = function() {
    var thisWNImgGlry = this;
    var output = "";
    output += "<div id=" + this.fV.domId + ">";
    output += "   <div class='wn-ad-gallery'>";
    output += "      <div class='wn-ad-nav'>";
    output += "         <div class='wn-ad-back'/>";
    output += "         <div class='wn-ad-thumbs'>";
    output += "            <ul class='wn-ad-thumb-list'>";
    var images = this.images;
    //var parent = this.parentJSName;
    var playerHeight = this.fV.playerHeight;
    $wn.each(images, function(index, value) {
        /*output += '<li><a onclick="' + parent + '.htmlVersion.showImage(' + index + ', true); return false;"';
        output += '><img src="' + unescape(images[index].url);*/
        output += "<li><a><img src='" + unescape(images[index].url);
        output += "' class='image" + index + "' height='" + (playerHeight - 20) + "'/></a></li>";
    });
    output += "            </ul>";
    output += "         </div>";
    output += "         <div class='wn-ad-forward'/>";
    output += "      </div>";
    output += "   </div>";
    output += "</div>";

    /* Set styles and dimentions */
    output += "<style type='text/css'>";
    output += "#" + this.fV.domId + " .wn-ad-gallery {width: " + (this.fV.playerWidth - 2) + "px; height: " + (this.fV.playerHeight - 2) + "px}";
    output += "#" + this.fV.domId + " .wn-ad-gallery .wn-ad-nav .wn-ad-thumbs {width: " + (this.fV.playerWidth - 80) + "px}";
    output += "#" + this.fV.domId + " .wn-ad-gallery .wn-ad-thumbs li a img {height:" + (this.fV.playerHeight - 20) + "px}"; // thumbnails height(scale) based on the gallery widget height;
    output += "#" + this.fV.domId + " .wn-ad-gallery .wn-ad-forward, ";
    output += "#" + this.fV.domId + " .wn-ad-gallery .wn-ad-back {top: " + Math.round((this.fV.playerHeight - 21) / 2) + "px}";
    output += "</style>";

    $wn("#" + this.parentDivId).append(output);

    //register for events
    $wn("#" + this.fV.domId).data("widget", this);
    evntMgr.Register({
        evnt: "NextImageRequest",
        func: "onNextImageRequest",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "PrevImageRequest",
        func: "onPrevImageRequest",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "imageCanvasLoad",
        func: "onImageCanvasLoad",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });

    //$wn("#" + this.parentDivId).parent().attr('class', 'WNWidgetsContainer');
    var current_class = $wn("#" + this.parentDivId).attr('class');
    if (typeof current_class != 'indefined') {
        $wn("#" + this.parentDivId).attr('class', current_class + ' WNWidgetsContainer');
    } else {
        $wn("#" + this.parentDivId).attr('class', 'WNWidgetsContainer');
    }

    var containerDiv = $wn('#' + this.fV.domId);
    var thumb_wrapper_width = 0;
    var thumbs_loaded = 0;
    var thumbs = containerDiv.find('a');
    var thumb_count = thumbs.length;
    thumbs.each(
        function(i) {
            var link = $wn(this);
            var thumb = link.find('img');
            // Check if the thumb has already loaded
            if (!wnVideoWidgets.isImageLoaded(thumb[0])) {
                thumb.load(
                    function() {
                        thumb_wrapper_width += this.parentNode.parentNode.offsetWidth;
                        thumbs_loaded++;
                    }
                );
            } else {
                thumb_wrapper_width += thumb[0].parentNode.parentNode.offsetWidth;
                thumbs_loaded++;
            };
            link.addClass('ad-thumb' + i);
            link.click(
                function() {
                    thisWNImgGlry.showImage(i, true);
                    thisWNImgGlry.highlightThumb(i);
                }
            );
        }
    );
    // Wait until all thumbs are loaded, and then set the width of the ul (thumbs container)
    var inter = setInterval(
        function() {
            if (thumb_count == thumbs_loaded) {
                /*
                thumb_wrapper_width -= 100;
                if (thumb_wrapper_width < 0) thumb_wrapper_width = 0;
                */
                var list = containerDiv.find('.wn-ad-thumb-list');
                thumb_wrapper_width = thumb_wrapper_width + 2;
                list.css('width', thumb_wrapper_width + 'px');
                var increased = false;
                var i = 1;
                var last_height = list.height();
                while (i < 201) {
                    list.css('width', (thumb_wrapper_width + i) + 'px');
                    if (last_height != list.height()) {
                        increased = true;
                        break;
                    }
                    last_height = list.height();
                    i++;
                }
                if (!increased) list.css('width', thumb_wrapper_width + 'px');
                clearInterval(inter);
            };
        },
        100
    );

    var that = this;
    var urlImageId = parseInt(this.fV.imageId) - 1;
    var imgIndex = (urlImageId > 0) ? urlImageId : 0;

    var thumbs_wrapper = containerDiv.find('.wn-ad-thumbs');
    var scroll_forward = containerDiv.find('.wn-ad-forward');
    var scroll_back = containerDiv.find('.wn-ad-back');
    var thumbs_wrapper_list = containerDiv.find('.wn-ad-thumb-list');
    var has_scrolled = 0;
    var thumbs_scroll_interval = false;
    $wn(scroll_back).add(scroll_forward).click(
        function() {
            // We don't want to jump the whole width, since an image
            // might be cut at the edge
            var eventName = 'gallerynext';
            var width = thumbs_wrapper.width() - 50;
            if ($wn(this).is('.wn-ad-forward')) {
                var left = thumbs_wrapper.scrollLeft() + width;
            } else {
                var left = thumbs_wrapper.scrollLeft() - width;
                eventName = 'galleryprevious'
            };
            // Disabling animation for the widget builder. Conflicts with an other script on the page.
            if (((document.location).toString().indexOf("https://manage") > -1 ||
                    (document.location).toString().indexOf("http://manage") > -1) &&
                (document.location).toString().indexOf("worldnow.com") > -1) {
                thumbs_wrapper.scrollLeft(left);
                WNImageGallery.handleGalleryPrevNextUI(thumbs_wrapper.width(),
                    thumbs_wrapper.scrollLeft(),
                    thumbs_wrapper_list.width(),
                    scroll_forward,
                    scroll_back);
            } else {
                thumbs_wrapper.animate({
                    scrollLeft: left + 'px'
                }, function() {
                    // Handle Prev/Next UI
                    WNImageGallery.handleGalleryPrevNextUI(thumbs_wrapper.width(),
                        thumbs_wrapper.scrollLeft(),
                        thumbs_wrapper_list.width(),
                        scroll_forward,
                        scroll_back);
                });
            }
            // Adding params for reporting consistency
            var objImage = that.images[imgIndex];
            if (typeof objImage !== 'undefined') {
                objImage['slideshowHeadline'] = that.fV.slideshowHeadline;
                objImage['widgetId'] = that.fV.widgetId;
                objImage['ownerAffiliateName'] = 'NA';
                evntMgr.Notify({
                    evnt: eventName,
                    idKey: that.fV.idKey,
                    objClip: objImage,
                    system: "slideshow"
                });
            }
            return false;
        }
    )
    WNImageGallery.handleGalleryPrevNextUI(thumbs_wrapper.width(),
        thumbs_wrapper.scrollLeft(),
        thumbs_wrapper_list.width(),
        scroll_forward,
        scroll_back);

    if (this.isPrimaryGallery) {
        evntMgr.Notify({
            evnt: "primaryImageGalleryLoad",
            idKey: this.fV.idKey,
            objClip: {
                urlImageIndex: imgIndex
            }
        });
    }
};

WNImageGallery.prototype.showImage = function(index, isClicked) {
    var img = this.images[index];
    if (img == undefined) return;

    var fV = this.fV;
    var adTag = (img.adTag != undefined && img.adTag != "") ? img.adTag : fV.adTag;
    var widgetId = (this.fV.urlWidgetId != undefined) ? this.fV.urlWidgetId : fV.widgetId;
    var clip = {
        // copy image properties
        id: img.id,
        url: unescape(img.url),
        headline: unescape(img.headline),
        mainHeadline: unescape(img.headline),
        description: unescape(img.description),
        summary: unescape(img.description),
        publishingDate: img.publishingDate,
        credits: unescape(img.credits),
        href: unescape(img.href),
        adTag: unescape(adTag),
        // widget properties
        widgetId: widgetId,
        groupId: fV.idKey,
        slideshowHeadline: fV.slideshowHeadline,
        index: index,
        total_number: this.images.length,
        imageNumber: '', //(index+1) + " of " + this.images.length,
        adCallDomain: fV.hostDomain,
        ownerAffiliateName: wnSiteConfigGeneral.affiliateName,
        disableAd: true, // set to "false" by NewSlideshowImage JS method when needed
        isClicked: isClicked
    };

    if (this.notClickInitiatedLoad) {
        this.notClickInitiatedLoad = false;
        clip.disableAd = false; // always make an ad request when first image in the gallery is shown
        clip.onGalleryLoad = true; // indicate that image wasn't choosen by user, but pops on gallery load
    }

    //  Handle and Next button for slideshow
    //this.handlePrevNextUI(index, "WNImageCanvas" + this.fV.idKey + "divWNImageCanvas" + widgetId);
    wnSlideshowImage.NewSlideshowImage(clip);
};

/*WNImageGallery.prototype.handlePrevNextUI  = function(currentImgindex, canvasId)
{
    $wn('#' + canvasId).find (".wn-ad-prev, .wn-ad-next").show();

    //  hide previous button if its 1st slide
    if(currentImgindex <=0 ) {
        $wn('#' + canvasId +" .wn-ad-prev").hide();
    }

    // hide next if displaying last slide
    if(currentImgindex >=  (this.images.length - 1)){
        $wn('#' + canvasId +" .wn-ad-next").hide();
    }
};*/

WNImageGallery.handleGalleryPrevNextUI = function(thumbs_wrapper_width, thumbs_wrapper_scrollLeft, thumbs_wrapper_list_width, scroll_forward, scroll_back) {
    // show both  scroll_forward, scroll_back
    $wn(scroll_back).show();
    $wn(scroll_forward).show();
    if (thumbs_wrapper_scrollLeft == 0) {
        // horizontal scroll positon is 0 its time to hide the scroll_back
        $wn(scroll_back).hide();
    }
    if (thumbs_wrapper_width + thumbs_wrapper_scrollLeft >= thumbs_wrapper_list_width) {
        // Completely scorrlled left hide scroll_forward
        $wn(scroll_forward).hide();
    }
};

// Called as respons on imageCanvas loaded event
WNImageGallery.prototype.onImageCanvasLoad = function(p_evtObj) {
    if (this.isPrimaryGallery) {
        this.notClickInitiatedLoad = true;
        //var urlImageId = parseInt(this.fV.imageId) - 1;
        var urlImageId = parseInt(this.fV.imageId);
        var imgIndex = (urlImageId > 0) ? urlImageId : 0;
        this.showImage(imgIndex, false);
        this.highlightThumb(imgIndex);
    }
};

WNImageGallery.prototype.onNewImage = function(p_evtObj) {
    var widgetId = (parseInt(this.fV.urlWidgetId) > 0) ? this.fV.urlWidgetId : this.fV.widgetId;
    if (widgetId != p_evtObj.objClip.widgetId) {
        // deactivate current active image
        var galleryDiv = $wn('#' + this.fV.domId);
        galleryDiv.find('.wn-ad-active').removeClass('wn-ad-active');
    }
};

WNImageGallery.prototype.onNextImageRequest = function(p_evtObj) {
    var widgetId = (parseInt(this.fV.urlWidgetId) > 0) ? this.fV.urlWidgetId : this.fV.widgetId;
    if (p_evtObj.objClip.widgetId == widgetId ||
        this.isPrimaryGallery && p_evtObj.objClip.index == -1) {
        var img = p_evtObj.objClip;
        /*
        // Go to the first image, if "next" clicked on the last image
        if (img.index > (this.images.length - 2) || img.index < 0) {
        this.showImage(0, false);
        this.highlightThumb(0);
        }
        */

        // Stop slideshow on last image
        if (img.index >= (this.images.length - 1)) {
            evntMgr.Notify({
                evnt: "LastImageReached",
                idKey: this.fV.idKey,
                objClip: {}
            });
            // show first image
        } else if (img.index < 0) {
            this.showImage(0, false);
            this.highlightThumb(0);
            // show image
        } else {
            this.showImage(img.index + 1, false);
            this.highlightThumb(img.index + 1);

        }
    }
};

WNImageGallery.prototype.onPrevImageRequest = function(p_evtObj) {
    var widgetId = (parseInt(this.fV.urlWidgetId) > 0) ? this.fV.urlWidgetId : this.fV.widgetId;
    if (p_evtObj.objClip.widgetId == widgetId ||
        this.isPrimaryGallery && p_evtObj.objClip.index == -1) {
        var img = p_evtObj.objClip;
        /*
        // Show last image if "prev" clicked on the first image
        if (img.index < 1) {
        this.showImage(this.images.length - 1, false);
        this.highlightThumb(this.images.length - 1);
        }
        */
        // Don't show last image if "prev" clicked on the first image
        if (img.index < 1) {
            /**/
        } else {
            this.showImage(img.index - 1, false);
            this.highlightThumb(img.index - 1);
        }
    }
};




/* Class: WNHeadline
- HTML version of the Flash headline widget
*/
WNHeadline = function(parentDivId, flashVars) {
    this.fV = flashVars;
    this.widgetClassType = "WNHeadline";
    this.parentDivId = parentDivId;
    this.parentJSName = this.widgetClassType + this.fV.widgetId;
    this.groupId = this.fV.idKey;
    this.defaultTitleText = "";
    if (((this.fV.defaultTitleText == undefined) || ((this.fV.defaultTitleText.length - 1) < 0)) &&
        (this.fV.slideshowHeadline == "undefined" || this.fV.slideshowHeadline == undefined || this.fV.slideshowHeadline == "")
    ) {
        this.defaultTitleText = "Video Player";
    } else if (this.fV.slideshowHeadline != "undefined" && this.fV.slideshowHeadline != undefined && this.fV.slideshowHeadline != "") {
        this.defaultTitleText = this.fV.slideshowHeadline;
    } else {
        this.defaultTitleText = this.fV.defaultTitleText;
    }

    // Respond to screen width
    if (wn_isMobile) {
        var div = $wn("#" + this.parentDivId);
        this.fV.widgetsContainer = div.parent();
        var leftOffset = parseInt(div.position().left);
        var oldWidth = parseInt(this.fV.playerWidth) + leftOffset;
        var newWidth = div.parent().parent().width() - leftOffset;
        //var newWidth = wnVideoUtils.getResponsiveWidth(this.fV.playerWidth);
        if (newWidth < oldWidth) this.fV.playerWidth = newWidth;
        // resize widgets group container
        containerWidth = div.parent().parent().width();
        oldWidth = div.parent().width();
        if (containerWidth < oldWidth) div.parent().width(containerWidth);
    }

    this.render();

    //register for events
    $wn("#" + this.fV.domId).data("widget", this);
    evntMgr.Register({
        evnt: "NewMedia",
        func: "setHeadline",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "NewClip",
        func: "setHeadline",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "ClearHeadlineEvent",
        func: "setHeadline",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "NoMedia",
        func: "setHeadline",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "ClipEnded",
        func: "setHeadline",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "NewClip",
        func: "setHeadline",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "LoadingMedia",
        func: "showMessage",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });

    /*onWidgetLoad(this.fV.domId, this.fV.idKey, "imageCanvasIsReady", "", {});*/
    //imageCanvasIsReady(this.parentDivId, this.fV.idKey);
    /*evntMgr.Notify({ evnt: "imageCanvasLoad", idKey: this.fV.idKey, objClip: {} });*/
}

WNHeadline.prototype.render = function() {
    var output = "";
    output += "<div id=" + this.fV.domId + " class='wn-widget-headline'>";
    output += "   <div class='wn-widget-headline-title'></div>";
    output += "   <div class='wn-widget-headline-time'></div>";
    output += "</div>";

    /* Set styles and dimentions */
    output += "<style type='text/css'>";
    output += "#" + this.fV.domId + " {width: " + (this.fV.playerWidth - 2) + "px; height: " + (this.fV.playerHeight - 2) + "px}";
    output += "#" + this.fV.domId + " div {height: " + (this.fV.playerHeight - 5) + "px; line-height: " + (this.fV.playerHeight - 5) + "px; font-size: " + this.fV.fontSize + "px;}";
    output += "#" + this.fV.domId + " .wn-widget-headline-title {width: " + (this.fV.playerWidth - 14) + "px}";
    output += "#" + this.fV.domId + " .wn-widget-headline-time {left: " + (this.fV.playerWidth - 51) + "px; top: 1px;}";
    output += "</style>";

    $wn("#" + this.parentDivId).empty();
    $wn("#" + this.parentDivId).append(output);
}

WNHeadline.prototype.setHeadline = function(p_evtObj) {
    var headlineDiv = $wn('#' + this.fV.domId);
    var titleDiv = headlineDiv.find('.wn-widget-headline-title');
    var timeDiv = headlineDiv.find('.wn-widget-headline-time');
    var clip = p_evtObj.objClip;

    var headline;
    if (wnVideoUtils.validateString(clip.headline)) {
        headline = clip.headline;
    } else if (wnVideoUtils.validateString(clip.mainHeadline)) {
        headline = clip.mainHeadline;
    } else {
        headline = "";
    }

    /* DE13423 - on the slideshow landing page title of the shared slideshow must be shown
       DE14248 - on original slideshow title specified in the headline widget must be shown */
    if (wnVideoUtils.validateString(clip.slideshowHeadline)) {
        // shared slideshow
        if (wnVideoUtils.validateString(wnVideoUtils.getQS("widgetId"))) {
            headline = clip.slideshowHeadline;
            // original slideshow
        } else {
            headline = this.defaultTitleText;
        }
    }

    var duration = parseInt(clip.duration);
    var durationString = isNaN(duration) ? "" : wnVideoWidgets.GetTimeString(duration);
    if (clip.preferredFormat == "WMV" || clip.isThirdPartyAd) {
        durationString = "";
    }
    if (wnVideoUtils.validateString(clip.imageNumber)) durationString = clip.imageNumber;
    if (this.fV.hasImageCount != "true" && this.fV.hasImageCount != true) durationString = "";

    titleDiv.empty();
    timeDiv.empty();
    titleDiv.append(headline);
    timeDiv.append(durationString);
}

WNHeadline.prototype.showMessage = function(p_evtObj) {
    var clip = evntObj.parameters.objClip;
    if (wnVideoUtils.validateString(clip.headline) || wnVideoUtils.validateString(clip.mainHeadline)) {
        this.setHeadline(evntObj);
    } else {
        var message = evntObj.message;
        timeDiv.empty();
        titleDiv.empty();
        if (wnVideoUtils.validateString(message)) titleDiv.append(headline);
    }
}

/* Class: WNInfoPane
- HTML version of the Flash InfoPane widget
*/
WNInfoPane = function(parentDivId, flashVars) {
    this.fV = flashVars;
    this.widgetClassType = "WNInfoPane";
    this.parentDivId = parentDivId;
    this.parentJSName = this.widgetClassType + this.fV.widgetId;
    this.groupId = this.fV.idKey;
    this.failSafeSummaryImageText = "";
    if (this.fV.failSafeSummaryImageText == undefined || (this.fV.failSafeSummaryImageText.length - 1) < 0) {
        this.failSafeSummaryImageText = "VIDEO";
    } else {
        this.failSafeSummaryImageText = this.fV.failSafeSummaryImageText;
    }
    this.hasImage = (this.fV.hasImage).toLowerCase() == "false" ? false : true;
    this.hasHeadline = (this.fV.hasHeadline).toLowerCase() == "false" ? false : true;
    this.hasSummary = (this.fV.hasSummary).toLowerCase() == "false" ? false : true;
    this.hasDuration = (this.fV.hasDuration).toLowerCase() == "false" ? false : true;
    this.hasCredit = (this.fV.hasPhotoCredit).toLowerCase() == "false" ? false : true;
    this.hasDateStamp = (this.fV.hasDateStamp).toLowerCase() == "false" ? false : true;
    //if (!this.hasSummary && !this.hasDateStamp && !this.hasCredit) this.hasSummary = false; else this.hasSummary = true;

    // Respond to screen width
    if (wn_isMobile) {
        var div = $wn("#" + this.parentDivId);
        this.fV.widgetsContainer = div.parent();
        var leftOffset = parseInt(div.position().left);
        var oldWidth = parseInt(this.fV.playerWidth) + leftOffset;
        var newWidth = div.parent().parent().width() - leftOffset;
        if (newWidth < oldWidth) this.fV.playerWidth = newWidth;
        /* Reposition according to the new video canvas size */
        var canvas = getWigdetByType(this.groupId, "c");
        if (canvas != {} &&
            canvas.playerHeight < canvas.originalHeight &&
            // are in the same container
            canvas.widgetsContainer.attr("id") == div.parent().attr("id") &&
            // position relative to the canvas
            div.position().top >= (canvas.top + canvas.originalHeight - 10) &&
            div.position().left < (canvas.left + canvas.originalWidth - 10)) {
            var diff = canvas.originalHeight - canvas.playerHeight;
            div.css("top", Math.floor(div.position().top - diff));
            // resize widgets group container
            /*if (canvas.originalContainerHeight == div.parent().height()) {
                div.parent().height(div.parent().height() - diff);
            }*/
        }
        // resize widgets group container
        containerWidth = div.parent().parent().width();
        oldWidth = div.parent().width();
        if (containerWidth < oldWidth) div.parent().width(containerWidth);
    }

    this.render();

    //register for events
    $wn("#" + this.fV.domId).data("widget", this);
    evntMgr.Register({
        evnt: "NewMedia",
        func: "showInfo",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });
    evntMgr.Register({
        evnt: "NewClip",
        func: "showInfo",
        idKey: this.groupId,
        lstnr: this.fV.domId
    });

    /*onWidgetLoad(this.fV.domId, this.fV.idKey, "imageCanvasIsReady", "", {});*/
    //imageCanvasIsReady(this.parentDivId, this.fV.idKey);
    /*evntMgr.Notify({ evnt: "imageCanvasLoad", idKey: this.fV.idKey, objClip: {} });*/
}

WNInfoPane.prototype.render = function() {
    var output = "";
    output += "<div id=" + this.fV.domId + " class='wn-widget-infopane'>";
    output += "   <div class='wn-widget-infopane-image'></div>";
    output += "   <div class='wn-widget-infopane-textinfo'>";
    output += "      <div class='wn-widget-infopane-title'></div>";
    output += "      <div class='wn-widget-infopane-summary'></div>";
    output += "      <div class='wn-widget-infopane-time'></div>";
    output += "   </div>";
    output += "</div>";

    var padding = 7;
    var maxImageHeight = parseInt(this.fV.playerHeight) - padding * 2;
    var maxImageWidth = parseInt(this.fV.playerWidth);
    var imageHeight = (parseInt(this.fV.imageHeight) <= maxImageHeight) ? parseInt(this.fV.imageHeight) : maxImageHeight;
    var imageWidth = (parseInt(this.fV.imageWidth) <= maxImageWidth) ? parseInt(this.fV.imageWidth) : maxImageWidth;
    var imageContainerWidth = imageWidth + padding;
    if (!this.hasImage) imageContainerWidth = 0;
    var titleHeight = parseInt(this.fV.headlineFontSize) + 6;
    if (!this.hasHeadline) titleHeight = 0;
    var timeHeight = parseInt(this.fV.durationFontSize) + 6;
    if (!this.hasDuration) timeHeight = 0;
    var summaryHeight = parseInt(this.fV.playerHeight) - titleHeight - timeHeight - padding * 2;

    /* Set styles and dimentions */
    output += "<style type='text/css'>";
    output += "#" + this.fV.domId + " {width: " + (this.fV.playerWidth - 2) + "px; height: " + (this.fV.playerHeight - 2) + "px}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-image {top: " + padding + "px; left:" + padding + "px}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-image, ";
    output += "#" + this.fV.domId + " .wn-widget-infopane-image img {width: " + imageWidth + "px; height:" + imageHeight + "px}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-image {line-height: " + imageHeight + "px; font-size: " + Math.round(imageHeight / 4) + "px;}";
    if (!this.hasImage) output += "#" + this.fV.domId + " .wn-widget-infopane-image {display: none;}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-textinfo {left: " + (imageContainerWidth + padding) + "px; top: " + padding + "px;}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-title, ";
    output += "#" + this.fV.domId + " .wn-widget-infopane-summary {width: " + (this.fV.playerWidth - imageContainerWidth - 2 * padding) + "px;}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-title {height: " + titleHeight + "px; font-size: " + this.fV.headlineFontSize + "px;}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-summary {height: " + summaryHeight + "px; font-size: " + this.fV.summaryFontSize + "px;}";
    output += "#" + this.fV.domId + " .wn-widget-infopane-time {height: " + timeHeight + "px; font-size: " + this.fV.durationFontSize + "px;}";
    if (!this.hasHeadline) output += "#" + this.fV.domId + " .wn-widget-infopane-title {display: none;}";
    if (!this.hasDuration) output += "#" + this.fV.domId + " .wn-widget-infopane-time {display: none;}";
    //if (!this.hasSummary) output += "#" + this.fV.domId + " .wn-widget-infopane-summary {visibility: hidden;}";
    output += "</style>";

    $wn("#" + this.parentDivId).empty();
    $wn("#" + this.parentDivId).append(output);
}

WNInfoPane.prototype.showInfo = function(p_evtObj) {
    var infoPaneDiv = $wn('#' + this.fV.domId);
    var imageDiv = infoPaneDiv.find('.wn-widget-infopane-image');
    var titleDiv = infoPaneDiv.find('.wn-widget-infopane-title');
    var summaryDiv = infoPaneDiv.find('.wn-widget-infopane-summary');
    var timeDiv = infoPaneDiv.find('.wn-widget-infopane-time');
    var clip = p_evtObj.objClip;

    var headline = wnVideoUtils.validateString(clip.headline) ? clip.headline : clip.mainHeadline;
    var _summary = "";
    var summary = wnVideoUtils.validateString(clip.summary) ? clip.summary : "";
    var publishingDate = wnVideoUtils.validateString(clip.publishingDate) ? clip.publishingDate : "";
    var credits = wnVideoUtils.validateString(clip.credits) ? clip.credits : "";

    if (this.hasDateStamp) _summary += publishingDate;
    if (this.hasDateStamp && this.hasSummary) _summary += "<br/>";
    if (this.hasSummary) _summary += summary;
    if ((this.hasSummary || this.hasDateStamp) && this.hasCredit) _summary += "<br/>";
    if (this.hasCredit) _summary += credits;
    summary = _summary;

    var duration = parseInt(clip.duration);
    var durationString = isNaN(duration) ? "" : wnVideoWidgets.GetTimeString(duration);
    // for wmv streaming media there is no duration possible so display nothing on the UI
    if (clip.preferredFormat == "WMV" || clip.isThirdPartyAd) {
        durationString = "";
    }

    if (this.hasImage) {
        var imageUrl = "";
        if (typeof(clip.thumb) == 'string') {
            imageUrl = clip.thumb;
        } else if (typeof(clip.graphic) == 'string') {
            imageUrl = clip.graphic;
        } else {
            // TODO: add default image
        }

        imageDiv.empty();
        if (imageUrl != "") {
            var img = $wn(new Image()).attr('src', imageUrl);
            imageDiv.append(img);
        } else {
            imageDiv.append("<div>Video</div>");
        }
    }

    titleDiv.empty();
    summaryDiv.empty();
    timeDiv.empty();
    titleDiv.append(headline);
    summaryDiv.append(summary);
    timeDiv.append(durationString);
}



function wnEmbedPlayer(qs) {
    if (qs != "") {
        var params = wnVideoUtils.parseQuery(qs);
        var WN_rnd = parseInt(params['rnd']);
        if (isNaN(WN_rnd) || WN_rnd < 1) WN_rnd = 1;
        var WN_hostdomain = params['hostDomain'];
        var WN_playerwidth = parseInt(params['playerWidth']);
        var WN_playerheight = parseInt(params['playerHeight']);
        // the headline widget will use the height set in the global.inc (WNVideo.asp)
        var WN_isShowIcon = wnVideoUtils.stringToBoolean(params['isShowIcon']);
        if (params['clipId'] != undefined && ['clipId'] != "")
            var WN_clipId = parseInt(params['clipId']);
        if (params['flvUri'] != undefined && params['flvUri'] != "")
            var WN_flvUri = params['flvUri'];
        if (params['partnerclipid'] != undefined && params['partnerclipid'] != "")
            var WN_partnerclipid = parseInt(params['partnerclipid']);
        if (params['adTag'] != undefined && params['adTag'] != "")
            var WN_adTag = decodeURIComponent(decodeURIComponent(params['adTag']));
        if (params['advertisingZone'] != undefined && params['advertisingZone'] != "")
            var WN_advertisingZone = decodeURIComponent(decodeURIComponent(params['advertisingZone']));
        if (params['enableAds'] != undefined && params['enableAds'] != "")
            var WN_enableAds = wnVideoUtils.stringToBoolean(params['enableAds']);
        if (params['landingPage'] != undefined && params['landingPage'] != "")
            var WN_landingPage = decodeURIComponent(decodeURIComponent(params['landingPage']));
        if (params['islandingPageoverride'] != undefined && params['islandingPageoverride'] != "")
            var WN_islandingPageoverride = wnVideoUtils.stringToBoolean(params['islandingPageoverride']);
        if (params['playerType'] != undefined && params['playerType'] != "")
            var WN_playerType = params['playerType'];
        var WN_canvasVerNum = (params['v'] == "2") ? 2 : "";
        if (params['controlsType'] != undefined && params['controlsType'] != "")
            var WN_controlsType = (params['controlsType'] == "overlay") ? "overlay" : "fixed";
        if (params['isLiveStream'] != undefined && params['isLiveStream'] != "")
            var WN_isLiveStream = wnVideoUtils.stringToBoolean(params["isLiveStream"]);
        if (params['streamType'] != undefined && params['streamType'] != "")
            var WN_streamType = (params['streamType'] == "live") ? "live" : "ondemand";
        if (params['headline'] != undefined && params['headline'] != "")
            var WN_headline = params['headline'];

        var v_divWNContainer = "divWNWidgetsContainer" + WN_rnd;
        var v_divWNHeadline = "divWNHeadline" + WN_rnd;
        var v_divWNVideoCanvas = "divWNVideoCanvas" + WN_rnd;

        //document.write('<div id="' + v_divWNContainer + '"></div>');
        if (typeof wnScriptsOnPage === 'undefined') {
            var wnScriptsOnPage = document.getElementsByTagName('script');
        }
        var wnVideoEmbedScriptNode = "";
        for (var i = 0, l = wnScriptsOnPage.length; i < l; i++) {
            if (wnScriptsOnPage[i].src.toLowerCase().indexOf("/wnvideo.js?") > -1 &&
                wnScriptsOnPage[i].src.indexOf(qs.substr(1)) > -1 &&
                document.getElementById(v_divWNContainer) == null) {
                wnVideoEmbedScriptNode = wnScriptsOnPage[i];
                break;
            }
        }
        $wn(wnVideoEmbedScriptNode).after('<div id="' + v_divWNContainer + '"></div>');
        var html = '<style type="text/css">' +
            '#' + v_divWNVideoCanvas + ' {top:25px; left:0px; position:absolute}' +
            '#' + v_divWNHeadline + ' {top:0px; left:0px; position:absolute}' +
            '#' + v_divWNContainer + ' {position:relative;overflow:hidden;height:' + (WN_playerheight + 25) + 'px;width:' + WN_playerwidth + 'px;}' +
            '</style>' +
            '    <div id="' + v_divWNHeadline + '"></div>' +
            '    <div id="' + v_divWNVideoCanvas + '"></div>';
        $wn('#' + v_divWNContainer).append(html);

        var videoHeadline = new WNVideoWidget("WNHeadline", v_divWNHeadline, WN_rnd);
        //var videoHeadline = new WNVideoWidget("WNHeadline", v_divWNHeadline);
        videoHeadline.SetWidth(WN_playerwidth);
        videoHeadline.RenderWidget();

        var videoCanvas = new WNVideoWidget("WNVideoCanvas" + WN_canvasVerNum, v_divWNVideoCanvas, WN_rnd);
        //var videoCanvas = new WNVideoWidget("WNVideoCanvas" + WN_canvasVerNum, v_divWNVideoCanvas);
        videoCanvas.SetWidth(WN_playerwidth);
        videoCanvas.SetHeight(WN_playerheight);
        videoCanvas.SetVariable("hostDomain", WN_hostdomain);
        videoCanvas.SetVariable("isShowIcon", WN_isShowIcon);
        if (WN_clipId != undefined && !isNaN(WN_clipId)) {
            videoCanvas.SetVariable("clipId", WN_clipId);
        } else if (WN_flvUri != undefined && !WN_isLiveStream) {
            videoCanvas.SetVariable("flvUri", WN_flvUri);
        } else if (WN_partnerclipid != undefined && !isNaN(WN_partnerclipid)) {
            videoCanvas.SetVariable("partnerclipid", WN_partnerclipid);
        }
        if (WN_adTag != undefined) videoCanvas.SetVariable("adTag", WN_adTag);
        if (WN_advertisingZone != undefined) videoCanvas.SetVariable("advertisingZone", WN_advertisingZone);
        if (WN_enableAds != undefined) videoCanvas.SetVariable("enableAds", WN_enableAds);
        if (WN_landingPage != undefined) videoCanvas.SetVariable("landingPage", WN_landingPage);
        if (WN_islandingPageoverride != undefined) videoCanvas.SetVariable("islandingPageoverride", WN_islandingPageoverride);
        if (WN_playerType != undefined) videoCanvas.SetVariable("playerType", WN_playerType);
        if (WN_canvasVerNum != undefined) videoCanvas.SetVariable("v", WN_canvasVerNum);
        if (WN_controlsType != undefined) videoCanvas.SetVariable("controlsType", WN_controlsType);
        videoCanvas.SetVariable("transportShareButtons", "cc");
        if (WN_isLiveStream && (WN_clipId == undefined || isNaN(WN_clipId))) {
            var flashLiveStreamObj = {
                strUrl: WN_flvUri,
                streamType: WN_streamType,
                strHeadline: WN_headline,
                strAdTag: WN_adTag,
                hasPreroll: WN_enableAds,
                mobileStreams: []
            }
            if (typeof params['wnms1'] !== "undefined" && params['wnms1'] != "" ||
                typeof params['wnms2'] !== "undefined" && params['wnms2'] != "" ||
                typeof params['wnms3'] !== "undefined" && params['wnms3'] != "") {
                if (typeof params['wnms1'] !== "undefined" && params['wnms1'] != "") flashLiveStreamObj.mobileStreams.push({
                    url: params['wnms1'],
                    type: "video/mp4"
                });
                if (typeof params['wnms2'] !== "undefined" && params['wnms2'] != "") flashLiveStreamObj.mobileStreams.push({
                    url: params['wnms2'],
                    type: "video/mp4"
                });
                if (typeof params['wnms3'] !== "undefined" && params['wnms3'] != "") flashLiveStreamObj.mobileStreams.push({
                    url: params['wnms3'],
                    type: "video/mp4"
                });
            }
            videoCanvas.SetFlashLiveStream(flashLiveStreamObj);
        }
        videoCanvas.RenderWidget();
    }
}


var Namespace_VideoReporting_Worldnow = function() {

    //can store the includes in a string in the global.incs
    // application("Video_reportingIncludes") = "<script src="">"
    var functionsArray = new Array;
    var arrIndex = 0;

    var functionsSlideShowArray = new Array;
    var slArrIndex = 0;
    var isPaused = false;

    return {

        getVideoCategory: function() {
            var category = 'Video Embedded [Externally]';
            if (wnPageType == 'videostorypage')
                category = 'Video In Story';
            if (wnPageType == 'videolandingpage')
                category = 'Video Landing Page';
            if (wnPageType == 'slideshowlandingpage')
                category = 'Video In Slideshow Landing Page';
            return category;
        },
        getSlideshowCategory: function() {
            var category = 'Slideshow Embedded [Externally]';
            if (wnPageType == 'videostorypage') {
                category = 'Slideshow In Story';
            }
            if (wnPageType == 'videolandingpage') {
                category = 'Slideshow In Video Landing Page';
            }
            if (wnPageType == 'slideshowlandingpage') {
                category = 'Slideshow Landing Page';
            }
            return category;
        },
        getVideoValue: function(p_len, p_pos, p_eventType) {
            var value = p_len;
            try {
                value = Math.floor(parseFloat(p_len));
            } catch (e) {}

            var currentTime = p_pos;
            try {
                currentTime = Math.floor(parseFloat(currentTime));
            } catch (e) {}

            if (p_eventType.toLowerCase() == 'play' || p_eventType.toLowerCase() == 'pause' || p_eventType.toLowerCase() == 'volumechange' || p_eventType.toLowerCase() == 'mute' || p_eventType.toLowerCase() == 'unmute') {
                value = currentTime;
            }
            return value;
        },

        getVideoEventAction: function(p_eventType) {
            if (typeof p_eventType !== 'string') return p_eventType;

            // Prettify event names
            var eventAction = p_eventType;

            if (p_eventType.toLowerCase() == 'newclip') {
                eventAction = 'Render';
                isPaused = false;
            }
            if (p_eventType.toLowerCase() == 'start' || p_eventType.toLowerCase() == 'duration') {
                eventAction = 'Play';
            }
            if (p_eventType.toLowerCase() == 'play') {
                if (isPaused) {
                    eventAction = 'Resume';
                } else {
                    eventAction = '';
                }
            }
            if (p_eventType.toLowerCase() == 'pause') {
                eventAction = 'Pause';
                isPaused = true;
            }
            if (p_eventType.toLowerCase() == 'ended') {
                eventAction = 'Ended';
            }
            if (p_eventType.toLowerCase() == 'volumechange') {
                eventAction = 'Volume Change';
            }
            if (p_eventType.toLowerCase() == 'mute') {
                eventAction = 'Mute';
            }
            if (p_eventType.toLowerCase() == 'unmute') {
                eventAction = 'Unmute';
            }
            if (p_eventType.toLowerCase() == 'email') {
                eventAction = 'Email';
            }
            if (p_eventType.toLowerCase() == 'fullscreen') {
                eventAction = 'Full Screen';
            }
            return eventAction;
        },

        getSlideshowEventAction: function(p_eventType) {
            if (typeof p_eventType !== 'string') return p_eventType;

            // Prettify event names
            var eventAction = p_eventType;
            if (p_eventType.toLowerCase() == 'start') {
                eventAction = 'Start';
            }
            if (p_eventType.toLowerCase() == 'stop') {
                eventAction = 'Pause';
            }
            if (p_eventType.toLowerCase() == 'next') {
                eventAction = 'Next';
            }
            if (p_eventType.toLowerCase() == 'gallerynext') {
                eventAction = 'ClickGalleryNext';
            }
            if (p_eventType.toLowerCase() == 'previous') {
                eventAction = 'Back';
            }
            if (p_eventType.toLowerCase() == 'galleryprevious') {
                eventAction = 'ClickGalleryBack';
            }
            if (p_eventType.toLowerCase() == 'imageview') {
                eventAction = 'ImageView';
            }

            // Block certain events to GA for slidheshows
            if (p_eventType.toLowerCase() == 'newclip') {
                return;
            }
            return eventAction;
        },

        getVideoLabel: function(p_clipAdTag, p_title, p_clipId) {
            var label = decodeURIComponent(p_clipAdTag + ' - ' + p_title + ' - ' + p_clipId);
            return label;
        },
        getSlideshowLabel: function(p_clipAdTag, p_widgetHeadline, p_clipId) {
            var label = decodeURIComponent(p_clipAdTag + ' - ' + p_widgetHeadline + ' - ' + p_clipId);
            return label;
        },

        logVideoEventParameters: function(p_strEventType, p_strTitle, p_strCurPos, p_strClipId, p_strClipAdTag, p_strHostPage, p_strAffiliateName, p_strOwnerAffiliateNo, p_isAd, p_ciid, p_strReferer, p_baseUrl, p_reportingKeywords, p_uri, p_len, p_pctViewed, p_location, p_contentSource, p_keywords, p_playerType, p_isExpressReport, p_PlayerName, p_PlayerId, p_SiteId, p_taxonomy1, p_taxonomy2, p_taxonomy3) {
            //only log if there is a valid clipid
            if (p_strClipId != 'undefined') {
                if (p_strTitle == '') {
                    p_strTitle = 'N/A'
                }

                // Quick hack to determine if this is a live stream event
                var stream_type = 'vod';
                var stream_type_number = 0;
                if (typeof wnVideoWidgets.canvasObjectFlashVars === 'string') {
                    var fv = wnVideoWidgets.canvasObjectFlashVars.toLowerCase();
                    if (fv.indexOf('&streamtype=live') >= 0) {
                        stream_type = 'live';
                        stream_type_number = 1;
                    }
                }

                // Normalize time into seconds
                // Flash duration is in milliseconds
                try {
                    //p_len = parseFloat(p_len/1000);
                    p_len = Math.floor(p_len);
                } catch (e) {}

                try {
                    p_strCurPos = Math.floor(parseFloat(p_strCurPos));
                } catch (e) {}

                // temp fix for IE8 jquery conflict
                var currDate = new Date();
                currDate = currDate.toUTCString();
                try {
                    currDate = $wn.format.date(new Date(), "yyyy-MM-dd");
                } catch (e) {}

                var category = this.getVideoCategory(); // NOTE: Move this to XML
                var eventAction = this.getVideoEventAction(p_strEventType);
                if (eventAction != "") {
                    var eventMeta = {
                        affiliate_name: p_strAffiliateName,
                        affiliate_ownerno: p_strOwnerAffiliateNo,
                        contentprovider: p_strOwnerAffiliateNo,
                        baseurl: p_baseUrl,
                        clip_title: escape(p_strTitle),
                        clip_createddate: currDate,
                        clip_length: p_len,
                        clip_position: p_strCurPos,
                        clip_id: p_strClipId,
                        clip_adtag: escape(p_strClipAdTag),
                        clip_uri: p_uri,
                        clip_pctviewed: p_pctViewed,
                        clip_pausecount: 0, //NOTE: needs value
                        clip_isad: p_isAd,
                        clip_bufferingtime: 0, //NOTE: needs value
                        clip_source: escape(p_contentSource),
                        clip_keywords: escape(p_keywords),
                        event_action: eventAction,
                        hostpage: escape(p_strHostPage),
                        heartbeat_counter: 1, //NOTE: needs value
                        reporting_keywords: p_reportingKeywords,
                        page_url: document.location.href,
                        player_type: escape(p_playerType),
                        player_name: escape(p_strHostPage),
                        player_id: p_PlayerId,
                        player_version: '5.2',
                        site_id: p_SiteId,
                        site_useexpressreports: p_isExpressReport,
                        streamtype: stream_type,
                        streamtypenumber: stream_type_number,
                        timestamp: new Date().getTime(),
                        user_bandwidth: 0, //NOTE: needs value
                        user_interactionflag: '', //NOTE: needs value
                        windowstate: 'norm', //    "Windowstate ("full" / "norm" / "min" / "max" / custom value)"
                        category: category
                    };
                    if (wnVideoUtils.validateString(p_taxonomy1)) eventMeta.taxonomy1 = p_taxonomy1;
                    if (wnVideoUtils.validateString(p_taxonomy2)) eventMeta.taxonomy2 = p_taxonomy2;
                    if (wnVideoUtils.validateString(p_taxonomy3)) eventMeta.taxonomy3 = p_taxonomy3;

                    // Temp patch to not let any ad events through to URF
                    if (!p_isAd) {
                        Worldnow.EventMan.event('video_' + p_strEventType.toLowerCase(), eventMeta);
                    }
                }
            }

            // Legacy web trends code - remove once web trends is no longer a supported system
            if (p_strTitle != "" && p_strClipId != "undefined") {
                var strFunctionCall;
                var strInputParams;

                //escape
                p_strTitle = escape(p_strTitle);
                p_strClipAdTag = escape(p_strClipAdTag);
                p_strHostPage = escape(p_strHostPage);
                p_contentSource = escape(p_contentSource);
                p_keywords = escape(p_keywords);
                p_playerType = escape(p_playerType);

                // Normalize time into seconds
                // Flash duraion is in milliseconds
                if (p_strEventType.toLowerCase() == 'duration') {
                    p_len = parseFloat(p_len / 1000);
                }

                //construct input params string
                strInputParams = "('" + p_strEventType + "',";
                strInputParams += "'" + p_strTitle + "',";
                strInputParams += "'" + p_strCurPos + "',";
                strInputParams += "'" + p_strClipId + "',";
                strInputParams += "'" + p_strClipAdTag + "',";
                strInputParams += "'" + p_strHostPage + "',";
                strInputParams += "'" + p_strAffiliateName + "',";
                strInputParams += "'" + p_strOwnerAffiliateNo + "',";
                strInputParams += "'" + p_isAd + "',";
                strInputParams += "'" + p_ciid + "',";
                strInputParams += "'" + p_strReferer + "',";
                strInputParams += "'" + p_baseUrl + "',";
                strInputParams += "'" + p_reportingKeywords + "',";
                strInputParams += "'" + p_uri + "',";
                strInputParams += "'" + p_len + "',";
                strInputParams += "'" + p_pctViewed + "',";
                strInputParams += "'" + p_location + "',";
                strInputParams += "'" + p_contentSource + "',";
                strInputParams += "'" + p_keywords + "',";
                strInputParams += "'" + p_playerType + "',";
                strInputParams += "'" + p_isExpressReport + "',";
                strInputParams += "'" + p_PlayerName + "',";
                strInputParams += "'" + p_PlayerId + "',";
                strInputParams += "'" + p_SiteId + "')";

                //loop thru all registred functions in functionsArray
                for (var x = 0; x < functionsArray.length; x++) {
                    try {
                        //construct function call
                        strFunctionCall = functionsArray[x] + strInputParams;
                        //make the function call using eval
                        eval(strFunctionCall);
                    } catch (e) {
                        //maybe make a reporting call indicating an error so at least
                        //we can determine if a reporting call is failing
                    }
                }
            }

            // return info on func calls - makes the function more testable
            return {
                'eventName': 'video_' + p_strEventType.toLowerCase(),
                'eventMeta': eventMeta,
                'evalFuncCall': strFunctionCall
            }
        },

        logSlideShowEventParameters: function(p_strEventType, p_strTitle, p_strWidgetHeadline, p_strClipId, p_strImgId, p_strClipAdTag, p_strBaseUrl, p_strAffiliateName, p_strPlayerType, p_imgIndex) {
            if (p_strTitle != "" && p_strClipId != "undefined") {
                var category = this.getSlideshowCategory();
                var eventAction = this.getSlideshowEventAction(p_strEventType);

                var count = 0;
                try {
                    var counter = $wn('.wn-number').html();
                    var countArr = counter.split('/');
                    count = parseFloat(countArr[1]);

                    if (isNaN(count)) count = 0;
                } catch (e) {}

                var index = 0;
                if (typeof p_imgIndex !== 'undefined') {
                    index = parseFloat(p_imgIndex);
                    index = index + 1;
                }

                var eventMeta = {
                    event_action: eventAction,
                    page_url: document.location.href,
                    category: category,
                    slideshow_headline: p_strWidgetHeadline,
                    slideshow_widgetid: p_strClipId,
                    slideshow_widgetheadline: p_strWidgetHeadline,
                    slideshow_adtag: p_strClipAdTag,
                    slideshow_imageid: p_strImgId,
                    slideshow_imageindex: index,
                    slideshow_imagename: p_strTitle,
                    slideshow_count: count
                };

                Worldnow.EventMan.event('slideshow_' + p_strEventType.toLowerCase(), eventMeta);
            }

            // Legacy reporting code - to be removed once web trends is no longer supported
            if (p_strTitle != "" && p_strClipId != "undefined") {
                var strFunctionCall;
                var strInputParams;

                //escape
                p_strTitle = escape(p_strTitle);
                p_strClipAdTag = escape(p_strClipAdTag);
                p_strPlayerType = escape(p_strPlayerType);

                //construct input params string
                strInputParams = "('" + p_strEventType + "',";
                strInputParams += "'" + p_strTitle + "',";
                strInputParams += "'" + p_strWidgetHeadline + "',";
                strInputParams += "'" + p_strClipId + "',";
                strInputParams += "'" + p_strImgId + "',";
                strInputParams += "'" + p_strClipAdTag + "',";
                strInputParams += "'" + p_strBaseUrl + "',";
                strInputParams += "'" + p_strAffiliateName + "',";
                strInputParams += "'" + p_strPlayerType + "',";
                strInputParams += "'" + p_imgIndex + "')";

                //loop thru all registred functions in functionsArray
                for (var x = 0; x < functionsSlideShowArray.length; x++) {
                    try {
                        //construct function call
                        strFunctionCall = functionsSlideShowArray[x] + strInputParams;
                        eval(strFunctionCall);
                    } catch (e) {
                        //error
                    }
                }
            }

            // return info on func calls - makes the function more testable
            return {
                'eventName': 'slideshow_' + p_strEventType.toLowerCase(),
                'eventMeta': eventMeta,
                'evalFuncCall': strFunctionCall
            }
        },

        //returns a time interval string that a video was viewed till
        //"5-10 seconds", "10-5 seconds"...
        getVideoTimeInterval: function(p_pos) {
            var intLowRange, intHighRange, strTimeInterval;
            var intIntervalDenominator = 5; //should be able to override thru global.incs or as a query string param for custom videos

            //calculate which time interval the current position falls into
            p_pos = Math.floor(p_pos / intIntervalDenominator);
            intLowRange = p_pos * intIntervalDenominator;
            intHighRange = intLowRange + intIntervalDenominator;
            strTimeInterval = intLowRange + "-" + intHighRange + " seconds";

            return strTimeInterval;
        },

        //registers the function in the global functionsArray
        registerFunctionName: function(p_strFunctionName) {
            functionsArray[arrIndex] = p_strFunctionName;
            arrIndex = arrIndex + 1;
        },

        //registers the function in the global functionsSlideShowArray
        registerSlideShowFunctionName: function(p_strFunctionName) {
            functionsSlideShowArray[slArrIndex] = p_strFunctionName;
            slArrIndex = slArrIndex + 1;
        }

    };

}();


// Zoomable totalImagesCount// Currently added to support zoomable image analytics
//$(document).ready(function() {
// Story page zoomable images
$wn('.wnFancyBox').bind('click', function() {
    try {
        var eventMeta = {
            event_action: 'images_enlarge',
            story_title: wng_pageInfo.headline,
            container_id: wng_pageInfo.containerId,
            image_title: $wn(this).children('img').attr('src')
        };

        Worldnow.EventMan.event('images_enlarge', eventMeta);
    } catch (e) {
        wnLog('Error on Zoomable Image click');
    }
});
//});


// Sniff around for code to activate - load/sync resolution
if (typeof wnEmbedPickUpList !== 'undefined') {
    for (var i = 0; i < wnEmbedPickUpList.length; i++) {
        wnEmbedPlayer(wnEmbedPickUpList[i]);
    };
}
