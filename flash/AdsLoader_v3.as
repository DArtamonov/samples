package codebase.worldnow.ad
{
	import codebase.worldnow.com.Observer;
	import codebase.worldnow.util.StringUtil;
	import codebase.worldnow.reuse.General;
	import codebase.worldnow.reuse.Globals;
	import mx.core.FlexGlobals;
	
	import com.google.ads.ima.api.AdError;
	import com.google.ads.ima.api.AdErrorCodes;
	import com.google.ads.ima.api.AdErrorEvent;
	import com.google.ads.ima.api.AdsRenderingSettings;
	import com.google.ads.ima.api.AdsManagerLoadedEvent;
	import com.google.ads.ima.api.CustomContentLoadedEvent;
	import com.google.ads.ima.api.AdsLoader;
	import com.google.ads.ima.api.AdsManager;
	import com.google.ads.ima.api.AdsRequest;
	

	public class AdsLoader_v3
	{
		private static var app:Object = FlexGlobals.topLevelApplication;
		
		public function AdsLoader_v3()
		{
			
		}
		
		public static function FetchAdXMLData(url:String, callbackMiscObj:Object,
											  callbackComplete:Function,
											  callbackSecurityError:Function,
											  callbackIoError:Function,
											  callbackInvalidData:Function):void 
		{
			if(!StringUtil.validateString(url)) {
				callbackInvalidData(callbackMiscObj);
				return;
			}
			trace(" Ad url: " + url);
			trace("Disable Google IMA SDK Ad Call Overwrite: "+ StringUtil.stringToBoolean(Globals.vars.disableGoogleSDKAdCallOverwrite));
			
			var request:AdsRequest = new AdsRequest();
			request.linearAdSlotWidth = app.width;
			request.linearAdSlotHeight = app.height - app._controlBarHeight;
			request.nonLinearAdSlotWidth = app.width;
			request.nonLinearAdSlotHeight = app.height - app._controlBarHeight;
			//			request.adType = AdTypes.VAST;
			//			request = HorizontalAlignment.CENTER;
			//			request.adSlotVerticalAlignment = VerticalAlignment.BOTTOM;
			
			if (StringUtil.stringToBoolean(Globals.vars.disableGoogleSDKAdCallOverwrite) && 
				url.indexOf("ad.doubleclick.net") > -1 && 
				url.indexOf("ad.doubleclick.net/pfadx") < 0 &&
				!callbackMiscObj.isWNVASTRedirect) {
				
				trace("Custom overwrite is not supported any more");
				/* NOTE: Custom overwrite is not supported any more */
				
				/*			
				// parsing ad url
				//http://ad.doubleclick.net/adx/wn.loc.wndemo1.video/education;sz=10x10;wnsz=30;tile=1;wncc=Education;imp=creative;;wnad41=wnow;wnad46=wnow;wnad43=wnow;wnad30=wndemo1;apptype=videostandard;env=production;ord=10270989?
				url = url.substring(url.indexOf("adx/")+4); 
				var lastSlash:int = url.indexOf("/");
				request.site = url.substring(0, lastSlash);
				url = url.substring(lastSlash+1);
				request.zone = url.substring(0, url.indexOf(";"));
				url = url.substring(url.indexOf(";")+1);
				var params:Array = url.split(";");  
				request.adCommand = "adx";
				request.extraTargetingKeyValues = {}
				for (var i:int=0; i<params.length; i++) {
				if (StringUtil.validateString(params[i])) {
				var param:Array = params[i].split("=");
				if (StringUtil.validateString(param[0]) && StringUtil.validateString(param[1])) {
				if (param[0] == "sz") {
				request.size = param[1]; 
				} else if (param[0] == "ord") {
				request.ordinal = param[1].replace("?", "");
				} else {
				request.extraTargetingKeyValues[param[0]] = param[1];
				}
				}
				}
				}
				*/
				/*request.adCommand = "adx";
				//request.site = "wn.loc.wndemo1.video";
				//request.zone = "education";
				//request.size = "10x10";
				request.extraTargetingKeyValues = {
				"wnsz": "30",
				"tile":"1",
				"wncc": "Education",
				"imp": "creative",
				"wnad41": "wnow",
				"wnad46": "wnow",
				"wnad43": "wnow",
				"wnad30": "wndemo1",
				"apptype": "videostandard",
				"env": "production"
				}
				request.ordinal = "10270989";*/
			} else {
				request.adTagUrl = url;
			}
			
			// Prepare the ads loader 
			var adsLoader:AdsLoader = new AdsLoader();
			adsLoader.loadSdk();
			/*
			 
			app.adContainer.addChild(adsLoader);
			
			*/
			// Add event handlers
			adsLoader.addEventListener(AdsManagerLoadedEvent.ADS_MANAGER_LOADED, onAdsManagerLoaded); // ads are successfully returned
			adsLoader.addEventListener(AdErrorEvent.AD_ERROR, onAdError); // no ads were found for the request
			adsLoader.addEventListener(CustomContentLoadedEvent.CUSTOM_CONTENT_LOADED, onCustomContentLoadedEvent);
			// Request the ads
			adsLoader.requestAds(request, {
				callbackMiscObj:callbackMiscObj,
				callbackComplete:callbackComplete,
				callbackSecurityError:callbackSecurityError,
				callbackIoError:callbackIoError,
				callbackInvalidData:callbackInvalidData
			});
		}
		
		private static function onAdsManagerLoaded(e:AdsManagerLoadedEvent):void
		{
			// Publishers can modify the default preferences through this object.
			var adsRenderingSettings:AdsRenderingSettings = new AdsRenderingSettings();
			
			// In order to support ad rules playlists, ads manager requires an object that
			// provides current playhead position for the content.
			var contentPlayhead:Object = {};
			contentPlayhead.time = function():Number {
				//trace("contentPlayhead.time = " + app.display._currentTime);
				return app.display._currentTime * 1000;
				//return 0 * 1000; // convert to milliseconds.
			};

			// https://developers.google.com/interactive-media-ads/docs/sdks/flash/v3/ads
			// Alternatively, when adRenderingSettings.autoAlign is false, all ads are positioned in the top left (0,0) corner of the adsContainer.
			adsRenderingSettings.autoAlign = false;
			// Maximum bitrate in Kbps. Is not required as maximum limit, but must be specified in order for highest bitrate to be picked by a player
			adsRenderingSettings.bitrate = 10000;

	
			// Get a reference to the AdsManager object through the event object.
			var adsManager:AdsManager = e.getAdsManager(contentPlayhead, adsRenderingSettings);

			var adContext:Object = e.userRequestContext;
			
			app.adContainer_v3.ProcessAd(adsManager, adContext.callbackMiscObj);
			
			
			/* NOTE: Custom XML is supported diffrently now - CustomContentLoadedEvent; getCustomContentAd */
			
			/*
			var adMngr:AdsManager = adsLoadedEvent.adsManager;
			var adContext:Object = adsLoadedEvent.userRequestContext;
			//trace("adMngr.type = " + adMngr.type);
			if (adsManager. == AdsManagerTypes.CUSTOM_CONTENT) {
			trace("Loaded ad XML isn't VAST. Parsing it as WN xml.");
			var adManager:CustomContentAdsManager = adMngr as CustomContentAdsManager;
			trace("XML: " + adManager.ads[0].content);
			fetchXml(adManager.ads[0].content,
			adContext.callbackMiscObj,
			adContext.callbackComplete,
			adContext.callbackInvalidData);
			} else {
			app.adContainer.ProcessAd(adMngr, adContext.callbackMiscObj);
			}
			*/
		}
		
		private static function onCustomContentLoadedEvent(e:CustomContentLoadedEvent):void
		{
			trace("Loaded ad XML isn't VAST. Parsing it as WN xml.");
			trace("XML: " + e.content);
			var adContext:Object = e.userRequestContext;
			General.fetchXml(e.content,
				adContext.callbackMiscObj,
				adContext.callbackComplete,
				adContext.callbackInvalidData);
		}
		
		private static function onAdError(adErrorEvent:AdErrorEvent):void
		{
			/*
			Error codes - https://developers.google.com/interactive-media-ads/docs/sdks/googleflashas3_apis#AdError
			*/
			var adError:AdError = adErrorEvent.error;
			var adContext:Object = adErrorEvent.userRequestContext;
			trace("General.onAdError(): " + adError.errorMessage);
			if (adError.innerError != null) {
				trace("Caused by: " + adError.innerError.message);
			}
			if (adErrorEvent.error.errorCode == 0) {
				General.fetchXml("<NO DATA/>",
					adContext.callbackMiscObj,
					adContext.callbackComplete,
					adContext.callbackInvalidData);
			} else {
				trace("Unsupported error code: " + adErrorEvent.error.errorCode);
				adContext.callbackInvalidData(adContext.callbackMiscObj);
			}
		}
		
	}
}