package codebase.worldnow.com
{
	import codebase.worldnow.ad.WNBeacon;
	import codebase.worldnow.reuse.Constants;
	import codebase.worldnow.reuse.General;
	import codebase.worldnow.ad.AdsLoader_v3;
	import codebase.worldnow.reuse.Globals;
	import codebase.worldnow.reuse.Helper;
	import codebase.worldnow.util.StringUtil;
	import codebase.worldnow.util.caption.Decoder;
	
	import flash.display.StageDisplayState;
	import flash.events.TimerEvent;
	import flash.external.*;
	import flash.utils.Timer;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import mx.core.FlexGlobals;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.events.ResizeEvent;
	
	import org.osmf.captioning.CaptioningPluginInfo;
	import org.osmf.captioning.model.Caption;
	import org.osmf.containers.MediaContainer;
	import org.osmf.display.ScaleMode;
	import org.osmf.elements.ImageElement;
	import org.osmf.elements.LightweightVideoElement;
	import org.osmf.elements.ProxyElement;
	import org.osmf.elements.SerialElement;
	import org.osmf.elements.VideoElement;
	import org.osmf.elements.F4MElement;	
	import org.osmf.events.AudioEvent;
	import org.osmf.events.BufferEvent;
	import org.osmf.events.DisplayObjectEvent;
	import org.osmf.events.DynamicStreamEvent;
	import org.osmf.events.LoadEvent;
	import org.osmf.events.MediaErrorEvent;
	import org.osmf.events.MediaFactoryEvent;
	import org.osmf.events.MediaPlayerCapabilityChangeEvent;
	import org.osmf.events.MediaPlayerStateChangeEvent;
	import org.osmf.events.PlayEvent;
	import org.osmf.events.TimeEvent;
	import org.osmf.events.TimelineMetadataEvent;
	import org.osmf.layout.LayoutMetadata;
	import org.osmf.layout.LayoutRenderer;
	import org.osmf.media.DefaultMediaFactory;
	import org.osmf.media.MediaElement;
	import org.osmf.media.MediaFactory;
	import org.osmf.media.MediaPlayer;
	import org.osmf.media.MediaPlayerState;
	import org.osmf.media.MediaResourceBase;
	import org.osmf.media.PluginInfoResource;
	import org.osmf.media.URLResource;
	import org.osmf.metadata.*;
	import org.osmf.net.DynamicStreamingItem;
	import org.osmf.net.DynamicStreamingResource;
	import org.osmf.net.NetLoader;
	import org.osmf.net.StreamType;
	import org.osmf.net.StreamingURLResource;
	import org.osmf.traits.MediaTraitType;
	import org.osmf.traits.TimeTrait;
	import org.osmf.utils.URL;
	
	import spark.components.Label;

	
	public class OSMF_VideoDisplay extends UIComponent
	{
		private var mediaPlayer:MediaPlayer;
		private var mediaFactory:MediaFactory = new DefaultMediaFactory();
		private var mediaElement:LightweightVideoElement;
		private var imageElem:ImageElement;
		private var containerRenderer:LayoutRenderer;
		private var canvas:MediaContainer;
		private var parallelLayout:LayoutMetadata = new LayoutMetadata();
		private var pluginManager:PluginManager = new PluginManager();
		private var mediaPlayerTimeLoaded:Number = 0;
		private var mediaPlayerVolume:Number = .5;
		private var _width:Number;
		private var _height:Number;
		private var _volume:Number;
		public var _playingAttempts:int;
		public var _currentTime:Number = 0;
		private var clipPlaySeqTracker:PlaybackSeqTracker;
		private var beaconTimer:Timer;
		private var autostartTimeout:uint;
		private var autostartDelay:int; // OSMF displayObject property may not be immediately available after the assignment of "media" to the MediaPlayer
		private var liveStreamConnectionTimer:Timer = new Timer(7000,1);
		[Bindable] public var currentContentClip:Object = null; // BE CAREFUL: there is logic flow that relies on checks against null
		private var currentAdClip:Object = null; // BE CAREFUL: there is logic flow that relies on checks against null
		private var currentClip:Object = null; // used only for cases when clip starts plaing just after the "MEDIA_PLAYER_STATE_CHANGE" event
		//public var netStreamLoadTrait:NetStreamLoadTrait;
		private var liveStreamCcDecoder:Decoder;
		
		public function OSMF_VideoDisplay()
		{

			// Construct a container
			canvas = new MediaContainer();
			canvas.clipChildren = true;
			canvas.x = 0;
			canvas.y = 0;
			canvas.backgroundColor = 0x000000;
			canvas.backgroundAlpha = 1;
			addChild(canvas);
			
			// Construct a media player
			mediaPlayer = new MediaPlayer();
			mediaPlayer.autoPlay = false;
			mediaPlayer.autoRewind = false;
			
			this.clipPlaySeqTracker = new PlaybackSeqTracker();
			this.beaconTimer = new Timer(1);
			//processBeacons();
			this.beaconTimer.addEventListener("timer", processBeacons);	
			
			this.addEventListener(FlexEvent.INITIALIZE, function(e:FlexEvent):void{
				canvas.width = _width = e.target.width;
				canvas.height = _height = e.target.height;
			});
			this.addEventListener(ResizeEvent.RESIZE, function(e:ResizeEvent):void{
				canvas.width = _width = e.target.width;
				canvas.height = _height = e.target.height;
			});
			mediaPlayer.addEventListener(PlayEvent.PLAY_STATE_CHANGE, function(e:PlayEvent):void{
				Observer.Notify("VIDEO_PLAY_CHANGE", {playing:e.playState});
				// external event
				if (e.playState == "playing") {
					if (_currentTime > 0) fireExternalEvent("play");
				}/* else {
					fireExternalEvent("pause");
				}*/
			});
			mediaPlayer.addEventListener(TimeEvent.CURRENT_TIME_CHANGE, function(e:TimeEvent):void{
				Observer.Notify("VIDEO_PLAYHEAD_CHANGE", {position:e.time});
			});
			mediaPlayer.addEventListener(TimeEvent.COMPLETE, onVideoDurationReached);
			mediaPlayer.addEventListener(TimeEvent.DURATION_CHANGE, function(e:TimeEvent):void{
				// Some livestreams fire DURATION_CHANGE continiously. Ignore such events.
				if (currentContentClip.isLiveStream && 
					Math.round(mediaPlayer.duration) == Math.round(e.time))
				{
					// don't update
				} else {
					Observer.Notify("VIDEO_DURATION_CHANGE", {duration:e.time});
					// If video is cached, BYTES_DOWNLOADED_CHANGE won't be fired and buffer in the Progress bar won't be set properly.
					// Therefore notifyBufferLoadingChange() must be called.
					notifyBufferLoadingChange();
				}
			});
			mediaPlayer.addEventListener(LoadEvent.BYTES_LOADED_CHANGE, function(e:LoadEvent):void{
				notifyBufferLoadingChange(e.bytes);
			});
			mediaPlayer.addEventListener(AudioEvent.VOLUME_CHANGE, function(e:AudioEvent):void{
				Observer.Notify("VIDEO_VOLUME_CHANGE", {newVolume:e.volume});
				if (e.volume > 0 && _volume == 0) fireExternalEvent("unmute");
				if (e.volume == 0) fireExternalEvent("mute");
				_volume = e.volume;
			});
			mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, function (e:MediaPlayerStateChangeEvent):void {
				trace("> MEDIA_PLAYER_STATE_CHANGE - state = " + e.state);
				Observer.Notify("MEDIA_PLAYER_STATE_CHANGE", {"state":e.state});
				if (currentContentClip.isLiveStream) Observer.Notify("HIDE_MESSAGE", {});
				
				/* HDS plugin doesn't fire TimeEvent.CURRENT_TIME_CHANGE.
				* mediaPlayer.currentTime always stays 0. It brakes players time related logic.
				* Following code updates mediaPlayer.currentTime when HDS playing starts */
				if (currentContentClip != null && currentContentClip.isLiveStream && e.state == "playing" && mediaPlayer.currentTime == 0) {
					_currentTime = 0.1;					
				}
				
				//if (currentContentClip != null && currentContentClip.isLiveStream && e.state == "buffering" && mediaPlayer.currentTime == 0) {
				if (currentContentClip != null && currentContentClip.isLiveStream && e.state == "buffering" && _currentTime == 0) {
					trace("liveStreamConnectionTimer start");
					Observer.Notify("HIDE_MESSAGE", {});
					liveStreamConnectionTimer.stop();
					if (!liveStreamConnectionTimer.hasEventListener("timerComplete")) {
						liveStreamConnectionTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onliveStreamConnectionTimeLimitReached);
					}
					function onliveStreamConnectionTimeLimitReached(e:TimerEvent):void {
						trace("LiveStreamConnectionTimeLimitReached");
						Observer.Notify("SHOW_MESSAGE", {message:"LIVE STREAM IS UNAVAILABLE"});
					}
					liveStreamConnectionTimer.start();
				}
				if (currentContentClip != null && currentContentClip.isLiveStream && e.state != "buffering" && liveStreamConnectionTimer.running) {
					liveStreamConnectionTimer.stop();
				}
			});
			
			mediaPlayer.addEventListener(MediaErrorEvent.MEDIA_ERROR, function(e:MediaErrorEvent):void{
				trace(":::::::: MEDIA_ERROR > e.error = "+e.error);
				var clipType:PlaybackClipTypeEnum = clipPlaySeqTracker.WhatToPlay();
				if (mediaPlayer.media.hasTrait("play")) {
					if (clipType == PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD) {
						clipPlaySeqTracker.ForceComplete();
					} else if (clipType == PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD) {
						onVideoDurationReached(new TimeEvent(TimeEvent.COMPLETE));
					} else {
						if (currentContentClip.isLiveStream) {
							Observer.Notify("SHOW_MESSAGE", {message:"LIVE STREAM IS UNAVAILABLE"});
						} else {						
							Observer.Notify("SHOW_MESSAGE", {});
						}
					}
				}
			});
			
			Observer.Register("VIDEO_PLAYHEAD_CHANGE", this, "onPlayheadChange");
			Observer.Register("PROGRESS_BAR_CHANGE", this, "seekVideoPosition");
			Observer.Register("PLAY_BUTTON_CLICKED", this, "play");
			Observer.Register("PAUSE_BUTTON_CLICKED", this, "pause");
			Observer.Register("MUTE_BUTTON_CLICKED", this, "mute");
			Observer.Register("SOUNDON_BUTTON_CLICKED", this, "turnSoundOn");
			Observer.Register("VOLUME_BAR_CHANGE", this, "setVolumeLevel");
			Observer.Register("LOAD_CONTENT_CLIP", this, "loadContentClip");
			Observer.Register("LOAD_AD_CLIP", this, "loadAdClip");
			Observer.Register("PLAYBACK_SEQ_CHANGED", this, "playback");
			Observer.Register("CLIP_SEQ_COMPLETE", this, "clipSeqComplete");
			Observer.Register("FULLSCREEN_EVENT", this, "fullScreenToggle");
			Observer.Register("VAST_VIDEO_AD", this, "vastVideoAd");
			Observer.Register("VAST_OVERLAY", this, "vastOverlay");
			Observer.Register("LINEAR_AD_STOPPED", this, "vastVideoStopped");
			Observer.Register("VAST_VIDEO_AD_COMPLETE", this, "vastVideoComplete");
			Observer.Register("EMBEDDED_GALLERY_CLIP", this, "playEmbeddedGalleryClip");
			Observer.Register("EMAIL_SENT", this, "onEmailSent");
			Observer.Register("OPEN_PANE", this, "onPaneOpen");
			Observer.Register("CC_SETTINGS_OPEN", this, "onCcSettingsOpen");
			
			/* Remove after 01/01/2014 - transition to new CC is complete. Livestream and VOD closed caption use the same UI */
			Observer.Register("SHOW_CC", this, "showLivestreamCc");
			Observer.Register("HIDE_CC", this, "hideLivestreamCc");
			/* */
			
			ExternalInterface.addCallback("playPause", playPause);
			ExternalInterface.addCallback("playMedia", play);
			ExternalInterface.addCallback("pauseMedia", pause);
			ExternalInterface.addCallback("ExternalGetTimerObject", externalGetTimerObject);
			ExternalInterface.addCallback("setVolume", setVolumeExternal);
			ExternalInterface.addCallback("showEmail", externalShowEmail);
			ExternalInterface.addCallback("showLink", externalShowLink);
			ExternalInterface.addCallback("showShare", externalShowShare);
			ExternalInterface.addCallback("launchHelp", externalLaunchHelp);
			ExternalInterface.addCallback("showClosedCaption", externalShowCC);
			ExternalInterface.addCallback("showSummary", externalShowSummary);
			ExternalInterface.addCallback("FireBeacon", FireReportingBeacon);
		}
		
		public function vastVideoAd(e:Object=null):void
		{
			trace("vastVideoAd() {");
			if (canvas.containsMediaElement(imageElem))	canvas.removeMediaElement(imageElem);
			
			var adHeadline:String = (e.parameters.adTitle != "") ? "Commercial - " + e.parameters.adTitle : "Commercial"; 
			var clip:Object = {isAd:true, headline:adHeadline};
			var adPosition:PlaybackClipTypeEnum = this.clipPlaySeqTracker.WhatToPlay();
			switch(adPosition)
			{
				case PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD:
					clip.isPreRoll = true; 
					break;
				case PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD:
					clip.isPostRoll = true;
					break;
			}
			//currentAdClip = clip;
			Observer.Notify("NewMedia", clip, true);
			Observer.Notify('newCaptionClip', {"clip":{}, "isAd":true});
			trace("} // vastVideoAd");
		}
		
		public function vastOverlay(e:Object=null):void
		{
			trace("vastOverlay() {");
			clipPlaySeqTracker.ForceComplete(); // start content clip
			trace("} // vastOverlay");
		}

		public function vastVideoStopped(e:Object=null):void
		{
			trace("> vastVideoStopped()");
			var whichIsPlaying:PlaybackClipTypeEnum = clipPlaySeqTracker.WhichIsPlaying();
			// if pre-roll  
			if (whichIsPlaying == PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD) {
				trace("current vast video - preroll");
			// if post-roll, go to next
			} else if (whichIsPlaying == PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD) {
				trace("current vast video - postroll");
				//clipPlaySeqTracker.ForceComplete();
			// If mid-roll, play content; 
			} else {
				trace("current vast video - midroll");
				Observer.Notify('HIDE_CC', {"clip":currentContentClip, "isAd":false});
				Observer.Notify("VIDEO_DURATION_CHANGE", {duration : currentContentClip.duration / 1000});
				notifyBufferLoadingChange();
				mediaPlayer.play();
				Observer.Notify("NewMedia", currentContentClip, true);
				Observer.Notify('newCaptionClip', {"clip":currentContentClip, "isAd":false});
			}
		}		
		
		public function vastVideoComplete(e:Object=null):void
		{
			trace("> vastVideoComplete()");
			clipPlaySeqTracker.ForceComplete();
		}
		
		public function playEmbeddedGalleryClip(e:Object):void
		{
			PlayGalleryClip(e.parameters.clip);
		}
		
		public function PlayGalleryClip(clip:Object):void
		{
			trace(">>>>>>>>>>>>>>>>> New gallery clip >>>>>>>>>>>>>>>>>");
			// crear autostartrelated logic and enable play button
			if (Globals.vars.isAutoStart) {
				clearTimeout(autostartTimeout);
				mediaPlayer.removeEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChangeAutostart);
			}
			
			var numberOfProperties:int = 0;
			for (var key:String in clip){
				trace("   " + key + ": " + clip[key]);
				numberOfProperties++;
			}
			trace(">>>");
			if (numberOfProperties == 2 && clip.id != undefined) {
			// html gallery clip
				resetCanvasWithEmptyClipObject();
				PlayClipById(clip.id, Globals.vars.affiliateNumber, true);
			} else if (clip.startTime != undefined && clip.ownerAffiliateNo != undefined) {
			// gallery clip from search results
				resetCanvasWithEmptyClipObject();
				PlayClipById(clip.id, clip.ownerAffiliateNo, true);
			} else {
			// normal gallery clip
				clip.playgalleryclip = true;
				Observer.Notify("LOAD_CONTENT_CLIP", clip, false);
				Observer.Notify("NewClip", clip, true);
			}
		}
		
		private function resetCanvasWithEmptyClipObject():void {
			Observer.Notify("LOAD_CONTENT_CLIP", {}, false);
			Observer.Notify("NewClip", {}, false);
			Observer.Notify("NewMedia", {}, false);
			this.currentContentClip = null;
			this.clipPlaySeqTracker.ForceComplete();
		}
		
		public function FetchGalleryClip(clip:Object):void
		{
			trace("FetchGalleryClip()");
			trace("Globals.vars.isAutoStart = " + Globals.vars.isAutoStart);
			trace(">>>>>>>>>>>>>>>>> New gallery clip - "+ clip +" >>>>>>>>>>>>>>>>>");
			for (var key:String in clip){
				trace(key + ": " + clip[key]);
			}
			trace("");
			clip.playgalleryclip = false;
			Observer.Notify("LOAD_CONTENT_CLIP", clip, false);
			Observer.Notify("NewClip", clip, true);
		}
		
		public function playback(e:Object=null):void
		{
			var clipType:PlaybackClipTypeEnum = this.clipPlaySeqTracker.WhatToPlay();
			notePlaybackEnding(clipType);
			switch(clipType)
			{
				case PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD:
					trace("playback - PRE_ROLL_AD");
					Observer.Notify("AD_LOAD_STARTED", {});
					if(!AdLogic()) {
						this.clipPlaySeqTracker.ForceComplete();
					}
					break;
				case PlaybackClipTypeEnum.PLAYBACKCLIP_CONTENT_CLIP:
					trace("playback - CONTENT_CLIP");
					Observer.Notify("AD_LOAD_COMPLETED", {});
					this.beaconTimer.stop();
					this.currentAdClip = null;
					if(this.currentContentClip != null) {
						PlayLoadedClip(this.currentContentClip, false);
					}
					break;
				case PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD:
					trace("playback - POST_ROLL_AD");
					if(!AdLogic()) {
						this.clipPlaySeqTracker.ForceComplete();
					}
					break;
			}
		}
		
		public function notePlaybackEnding(clipTypeWhatToPlay:PlaybackClipTypeEnum):void
		{
			switch(clipTypeWhatToPlay)
			{
				case PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD:
				case PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD:
					if(this.currentContentClip != null) {
						Observer.Notify("MediaEnded", this.currentContentClip, true);
					}
					break;
				case PlaybackClipTypeEnum.PLAYBACKCLIP_CONTENT_CLIP:
					if(this.currentAdClip != null) {
						Observer.Notify("MediaEnded", this.currentAdClip, true);
					}
					break;
			}
		}
		
		public function clipSeqComplete(e:Object=null):void
		{
			if (this.currentContentClip != null) setVideo(this.currentContentClip);
			Observer.Notify("ResetClip", this.currentContentClip, true);
			Observer.Notify("ClipEnded", this.currentContentClip, true);
		}

		public function onMediaPlayerStateChange(e:MediaPlayerStateChangeEvent):void {
			trace(".. onMediaPlayerStateChange() {");
			trace("mediaPlayer.state = " + this.mediaPlayer.state);
			if (this.mediaPlayer.state == "ready") {			
				this.mediaPlayer.play();
				Observer.Notify("NewMedia", currentClip, true);
				Reporting(currentClip, currentClip.isAd);
				this.mediaPlayer.removeEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChange);
			}/* else if (currentContentClip.isLiveStream && (e.state == "playing" || e.state == "playbackError")) {
				Observer.Notify("NewMedia", currentClip, true);
			}*/
			trace(".. // onMediaPlayerStateChange");
		}
		
		/* Remove after 01/01/2014 - transition to new CC is complete. Livestream and VOD closed caption use the same UI */
		/* { */	
		public function onMediaPlayerStateChangeLiveStream (e:MediaPlayerStateChangeEvent):void {
			trace(".. onMediaPlayerStateChangeLiveStream() {");
			trace("mediaPlayer.state = " + mediaPlayer.state);
			if (e.state == "playing" || e.state == "playbackError") {
				if (currentContentClip.isLiveStream) {
					Observer.Notify("NewMedia", currentClip, true);
					mediaPlayer.removeEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChangeLiveStream);
				}
			}
			if (e.state == "ready") {
				if (currentContentClip.isLiveStream) {
					trace("< stream is loaded and ready");
					// Start video playback
					Observer.Notify("NewMedia", currentClip, true);
					mediaPlayer.play();
					mediaPlayer.removeEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChangeLiveStream);
					FireReportingBeacon();
				}
			}
			trace(".. // onMediaPlayerStateChangeLiveStream");
		}
		/* } */			
		
		public function onMediaPlayerStateChangeAutostart(e:MediaPlayerStateChangeEvent):void {
			trace(".. onMediaPlayerStateChangeAutostart() {");
			trace(" mediaPlayer.state = " + mediaPlayer.state);
			if (mediaPlayer.state == "ready") {
				clearTimeout(autostartTimeout);
				autostartTimeout = setTimeout(function ():void { 
					clipPlaySeqTracker.ForceComplete(); 
				}, autostartDelay);
				//Observer.Notify("PLAY_BUTTON_CLICKED", {});
				mediaPlayer.removeEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChangeAutostart);
			}
			trace(".. // onMediaPlayerStateChangeAutostart");
		}
		
		private function onVideoDurationReached(e:TimeEvent):void{
			trace(".. onVideoDurationReached() {");
			trace(" currentContentClip.isLiveStream = " + currentContentClip.isLiveStream);
			_currentTime = 0;
			if (!currentContentClip.isLiveStream) { 
				/*if (this.clipPlaySeqTracker.WhichIsPlaying() == PlaybackClipTypeEnum.PLAYBACKCLIP_CONTENT_CLIP) {
					Reporting(this.currentContentClip, false);
				}*/
				if (this.mediaPlayer.canSeek) {
					trace("mediaPlayer.seek(0)");
					mediaPlayer.seek(0);
				} else {
					trace("mediaPlayer.canSeek = false");
					this.addEventListener(MediaPlayerCapabilityChangeEvent.CAN_SEEK_CHANGE, seekWhenReady);
				}
				//setImage(this.currentContentClip);
			}
			/* When content clip isn't a livestream, fire on finish for both content and ad clips.
			   For livesteram content clip, fire this, only when ad clip is over. */
			if (!currentContentClip.isLiveStream || 
				currentClip != null && currentAdClip != null && currentClip == currentAdClip)
				// Math.round(mediaPlayer.currentTime) == Math.round(currentAdClip.duration)
			{
				Observer.Notify("VIDEO_DURATION_REACHED", {});
			}			
			trace(".. } // onVideoDurationReached");
		}
		
		public function seekWhenReady(e:MediaPlayerCapabilityChangeEvent):void {
			trace("mediaPlayer.seek(0)");
			mediaPlayer.seek(0);			
			this.mediaPlayer.removeEventListener(MediaPlayerCapabilityChangeEvent.CAN_SEEK_CHANGE, seekWhenReady);
		}
		
		public function play(e:Object=null):void
		{
			trace(".. play() {");
			trace("mediaPlayer.currentTime = " + this.mediaPlayer.currentTime);
			trace("_currentTime = " + _currentTime);
			//if(this.mediaPlayer.currentTime == 0)
			if(_currentTime == 0)
				this.clipPlaySeqTracker.ForceComplete();
			else
				this.mediaPlayer.play();
			trace(".. } // play");
		}
		public function pause(e:Object=null):void
		{
			trace("pause()");
			trace("currentContentClip.isLiveStream = " + currentContentClip.isLiveStream);
			trace("mediaPlayer.canPause = " + mediaPlayer.canPause);
			if (!mediaPlayer.canPause && currentContentClip.isLiveStream) {
				mediaPlayer.stop();
			} else if (mediaPlayer.canPause) {
				mediaPlayer.pause();
			}
			fireExternalEvent("user_pause");
		}
		public function playPause(e:Object=null):void
		{
			if (this.mediaPlayer.playing) 
				this.mediaPlayer.pause();
			else
				play();
		}
		public function mute(e:Object=null):void
		{
			this.mediaPlayer.volume = 0;
		}
		public function turnSoundOn(e:Object=null):void
		{
			this.mediaPlayer.volume = e.parameters.volume;
		}
		/*public function muteToggle(e:Object=null):void
		{
		if (this.mediaPlayer.volume > 0) this.mediaPlayer.volume = 0; 
		else this.mediaPlayer.muted = true;
		}*/
		public function setVolumeLevel(e:Object=null):void
		{
			this.mediaPlayer.volume = e.parameters.volume;
		}
		public function setVolumeExternal(volumeVal:Number):void	
		{
			var vol:Number=Number(volumeVal);		
			if(isNaN(vol)) return;
			// External Interface Volume Value Range
			if(vol>=0  && vol<=10)
			{			
				vol=int(vol) * 0.1;
				this.mediaPlayer.volume = vol;
			}
		}
		
	
		
		
		
/*
		calls sequence
	
		loadAdClip -> PlayLoadedClip -> loadClip -> setVideo - mediaPlayer.media = mediaElement
		loadContentClip -> loadClip -> …
		playback -> PlayLoadedClip
		Observer.Register("PLAYBACK_SEQ_CHANGED", this, "playback");

		
*/
		
		
		
		public function PlayLoadedClip(clip:Object, isAd:Boolean=false):Boolean
		{
			trace(".. PlayLoadedClip() {");
			loadClip(clip, isAd);
			
			// Check if Windows Live Stream was set 
			if(clip.preferredFormat == "WMV" && !isAd)
			{
				// because of security concerns you cannot switch silverlight to full screen automatically
				// so ensure you are down in embedded mode and the swap is seen by the user
				if (stage != null) stage.displayState=StageDisplayState.NORMAL;
				clip.domId = Globals.vars.domId;
				Observer.Notify("NewMedia", clip, true);
				//render SL canvas
				ExternalInterface.call("wnVideoWidgets.DisplaySilverlightScreen", clip);
				trace("Calling ext interface func wnVideoWidgets.DisplaySilverlightScreen");
				return false;
			}
			
			trace("<--- Currently playing clip:");
			for (var key:String in clip){
				trace(key + ": " + clip[key]);
			}
			trace("--->");
			
			/* Old way of getting CC via XML is replaced with TTML 
			if (!isAd && clip.hasCC) GetClosedCaption();
			*/
			Observer.Notify('HIDE_CC', {"clip":clip, "isAd":isAd});
			Observer.Notify('newCaptionClip', {"clip":clip, "isAd":isAd});
			
			if (canvas.containsMediaElement(imageElem))	canvas.removeMediaElement(imageElem);
			
			if (this.mediaPlayer.state == "ready") {
				trace("clip is loaded and ready");
				this.mediaPlayer.play();
				Observer.Notify("NewMedia", clip, true);
				Reporting(clip, isAd);
			} else {
				trace("PlayLoadedClip - clip isn't loaded yet");
				currentClip = clip;
				currentClip.isad = isAd; 
				this.mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChange); 
			}
			trace(".. } // PlayLoadedClip");
			return true;
		}
		
		
		public function loadContentClip(e:Object=null):void
		{
			trace(".. loadContentClip() {")
			this.clipPlaySeqTracker.Reset();
			_currentTime = 0;
			loadClip(e.parameters, false);
			if(e.parameters.playgalleryclip) {
				this.clipPlaySeqTracker.ForceComplete();
			} else if(Globals.vars.isAutoStart) {
				//this.clipPlaySeqTracker.ForceComplete();
				/*var player:Object = this;
				autostartTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onComplete);
				function onComplete(e:TimerEvent):void {
					//Observer.Notify("PLAY_BUTTON_CLICKED", {});
				*/
				autostartDelay = (Globals.vars.videoType == "livestream") ? 2500 : 1500;
				if (mediaPlayer.state == "ready") {
					trace("Autostart is true. Clip is ready.");
					clearTimeout(autostartTimeout);
					autostartTimeout = setTimeout(function ():void { 
						clipPlaySeqTracker.ForceComplete(); 
					}, autostartDelay);
				} else {
					trace("Autostart is true, but content clip isn't ready yet. Waiting for READY event...");
					mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChangeAutostart); 
				}
				/*}
				autostartTimer.start();
				trace("autostartTimer start");
				*/
			}
			trace(".. } // loadContentClip")
		}
		
		public function loadAdClip(e:Object=null):void
		{
			trace(".. loadAdClip() {");
			this.beaconTimer.start();
			PlayLoadedClip(e.parameters, true);
			trace(".. } // loadAdClip");
		}
		
		public function loadClip(clip:Object, isAd:Boolean=false):void
		{
			trace(".. loadClip() {");
			Observer.Notify('CLOSE_PANE', {paneName:'all'});
			if(isAd) {
				var adPosition:PlaybackClipTypeEnum = this.clipPlaySeqTracker.WhatToPlay();
				switch(adPosition)
				{
					case PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD:
						clip.isPreRoll = true; 
						break;
					case PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD:
						clip.isPostRoll = true;
						break;
				}
				var commercialHeadlinePrefix:String;
				if (StringUtil.validateString(Globals.vars.commercialHeadlinePrefix)) commercialHeadlinePrefix = Globals.vars.commercialHeadlinePrefix;
				else commercialHeadlinePrefix = Constants.DEFAULT_COMMERCIAL_HEADLINE_PREFIX;
				clip.headline = commercialHeadlinePrefix + " - " + clip.headline;
				this.currentAdClip = clip;
			} else {
				clip.isAd = clip.isad = false;
				this.currentContentClip = clip;
			}
			setVideo(clip);
			trace(".. } // loadClip");
		}
		
		public function wnGetQS(qsParameter:String, urlToParse:String):String {
			
			var searchString:Array = urlToParse.split("?");
			
			if (!searchString[1]) return null
			
			var qs:Array = new Array();
			var url:String = searchString[1];
			var params:Array = url.split("&");
			
			for (var i:int = 0; i < params.length; i++) 
			{
				var param:Array = params[i].split("=");
				qs[param[0]] = param[1];
			}
			
			return qs[qsParameter];	
		}

		
		/*
		public function onTraitAdd(event:MediaElementEvent):void
		{
			trace(" [add]", event.toString()); 
			if (event.traitType == MediaTraitType.LOAD)
			{
				if (mediaPlayer.media.hasTrait(MediaTraitType.LOAD))
				{
					netStreamLoadTrait = mediaPlayer.media.getTrait(MediaTraitType.LOAD) as NetStreamLoadTrait;
					if (netStreamLoadTrait != null) netStreamLoadTrait.addEventListener(LoadEvent.LOAD_STATE_CHANGE, onLoaded);
				}
			}
		}
		public function onLoaded(event:LoadEvent):void
		{
			var netStream:NetStream = netStreamLoadTrait.netStream;
			if (netStream != null) netStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
		}
		public function onNetStatus(e:NetStatusEvent):void
		{
			trace("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% - " + e.info.code);
		}
		*/
		
		public function setVideo(clip:Object):void
		{
			trace(".. setVideo() {");
			setImage(clip);
			var videoUrl:String = clip.flvUri; 
			var graphicXml:XML = new XML(clip.graphic); 
			var imageUrl:String = graphicXml..FILENAME;

			if (canvas.containsMediaElement(mediaElement)) 
			{
				trace("removing old clip video canvas");
				canvas.removeMediaElement(mediaElement);
				//mediaPlayerVolume = mediaPlayer.volume; 
			}

			// Default - Progressive video 
			var resource:URLResource = new URLResource(videoUrl);
			if (!clip.isPreRoll && !clip.isPostRoll && clip.hasCC && clip.preferredFormat != "WMV") { 
				var metadata:Metadata = new Metadata();
				trace("Globals.vars.affiliate = " + Globals.vars.affiliate);
				trace("clip.ownerAffiliateName = " + clip.ownerAffiliateName);
				var CAPTION_URL:String;
				if (Globals.vars.affiliate != clip.ownerAffiliateName) {
				// clip shared between two affiliates
					var clipOwnersImageBaseUrl:String = clip.thumb;
					if (StringUtil.validateString(clipOwnersImageBaseUrl)) {
						// try to get image base url from the clip's graphic
						clipOwnersImageBaseUrl = clipOwnersImageBaseUrl.substr(0, clipOwnersImageBaseUrl.indexOf("/image"));
					} else {
						clipOwnersImageBaseUrl = Globals.vars.staticImgPath.replace(Globals.vars.affiliate, clip.ownerAffiliateName);
					}
					CAPTION_URL = clipOwnersImageBaseUrl + "/metadata/"+ clip.id +".ttml";
				} else {
					CAPTION_URL = Globals.vars.staticImgPath + "/metadata/"+ clip.id +".ttml";
				}
				//var CAPTION_URL:String = "http://localhost/8070078.xml";
				metadata.addValue(CaptioningPluginInfo.CAPTIONING_METADATA_KEY_URI, CAPTION_URL);
				resource.addMetadataValue(CaptioningPluginInfo.CAPTIONING_METADATA_NAMESPACE, metadata);
			}
			var mediaElement:MediaElement = mediaFactory.createMediaElement(resource);
			
			// Flash live streaming
			if (clip.StreamType == "live")
			{
				//mediaElement = new LightweightVideoElement(new StreamingURLResource(videoUrl, StreamType.LIVE));
				var streamResource:StreamingURLResource = new StreamingURLResource(videoUrl, StreamType.LIVE)
				if (!clip.isPreRoll && !clip.isPostRoll && clip.hasCC) {
					streamResource.addMetadataValue(CaptioningPluginInfo.CAPTIONING_METADATA_NAMESPACE, metadata);
				}
				mediaElement = null;
				mediaElement = mediaFactory.createMediaElement(streamResource);

				//mediaElement.addEventListener(MediaElementEvent.TRAIT_ADD, onTraitAdd);
				/* Buffering
					http://stackoverflow.com/questions/5390979/smooth-image-rotation-in-flex
					http://livedocs.adobe.com/flex/3/html/help.html?content=embed_4.html - Embedding SWF symbols
				*/
			} 
			// ABR streaming
			/*else if (Globals.vars.enableABRStreaming && clip.allMediaItems != undefined && clip.allMediaItems.length >= 1) {
				// Get .f4m item, and overwrite the default media element
				for each(var item:Object in clip.allMediaItems) {
					if (item.type == "video/mp4" && item.url != undefined && item.url.indexOf(".f4m") != -1) {
						mediaElement = null;
						var manifestElement:F4MElement = new F4MElement();
						//manifestElement.resource = new URLResource("http://mediapm.edgesuite.net/osmf/content/test/manifest-files/progressive.f4m");
						//manifestElement.resource = new URLResource("http://flash.flowplayer.org/demos/standalone/plugins/streaming/bbb.f4m");
						//manifestElement.resource = new URLResource("http://dsys1videovod-vh.akamaihd.net/z/WNOW_0806201509401800000.smil/manifest.f4m?g=WMBHEYYJGPDN&hdcore=3.5.0");
						//trace("Play ABR from " + item.url + "?g=WMBHEYYJGPDN&hdcore=3.5.0");
						//manifestElement.resource = new URLResource(item.url);
						manifestElement.resource = new URLResource(item.url + "?hdcore=3.5.0");
						mediaElement = manifestElement;
						break;
					}
				}
			}*/
			// Dynamic streaming
			else if (Globals.vars.dynamicStreamingEnabled &&
					 StringUtil.validateString(Globals.vars.dynamicStreamingBaseUrl) &&
					 clip.allMediaItems != undefined &&
					 clip.allMediaItems.length >= 1) 
			{
				trace("clip.allMediaItems = " + clip.allMediaItems);
				// Get dynamic stream item by applying a filter to allMediaItems
				var dynamicStreamItems:Array = new Array;
				for each(var item:Object in clip.allMediaItems) 
				{
					if (item.type == "video/mp4" && // Only h.264 encoded files are used for dynamic streaming 
						item.url != undefined &&
						item.url.indexOf("rtsp://") == -1 &&
						item.url.indexOf("rtmp://") == 0 &&
						item.bitrate != undefined)
					{
						if (int(Globals.vars.playbackMaxBitrate) < 1 ||
							int(Globals.vars.playbackMaxBitrate) > 0 && item.bitrate < Globals.vars.playbackMaxBitrate)
						{
							dynamicStreamItems.push(item);
						}
					}
				}
				
				// If any items are eligable for dynamic streaming 
				// 	then overwrite the default media element
				if (dynamicStreamItems.length >= 1) {
					mediaPlayer.addEventListener(MediaPlayerCapabilityChangeEvent.IS_DYNAMIC_STREAM_CHANGE, onDynamicStreamChange);
					mediaPlayer.addEventListener(DynamicStreamEvent.SWITCHING_CHANGE, onSwitchingChangeRequest);
					mediaPlayer.bufferTime = 8;
					/** 
					 * The first two parameters of each DynamicStreamingItem specify 
					 * stream name and bitrate and are required. 
					 * The second two parameters of each DynamicStreamingItem are optional; 
					 * they specify the stream's width and height, in that order. 
					 */
					// get the dynamic stream base url from the wnbaseurl query string
					//  if not there then use the site config baseurl
					var dynQSBase:String
					dynQSBase = wnGetQS("wnbaseurl", dynamicStreamItems[0].url);
					if (!dynQSBase) { dynQSBase = Globals.vars.dynamicStreamingBaseUrl; }
					
					var dynResource:DynamicStreamingResource = new DynamicStreamingResource(dynQSBase);
					trace (dynQSBase);
					
					for (var i:int = 0; i < dynamicStreamItems.length; i++)
					{
						// DynamicStreamingItem class requires the bitrate in kps.
						var streamUrl:String = "mp4:" + dynamicStreamItems[i].url.substr(dynQSBase.length);
						var dsi:DynamicStreamingItem = new DynamicStreamingItem(
							streamUrl,
							dynamicStreamItems[i].bitrate, // Divide by 1000 if passed in bits
							dynamicStreamItems[i].width,
							dynamicStreamItems[i].height
						);
						dynResource.streamItems.push(dsi);
					}
					dynResource.streamItems = dynResource.streamItems.sort(sortByBitrate);
					dynResource.initialIndex = Math.floor(dynResource.streamItems.length / 2);
					dynResource.streamType = StreamType.LIVE_OR_RECORDED;
					trace("streams:");
					for(var ii in dynResource.streamItems) 
					{
						trace("   " + dynResource.streamItems[ii].streamName+", "+dynResource.streamItems[ii].bitrate+", "+dynResource.streamItems[ii].width+", "+dynResource.streamItems[ii].height);
					}
					trace("initialIndex = " + dynResource.initialIndex);
					
					/*
					var dynResource:DynamicStreamingResource = new DynamicStreamingResource("rtmp://cp5381.live.edgefcs.net/ondemand");
					dynResource.streamItems = Vector.<DynamicStreamingItem>(
						[ new DynamicStreamingItem("mp4:WNOW_0109201113460278208AB.mp4?wnbaseurl=rtmp://cp5381.live.edgefcs.net", 208, 480, 360) 
						, new DynamicStreamingItem("mp4:WNOW_0109201113460278208AC.mp4?wnbaseurl=rtmp://cp5381.live.edgefcs.net", 952, 480, 360) 
						, new DynamicStreamingItem("mp4:WNOW_0109201113460278208AD.mp4?wnbaseurl=rtmp://cp5381.live.edgefcs.net", 1680, 480, 360)
						]
					);
					*/
					
					// Overwrite the default clip with the new dynamic resource based clip
					mediaElement = mediaFactory.createMediaElement(dynResource);
					//var mediaElement = new VideoElement(dynResource);
				}
			// Windows Media streaming
			} else if (clip.preferredFormat == "WMV") {
				// Video ads can't be displayed without a display object
				mediaElement = new DisplayObjectElement();
			}

			trace("adding new clip video canvas");
			
			if (mediaElement != null) {
				// Listen for captions being added.
				if (!clip.isPreRoll && !clip.isPostRoll && clip.hasCC) {
					var captionMetadata:TimelineMetadata;
					captionMetadata = mediaElement.getMetadata(CaptioningPluginInfo.CAPTIONING_TEMPORAL_METADATA_NAMESPACE) as TimelineMetadata;
					if (captionMetadata == null) {
						captionMetadata = new TimelineMetadata(mediaElement);
						mediaElement.addMetadata(CaptioningPluginInfo.CAPTIONING_TEMPORAL_METADATA_NAMESPACE, captionMetadata);
					}
					captionMetadata.addEventListener(TimelineMetadataEvent.MARKER_TIME_REACHED, function (event:TimelineMetadataEvent):void
					{
						var caption:Caption = event.marker as Caption;
						//if (captioningEnabled && caption != null)
						if (caption != null) {
							//renderer.textFlow = TextConverter.importToFlow(caption.text, TextConverter.TEXT_FIELD_HTML_FORMAT);
							Observer.Notify('CC_UPDATE', {"cc": caption});
						}
					});
					clip.ccAvailable = false;
					captionMetadata.addEventListener(TimelineMetadataEvent.MARKER_ADD, function (event:TimelineMetadataEvent):void
					{
						clip.ccAvailable = true;
						//Observer.Notify('CC_ENABLE', {});
					});
				}

				
				if (mediaElement is VideoElement) mediaElement["smoothing"] = true;
				canvas.addMediaElement(mediaElement);
			}
			try { mediaPlayer.media = null; } catch (e:Error) {}
			mediaPlayer.media = mediaElement;
			
			// Livesteam CC
			if (clip.isLiveStream) {
				if (mediaElement != null && 
					mediaElement.hasOwnProperty("proxiedElement") &&
					mediaElement["proxiedElement"].hasTrait(MediaTraitType.LOAD)
				) {					
					if (mediaElement["proxiedElement"].hasOwnProperty("client")) {
						liveStreamCcDecoder = new Decoder();
						liveStreamCcDecoder.addEventListener("STRING_DECODED", function(e:Event):void {
							//trace("STRING_DECODED: " + e.target.decodedString);
							var caption:Caption = new Caption(0, 0, 0, e.target.decodedString);
							Observer.Notify('CC_UPDATE', {"cc": caption});
						});
						mediaElement["proxiedElement"].client.addHandler("onCaptionInfo", function (obj:Object):void {
							liveStreamCcDecoder.decode(obj.data);
						}, 1);
						currentContentClip.ccAvailable = true;
						
						mediaElement["proxiedElement"].client.addHandler("onTextData", onTextData, 1);
						mediaElement["proxiedElement"].client.addHandler("onCuePoint", onCuePoint, 1);
						/*
						mediaElement["proxiedElement"].client.addHandler(NetStreamCodes.ON_META_DATA, onMetaData, 1);
						mediaElement["proxiedElement"].client.addHandler(NetStreamCodes.ON_PLAY_STATUS, onPlayStatus, 1);
						mediaElement["proxiedElement"].client.addHandler(NetStreamCodes.ON_CUE_POINT, onCuePoint, 1);
						mediaElement["proxiedElement"].client.addHandler("onXMPData", onXMPData, 1);
						mediaElement["proxiedElement"].client.addHandler("onTextData", onTextData, 1);
						*/
					} else {
						trace("Warning: NetStream isn't ready at the time of CC handling assignment. Should be handled in onStreamLoaded event handler.");
						/*netStreamLoadTrait = mediaElement["proxiedElement"].getTrait(MediaTraitType.LOAD) as NetStreamLoadTrait;
						if (netStreamLoadTrait != null) {
							netStreamLoadTrait.addEventListener(LoadEvent.LOAD_STATE_CHANGE, onStreamLoaded);
						}*/
					}
				}
			}
			trace(".. } // setVideo");
		}
		
		public function onTextData (obj:Object):void {
			trace("onTextData");
			trace(obj);
		}
		public function onCuePoint (obj:Object):void {
			trace("onCuePoint");
			trace(obj);
		}
		
		public function setImage(clip:Object):void
		{
			trace(".. setImage() {");
			var imageUrl:String = "";
			if (StringUtil.validateString(clip.graphic) && clip.graphic.indexOf("FILENAME") > -1) {
				var graphicXml:XML = new XML(clip.graphic); 
				imageUrl = graphicXml..FILENAME;
			} else {
				imageUrl = clip.graphic;
			}
			if (canvas.containsMediaElement(imageElem)) canvas.removeMediaElement(imageElem);
			trace("imageUrl = " + imageUrl);
			if (StringUtil.validateString(imageUrl)) {
				imageElem = new ImageElement(new URLResource(imageUrl));
				imageElem.smoothing = true;
				canvas.addMediaElement(imageElem);
				mediaPlayer.media = imageElem;
			}
			trace(".. } // setImage");
		}
		
		
		public function seekVideoPosition(e:Object):void
		{
			trace("seekVideoPosition("+e.parameters.position+")");
			/*trace(" mediaPlayerTimeLoaded = " + this.mediaPlayerTimeLoaded);
			trace(" mediaPlayer.isDynamicStream = " + mediaPlayer.isDynamicStream);
			trace(" currentClip.StreamType = " + this.currentContentClip.StreamType);
			if (mediaPlayer.isDynamicStream || this.currentContentClip.StreamType == "ondemand") {
				mediaPlayer.seek(e.parameters.position);
			} else if (e.parameters.position <= this.mediaPlayerTimeLoaded) {
				mediaPlayer.seek(e.parameters.position);
			}*/
			trace("mediaPlayer.canSeek = " + mediaPlayer.canSeek);
			if(mediaPlayer.canSeek) {
				mediaPlayer.seek(e.parameters.position);
				fireExternalEvent("seek");
			}
		}

		private function notifyBufferLoadingChange(bytesLoaded:Number = 0):void
		{
			if (mediaPlayer.duration > 0 && mediaPlayer.bytesTotal > 0) {
				var timeLoaded:Number = bytesLoaded * mediaPlayer.duration / mediaPlayer.bytesTotal;
				this.mediaPlayerTimeLoaded = mediaPlayer.bytesLoaded * mediaPlayer.duration / mediaPlayer.bytesTotal;
				Observer.Notify("VIDEO_TIME_LOADED_CHANGE", {timeLoaded:this.mediaPlayerTimeLoaded});
			}
		}
		
		public function fullScreenToggle(e:Object):void
		{
			if (e.parameters.fullScreen) {
				/*parallelLayout.width = stage.stageWidth;
				parallelLayout.height = stage.stageHeight;*/
				fireExternalEvent("fullScreen");
			} else {
				/*parallelLayout.width = _width;
				parallelLayout.height = _height;*/
				//fireExternalEvent("fullscreenoff");
			}			
		}

		
		public function init():void
		{
			trace(".. init()");
			
			// Load Akamai Core HD plugin
			if (Globals.vars.videoType == "livestream" && StringUtil.stringToBoolean(Globals.vars.enableHDSLivestreaming)) {
				var pluginAkamaiHDCore:MediaResourceBase;
				var pluginUrl:String = "http://players.edgesuite.net/flash/plugins/osmf/advanced-streaming-plugin/v2.11/osmf1.5/fp10.1/AkamaiAdvancedStreamingPlugin.swf";
				pluginAkamaiHDCore = new URLResource(pluginUrl);
				loadPluginFromResource(pluginAkamaiHDCore);
				
				mediaFactory.loadPlugin(pluginAkamaiHDCore);
			}
			
			var pluginInfoRef:Class = flash.utils.getDefinitionByName("org.osmf.captioning.CaptioningPluginInfo") as Class;
			var pluginResource:MediaResourceBase = new PluginInfoResource(new pluginInfoRef);
			mediaFactory.addEventListener(MediaFactoryEvent.PLUGIN_LOAD_ERROR, function(e:Event):void {
				trace("Error: Captioning plugin load failed");
			});
			
			//pluginManager.addEventListener(Event.COMPLETE, function(e:Event):void {
			mediaFactory.addEventListener(MediaFactoryEvent.PLUGIN_LOAD, function(e:MediaFactoryEvent):void {
				if (Globals.vars.videoType == "livestream" && StringUtil.stringToBoolean(Globals.vars.enableHDSLivestreaming)) {
					if (e.resource is URLResource) {
						if (StringUtil.validateString(e.resource["url"]) && e.resource["url"].indexOf("AkamaiAdvancedStreamingPlugin") != -1) {
							onPluginsLoaded();
						} 
					}
				} else {
					onPluginsLoaded()
				}
			});
			mediaFactory.loadPlugin(pluginResource);
			//pluginManager.loadPlugins(mediaFactory);
		}
		
		public function onPluginsLoaded():void {
			ExternalInterface.addCallback("wnFlashVideoCanvas_PlayClip", PlayGalleryClip);
			ExternalInterface.addCallback("wnFlashVideoCanvas2_FetchGalleryClip", FetchGalleryClip);
			if(Globals.vars.thirdpartymrssurl != "") {
				PlayMrss(Globals.vars.thirdpartymrssurl);
			} else if (Globals.vars.videoType == "livestream") {
				var liveStreamClipStr:String = '<CLIP>';
				liveStreamClipStr += '<media:group xmlns:media="http://search.yahoo.com/mrss/">';
				liveStreamClipStr += '	<media:content url="'+ Globals.vars.liveStreamUrl+'"';
				if (Globals.vars.preferredFormat == "WMV") liveStreamClipStr += ' type="video/x-ms-wmv"  bitrate=""';
				liveStreamClipStr += '/>';
				liveStreamClipStr += '</media:group>';
				if (Globals.vars.preferredFormat == "WMV")
					liveStreamClipStr += '<PREFERREDFORMAT>'+ Globals.vars.preferredFormat +'</PREFERREDFORMAT>';
				if (StringUtil.validateString(Globals.vars.liveStreamUrl))
					liveStreamClipStr += '<SHOWCC>1</SHOWCC>';
				if (StringUtil.validateString(Globals.vars.StreamType))
					liveStreamClipStr += '<STREAMTYPE>'+ Globals.vars.StreamType +'</STREAMTYPE>';
				if (StringUtil.validateString(Globals.vars.liveStreamHeadline))
					liveStreamClipStr += '<HEADLINE>'+ Globals.vars.liveStreamHeadline +'</HEADLINE>';
				if (StringUtil.validateString(Globals.vars.liveStreamAdTag))
					liveStreamClipStr += '<TARGETADSTAG>'+ Globals.vars.liveStreamAdTag +'</TARGETADSTAG>';
				if (StringUtil.validateString(Globals.vars.liveStreamSummaryImgUrl))
					liveStreamClipStr += '<ABSTRACTIMAGE><FILENAME>'+ Globals.vars.liveStreamSummaryImgUrl +'</FILENAME></ABSTRACTIMAGE>';
				/*if (StringUtil.validateString(Globals.vars.liveStreamSummary))
				liveStreamClipStr += '<ABSTRACT>'+ Globals.vars.liveStreamSummary +'</ABSTRACT>';*/
				liveStreamClipStr += '<ADS>';
				if (Globals.vars.preferredFormat == "WMV") {
					liveStreamClipStr += '<AD SZ="wnsz_30"><SHOW>true</SHOW></AD>';	
				} else if (StringUtil.validateString(Globals.vars.liveStreamHasPreroll)) {
					liveStreamClipStr += '<AD SZ="wnsz_30"><SHOW>'+ Globals.vars.liveStreamHasPreroll +'</SHOW></AD>';
				}
				/*if (StringUtil.validateString(Globals.vars.liveStreamHasPostroll))
				liveStreamClipStr += '<AD SZ="wnsz_31"><SHOW>'+ Globals.vars.liveStreamHasPostroll +'</SHOW></AD>';*/
				liveStreamClipStr += '</ADS>';
				liveStreamClipStr += '</CLIP>';
				var liveStreamClipXml:XML = new XML(liveStreamClipStr);
				trace("> Constructing clipObj based on passed live stream info...");
				trace(liveStreamClipXml);
				Worker.GetContentClipSuccess(liveStreamClipXml, {isLiveStream: true});
			} else if (StringUtil.validateString(Globals.vars.flvUri)) {
				var clipStr:String = '<CLIP>';
				clipStr += '<media:group xmlns:media="http://search.yahoo.com/mrss/">';
				clipStr += '	<media:content url="'+ Globals.vars.flvUri+'"/>';
				clipStr += '</media:group>';
				if (StringUtil.validateString(Globals.vars.headline))
					clipStr += '<HEADLINE>'+ Globals.vars.headline +'</HEADLINE>';
				if (StringUtil.validateString(Globals.vars.adTag))
					clipStr += '<TARGETADSTAG>'+ Globals.vars.adTag +'</TARGETADSTAG>';
				if (StringUtil.validateString(Globals.vars.summaryImageUrl))
					clipStr += '<ABSTRACTIMAGE><FILENAME>'+ Globals.vars.summaryImageUrl +'</FILENAME></ABSTRACTIMAGE>';
				if (StringUtil.validateString(Globals.vars.summary))
					clipStr += '<ABSTRACT>'+ Globals.vars.summary +'</ABSTRACT>';
				clipStr += '<ADS>';
				if (StringUtil.validateString(Globals.vars.enableAds)) {
					clipStr += '<AD SZ="wnsz_30"><SHOW>'+ Globals.vars.enableAds +'</SHOW></AD>';
					clipStr += '<AD SZ="wnsz_31"><SHOW>'+ Globals.vars.enableAds +'</SHOW></AD>';
				}
				clipStr += '</ADS>';
				clipStr += '</CLIP>';
				var clipXml:XML = new XML(clipStr);
				trace("> Constructing clipObj based on passed flvUri ...");
				Worker.GetContentClipSuccess(clipXml, {});
			} else {
				if (StringUtil.validateString(Globals.vars.clipId))
					PlayClipById(Globals.vars.clipId, Globals.vars.affiliateNumber);
			}
			if (mediaPlayerVolume > 0) mediaPlayer.volume = mediaPlayerVolume;
			if (Globals.vars.isMute) mediaPlayer.volume = mediaPlayerVolume = 0;
			
			var dependOn:String = "";
			if (!StringUtil.validateString(Globals.vars.clipId) && Globals.vars.videoType != "livestream") {
				dependOn = "g";	
			}
			//ExternalInterface.call("onWidgetLoad", Globals.vars.domId, Globals.vars.idKey, "canvas2IsReady", dependOn);
			ExternalInterface.call("onWidgetLoad", Globals.vars.domId, Globals.vars.idKey, "canvas2IsReady", dependOn);
		} 
			
		
		public function loadPluginFromResource(pluginResource:MediaResourceBase):void
		{
			setupListeners();
			mediaFactory.loadPlugin(pluginResource);
		}
		
		public function setupListeners(add:Boolean=true):void
		{
			if (add)
			{
				mediaFactory.addEventListener(
					MediaFactoryEvent.PLUGIN_LOAD,
					onPluginLoad);
				mediaFactory.addEventListener(
					MediaFactoryEvent.PLUGIN_LOAD_ERROR,
					onPluginLoadError);
			}
			else
			{
				mediaFactory.removeEventListener(
					MediaFactoryEvent.PLUGIN_LOAD,
					onPluginLoad);
				mediaFactory.removeEventListener(
					MediaFactoryEvent.PLUGIN_LOAD_ERROR,
					onPluginLoadError);
			}
		}
		
		function onPluginLoad(event:MediaFactoryEvent):void
		{
			trace("plugin loaded successfully.");
			setupListeners(false);
		}
		
		function onPluginLoadError(event:MediaFactoryEvent):void
		{
			trace("plugin failed to load.");
			setupListeners(false);
		}

		
		public function PlayClipById(strClipId:String, strAffiliateNumber:String, playgalleryclip:Boolean=false):void 
		{
			trace(".. PlayClipById()");
			Observer.Notify("LoadingMedia", {}, true);
			
			if ((isNaN(Number(strClipId)) == false) && (isNaN(Number(strAffiliateNumber)) == false))
			{
				General.FetchClip(Number(strClipId),Number(strAffiliateNumber), {playgalleryclip:playgalleryclip},
					Worker.GetContentClipSuccess,
					Worker.GetContentClipFailure,
					Worker.GetContentClipFailure,
					Worker.GetContentClipInvalid);
			}
		}
					
		public function PlayMrss(url:String):void
		{
			General.FetchXmlData(url, null,
				Worker.GetMrssSuccess,
				Worker.GetMrssFailure,
				Worker.GetMrssFailure,
				Worker.GetMrssInvalid);
		}
		
		public function AdLogic():Boolean
		{
			trace(".. AdLogic() {}");
			Observer.Notify("LoadingMedia", {}, true);
			
			if(this.currentContentClip == null) return false;			
			if(!(this.currentContentClip.hasAdPreRoll || this.currentContentClip.hasAdPostRoll)) return false;
			
			var adPosition:PlaybackClipTypeEnum = this.clipPlaySeqTracker.WhatToPlay();
			switch(adPosition)
			{
				case PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD:
					if(!this.currentContentClip.hasAdPreRoll) return false;
					break;
				case PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD:
					if(!this.currentContentClip.hasAdPostRoll) return false;
					break;
				default:
					return false;
			}							
			
			var adProps:Object = new Object();
			adProps.wncc = this.currentContentClip.adTag;
			adProps.wnccx = Globals.vars.advertisingZone; 
			if (Globals.vars.playerAdvertisingType != "none")
			{
				adProps.apptype = Globals.vars.playerAdvertisingType;
			}
			else
			{
				// Override the player ad name for previously specified players
				switch (Globals.vars.playerType.toLowerCase())
				{
					case 'popup':
						adProps.apptype = 'videopopup';
						break;
					case 'standard':
						adProps.apptype = 'videostandard';
						break; 
					case 'embedded':
						adProps.apptype = 'videoembedded';
						break; 
					case 'landingpage':
						adProps.apptype = 'videolandingpage';
						break;
					default:
						adProps.apptype = 'video';  
				}                          
			}
			this.currentContentClip.apptype = adProps.apptype;
			this.currentContentClip.idKey = Globals.vars.idKey;
			
			switch(adPosition)
			{
				case PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD:
					adProps.width = adProps.height = "10";
					adProps.wnsz = "30";	
					if (Globals.vars.usePrerollMaster.toLowerCase() == "true")
					{
						adProps.sequence = 1;	// Forces the admanager.js to reset its counters (tile=1, new ord number etc)
					}
					else 
					{
						// If no master ad widget exists on the page then reset admanager counters
						var masterAdExists:Boolean = false;
						masterAdExists = ExternalInterface.call("wnVideoWidgets.masterAdExists", this.currentContentClip);
						if (masterAdExists != true)
						{
							adProps.sequence = 1;      
						}
					}	
					break;
				case PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD:
					adProps.width = adProps.height = "11";
					adProps.wnsz = "31";
					break
				default:
					return false;
			}
			
			// adding property needed for dfpvideo and realvu
			adProps['videoplayerdivid'] = Globals.divId; // vars.domId
			adProps['playerwidth'] = this._width;
			adProps['playerheight'] = this._height;
			
			var ad:Object = ExternalInterface.call("wnVideoWidgets.UAFHelper", adProps, this.currentContentClip, 'raw');
			if (ad != null) {
				var adUrl:String = ad.url;
				adProps.owner = ad.owner;  // Override the companion owners with the pre-roll owner
 
				trace("Globals.vars.debugAdUrl = " + Globals.vars.debugAdUrl);
				if (StringUtil.validateString(Globals.vars.debugAdUrl)) {
					adUrl = Globals.vars.debugAdUrl;
				}

				AdsLoader_v3.FetchAdXMLData(
					adUrl,
					{
						adPosition:adPosition, 
						adProps:adProps, 
						clipObj:this.currentContentClip
					},
					Worker.GetRedirectSuccess,
					Worker.GetRedirectFailure,
					Worker.GetRedirectFailure,
					Worker.GetRedirectInvalid);
				return true;
			} else {
				trace("Error - UAFHelper returned null");
				return false;
			}
		}


		/* Remove after 01/01/2014 - transition to new CC is complete. Livestream and VOD closed caption use the same UI */
		/* { */	
		public function showLivestreamCc(e:Object):void
		{
			if (Globals.vars.enableLivestreamCC) return;
				
			trace("> currentContentClip.isLiveStream = " + currentContentClip.isLiveStream);
			if (currentContentClip.isLiveStream && StringUtil.validateString(currentContentClip.flvUri)) {
				if (mediaPlayer.canPause) mediaPlayer.pause();
				var streamUrlNoCC:String = "";
				if (StringUtil.validateString(currentContentClip.streamUrlNoCC)) {
					streamUrlNoCC = currentContentClip.streamUrlNoCC;
				} else {
					streamUrlNoCC = currentContentClip.streamUrlNoCC = currentContentClip.flvUri;
				}
				var streamUrlCC:String = "";
				if (StringUtil.validateString(currentContentClip.streamUrlCC)) {
					streamUrlCC = currentContentClip.streamUrlCC;
				} else {
					//var streamUrlNoCC:String = currentContentClip.flvUri;
					//var split:int = currentContentClip.streamUrlNoCC.lastIndexOf("@");
					//var streamUrlCC:String = currentContentClip.streamUrlNoCC.substring(0, split) + "_cc@" + currentContentClip.streamUrlNoCC.substring(split+1);					
					var pattern:RegExp;
					if (streamUrlNoCC.indexOf("rtmp://") > -1 || streamUrlNoCC.indexOf("rtsp://") > -1) {
						pattern = /^.*\/([^_@]*)/g; // expecting - rtmp://cp5381.live.edgefcs.net/live/dave@37623
						streamUrlCC = streamUrlNoCC.replace(pattern, "$&"+"CC");
					} else {
						pattern = /(^.*\/)([^_@]*)(.*\/)/g; // expecting - http://wnow_live_test-f.akamaihd.net/z/DAVE_01@92456/manifest.f4m
						streamUrlCC = streamUrlNoCC.replace(pattern, "$1"+"$2"+"CC"+"$3");
					}
				}
				trace("streamUrlNoCC: " + currentContentClip.streamUrlNoCC);
				trace("streamUrlCC: " + streamUrlCC);
				currentContentClip.streamUrlCC = currentContentClip.videoUrl = currentContentClip.flvUri = streamUrlCC;
				//PlayLoadedClip(currentContentClip, false);
				
				loadClip(currentContentClip, false);
				
				trace("<--- Currently playing stream:");
				for (var key:String in currentContentClip){
					trace(key + ": " + currentContentClip[key]);
				}
				trace("--->");
				if (canvas.containsMediaElement(imageElem))	canvas.removeMediaElement(imageElem);
				if (mediaPlayer.state == "ready") {
					trace("stream is loaded and ready");
					// Start video playback
					Observer.Notify("NewMedia", currentContentClip, true);
					mediaPlayer.play();
				} else {
					trace("showLivestreamCc - stream isn't loaded yet");
					//mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChange);
					mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChangeLiveStream);
				}
			}
		}
		public function hideLivestreamCc(e:Object):void
		{
			if (Globals.vars.enableLivestreamCC) return;
			
			trace("< currentContentClip.isLiveStream = " + currentContentClip.isLiveStream);
			trace("< e.parameters.isAd = " + e.parameters.isAd);
			if (!e.parameters.isAd && 
				currentContentClip.isLiveStream && 
				StringUtil.validateString(currentContentClip.flvUri))
			{
				if (mediaPlayer.canPause) mediaPlayer.pause();
				var streamUrlCC:String = "";
				if (StringUtil.validateString(currentContentClip.streamUrlCC)) {
					streamUrlCC = currentContentClip.streamUrlCC;
				} else {
					streamUrlCC = currentContentClip.flvUri;
				}
				//var split:int = streamUrlNoCC.lastIndexOf("_cc@");
				//var streamUrlNoCC:String = streamUrlCC.replace("_cc@", "@");
				var streamUrlNoCC:String = currentContentClip.streamUrlNoCC;
				if (!StringUtil.validateString(streamUrlNoCC)) {
					streamUrlNoCC = streamUrlCC;
				}
				//currentContentClip.flvUri = "rtmp://cp5381.live.edgefcs.net/live/dave@37623";
				//currentContentClip.videoUrl = "rtmp://cp5381.live.edgefcs.net/live/dave@37623";
				currentContentClip.flvUri = streamUrlNoCC;
				currentContentClip.videoUrl = streamUrlNoCC;
				
				loadClip(currentContentClip, false);
				
				trace("<--- Currently playing stream:");
				for (var key:String in currentContentClip){
					trace(key + ": " + currentContentClip[key]);
				}
				trace("--->");
				if (canvas.containsMediaElement(imageElem))	canvas.removeMediaElement(imageElem);
				if (mediaPlayer.state == "ready") {
					trace("stream is loaded and ready");
					// Start video playback
					Observer.Notify("NewMedia", currentContentClip, true);
					mediaPlayer.play();
					FireReportingBeacon();
				} else {
					trace("hideLivestreamCc - stream isn't loaded yet");
					//mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChange);
					mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, onMediaPlayerStateChangeLiveStream);
				}
			}
		}
		/* } */
		
		private function onDynamicStreamChange(event:MediaPlayerCapabilityChangeEvent):void
		{
			//isDynamicStream = event.enabled;
			trace("onDynamicStreamChange()");
			if (event.enabled)
			{
				var streamMsg:String = "Current streaming profile index: " + mediaPlayer.currentDynamicStreamIndex + " of " + mediaPlayer.maxAllowedDynamicStreamIndex;
				trace(streamMsg);
				//updateSwitchingControls();
			}
		}
		private function onSwitchingChangeRequest(event:DynamicStreamEvent):void
		{
			var msg:String = "Switching change request "
			var showCurrentIndex:Boolean = false;
			
			if (event.switching) {
				msg += "REQUESTED";
			} else {
				msg += "COMPLETE";
				showCurrentIndex = true;
			}
			trace(msg);
			if (showCurrentIndex) {
				var streamMsg:String = "Current streaming profile index: " + mediaPlayer.currentDynamicStreamIndex + " of " + mediaPlayer.maxAllowedDynamicStreamIndex;
				trace(streamMsg);
				
				streamMsg = "Current bitrate = " + mediaPlayer.getBitrateForDynamicStreamIndex(mediaPlayer.currentDynamicStreamIndex) + "kbps";
				trace(streamMsg);
			}
			
		}

		public function FireReportingBeacon():void
		{
			trace("Firing Beacon:")
			for (var i in currentContentClip)
			{
				trace(i + "=" + currentContentClip[i]);
			}
			Reporting(currentContentClip, false);
		}

		public function Reporting(clip:Object, isAd:Boolean):void
		{
			if(clip == null) return;
			if(clip.isPreRoll || clip.isPostRoll) {
				isAd = true;
			} else {
				isAd = false;
			}
			ExternalInterface.call("Namespace_VideoReporting_Worldnow.logVideoEventParameters",
				"Duration", 
				clip.headline, 
				"0", //this is hardcoded as 0 since we're not passing in real durations at this time
				clip.id, 
				clip.adTag, 
				"FlashPlayer.wnfl", 
				Globals.vars.affiliate, 
				clip.ownerAffiliateName, 
				isAd, 
				clip.ciid,
				clip.baseUrl,
				Globals.vars.referrer,
				Globals.vars.reportingKeywords,
				clip.flvUri,  
				clip.duration/1000, 
				Math.round((mediaPlayer.currentTime / clip.duration)*100), //clip.pctViewed,
				Globals.vars.locationUrl,
				clip.contentSource,
				clip.keywords,
				Globals.vars.playerType,
				Globals.vars.enableExpressReport,
				clip.taxonomy1,
				clip.taxonomy2,
				clip.taxonomy3
			);
		}
		
		public function processBeacons(e:Object=null):void
		{
			if(this.currentAdClip == null) return;
			if(this.currentAdClip.beacons.length == 0) return;
			if(this.mediaPlayer.state != MediaPlayerState.PLAYING) return;
			
			var ratio:Number = this.mediaPlayer.currentTime / this.mediaPlayer.duration;
			var percentage:Number = Math.round(ratio * 100);
			
			var clipType:PlaybackClipTypeEnum = this.clipPlaySeqTracker.WhichIsPlaying();
			switch(clipType)
			{
				case PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD:
				case PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD:				
					for(var i:int=0; i < this.currentAdClip.beacons.length; i++) {
						var percentageViewed:Number = Number(this.currentAdClip.beacons[i].percentageViewed);
						trace('processBeacons: ' + percentage + '==' + percentageViewed );
						if(percentage == percentageViewed && !this.currentAdClip.beacons[i].hasFired) {
							General.BlankRequest(this.currentAdClip.beacons[i].url);
							
							// Set hasFired to true to avoid multiple beacons from firing 
							trace('Tracking Beacon Fired: ' + this.currentAdClip.beacons[i].url);
							trace('Percentage: ' + this.currentAdClip.beacons[i].percentageViewed);
							trace('Counter: ' + i);
							this.currentAdClip.beacons[i].hasFired = true;
						}
					}
					break;
			}
		}		
		
		/* Old way of getting CC via XML is replaced with TTML
		private function GetClosedCaption():void
		{
			var rndNum:Number 	= Helper.GetRandomNumber(); 
			//IMPORTANT NOTE: The rnd query string param should not be name changed since its being ignored by the akamai cache key
			var xmlPath:String = Globals.ccDomain + "?ShowCC=true&ClipID=" + this.currentContentClip.id + "&rnd=" + rndNum;
			trace("Globals.ccDomain = " + xmlPath);
			General.FetchXmlData(xmlPath, null,
				Worker.GetCcSuccess,
				Worker.GetCcFailure,
				Worker.GetCcFailure,
				Worker.GetCcInvalid);
		}*/
		
		public function externalGetTimerObject():Object
		{
			//if((this.mediaPlayer.media != null) && (this.mediaPlayer.duration > 0))
			if((this.currentContentClip != null) && (this.mediaPlayer.duration > 0))
			{
				var timerObject:Object=new Object();
				
				//get video full duration 
				timerObject.videoDuration=Number(this.mediaPlayer.duration); // /1000;				
				timerObject.videoDurationFormatted=StringUtil.GetTimeString(timerObject.videoDuration, false);
				
				//get video current time
				timerObject.videoCurrentTime=Number(this.mediaPlayer.currentTime);
				timerObject.videoCurrentTimeFormatted=StringUtil.GetTimeString(Number(this.mediaPlayer.currentTime), false);
				
				//get video current position (percentage)
				timerObject.videoCurrentPosition=Math.round(timerObject.videoCurrentTime/timerObject.videoDuration*100);
				timerObject.videoCurrentPositionFormatted=timerObject.videoCurrentPosition+"%";
				
				return timerObject;
			}
			else
			{
				return null;
			}
		}
		public function onPlayheadChange(e:Object):void
		{	
			_currentTime = e.parameters.position;
		}
		public function externalShowEmail():void
		{
			Observer.Notify('OPEN_PANE', {paneName:'overlayEmailPane'});
		}
		public function externalShowLink():void
		{
			Observer.Notify('OPEN_PANE', {paneName:'overlayGetLinkPane'});
		}
		public function externalShowShare():void
		{
			Observer.Notify('OPEN_PANE', {paneName:'sharePane'});
		}
		public function externalLaunchHelp():void
		{
			Observer.Notify('HELP_CLICKED', {});
		}
		public function externalShowCC():void
		{
			Observer.Notify('OPEN_PANE', {paneName:'ccPane'});
		}
		public function externalShowSummary():void
		{
			Observer.Notify('OPEN_PANE', {paneName:'summaryPane'});
		}

		public function onEmailSent(e:Object):void
		{
			fireExternalEvent("email");
		}
		public function onPaneOpen(e:Object):void
		{
			if (e.parameters.paneName == 'overlayGetLinkPane') fireExternalEvent("embed");
		}
		public function onCcSettingsOpen(e:Object):void
		{
			if (!currentContentClip.isLiveStream) {
				Observer.Notify('PAUSE_BUTTON_CLICKED', {});
			}
		}
		
		private function fireExternalEvent(eventName:String):void {
			var clipType:PlaybackClipTypeEnum = clipPlaySeqTracker.WhatToPlay();
			if (clipType == PlaybackClipTypeEnum.PLAYBACKCLIP_PRE_ROLL_AD ||
				clipType == PlaybackClipTypeEnum.PLAYBACKCLIP_POST_ROLL_AD)
			{
				if (currentAdClip) currentAdClip.currentTime = _currentTime;
				Observer.Notify(eventName, currentAdClip, true);
			} else {
				if (currentContentClip) currentContentClip.currentTime = _currentTime;
				Observer.Notify(eventName, currentContentClip, true);
			}
		}
		
		private function sortByBitrate(stream1:DynamicStreamingItem, stream2:DynamicStreamingItem):Number
		{
			if (stream1.bitrate < stream2.bitrate) { return -1; } 
			else if (stream1.bitrate > stream2.bitrate) { return 1; } 
			else 	{ return 0; } 	
		}

		public function get getMediaPlayer():MediaPlayer
		{
			return mediaPlayer;
		}
		public function get getCanvas():MediaContainer
		{
			return canvas;
		}
		public function get getImageElement():ImageElement
		{
			return imageElem;
		}

		
		/*
		private function onStreamLoaded(event:LoadEvent):void {
			trace(event);
			var netStream:NetStream = netStreamLoadTrait.netStream;
			//netStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			if (netStream != null) {
				netStream.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);			
				// Hook up our metadata listeners
				netStream.client.addHandler(NetStreamCodes.ON_META_DATA, onMetaData, 1);
				netStream.client.addHandler(NetStreamCodes.ON_PLAY_STATUS, onPlayStatus, 1);
				netStream.client.addHandler(NetStreamCodes.ON_CUE_POINT, onCuePoint, 1);
				netStream.client.addHandler("onCaptionInfo", onCaptionInfo, 1);
				netStream.client.addHandler("onCaption", onCaption, 1);
				netStream.client.addHandler("onXMPData", onXMPData, 1);
			}
		}
		public function onMetaData(info:Object):void {
			trace("handling onMetaData here - metadata: duration=" + info.duration + " width=" + info.width + 
				" height=" + info.height + " framerate=" + info.framerate);
		}
		*/
	}
}
